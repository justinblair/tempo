﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Com.Interwoven.WorkSite.iManage;

namespace Templates1
{
    /// <summary>
    /// Class to handle personal settings
    /// </summary>
    class PersonalSettings
    {
        private String SettingsFile = "personalSettings.xml";
        private String SettingsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Mishcon de Reya\Tempo";
        private XmlDocument xmlDoc;
        public RecentDocuments RecentDocs { get; set; }
        public XmlDocument XmlDoc
        {
            get { return xmlDoc; }
            set { xmlDoc = value; }
        }
       
        /// <summary>
        /// Constructor
        /// </summary>
        public PersonalSettings()
        {
            xmlDoc = new XmlDocument();
            // On first load the personal settings file is copied to the user's profile.  It contains defaults of London/English UK/Grey task panes
            if(!System.IO.Directory.Exists(SettingsDirectory))
                System.IO.Directory.CreateDirectory(SettingsDirectory);
            if (!System.IO.File.Exists(System.IO.Path.Combine(SettingsDirectory, SettingsFile)))
                System.IO.File.Copy(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA, SettingsFile), System.IO.Path.Combine(SettingsDirectory, SettingsFile));

            xmlDoc.Load(System.IO.Path.Combine(SettingsDirectory, SettingsFile));
            this.RecentDocs = LoadRecentDocuments();
        }

        public int Office
        {
            get { return Convert.ToInt16(xmlDoc.SelectSingleNode("//office").InnerText); }
            set { xmlDoc.SelectSingleNode("//office").InnerText = value.ToString(); }
        }

        public int Language
        {
            get { return Convert.ToInt16(xmlDoc.SelectSingleNode("//language").InnerText); }
            set { xmlDoc.SelectSingleNode("//language").InnerText = value.ToString(); }
        }

        public int ColourPreference
        {
            get { return Convert.ToInt16(xmlDoc.SelectSingleNode("//colpreference").InnerText); }
            set { xmlDoc.SelectSingleNode("//colpreference").InnerText = value.ToString(); }
        }

        public void Save()
        {
            xmlDoc.Save(System.IO.Path.Combine(SettingsDirectory,SettingsFile));
        }

        /// <summary>
        /// Class handling recent documents
        /// </summary>
        /// <returns>A list of recent documents</returns>
        public RecentDocuments LoadRecentDocuments()
        {
            RecentDocuments rcd = new RecentDocuments();
            foreach(XmlNode node in xmlDoc.SelectNodes("//recentDoc"))
            {
                if(node.SelectSingleNode("@id").InnerText == String.Empty)
                {
                    //Do nothing
                }
                else
                { 
                RecentDocument rd = new RecentDocument(Convert.ToInt32(node.SelectSingleNode("@id").InnerText), 
                    Convert.ToInt32(node.SelectSingleNode("@version").InnerText),
                    node.SelectSingleNode("@path").InnerText, node.SelectSingleNode("@description").InnerText);
                rcd.Add(rd);
                }

            }
            return rcd;
        }

        /// <summary>
        /// Not used
        /// </summary>
        /// <param name="existingXML">Not used</param>
        /// <returns>Not used</returns>
        public String PrepareXML(String existingXML)
        {
            RecentDocument rd;
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(existingXML.Replace("xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\"", ""));
            //NameTable nt = new NameTable();
            //XmlNamespaceManager ns = new XmlNamespaceManager(nt);
            //ns.AddNamespace(String.Empty,"http://schemas.microsoft.com/office/2009/07/customui");
            XmlNode grpNode = xml.SelectSingleNode("//group[@id='Col2Grp3']");
            XmlNode hyperlinkNode = grpNode.SelectSingleNode("topItems/hyperlink[@id='HyperlinkDummy']");
            XmlNode labelNode = grpNode.SelectSingleNode("topItems/labelControl[@id='mdrDummy']");

            for (int i = 0; i < 11; i++ )
            {
                XmlNode hyperClone = hyperlinkNode.CloneNode(true);
                XmlNode labelClone = labelNode.CloneNode(true);
                if(i<this.RecentDocs.Count)
                { 
                    rd = this.RecentDocs[i];
                    
                    //IManDocument imanDoc = Globals.ThisAddIn.DMS.GetDocument(Globals.ThisAddIn.DMS.GetPreferredDatabase.Name, rd.Number, rd.Version);
                    hyperClone.SelectSingleNode("@id").InnerText = "h" + i;
                    hyperClone.SelectSingleNode("@label").InnerText = rd.Description;
                    hyperClone.SelectSingleNode("@tag").InnerText = rd.Number + "." + rd.Version;
                    labelClone.SelectSingleNode("@label").InnerText = rd.Number + "." + rd.Version;
                    labelClone.SelectSingleNode("@id").InnerText = "l" + i;
                    hyperlinkNode.ParentNode.InsertBefore(hyperClone, hyperlinkNode);
                    labelNode.ParentNode.InsertBefore(labelClone, labelNode);
                }
            }
            hyperlinkNode.ParentNode.RemoveChild(hyperlinkNode);
            labelNode.ParentNode.RemoveChild(labelNode);

                return xml.OuterXml.Replace("<customUI", "<customUI xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\"");
        }
    }

    /// <summary>
    /// class representing a recent document in the personal settings file
    /// </summary>
    public class RecentDocument
    {
        /// <summary>
        /// Properties
        /// </summary>
        public Int32 Number { get; set; }
        public Int32 Version { get; set; }
        public String FilePath { get; set; }
        public String Description { get; set; }
        
        
        /// <summary>
        /// Constructor: used when there is a document object available
        /// </summary>
        /// <param name="doc">A document object representing a recent document</param>
        public RecentDocument(Document doc)
        {
            this.FilePath = doc.WordDocument.FullName;
            if(Globals.ThisAddIn.DMS.IsOnline)
            if(Globals.ThisAddIn.DMS.IsProfiled(doc.WordDocument))
            {
                this.Number = Globals.ThisAddIn.DMS.GetDocument(doc.WordDocument.FullName).Number;
                this.Version = Globals.ThisAddIn.DMS.GetDocument(doc.WordDocument.FullName).Version;
                this.Description = Globals.ThisAddIn.DMS.GetDocument(doc.WordDocument.FullName).Description;
                
            }
        }
        /// <summary>
        /// Constructor: used when there is no document object available
        /// </summary>
        /// <param name="number">the FileSite number of the document</param>
        /// <param name="version">the FileSite verion of the document</param>
        /// <param name="path">the path of the document</param>
        /// <param name="description">the FileSite description of the document</param>
        public RecentDocument(Int32 number, Int32 version, String path, String description)
        {
            this.Number = number;
            this.Version = version;
            this.FilePath = path;
            this.Description = description;
        }
    }

    /// <summary>
    /// A list of recent documents
    /// </summary>
    public class RecentDocuments: List<RecentDocument>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public RecentDocuments()
        {

        }

        /// <summary>
        /// Adds a recent document to the personal settings file and the list of recent documents
        /// </summary>
        /// <param name="rd">A recent document</param>
        public void AddItem(RecentDocument rd)
        {
            XmlDocument xml = Globals.ThisAddIn.PersonalSettings.XmlDoc;
            XmlNode node = xml.SelectSingleNode("//recentDoc");
            XmlNode nodeclone = node.CloneNode(true);
            nodeclone.SelectSingleNode("@id").InnerText = rd.Number.ToString();
            nodeclone.SelectSingleNode("@version").InnerText = rd.Version.ToString();
            if (rd.Description == String.Empty)
                nodeclone.SelectSingleNode("@description").InnerText = rd.FilePath;
            else
                nodeclone.SelectSingleNode("@description").InnerText = rd.Description;
            nodeclone.SelectSingleNode("@path").InnerText = rd.FilePath;
            node.ParentNode.InsertBefore(nodeclone, node);
            this.Insert(0, rd);
            XmlNodeList ndl = xml.SelectNodes("//recentDoc");
            if (ndl.Count > 10)
                node.ParentNode.RemoveChild(node.ParentNode.LastChild);
            Globals.ThisAddIn.PersonalSettings.Save();
        }

        
        /// <summary>
        /// Not used
        /// </summary>
        
        public void ClearItems()
        {
            XmlDocument xml = Globals.ThisAddIn.PersonalSettings.XmlDoc;
            XmlNodeList ndl = xml.SelectNodes("//recentDoc");
            if(ndl.Count>0)
                ndl[0].ParentNode.RemoveAll();
            for(int i = ndl.Count -1; i>0; i--)
            {
                if(i<this.Count -1)
                { 
                    RecentDocument rd = this[i];
                    this.Remove(rd);
                }
            }
            XmlNode node = xml.SelectSingleNode("//recentDoc");
            node.SelectSingleNode("@id").InnerText = String.Empty;
            node.SelectSingleNode("@version").InnerText = String.Empty;
            node.SelectSingleNode("@description").InnerText = String.Empty;
            node.SelectSingleNode("@path").InnerText = String.Empty;
            Globals.ThisAddIn.PersonalSettings.Save();
        }
    }
}
