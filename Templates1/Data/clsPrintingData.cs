﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Templates1
{
    /// <summary>
    /// A class representing paper type (Plain, Bond, Letterhead etc)
    /// </summary>
    class MediaType
    {
        private String type;

        public String Type
        {
            get { return type; }
            set { type = value; }
        }
        public MediaType(String type)
        {
            this.type = type;
        }

    }
    /// <summary>
    /// A list of media/paper types
    /// </summary>
    class MediaTypes : List<MediaType>
    {
        public MediaTypes()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(System.Environment.GetEnvironmentVariable("PROGRAMFILES") + @"\Mishcon De Reya\Tempo\Printing" + @"\Additional.xml");
            foreach(XmlNode node in xml.SelectNodes("//MediaTypes/MediaType"))
            {
                MediaType m = new MediaType(node.SelectSingleNode("@ID").InnerText);
                this.Add(m);
            }
        }
    }
    /// <summary>
    /// A class handling the printer XML files (originally Novaplex's but adopted for efficiency)
    /// </summary>
    class PrintingData
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        public PrintingData()
        {

        }

        /// <summary>
        /// Adds a printer identified in AD to the Additional.xml print file
        /// </summary>
        /// <param name="printer"></param>
        public void AddPrinterToXML(ADPrinter printer)
        {
            XmlDocument xml = new XmlDocument();

            printer.Filepath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo" + @"\Additional.xml";
            xml.Load(printer.Filepath);
            XmlNode template1 = xml.SelectSingleNode("//template[@id='1']/Printers/Printer");
            XmlNode prntr = template1.Clone();
            XmlNode parent = xml.DocumentElement.SelectSingleNode("Printers");
            parent.AppendChild(prntr);
            XmlNode template2 = xml.SelectSingleNode("//template[@id='2']/Groups/Group/Printers/Printer");
            XmlNode prntr2 = template2.Clone();
            XmlNode parent2 = xml.DocumentElement.SelectSingleNode("Groups/Group/Printers");
            parent2.AppendChild(prntr2);

            prntr.SelectSingleNode("@ID").InnerText = printer.DeviceInfo;
            prntr2.SelectSingleNode("@ID").InnerText = printer.Name;
            prntr2.SelectSingleNode("WindowsName").InnerText = printer.Name.Replace(".mishcon.co.uk", "");
            prntr2.SelectSingleNode("PrinterType").InnerText = printer.DeviceInfo;
            XmlNode tray = prntr.SelectSingleNode("Trays/Tray");
            XmlNode mtype = prntr2.SelectSingleNode("MediaTypes/MediaType");
            foreach(PrintTray t in printer.PrintTrays)
            {
                XmlNode tr = tray.Clone();
                prntr.SelectSingleNode("Trays").AppendChild(tr);
                tr.SelectSingleNode("@ID").InnerText = t.TrayName;
                tr.SelectSingleNode("BinCode").InnerText = t.BinID.ToString();
                XmlNode mt = mtype.Clone();

                prntr2.SelectSingleNode("MediaTypes").AppendChild(mt);
                mt.SelectSingleNode("@ID").InnerText = String.Empty;
                mt.SelectSingleNode("Tray").InnerText = t.TrayName;
            }
            if (tray.SelectSingleNode("@ID").InnerText == String.Empty)
                tray.ParentNode.RemoveChild(tray);
            if (mtype.SelectSingleNode("@ID").InnerText == String.Empty)
                mtype.ParentNode.RemoveChild(mtype);
            xml.Save(printer.Filepath);




        }
    }
}
