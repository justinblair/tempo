﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;

namespace Templates1
{
    /// <summary>
    /// Class to build labels up
    /// </summary>
    class LabelBuilder
    {
        /// <summary>
        /// Properties
        /// </summary>
        public const String LabelXmlFile = "Labels.xml";
        private XMLLabel activeLabel;
        public XMLLabel ActiveLabel
        {
            get
        {
            if (activeLabel != null)
                return activeLabel;
            else
            {
                try
                {
                    Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["label"];
                    IEnumerable<XMLLabel> result = from d in this.Labels where d.Id == vbl.Value select d;
                    if (result.Count() > 0)
                    {
                        activeLabel = result.ToList()[0];
                        return activeLabel ;
                    }
                }

                catch { }
                return null;
            }
        }
            set { activeLabel = value; }
        }
        public XMLLabels Labels {get; set;}


        /// <summary>
        /// Converts the labels XML file into Label objects
        /// </summary>
        public LabelBuilder()
    {
        XmlDocument xml = new XmlDocument();
        Labels = new XMLLabels();
            xml.Load(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA, LabelXmlFile));
            foreach (XmlNode node in xml.SelectNodes("//label"))
            {
                XMLLabel lbl = new XMLLabel(node);

                Labels.Add(lbl);
                
            }
    }
        /// <summary>
        /// Builds the label document
        /// </summary>
        /// <param name="index">The index number according to the XML file</param>
        public void BuildLabels(int index)
        {
            XMLLabel label = Labels[index];
            this.ActiveLabel = label;
            Template tmp = new Template();
            tmp.Filename = "mdrLabels.dotx";
            XmlOffice office = Globals.ThisAddIn.Officedata.Offices.ActiveOffice;
            Language lan = Globals.ThisAddIn.Officedata.Languages.ActiveLanguage;
            Document doc = Globals.ThisAddIn.WordApp.CreateDocument(tmp, lan, office, false);
            doc.Label = label;
            Word.Document wordDoc = doc.WordDocument;
            // Store the label id in a document variable in case it is needed later
            Word.Variable vbl;
            try
            {
                vbl = wordDoc.Variables["label"];
                vbl.Value = label.Id;
            }
            catch
            {
                vbl = wordDoc.Variables.Add("label", label.Id);
            }
            Word.PageSetup p = wordDoc.Sections[1].PageSetup;
            p.PaperSize = label.PageSize;
            if(label.PageSize == Word.WdPaperSize.wdPaperCustom)
            {
                p.PageWidth = label.PageWidth;
                p.PageHeight = label.PageHeight;
            }
            p.Orientation = label.Pagesetup;
            p.TopMargin = label.TopMargin;
            p.BottomMargin = label.BottomMargin;
            p.LeftMargin = label.LeftMargin;
            p.RightMargin = label.RightMargin;
            p.HeaderDistance = 0;
            p.FooterDistance = 0;
            //Build a table dynamically to represent the labels
            Word.Table tbl = wordDoc.Tables.Add(wordDoc.Range(0, 0), label.RowCount, label.ColCount);
            foreach(Word.Row row in tbl.Rows)
                row.Height = label.Height;
            foreach (Word.Column col in tbl.Columns)
            {
                col.PreferredWidthType = Word.WdPreferredWidthType.wdPreferredWidthPoints;
                col.PreferredWidth = (col.Index % 2 == 0) ? label.Spacer : label.Width;
                foreach (Word.Cell cell in col.Cells)
                    cell.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalCenter;
            }
            Word.Range rng = doc.WordDocument.Range();
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rng.Paragraphs[1].Format.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceExactly;
            rng.Paragraphs[1].Format.LineSpacing = 1;
            Globals.ThisAddIn.WordApp.LaunchTaskPane(doc.WordDocument, doc.Template);
        }
    }
    
    /// <summary>
    /// A class representing a label
    /// </summary>
    class XMLLabel
    {
        /// <summary>
        /// Properties
        /// </summary>
        public Word.WdOrientation Pagesetup {get; set;}
        public Word.WdPaperSize PageSize { get; set; }
        public float TopMargin {get; set; }
        public float BottomMargin { get; set; }
        public float LeftMargin { get; set; }
        public float RightMargin { get; set; }
        public int RowCount { get; set; }
        public int ColCount { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public float Spacer { get; set; }
        public String Id { get; set; }
        public String Description { get; set; }
        public float PageWidth { get; set; }
        public float PageHeight { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="node">An XML node from the labels XML file</param>
        public XMLLabel(XmlNode node)
        {
            Pagesetup = (node.SelectSingleNode("orientation").InnerText == "portrait") ? Word.WdOrientation.wdOrientPortrait : Word.WdOrientation.wdOrientLandscape;
            TopMargin = Convert.ToSingle(node.SelectSingleNode("margins/margin[@id='top']").InnerText);
            BottomMargin = Convert.ToSingle(node.SelectSingleNode("margins/margin[@id='bottom']").InnerText);
            LeftMargin = Convert.ToSingle(node.SelectSingleNode("margins/margin[@id='left']").InnerText);
            RightMargin = Convert.ToSingle(node.SelectSingleNode("margins/margin[@id='right']").InnerText);
            RowCount = Convert.ToInt16(node.SelectSingleNode("rows").InnerText);
            ColCount = Convert.ToInt16(node.SelectSingleNode("cols").InnerText);
            Width = Convert.ToSingle(node.SelectSingleNode("colwidth").InnerText);
            Height = Convert.ToSingle(node.SelectSingleNode("rowheight").InnerText);
            Spacer = Convert.ToSingle(node.SelectSingleNode("spacer").InnerText);
            Id = node.SelectSingleNode("@id").InnerText;
            switch(node.SelectSingleNode("page").InnerText)
            {
                case "A4":
                    PageSize = Word.WdPaperSize.wdPaperA4;
                    break;
                case "Letter":
                    PageSize = Word.WdPaperSize.wdPaperLetter;
                    break;
                default:
                    String[] split = node.SelectSingleNode("page").InnerText.Split(new char[] {'x'});
                    PageSize = Word.WdPaperSize.wdPaperCustom;
                    PageWidth = Convert.ToSingle(split[0]);
                    PageHeight = Convert.ToSingle(split[1]);
                    break;
            }
            Description = node.SelectSingleNode("description").InnerText;

        }
    }
    /// <summary>
    /// A list of labels
    /// </summary>
    class XMLLabels : List<XMLLabel>
    {
       /// <summary>
       /// Constructor
       /// </summary>
        public XMLLabels()
        {

        }
    }
}
