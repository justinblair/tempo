﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    /// <summary>
    /// A class representing a written language
    /// </summary>
    public class Language
    {
        /// <summary>
        /// Properties
        /// </summary>
        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }


        private int langid;

        public int Langid
        {
            get { return langid; }
            set { langid = value; }
        }
        private String name;

        public String Name
        {
            get { return name; }
            set { name = value; }
        }
        
        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="languagenode">An XML node from the OfficeData.xml file</param>
        public Language(XmlNode languagenode)
        {
            this.langid = Convert.ToInt16(languagenode.SelectSingleNode("@val").InnerText);
            this.name = languagenode.SelectSingleNode("name").InnerText;
        }

        public Language()
        {

        }

    }
    /// <summary>
    /// A list of languages
    /// </summary>
    public class Languages : List<Language>
    {
        /// <summary>
        /// Properties
        /// </summary>
        private XmlDocument officedata;
        public Language ActiveLanguage
        {
            get { return new Language(officedata.SelectNodes("//languages/language")[Globals.ThisAddIn.Ribbonopts.Lang]);}
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="officedata">The OfficeData.xml XML file</param>
        public Languages (XmlDocument officedata)
        {
            this.officedata = officedata;
        }
    }

    public class FileSiteDocument
    {
        public long DocumentNumber { get; set; }
        public String Database { get; set; }
        public String Server { get; set; }
    }
    
    /// <summary>
    /// A class representing a template
    /// </summary>
    public class Template
    {
        /// <summary>
        /// Properties
        /// </summary>
          private Boolean bydefault;
          private int index;
          private String filename;
          private long documentNumber;
          
          private String name;
          private Boolean hasContactBoxFunctionality;
          private String np;
          private StyleSet styleset;
          private List<FileSiteDocument> filesiteDocs;

          public List<FileSiteDocument> FilesiteDocs
          {
              get { return filesiteDocs; }
          }
         
        /// <summary>
        /// Represents the Novaplex template file path
        /// </summary>
          public String Np
          {
              get { return np; }
              set { np = value; }
          }

        public StyleSet StyleSet
          {
              get { return styleset; }
          }
        
        public long DocumentNumber
          {
              get { return documentNumber; }
          }
        
        public int Index
        {
            get { return index; }
            
        }

        public Boolean Bydefault
        {
          get { return bydefault; }
          
        }
   
        public Boolean HasContactBoxFunctionality
        {
            get { return hasContactBoxFunctionality; }
            set { hasContactBoxFunctionality = value; }
        }


        public String Filename
        {
            get { return filename; }
            set { filename = value; }
        }
        

        public String Name
        {
            get { return name; }
            set { name = value; }
            
        }

        
        /// <summary>
        /// Constructors
        /// </summary>
        /// <param name="node">An XML node from the OfficeData.xml file</param>
        public Template(XmlNode node, List<StyleSet> stylesets)
        {
            try
            {
                name = node.SelectSingleNode("name").InnerText;
                filename = node.SelectSingleNode("@file").InnerText;
                np = node.SelectSingleNode("@np").InnerText;
                hasContactBoxFunctionality = node.SelectSingleNode("hasContactBoxFunctionality").InnerText == "-1";
                bydefault = node.SelectSingleNode("default").InnerText == "-1";

                XmlNodeList ndl = node.SelectNodes("docNum");
                filesiteDocs = new List<FileSiteDocument>();
                foreach(XmlNode n in ndl)
                {
                    FileSiteDocument fsd = new FileSiteDocument();
                    fsd.Server = n.SelectSingleNode("@server").InnerText;
                    fsd.Database = n.SelectSingleNode("@db").InnerText;
                    fsd.DocumentNumber = Convert.ToInt32(n.InnerText);
                    filesiteDocs.Add(fsd);
                }
                
                styleset = (from d in stylesets where d.ID == node.SelectSingleNode("styleset").InnerText select d).ToList()[0];

            }
            catch { }
        }

        public Template()
        {
            hasContactBoxFunctionality = false;
        }
    }

    /// <summary>
    /// A list of templates
    /// </summary>
    public class Templates : List<Template>
    {
        /// <summary>
        /// Constructors
        /// </summary>
        public Templates()
        {
            
        }

        public Templates(IEnumerable<Template> result)
            : base(result)
        {

        }

        /// <summary>
        /// A list of templates available as recipients of a conversion.  Currently the two legal documents only.
        /// </summary>
        public Templates DefaultTemplates
        {
            get
            {
                IEnumerable<Template> templates =
                    (from d in this
                     where d.Bydefault == true
                     select d);
                if (templates.Count() > 0)
                    return new Templates(templates);
                else
                    return null;
            }
    }
    }

    public class StyleSet
    {
        internal int DocumentNumber { get; set; }
        internal String Database { get; set; }
        internal IncludeStyles IncludeStyles { get; set; }
        internal String ID { get; set; }
        internal String File { get; set; }
        public StyleSet(XmlNode node)
        {
            IncludeStyles = new IncludeStyles();
            ID = node.SelectSingleNode("@id").InnerText;
            File = node.SelectSingleNode("@file").InnerText;
            DocumentNumber = Convert.ToInt32(node.SelectSingleNode("@docNumber").InnerText);
            Database = node.SelectSingleNode("@db").InnerText;
            foreach(XmlNode style in node.SelectNodes("styleSearch"))
            {
                IncludeStyle incStyle = new IncludeStyle();
                incStyle.SearchString = style.InnerText;
                if (style.SelectSingleNode("@custom").InnerText == "1")
                   incStyle.Custom = true;
                else
                    incStyle.Custom = false;
                incStyle.Priority = Convert.ToInt16(style.SelectSingleNode("@priority").InnerText);
                IncludeStyles.Add(incStyle);
            }

        }
    }

    public class IncludeStyle
    {
        internal String SearchString { get; set; }
        internal Boolean Custom { get; set; }
        internal int Priority { get; set; }
    }

    public class IncludeStyles: List<IncludeStyle>
    {
        public IncludeStyles() { }
    }
    
    /// <summary>
    /// A list of offices
    /// </summary>
    public class Offices : List<XmlOffice>
    {
        private XmlDocument officedata;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="officedata">The OfficeData.xml XML file</param>
        public Offices(XmlDocument officedata)
        {
            this.officedata = officedata;
        }

        public XmlOffice ActiveOffice
        {
            get { return this[Globals.ThisAddIn.Ribbonopts.Office]; }
        }
    }

    public class HeaderLogo
    {
        private String name;
        private String id;

        public String ID
        {
            get { return id; }
            set { id = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public HeaderLogo(XmlDocument officedata, String id)
        {
            this.id = id;
            this.name = officedata.SelectSingleNode("//headerLogos/headerLogo[@id='" + id + "']").InnerText;
        }


    }
    
    /// <summary>
    /// A class representing an office
    /// </summary>
    public class XmlOffice
    {
        /// <summary>
        /// Properties
        /// </summary>
        private int index;
        private Templates templates;
        private String shortname;
        private String fullname;
        private String address;
        private String email;
        private String vat;
        private String rubric;
        private String telephone;
        private String fax;
        private String faxTransError;
        private String footerCentre;
        private String dx;
        private String web;
        private String bankAccount;
        private String customerServices;
        private HeaderLogo logo;

        public HeaderLogo Logo
        {
            get { return logo; }
            set { logo = value; }
        }


        public String CustomerServices
        {
            get { return customerServices; }
            set { customerServices = value; }
        }

        public String BankAccount
        {
            get { return bankAccount; }
            set { bankAccount = value; }
        }

        public String Web
        {
            get { return web; }
            set { web = value; }
        }



        public String DX
        {
            get { return dx; }
            set { dx = value; }
        }


        public String FooterCentre
        {
            get { return footerCentre; }
            set { footerCentre = value; }
        }



        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public Templates Templates
        {
            get { return templates; }
            set { templates = value; }
        }

        public String Shortname
        {
            get { return shortname; }
            set { shortname = value; }
        }
        

        public String Fullname
        {
            get { return fullname; }
            set { fullname = value; }
        }
        

        public String Address
        {
            get { return address; }
            set { address = value; }
        }
        

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
        

        public String VAT
        {
            get { return vat; }
            set { vat = value; }
        }
        

        public String Rubric
        {
            get { return rubric; }
            set { rubric = value; }
        }
        

        public String Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
        

        public String Fax
        {
            get { return fax; }
            set { fax = value; }
        }
       

        public String FaxTransError
        {
            get { return faxTransError; }
            set { faxTransError = value; }
        }
        
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="node">An office node from the OfficeData.xml file</param>
        /// <param name="officedata">The OfficeData.xml file</param>
        public XmlOffice(XmlNode node, XmlDocument officedata, List<StyleSet> styleset)
        {
            shortname = node.SelectSingleNode("shortname").InnerText;
            fullname = node.SelectSingleNode("fullName").InnerText;
            address = clsOfficeData.GetAddress(node.SelectNodes("address"));
            rubric = node.SelectSingleNode("rubric").InnerText;
            telephone = node.SelectSingleNode("telephone").InnerText;
            fax = node.SelectSingleNode("fax").InnerText;
            faxTransError = node.SelectSingleNode("faxTransError").InnerText;
            vat = node.SelectSingleNode("vat").InnerText;
            email = node.SelectSingleNode("email").InnerText;
            footerCentre = node.SelectSingleNode("footerCentre").InnerText;
            dx = node.SelectSingleNode("dx").InnerText;
            web = node.SelectSingleNode("web").InnerText;
            bankAccount = node.SelectSingleNode("bankAccount").InnerText;
            customerServices = node.SelectSingleNode("customerServices").InnerText;
            if (node.SelectSingleNode("headerLogo").InnerText != "0")
                logo = new HeaderLogo(officedata, node.SelectSingleNode("headerLogo").InnerText);
            else
                logo = null;
            //Loads the templates applicable for that office
            templates = new Templates();
            XmlNodeList templatenodes = officedata.SelectNodes("//templates/template[visibleTo/Office/@id='" + node.SelectSingleNode("@id").InnerText + "']");
            FullTemplateSuite fs = new FullTemplateSuite();
            foreach (XmlNode templatenode in templatenodes)
            {
                Template temp = new Template(templatenode, styleset);
                if (fs.LoadFullTemplateSuite)
                    templates.Add(temp);
                else if (temp.Name == "Non-Legal Document" || temp.Name == "Two-page Flyer Document")
                    templates.Add(temp);
            }
            templates = new Templates(templates.OrderBy(c => c.Name));
            


        }

        public XmlOffice()
        {

        }
    }
    
    /// <summary>
    /// A class handling the OfficeData.xml file.  This is the top level of all templates, languages and offices
    /// </summary>
    public class clsOfficeData
    {
        /// <summary>
        /// Properties
        /// </summary>
        private XmlDocument officedata;
        private clsRibbonOptions ribbonopts;
        private Offices offices;
        private Languages languages;
        private Templates templates;
        public String APPPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo";
        private const String FILENAME = "OfficeData.xml";
        private String DATAPATH = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA);
        private List<StyleSet> stylesets;

        

        public static String APPPath2 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo";

        public List<StyleSet> StyleSets
        {
            get { return stylesets; }
        }
        
        public Templates Templates
        {
            get { return templates; }
        }

        public Languages Languages
        {
            get { return languages; }
        }
        
        public Offices Offices
        {
            get { return offices; }
        }
                
        //public XmlDocument Officedata
        //{
        //    get { return officedata; }
        //    set { officedata = value; }
        //}

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ribbonopts">the ribbon options object which stores values from the ribbon</param>
        public clsOfficeData(clsRibbonOptions ribbonopts)
        {
            this.ribbonopts = ribbonopts;
            Load();
            
        }

        internal clsOfficeData()
        {
            Load();
        }
       
        private void Load()
        {
            officedata = new XmlDocument();
            try
            {
                if (System.IO.File.Exists(System.IO.Path.Combine(DATAPATH, FILENAME)))
                    officedata.Load(System.IO.Path.Combine(DATAPATH, FILENAME));
                else
                    officedata.Load(System.IO.Path.Combine(APPPath, FILENAME));
            }
            catch
            {
                officedata.Load(System.IO.Path.Combine(clsOfficeData.APPPath2, FILENAME));
            }
            int i = -1;

            stylesets = new List<StyleSet>();

            foreach (XmlNode node in officedata.SelectNodes("//stylesets/styleset"))
            {
                stylesets.Add(new StyleSet(node));
            }

            offices = new Offices(officedata);
            foreach (XmlNode node in officedata.SelectNodes("//offices/office"))
            {
                i++;
                XmlOffice office = new XmlOffice(node, officedata, stylesets);
                office.Index = i;
                offices.Add(office);
            }
            i = -1;
            languages = new Languages(officedata);
            foreach (XmlNode node in officedata.SelectNodes("//languages/language"))
            {
                i++;
                Language lang = new Language(node);
                lang.Index = i;
                languages.Add(lang);
            }

            

            templates = new Templates();
            FullTemplateSuite fs = new FullTemplateSuite();
            foreach (XmlNode node in officedata.SelectNodes("//templates/template"))
            {
                
                Template temp = new Template(node, stylesets);
                if (fs.LoadFullTemplateSuite)
                    templates.Add(temp);
                else if (temp.Name == "Non-Legal Document" || temp.Name == "Two-page Flyer Document")
                    templates.Add(temp);
            }
            //List the templates in alphabetical order for ease of use from the ribbon
            templates = new Templates(templates.OrderBy(c => c.Name));

            
        }
            
        
        public String SRARubric
        {
            get { return officedata.SelectSingleNode("//office[@id='MdRLondon']/rubric").InnerText; }
        }

        public String CurrentRubric
       {
           get { return officedata.SelectNodes("//offices/office")[ribbonopts.Office].SelectSingleNode("rubric").InnerText; }
       }
        
        public String NYFaxNumber
        {
            get { return (officedata.SelectSingleNode("//offices/office[@id='MdRNewYork']/fax").InnerText).Trim(); }
        }


          
        /// <summary>
        /// Builds up the address based on a collection of nodes from the OfficeData.xml file
        /// </summary>
        /// <param name="addresses">A nodelist of <address/> tags</param>
        /// <returns></returns>
        public static String GetAddress(XmlNodeList addresses)
        {
            String address = "";
            int i = 0;
             int max = addresses.Count;
            foreach (XmlNode node in addresses)
            {
                i++;
                if (i == max)
                    address += node.InnerText;
                else
                    address += node.InnerText + Microsoft.VisualBasic.Constants.vbCrLf;
            }
            return address;
        }

        public String NYAddress
        {
            get
            {
                String address = "";
                int i = 0;
                XmlNodeList addresses = officedata.SelectNodes("//office[@id='MdRNewYork']/address");
                int max = addresses.Count;
                foreach(XmlNode node in addresses)
                {
                    i++;
                    if (i == max)
                        address += node.InnerText;
                    else
                        address += node.InnerText + Microsoft.VisualBasic.Constants.vbCrLf;
                }
                return address;
            }
        }

        public String NYPhoneNumber
        {
            get
            {
                return officedata.SelectSingleNode("//office[@id='MdRNewYork']/telephone").InnerText;
            }
        }

       

        public Boolean UseCurlyQuotes
        {
            get { return officedata.SelectNodes("//offices/office")[ribbonopts.Office].SelectSingleNode("curlyQuotes").InnerText == "-1"; }
        }

        public Microsoft.Office.Interop.Word.WdMeasurementUnits MeasurementUnits
        {
            get { return (Microsoft.Office.Interop.Word.WdMeasurementUnits)
                Convert.ToInt16(officedata.SelectNodes("//offices/office")[ribbonopts.Office].SelectSingleNode("measurementUnits").InnerText);
            }
        }

        public Boolean IsNYOffice
        {
            get
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return false;
                else 
                    return Globals.ThisAddIn.Application.ActiveDocument.BuiltInDocumentProperties["Company"].Value == "MdR New York"; 
            }
        }

        public Boolean IsNotNYOffice
        {
            get { return IsNYOffice == false; }
        }

     }
}
