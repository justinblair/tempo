﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Templates1
{
    /// <summary>
    /// Wrapper class to read/write to the registry
    /// </summary>
    public class Registry
    {
        /// <summary>
        /// Read a value from the registry
        /// </summary>
        /// <param name="HKEY"></param>
        /// <param name="KeyName"></param>
        /// <param name="ValueName"></param>
        /// <returns></returns>
        public static String ReadRegistry(RegistryKey HKEY, String KeyName, String ValueName)
        {
            RegistryKey rKey1 = HKEY;
            RegistryKey rKey2 = rKey1.OpenSubKey(KeyName);
            return rKey2.GetValue(ValueName, "").ToString();
        }
        /// <summary>
        /// Write a value to the registry
        /// </summary>
        /// <param name="KeyName"></param>
        /// <param name="KeyValue"></param>
        public static void WriteRegistryAsString(String KeyName, String KeyValue)
        {
            //  On Error GoTo catch_err

            //  Dim wsh As IWshRuntimeLibrary.WshShell
            //  Set wsh = New IWshRuntimeLibrary.WshShell

            //  Call wsh.RegWrite(KeyName, KeyValue, "REG_SZ")

            //finally:
            //  Exit Sub

            //catch_err:
            //  Resume finally
        }

        public static void WriteRegistry(String KeyName, String SubKey, String KeyValue)
        {
            Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyName,true);
            key.SetValue(SubKey, KeyValue);
        }
        
        /// <summary>
        /// Removes the disabled items key which prevents the FileSite addin from loading in Word
        /// </summary>
        
        public static void RemoveResiliencyKey()
        {
            Microsoft.Win32.RegistryKey key =
               Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Office\14.0\Word\Resiliency", true);

            if (key != null)
            {
                if (key.OpenSubKey("DisabledItems") != null)
                {
                    key.DeleteSubKey("DisabledItems");
                }
            }
        }
    }
}

