﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Templates1
{
    class SyncManager
    {
        internal WorkSite8Application DMS { get; set; }
        internal Boolean NoConnecction { get; set; }
        internal Boolean DataFileDownloaded { get; set; }
        private String progress = String.Empty;
        internal Boolean CatastrophicError { get; set; }

        internal SyncManager(WorkSite8Application dms)
        {
            if(dms==null)
            DMS = new WorkSite8Application();

        }

        /// <summary>
        /// Creates a new connection to London DMS in the absence of the extensibility object which at this point isn't loaded.
        /// </summary>
        /// <param name="dms">A temporary FileSite object</param>
        internal void Connect()
        {
           
            if (!DMS.IsConnectedToLondon)
                DMS.CreateLondonSession();
          
            NoConnecction = !DMS.IsConnectedToLondon;
            
        }

        /// <summary>
        /// Downloads the office data file from FileSite
        /// </summary>
        internal void DownloadOfficeData()
        {
            try
            {
                String path = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA);
                try
                {
                    if (!System.IO.Directory.Exists(path))
                        System.IO.Directory.CreateDirectory(path);
                }
                catch { System.Windows.Forms.MessageBox.Show("Unable to create " + path + ".  Check you have write access.  Tempo will not function until the correct rights have been assigned.");
                //throw new InvalidOperationException();
                    return;
                }
                path = System.IO.Path.Combine(path, "OfficeData.xml");
                System.IO.FileInfo info;
                DateTime lastModified;
                if (System.IO.File.Exists(path))
                {
                    info = new System.IO.FileInfo(path);
                    lastModified = info.LastWriteTime;
                }
                else
                    lastModified = DateTime.Parse("01/01/1900");
                lastModified = DateTime.Parse("01/01/1900");

                switch (DMS.OfficeLocation)
                {
                    case WorkSite8Application.DMSOfficeLocation.London:
                        DataFileDownloaded = DMS.DownloadDocument("Legal1", 27882564, 0, path, lastModified);
                        break;
                    case WorkSite8Application.DMSOfficeLocation.NewYork:
                        DataFileDownloaded = DMS.DownloadDocument("Legal1us", 299949, 0, path, lastModified);
                        break;
                }
            }
            catch (Exception e)
            {
                clsErrorHandling errHandler = new clsErrorHandling();
                errHandler.ProcessError(e);
                CatastrophicError =  true;
                throw new InvalidOperationException();

            }
                
        }

        /// <summary>
        /// Downloads the building blocks file from FileSite
        /// </summary>
        /// <returns></returns>
        internal Boolean DownloadBuildingBlocks()
        {
            try
            {
                long file = 27894708;
                long fileNY = 299951;
                String name = "Building Blocks.dotx";
                String path = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS, name);
                if (!System.IO.Directory.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS)))
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS));
                DateTime lastModified;
                System.IO.FileInfo info;
                if (System.IO.File.Exists(path))
                {
                    info = new System.IO.FileInfo(path);
                    lastModified = info.LastWriteTime;
                }
                else
                    lastModified = DateTime.Parse("01/01/1900");

                Boolean downloaded = false;

                switch (DMS.OfficeLocation)
                {
                    case WorkSite8Application.DMSOfficeLocation.London:
                        downloaded = DMS.DownloadDocument("Legal1", file, 0, path, lastModified);
                        break;
                    case WorkSite8Application.DMSOfficeLocation.NewYork:
                        downloaded = DMS.DownloadDocument("Legal1us", fileNY, 0, path, lastModified);
                        break;
                }
                return downloaded;
            }
            catch (Exception e)
            {
                clsErrorHandling errHandler = new clsErrorHandling();
                errHandler.ProcessError(e);
                return false;

            }

        }
        
        /// <summary>
        /// Downloads data xml files from FileSite
        /// </summary>
        internal void DownloadOtherDataFiles()
        {
            try
            {
                long[] files = { 27891174, 27891167, 27891140 };
                long[] filesNY = { 299950, 299947, 299946 };
                String[] fileNames = { "personalSettings.xml", "LetterParameters.xml", "Labels.xml" };
                int i = -1;
                String pathRoot = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA);
                long[] filesToUse = null;
                String db = String.Empty;

                switch (DMS.OfficeLocation)
                {
                    case WorkSite8Application.DMSOfficeLocation.London:
                        filesToUse = files;
                        db = "Legal1";
                        break;
                    case WorkSite8Application.DMSOfficeLocation.NewYork:
                        filesToUse = filesNY;
                        db = "Legal1us";
                        break;
                    default:
                        filesToUse = filesNY;
                        db = "Legal1us";
                        break;
                }

                foreach (long file in filesToUse)
                {
                    i++;
                    String path = System.IO.Path.Combine(pathRoot, fileNames[i]);
                    DateTime lastModified;
                    System.IO.FileInfo info;
                    if (System.IO.File.Exists(path))
                    {
                        info = new System.IO.FileInfo(path);
                        lastModified = info.LastWriteTime;
                    }
                    else
                        lastModified = DateTime.Parse("01/01/1900");

                    DMS.DownloadDocument(db, file, 0, path, lastModified);
                }
            }
            catch (Exception e)
            {
                clsErrorHandling errHandler = new clsErrorHandling();
                errHandler.ProcessError(e);
                CatastrophicError = true;
                throw new InvalidOperationException();

            }
        }

        internal Boolean CHeckPathsCreated
        {
            get
            {
                return System.IO.Directory.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS)) &&
                    System.IO.Directory.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.DATA)) &&
                    System.IO.Directory.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.TEMPLATES)) &&
                    System.IO.Directory.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.STYLESETS));
            }
        }

        internal Boolean DownloadStyleSets(List<StyleSet> stylesets, Boolean force)
        {
            try
            {
                String pathRoot = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.STYLESETS);
                if (!System.IO.Directory.Exists(pathRoot))
                    System.IO.Directory.CreateDirectory(pathRoot);
                Boolean downloaded = false;
                foreach (StyleSet styleset in stylesets)
                {
                    progress += "|||";
                    Globals.ThisAddIn.Application.StatusBar = String.Format("Checking for styleset updates: {0}", progress);
                    String path = System.IO.Path.Combine(pathRoot, styleset.File);
                    System.IO.FileInfo info;
                    DateTime lastModified;
                    if (System.IO.File.Exists(path) && !force)
                    {
                        info = new System.IO.FileInfo(path);
                        lastModified = info.LastWriteTime;
                    }
                    else
                        lastModified = DateTime.Parse("01/01/1900");

                    Boolean down = DMS.DownloadDocument(styleset.Database, styleset.DocumentNumber, 0, path, lastModified);
                    if (down)
                        downloaded = true;
                }
                return downloaded;
            }
            catch (Exception e)
            {
                clsErrorHandling errHandler = new clsErrorHandling();
                errHandler.ProcessError(e);
                return false;
            }
        }

        internal void MergeStyles(Templates templates)
        {
            String sourcePath = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.STYLESETS);
            String destPath = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.TEMPLATES);
            int i = 0;
            foreach(Template template in templates)
            {
                i++;
                progress += "|||";
                Globals.ThisAddIn.Application.StatusBar = String.Format("Merging styles ({0} of {1}): {2}", i, templates.Count, progress);

                if (template.StyleSet != null)
                {
 
                    String sourceFile = System.IO.Path.Combine(sourcePath, template.StyleSet.File);
                    String destFile = System.IO.Path.Combine(destPath, template.Filename);

                    Stylick.DocumentHandler source = new Stylick.DocumentHandler(sourceFile);
                    Stylick.DocumentHandler dest = new Stylick.DocumentHandler(destFile);

                    foreach (IncludeStyle includeStyle in template.StyleSet.IncludeStyles)
                    {
                        
                        IEnumerable<Stylick.Style> styles = from d in source.Styles where
                              d.Name.ToLower().Contains(includeStyle.SearchString.ToLower()) && includeStyle.Custom == d.Custom
                                                            select d;
                        foreach (Stylick.Style style in styles)
                            source.XML.CopyStyle(style, dest, true, true);
                    }

                    dest.Save();
                }
                
            }
        }
        
        
        /// <summary>
        /// Downloads the template files from FileSite
        /// </summary>
        /// <param name="templates">A collection of templates derived from the OfficeData.xml file</param>
        /// <returns></returns>
        internal Boolean DownloadTemplates(Templates templates, Boolean force)
        {
            String pathRoot = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.TEMPLATES);
            Boolean downloaded = false;
            if (!System.IO.Directory.Exists(pathRoot))
                System.IO.Directory.CreateDirectory(pathRoot);
            int i = 0;
            
            foreach(Template template in templates)
            {
                i++;
                                
                progress += "|||";

                Globals.ThisAddIn.Application.StatusBar = String.Format("Checking for updates ({0} of {1}): {2}", i, templates.Count, progress);
                DateTime lastModfied;
                String path = System.IO.Path.Combine(pathRoot, template.Filename);
                System.IO.FileInfo info;
                if (System.IO.File.Exists(path) && !force)
                {
                    info = new System.IO.FileInfo(path);
                    lastModfied = info.LastWriteTimeUtc;
                }
                else
                    lastModfied = DateTime.Parse("01/01/1900");

                foreach(FileSiteDocument fsd in template.FilesiteDocs)
                    try
                    {
                        if(DMS.IsPreferredDatabase(fsd.Database))
                            if (DMS.DownloadDocument(fsd.Database, fsd.DocumentNumber, 0, path, lastModfied))
                                downloaded = true;
                    }
                    catch { }
                
            }

            
            return downloaded;
        }

        internal void Disconnect()
        {
            Globals.ThisAddIn.Application.StatusBar = String.Format("Check for updates complete: {0}", progress);
            DMS.DisconnectTempSession();
        }
    }
}
