﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using Word = Microsoft.Office.Interop.Word;
using System.Management;
using System.Xml;
using System.ServiceProcess;
using System.Runtime.InteropServices;
using System.Xml.Linq;

namespace Templates1
{
    public class PrintMonitor
    {
        private ManagementEventWatcher newPrintJobs;
        private PrintJob mPrintJob;
        public PrintMonitor(ref PrintJob pj)
        {
            mPrintJob = pj;
            newPrintJobs = new ManagementEventWatcher();
            newPrintJobs.Scope = new ManagementScope(ManagementPath.DefaultPath);
            
            WqlEventQuery query =
            new WqlEventQuery("__InstanceCreationEvent",
            new TimeSpan(0, 0, 30),
            "TargetInstance ISA 'Win32_PrintJob'");
            newPrintJobs.Query = query;
            newPrintJobs.EventArrived += new EventArrivedEventHandler(newPrintJobs_EventArrived);
            newPrintJobs.Scope.Connect();
            newPrintJobs.InitializeLifetimeService();
            newPrintJobs.Options.BlockSize = 1;
            newPrintJobs.Start();
        }

        public void newPrintJobs_EventArrived(object sender, EventArrivedEventArgs e)
        {
            ManagementBaseObject printJob = (ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value;

            foreach (PropertyData prop in printJob.Properties)
            {
                string val = prop.Value == null ? "null" : prop.Value.ToString();
                
            }
        }
    }
    
    /// <summary>
    /// A base class that handles printer settings
    /// </summary>
    public class PrintJob
    {
        /// <summary>
        /// Properties
        /// </summary>
        
        private String pages;
        private String jobtype;
        private Word.Document document;
        private Printer printer;
        private Boolean jobCancelled;
        internal PrintTray selectedTray;
        internal String papertype;
        public delegate void PrintJobEventHandler(PrintJob printjob);
        public event PrintJobEventHandler JobPrinted;
        internal int duplex;
        private int pageCount;

        public int PageCountVariable
        {
            get { return pageCount; }
            set { pageCount = value; }
        }
        
        public int Duplex
        {
            get { return duplex; }
            set { duplex = value; }
        }
        

        public Boolean JobCancelled
        {
            get { return jobCancelled; }
            set { jobCancelled = value; }
        }

        internal Printer Printer
        {
            get { return printer; }
            set { printer = value; }
        }
        
        
        internal Word.Document Document
        {
            get { return document; }
            set { document = value; }
        }

        internal String Pages
        {
            get { return pages; }
            set { pages = value; }
        }

        internal String Jobtype
        {
            get { return jobtype; }
            set { jobtype = value; }
        }
        /// <summary>
        /// Methods
        /// </summary>
        public PrintJob()
        {
            this.papertype = "None";
        }
        /// <summary>
        /// Sets the printer tray
        /// </summary>
        /// <param name="Doc">the document being printed</param>
        /// <param name="tray">the tray to be used</param>
        public void SetTray(Word.Document Doc, PrintTray tray)
        {
            foreach (Word.Section sect in Doc.Sections)
            {
                sect.PageSetup.FirstPageTray = (Word.WdPaperTray)tray.BinID;
                sect.PageSetup.OtherPagesTray = (Word.WdPaperTray)tray.BinID;
            }
        }
        /// <summary>
        /// Gets the tray settings in the selected document from the document's page setup properties.  Used to reset the trays back to default once the document is printed.
        /// </summary>
        /// <param name="Doc">The document being printed</param>
        /// <returns>A list of two print trays</returns>
        public PrintTrays GetTrays(Word.Document Doc)
        {
            PrintTrays trays = new PrintTrays();
            foreach(Word.Section sect in Doc.Sections)
            { 
                PrintTray tray = new PrintTray();
                tray.BinID = (int)sect.PageSetup.FirstPageTray;
                tray.IsFirstPageTray = true;
                tray.Section = sect.Index;
                trays.Add(tray);
                tray = new PrintTray();
                tray.BinID = (int)sect.PageSetup.OtherPagesTray;
                tray.IsFirstPageTray = false;
                tray.Section = sect.Index;
                trays.Add(tray);
            }
            return trays;
        }

        /// <summary>
        /// Matches the tray with the paper type
        /// </summary>
        /// <returns>A tray object</returns>
        public PrintTray MatchTray()
        {
            IEnumerable<PrintTray> trays = from d in printer.PrintTrays where d.TrayType.ToLower() == this.papertype.ToLower() select d;
            return trays.Count() > 0 ? trays.ToList()[0] : null;
        }

        /// <summary>
        /// Finds the auto select tray for NY letter/plain combinations.  Based on information provided by Rafat 19/05/15
        /// </summary>
        /// <returns>A print tray matching the description</returns>
        public PrintTray MatchTrayNYLetterPlain()
        {
            IEnumerable<PrintTray> trays = from d in printer.PrintTrays where d.TrayName.ToLower().Contains("auto") select d;
            return trays.Count() > 0 ? trays.ToList()[0] : null;
        }
        /// <summary>
        /// Finds the manual/bypass tray for NY other combinataions
        /// </summary>
        /// <returns>A print tray matching the description of manual or bypass</returns>
        public PrintTray MatchTrayNYOther()
        {
            IEnumerable<PrintTray> trays = from d in printer.PrintTrays where 
                                               (d.TrayName.ToLower().Contains("manual") || d.TrayName.ToLower().Contains("bypass")) select d;
            return trays.Count() > 0 ? trays.ToList()[0] : null;
        }
        /// <summary>
        /// Prints out the document having set the tray and duplex settings
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>true if the print job is successful and not cancelled by the user/program</returns>
        public Boolean PrintOut(Word.Document doc)
        {
            if (this.selectedTray == null)
                if (Globals.ThisAddIn.Officedata.IsNotNYOffice)
                    this.selectedTray = MatchTray();
                else
                {
                    if (this.papertype == "None")
                        this.selectedTray = MatchTrayNYLetterPlain();
                    else
                      if (doc.Sections[1].PageSetup.PaperSize == Word.WdPaperSize.wdPaperLetter && this.papertype == "Plain")
                            this.selectedTray = MatchTrayNYLetterPlain();
                        else
                            this.selectedTray = MatchTrayNYOther();
                }

            if (this.selectedTray == null)
            {
                FallBackPrintTray();
                if (this.jobCancelled)
                    return false;
            }
            
           if(this.selectedTray!=null)
                this.SetTray(doc, selectedTray);
            
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            ds.SetPrinterDuplex(this.printer.Name, this.duplex, out oErr);
            if(oErr.Length>0)
                System.Windows.Forms.MessageBox.Show("Duplex error: " + oErr, Globals.ThisAddIn.APP_NAME1);
            if(this.pages.Contains("s"))
                this.pages = ConvertSectionToPages(doc, this.pages);
            doc.PrintOut(Pages: this.pages);
            this.PageCountVariable = this.PageCount();
            this.ShowHideLogo(true);
            this.JobPrinted(this);
            return true;
        }

        private String ConvertSectionToPages(Word.Document doc, String section)
        {
            int iSec = Convert.ToInt16(section.Replace("s", ""));
            String buffer = String.Empty;
            if(doc.Sections.Count > 1)
            { 
                Word.Range rng = doc.Sections[iSec].Range;
                rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                do
                {
                    rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                } while (rng.Sections.Count == 1);
                for (int i = 1; i < rng.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber] + 1; i++)
                    buffer += buffer + i + ",";

                return buffer.Substring(0, buffer.Length - 1);
            }
            else
            {
                return "1-" + doc.Sections[1].Range.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber];
            }

        }

        
        /// <summary>
        /// Launches a dialog box for the user to select the correct tray if it's not been identified in the XML files.  The print job can also be cancelled by the same dialog.
        /// </summary>
        public void FallBackPrintTray()
        {
            System.Windows.Forms.MessageBox.Show("Unable to set tray through matching.  Ensure your printer is registered centrally and that it is not a virtual printer.", Globals.ThisAddIn.APP_NAME1);
            Forms.frmPrintTray printTrayForm = new Forms.frmPrintTray(this.printer, this.Jobtype);
            printTrayForm.FormClosing += new System.Windows.Forms.FormClosingEventHandler(printTrayForm_Closing);
            printTrayForm.ShowDialog();
        }

        /// <summary>
        /// Pseudo-method for printing out the document.  Just triggers the JobPrinted event
        /// </summary>
        public void PrintOut()
        {
            this.PageCountVariable = this.PageCount();
            this.JobPrinted(this);
        }

        public int PageCount()
        {
            ManagementScope oManagementScope;
            oManagementScope = new ManagementScope(ManagementPath.DefaultPath);
            oManagementScope.Connect();

            SelectQuery oSelectQuery = new SelectQuery();
            oSelectQuery.QueryString = @"SELECT * FROM Win32_PrintJob WHERE Name = '" + this.Printer.Name.Replace(@"\", @"\\") +"'";

            ManagementObjectSearcher oObjectSearcher =
               new ManagementObjectSearcher(oManagementScope, @oSelectQuery);
            ManagementObjectCollection oObjectCollection = oObjectSearcher.Get();

            foreach (ManagementObject oItem in oObjectCollection)
            {
                return Convert.ToInt16(oItem.GetPropertyValue("pageCount"));
            }
            return 0;
        }

        /// <summary>
        /// Called when the dialog box to identify the correct tray is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printTrayForm_Closing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            Forms.frmPrintTray printTray = (Forms.frmPrintTray)sender;
            this.selectedTray = printTray.Printtray;
            this.jobCancelled = printTray.JobCancelled;
        }
        
        /// <summary>
        /// Handles watermarks
        /// </summary>
        /// <param name="doc">the owner document</param>
        /// <param name="watermarkText">the text of the watermark</param>
        public void ProcessWatermark(Document doc, String watermarkText)
        {
            if (watermarkText != String.Empty)
                if (!doc.HasWatermark())
                {
                    doc.Watermark = new Watermark(doc.WordDocument);
                    doc.Watermark.Add(watermarkText);
                }
                else
                    doc.Watermark.SetText(watermarkText);
            else
                if (doc.HasWatermark())
                {
                    doc.Watermark.Remove();
                    doc.Watermark = null;
                }
        }

        /// <summary>
        /// Toggles the logo found on the letterheads and bills
        /// </summary>
        /// <param name="visible">true if the logo is to be shown</param>
        public void ShowHideLogo(Boolean visible)
        {
            Word.Document doc = this.document;
            foreach (Word.Section sect in doc.Sections)
            {
                foreach (Word.HeaderFooter hdr in sect.Headers)
                { 
                    foreach (Word.Shape shp in hdr.Shapes)  //London templates
                        try
                        {
                            if (shp.Title == Globals.ThisAddIn.Officedata.Offices.ActiveOffice.Logo.Name)
                                shp.Visible = visible ? Microsoft.Office.Core.MsoTriState.msoTrue : Microsoft.Office.Core.MsoTriState.msoFalse;
                        }
                        catch { }
                    foreach(Word.ShapeRange shp in hdr.Range.InlineShapes) //NY templates
                        try
                        {
                            if (shp.Title == Globals.ThisAddIn.Officedata.Offices.ActiveOffice.Logo.Name)
                                shp.Visible = visible ? Microsoft.Office.Core.MsoTriState.msoTrue : Microsoft.Office.Core.MsoTriState.msoFalse;
                        }
                        catch { }
                    
                }

                foreach (Word.HeaderFooter hdr in sect.Footers)
                    foreach (Word.Shape shp in hdr.Shapes)
                        try
                        {
                            if (shp.Title == "MdRLogo")
                                shp.Visible = visible ? Microsoft.Office.Core.MsoTriState.msoTrue : Microsoft.Office.Core.MsoTriState.msoFalse;
                        }
                        catch { }
            }
            doc.Application.ScreenRefresh();
        }

    }

    /// <summary>
    /// A class inheriting PrintJob for printing file copies of documents
    /// </summary>
    class FileCopy : PrintJob
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="printer">the active printer object</param>
        /// <param name="doc">the document to be printed``</param>
        public FileCopy(Printer printer, Document doc)
        {
            this.selectedTray = null;
            this.Jobtype = "File copy";
            this.Printer = printer;
            this.Document = doc.WordDocument;
            this.papertype = "Plain";
            this.duplex = Globals.ThisAddIn.Ribbonopts.FileCopyDuplex;
            
        }
    }
    
    /// <summary>
    /// Class for printing labels
    /// </summary>
    class Labels : PrintJob
    {
        public Labels()
        { }

        /// <summary>
        /// Constructor: sets the label tray to be the manual or bypass tray
        /// </summary>
        /// <param name="printer">The printer doing the printing</param>
        /// <param name="doc">the document/labels to be printed</param>
        public Labels(Printer printer, Document doc)
        {
            this.selectedTray = null;
            this.Jobtype = "Labels";
            this.Document = doc.WordDocument;
            this.Pages = "1";
            this.Duplex = 1;
            this.Printer = printer;
            this.papertype = "Labels";
            foreach (PrintTray tray in printer.PrintTrays)
                if (tray.TrayName.ToLower().Contains("manual") || tray.TrayName.ToLower().Contains("bypass")) //Always send labels to the manual feed tray
                { 
                    this.selectedTray = tray;
                    break;
                }
                
            

        }
    }
    
    /// <summary>
    /// Class for printing onto letterheaded paper
    /// </summary>
    class Letterhead : PrintJob
    {
        
        public Letterhead()
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="printer">The printer doing the printing</param>
        /// <param name="doc">the document to be printed</param>
        /// <param name="pages">the page range to be printed</param>
        public Letterhead(Printer printer, Document doc, String pages)
        {
            this.selectedTray = null;
            this.Jobtype = "Letterhead";
            this.Document = doc.WordDocument;
            if (pages == null)
                this.Pages = "1-" + doc.WordDocument.Range().Information[Word.WdInformation.wdActiveEndPageNumber];
            else
                this.Pages = pages;
            if (doc.Template.Filename.ToLower().Contains("bill"))
                this.duplex = 2;
            else
                this.duplex = 1;
            this.Printer = printer;
            this.papertype = "Letterhead";
            
            if(!Globals.ThisAddIn.Ribbonopts.WaterMark)
                this.ProcessWatermark(doc, "");
            this.ShowHideLogo(false);
        }

       }
    /// <summary>
    /// A class for printing bills onto letterheaded paper
    /// </summary>
    class LetterheadWithNotes : Letterhead
    {
        /// <summary>
        /// Construvtor
        /// </summary>
        /// <param name="printer">the printer being printed to</param>
        /// <param name="doc">the document to be printed</param>
        /// <param name="watermark">watermark text</param>
        public LetterheadWithNotes(Printer printer, Document doc, String watermark)
        {
            this.selectedTray = null;
            this.Jobtype = "Letterhead with notes";
            this.Document = doc.WordDocument;
            this.duplex = 2;
            this.Printer = printer;
            this.papertype = "Letterhead";
            
            this.ProcessWatermark(doc, watermark);
            this.ShowHideLogo(false);
            if(Globals.ThisAddIn.Officedata.IsNotNYOffice)
                InsertNotesPage(doc.WordDocument);
            clsDynamicInserts.PopulateInvoiceNotes(doc.WordDocument);
            this.Pages = getNotesPages(doc.WordDocument);
        }
        /// <summary>
        /// The notes page is added to the end of the documnent and the print range set to repeat it every other page
        /// </summary>
        /// <param name="doc">the document to be printed</param>
        /// <returns>a string representing the print range</returns>
        private String getNotesPages(Word.Document doc)
        {
            int notespage = doc.Range().Information[Word.WdInformation.wdActiveEndPageNumber];
            String pages = String.Empty;
            for (int i = 1; i < notespage; i++)
                pages += String.Format("{0},{1},", i.ToString(), notespage.ToString());
            //Drop the final comma
                return pages.Substring(0, pages.Length - 1);
        }
        /// <summary>
        /// Inserts the notes page in a new section at the end of the document
        /// </summary>
        /// <param name="doc">the Word document to take the notes page</param>
        private void InsertNotesPage(Word.Document doc)
        {
            Word.Templates temps = doc.Application.Templates;
            temps.LoadBuildingBlocks();
            Word.Range rng = doc.Range();
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rng.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            Word.Section sect = doc.Sections[doc.Sections.Count];
            clsBuildingBlocks bb = new clsBuildingBlocks();
            Word.Template temp = bb.BuildingBlocksTemplate;
            temp.BuildingBlockEntries.Item("Payment notes").Insert(sect.Range);
        }

        /// <summary>
        /// Deletes the last section in the document which should be the notes page.
        /// Sets linktoprevious true and false, otherwise the headers and footers on 
        /// the first page disappear when the section break is deleted
        /// </summary>
        /// <param name="doc">The Word document with the notes page</param>
        public void RemoveNotesPage(Word.Document doc)
        {
            foreach (Word.HeaderFooter hdr in doc.Sections[doc.Sections.Count].Headers)
                hdr.LinkToPrevious = true;
            foreach (Word.HeaderFooter hdr in doc.Sections[doc.Sections.Count].Footers)
                hdr.LinkToPrevious = true;
            foreach (Word.HeaderFooter hdr in doc.Sections[doc.Sections.Count].Headers)
                hdr.LinkToPrevious = false;
            foreach (Word.HeaderFooter hdr in doc.Sections[doc.Sections.Count].Footers)
                hdr.LinkToPrevious = false;
            Word.Range rng = doc.Sections[doc.Sections.Count].Range;
            rng.MoveStart(Word.WdUnits.wdParagraph, -1);
            rng.Delete();
        }

    }
    /// <summary>
    /// A class handling engrossment printing
    /// </summary>
    class Engrossment : PrintJob
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="printer">the printer to be used</param>
        /// <param name="doc">the document to be printed</param>
        /// <param name="pages">the range of pages to be printed</param>
        public Engrossment(Printer printer, Document doc, String pages)
        {
            this.selectedTray = null;
            this.Jobtype = "Engrossment";
            this.Document = doc.WordDocument;
            this.Pages = pages;
            this.duplex = Globals.ThisAddIn.Ribbonopts.EngrossmentDuplex;
            this.Printer = printer;
            this.papertype = "Bond";
        }
    }
    /// <summary>
    /// A class to handle accounts printing
    /// </summary>
    class FinalThreeFiveJob : PrintJob
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="printer">the printer to do the printing</param>
        /// <param name="doc">the document to be printed</param>
        /// <param name="pages">the print range</param>
        /// <param name="watermarkText">watermark text</param>
        /// <param name="showLogo">whether to show the logo or not</param>
        public FinalThreeFiveJob(Printer printer, Document doc, String pages, String watermarkText, Boolean showLogo)
        {
            this.selectedTray = null;
            this.Jobtype = "Accounts";
            this.Document = doc.WordDocument;
            this.duplex = 2;
            this.Printer = printer;
            this.papertype = "Plain";
            this.Pages = pages;
            this.ProcessWatermark(doc, watermarkText);
            this.ShowHideLogo(showLogo);
        }
   }

    /// <summary>
    /// A class that handles the varying print jobs for account printing.  This is instead of calling the whole lot from the BeforePrint event in clsWordApp
    /// </summary>
    class FinalThreeFive
    {
        private FinalThreeFiveJob threefivejobs;
        private LetterheadWithNotes letterheadwithnotes;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="printer">the printer to do the printing</param>
        /// <param name="doc">the document to be printed</param>
        /// <param name="pages">a print range</param>
        /// <param name="five">True if the five types of accounts are to be printed.  Otherwise it will be a set of three.</param>
        
        public FinalThreeFive(Printer printer, Document doc, String pages, Boolean five)
        {
            
            if(five)
            { 
                letterheadwithnotes = (new LetterheadWithNotes(printer, doc, ""));
                letterheadwithnotes.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                letterheadwithnotes.PrintOut(doc.WordDocument);
                letterheadwithnotes.ProcessWatermark(doc, "Remittance Advice");
                letterheadwithnotes.PrintOut(doc.WordDocument);
                
                letterheadwithnotes.RemoveNotesPage(doc.WordDocument);

                threefivejobs = (new FinalThreeFiveJob(printer, doc, pages, "Accounts", true));
                threefivejobs.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                threefivejobs.PrintOut(doc.WordDocument);

                threefivejobs = (new FinalThreeFiveJob(printer, doc, pages, "File Copy", true));
                threefivejobs.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                threefivejobs.PrintOut(doc.WordDocument);

                threefivejobs = (new FinalThreeFiveJob(printer, doc, pages, "Credit Control", true));
                threefivejobs.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                threefivejobs.PrintOut(doc.WordDocument);

                //letterheadwithnotes = (new LetterheadWithNotes(printer, doc, "Remittance Advice"));
                //letterheadwithnotes.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                //letterheadwithnotes.PrintOut(doc.WordDocument);
                letterheadwithnotes.ProcessWatermark(doc, "");
                //letterheadwithnotes.RemoveNotesPage(doc.WordDocument);
                
            }
            else
            {
                letterheadwithnotes = (new LetterheadWithNotes(printer, doc, ""));
                letterheadwithnotes.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                letterheadwithnotes.PrintOut(doc.WordDocument);
                letterheadwithnotes.RemoveNotesPage(doc.WordDocument);

                threefivejobs = (new FinalThreeFiveJob(printer, doc, "s3", "", false));
                threefivejobs.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                if (doc.WordDocument.Sections.Count > 3)
                    threefivejobs.PrintOut(doc.WordDocument);
                else
                    System.Windows.Forms.MessageBox.Show("No third section found in the current document.  Unable to print the second of three.", Globals.ThisAddIn.APP_NAME1);

                threefivejobs = (new FinalThreeFiveJob(printer, doc, "s1", "File Copy", false));
                threefivejobs.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                threefivejobs.PrintOut(doc.WordDocument);
                threefivejobs.ProcessWatermark(doc, "");
            }
            


        }
    }
    /// <summary>
    /// A class that handles communication with printers
    /// </summary>
    class clsPrinting
    {
        private static ManagementScope oManagementScope = null;
        //private String xmlFileLocation = @"\\hounfas02\Software Repository\Software\Windows 7\Tier 2\Print Path\Current\Master\master.xml";

        /// <summary>
        /// Constructor
        /// </summary>
        public clsPrinting()
        {

        }
        /// <summary>
        /// Outputs various attributes of a printer using WNI.  Not used.
        /// </summary>
        /// <param name="sPrinterName">A printer's id</param>
        public static void GetPrinterInfo(string sPrinterName)
        {
            oManagementScope = new ManagementScope(ManagementPath.DefaultPath);
            oManagementScope.Connect();

            SelectQuery oSelectQuery = new SelectQuery();
            oSelectQuery.QueryString = @"SELECT * FROM Win32_Printer WHERE Name = '" + sPrinterName + "'";

            ManagementObjectSearcher oObjectSearcher =
               new ManagementObjectSearcher(oManagementScope, @oSelectQuery);
            ManagementObjectCollection oObjectCollection = oObjectSearcher.Get();

            foreach (ManagementObject oItem in oObjectCollection)
            {
                Console.WriteLine("Name : " + oItem["Name"].ToString());
                Console.WriteLine("PortName : " + oItem["PortName"].ToString());
                Console.WriteLine("DriverName : " + oItem["DriverName"].ToString());
                Console.WriteLine("DeviceID : " + oItem["DeviceID"].ToString());
                Console.WriteLine("Shared : " + oItem["Shared"].ToString());
                Console.WriteLine("---------------------------------------------------------------");
            }
        }

        /// <summary>
        /// Determines if the printer has a duplex bin using WMI.  Not used as not always accurate
        /// </summary>
        /// <param name="sPrinterName">The printer to be interrogated</param>
        /// <returns>True if the printer has a duplex bin</returns>
        public static Boolean HandlesDuplex(String sPrinterName)
        {
           oManagementScope = new ManagementScope(ManagementPath.DefaultPath);
            oManagementScope.Connect();

            SelectQuery oSelectQuery = new SelectQuery();
            sPrinterName = sPrinterName.Replace(@"\", @"\\");
            oSelectQuery.QueryString = @"SELECT * FROM Win32_Printer WHERE Name = '" + sPrinterName + "'";

            
            ManagementObjectSearcher oObjectSearcher =
               new ManagementObjectSearcher(oManagementScope, @oSelectQuery);
            ManagementObjectCollection oObjectCollection = oObjectSearcher.Get();

            String[] capabilitiesDesc ;
            int j = -1;
            foreach (ManagementObject oItem in oObjectCollection)
            {
                Int16[] capabilities = (Int16[])oItem["Capabilities"];
                capabilitiesDesc = (String[])oItem["CapabilityDescriptions"];
                foreach (Int16 i in capabilities)
                {
                    j++;
                    Console.WriteLine(capabilitiesDesc[j]);
                    if (i == 3)
                        return true;    
                }
            }
            
            return false;
            
            
                          
        }
        
        
    }

    /// <summary>
    /// A wrapper class represeting a printer
    /// </summary>
    public class Printer
    {
        [DllImport("winspool.drv")]
        public static extern bool AddPrinterConnection(string pName);

        /// <summary>
        /// Properties
        /// </summary>
        private PrintTrays printTrays;
        private String name;
        private Boolean defaultPrinter;
        private String deviceInfo;
        private Boolean handlesDuplex;
        private Boolean colourCapable;
        private int startDuplex;
        private Boolean error;
        private String filepath;
        private int pageCount;

        public int PageCount
        {
            get { return pageCount; }
            set { pageCount = value; }
        }



        public String Filepath
        {
            get { return filepath; }
            set { filepath = value; }
        }



        public Boolean Error
        {
            get { return error; }
            set { error = value; }
        }

        public int StartDuplex
        {
            get { return startDuplex; }
            set { startDuplex = value; }
        }

        public Boolean ColourCapable
        {
            get { return colourCapable; }
            set { colourCapable = value; }
        }

        public Boolean HandlesDuplex
        {
            get { return handlesDuplex; }
            set { handlesDuplex = value; }
        }

        public String DeviceInfo
        {
            get { return deviceInfo; }
            set { deviceInfo = value; }
        }

        public Boolean DefaultPrinter
        {
            get { return defaultPrinter; }
            set { defaultPrinter = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public PrintTrays PrintTrays
        {
            get { return printTrays; }
            set { printTrays = value; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Printer()
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">the name of the printer</param>
        public Printer(String name)
        {
            this.name = name;
            GetPrinterDetails(IsConnected());
        }


        /// <summary>
        /// Adds a connection to the printer if the printer was offline
        /// </summary>
        /// <returns></returns>
        public Boolean Connect()
        {
            Boolean exists;
            if (!IsConnected())
            {
                exists = false;
                ServiceController svc = new ServiceController("Spooler"); //Sometimes the connect method causes the print spooler service to hang.  This restarts it.
                if (svc.Status == ServiceControllerStatus.Stopped)
                    svc.Start();
                bool result = AddPrinterConnection(this.Name);
                if (!result)
                {
                    //System.Windows.Forms.MessageBox.Show("Unable to connect to printer " + name, "Mishcon de Reya templates");
                    this.error = true;
                    exists = false;
                }
                else
                {
                    this.error = false;
                    exists = true;
                }
            }
            else
            {
                exists = true;
                this.error = false;
            }
            return exists;
        }

        /// <summary>
        /// Checks to see if the printer is connected to the PC
        /// </summary>
        /// <returns>True if the printer is connected, otherwise false</returns>
        public Boolean IsConnected()
        {
            
                ManagementScope scope = new ManagementScope(@"\root\cimv2");
                scope.Connect();

                // Select Printer from WMI Object Collections
                ManagementObjectSearcher searcher = new
                 ManagementObjectSearcher("SELECT * FROM Win32_Printer WHERE Name = '" + this.Name.Replace(@"\", @"\\") + "'");

                try
                {
                    foreach (ManagementObject printer in searcher.Get())
                    {

                        if (printer["WorkOffline"].ToString().ToLower().Equals("true"))
                        {
                            // printer is offline
                            return false;
                        }
                        else
                        {
                            // printer is not offline
                            return true;
                        }

                    }
                }
                catch { }
                return false;
            
        }
        
        /// <summary>
        /// Communicates with the printer to extract key information from it
        /// </summary>
        /// <param name="isConnected">Boolean whether or not the printer is online</param>
        /// <returns></returns>
        public Printer GetPrinterDetails(Boolean isConnected)
        {
            System.Drawing.Printing.PrintDocument printDoc = null;
            if(isConnected)
            { 
                printDoc = new PrintDocument();
                printDoc.PrinterSettings.PrinterName = this.name;
                this.defaultPrinter = printDoc.PrinterSettings.IsDefaultPrinter;
                this.deviceInfo = GetPrinterModel(this.name);
                String oErr;
                this.startDuplex = new DuplexSettings().GetPrinterDuplex(this.name, out oErr);
            }
                GetTraysFromSource(printDoc, isConnected);
                return this;
        }

        /// <summary>
        /// Gets tray data from the printer itself
        /// </summary>
        /// <param name="printDoc">A print document object (native to dotNet)</param>
        /// <param name="isConnected">Boolean is the printer online</param>
        /// <returns>A list of print trays with information about them</returns>
        public PrintTrays GetTraysFromSource(PrintDocument printDoc, Boolean isConnected)
        {
            
            this.printTrays = new PrintTrays();
            if (isConnected)
            {
                for (int j = 0; j < printDoc.PrinterSettings.PaperSources.Count; j++)
                {
                    //if (this.printTrays.Count > 0)
                    //    if (this.printTrays[0].PrinterNotInXML)
                    //        break;
                    PaperSource pkSource = printDoc.PrinterSettings.PaperSources[j];
                    PrintTray pt = new PrintTray(this.deviceInfo, this.name, pkSource, this);
                    this.printTrays.Add(pt);

                }
            }

            return this.PrintTrays;
        }

        /// <summary>
        /// Fetches tray data from the XML files.  THis is much faster than connecting to each printer and used for the batch tool
        /// </summary>
        /// <param name="simplename"></param>
        /// <returns></returns>
        public PrintTrays GetTraysFromXML(String simplename)
        {
            this.printTrays = new Templates1.PrintTrays();
            System.IO.DirectoryInfo diTop = new System.IO.DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo\Printing");
            XmlDocument xmlfile;
            xmlfile = new XmlDocument();
            simplename = simplename.Replace("\\\\", "||");
            simplename = simplename.Replace("\\", "|");
            simplename = simplename.Replace("||", @"\\");
            simplename = simplename.Replace("|", @"\");
            simplename = simplename.Replace(".mishcon.co.uk", "");
            foreach (var fi in diTop.EnumerateFiles())
            {
                xmlfile.Load(fi.FullName);
                
                XmlNodeList ndl = xmlfile.DocumentElement.SelectNodes("Groups/Group/Printers/Printer");
                foreach (XmlNode node in ndl)
                    if(node.SelectSingleNode("WindowsName").InnerText.ToLower().Contains(simplename.ToLower()))
                    {
                        String model = node.SelectSingleNode("PrinterType").InnerText;
                        this.deviceInfo = model;
                        XmlNode xprinter = xmlfile.DocumentElement.SelectSingleNode("Printers/Printer[@ID='" + model + "']");
                        foreach (XmlNode tray in xprinter.SelectNodes("Trays/Tray"))
                        {
                            PrintTray pt = new PrintTray();
                            pt.Printer = this;
                            pt.TrayName = tray.SelectSingleNode("@ID").InnerText;
                            pt.BinID = Convert.ToInt16(tray.SelectSingleNode("BinCode").InnerText);
                            XmlNode type = node.SelectSingleNode("MediaTypes/MediaType[Tray='" + pt.TrayName + "']/@ID");
                            if(type!=null)
                                pt.TrayType = type.InnerText;
                            this.printTrays.Add(pt);
                            this.Filepath = fi.FullName;
                        }
                    }

            }
            return this.printTrays;
        }
        
        /// <summary>
        /// Determines the driver of the printer.  Uses WMI so printer must be connected to the PC.
        /// </summary>
        /// <param name="sPrinterName">the name of the printer</param>
        /// <returns></returns>
        private String GetPrinterModel(String sPrinterName)
        {
            ManagementScope oManagementScope = new ManagementScope(ManagementPath.DefaultPath);
            oManagementScope.Connect();

            SelectQuery oSelectQuery = new SelectQuery();
            oSelectQuery.QueryString = @"SELECT * FROM Win32_Printer WHERE Name = '" + sPrinterName.Replace(@"\", @"\\") + "'";

            ManagementObjectSearcher oObjectSearcher =
               new ManagementObjectSearcher(oManagementScope, @oSelectQuery);
            ManagementObjectCollection oObjectCollection = oObjectSearcher.Get();

            foreach (ManagementObject oItem in oObjectCollection)
            {
                return oItem["DriverName"].ToString();
            }

            return "";
        }
        /// <summary>
        /// Returns the printer's duplex setting to what it was before printing started.
        /// </summary>
        public void ResetDuplex()
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            ds.SetPrinterDuplex(this.name, this.startDuplex, out oErr);
        }

        /// <summary>
        /// Returns the printer's duplex setting to a value provided
        /// </summary>
        /// <param name="setting">A number between 1 and 3 used to set duplex</param>
        public void ResetDuplex(int setting)
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            ds.SetPrinterDuplex(this.name, setting, out oErr);
        }
    }

    /// <summary>
    /// A list of printers
    /// </summary>
    public class Printers : List<Printer>
    {
        /// <summary>
        /// Constructors
        /// </summary>
        public Printers()
        {
                      
        }
        public Printers(IEnumerable<Printer> printers)
            : base(printers)
        {

        }

        /// <summary>
        /// Finds the printer object in the list
        /// </summary>
        /// <param name="sName">the name of the printer</param>
        /// <returns></returns>
        public Printer GetActivePrinter(String sName)
        {
            IEnumerable<Printer> result =
            (from c in this
             where c.Name == sName
             select c);
            foreach (Printer p in result)
                return p;
            return null;
        }
    }
    /// <summary>
    /// A wrapper class identifying a print tray
    /// </summary>
    public class PrintTray
    {
        /// <summary>
        /// Properties
        /// </summary>
        private System.Drawing.Printing.PaperSource source;
        private Boolean usedForLetterhead;
        private Boolean usedForFileCopy;
        private Boolean usedForEngrossment;
        private String trayName;
        private String trayType;
        private int section;
        private Boolean isFirstPageTray;
        private int binID;
        private Boolean printerNotInXML;
        private Printer printer;


        public String xmlpath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo\Printing";

        public Printer Printer
        {
            get { return printer; }
            set { printer = value; }
        }
        
        
        public Boolean PrinterNotInXML
        {
            get { return printerNotInXML; }
            set { printerNotInXML = value; }
        }

        public int BinID
        {
            get { return binID; }
            set { binID = value; }
        }

        public Boolean IsFirstPageTray
        {
            get { return isFirstPageTray; }
            set { isFirstPageTray = value; }
        }

        public int Section
        {
            get { return section; }
            set { section = value; }
        }


        public string TrayType
        {
            get { return trayType; }
            set { trayType = value; }
        }

        public String TrayName
        {
            get { return trayName; }
            set { trayName = value; }
        }

        public Boolean UsedForLetterhead
        {
            get { return usedForLetterhead; }
            set { usedForLetterhead = value; }
        }
        

        public Boolean UsedForFileCopy
        {
            get { return usedForFileCopy; }
            set { usedForFileCopy = value; }
        }
       

        public Boolean UsedForEngrossment
        {
            get { return usedForEngrossment; }
            set { usedForEngrossment = value; }
        }

        public System.Drawing.Printing.PaperSource Source
        {
            get { return source; }
            set { source = value; }
        }

        /// <summary>
        /// Constructors
        /// </summary>
        public PrintTray() { }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="DeviceInfo">The printer's driver</param>
        /// <param name="PrinterName">the name of the printer</param>
        /// <param name="source">tray data as a PaperSource (native dotNet)</param>
        /// <param name="printer">the printer</param>
        public PrintTray(String DeviceInfo, String PrinterName, System.Drawing.Printing.PaperSource source, Printer printer)
        {
            this.source = source;
            this.printer = printer;
            this.binID = source.RawKind;
            this.trayName = source.SourceName;
            this.trayType = GetSetTrayType(DeviceInfo, PrinterName, String.Empty, String.Empty, String.Empty);
            
            
        }
        /// <summary>
        /// Identifies or sets the correct tray from an XML lookup based on papertype
        /// </summary>
        /// <param name="model">driver</param>
        /// <param name="name">printer name</param>
        /// <param name="binCode">if a value is put in the value is written into the XML file.  If blank the bin id is produced from the printer</param>
        /// <param name="paperType">the type of paper onto which to print</param>
        /// <param name="trayName">the name of the tray</param>
        /// <returns></returns>
        public String GetSetTrayType(String model, String name, String binCode, String paperType, String trayName)
        {

            name = name.Replace("\\\\", "||");
            name = name.Replace("\\", "|");
            name = name.Replace("||", @"\\");
            name = name.Replace("|", @"\");
            name = name.Replace(".mishcon.co.uk", "");
           
            
            String TrayType = String.Empty;
            XmlNode printer2 = null;
            System.IO.DirectoryInfo diTop = new System.IO.DirectoryInfo(xmlpath);
            XmlDocument xmlfile;
            xmlfile = new XmlDocument();
            if (printer.Filepath == null || printer.Filepath == String.Empty)
            {
                foreach (var fi in diTop.EnumerateFiles())
                {
                    xmlfile.Load(fi.FullName);
                    //XDocument xd = XDocument.Load(fi.FullName);
                    //IEnumerable<XElement> results = from d in xd.Descendants("WindowsName") where d.Value.ToLower() == name.ToLower() select d;
                    //XElement xe = results.ToList()[0].Parent;

                    XmlNodeList printers = xmlfile.DocumentElement.SelectNodes("Groups/Group/Printers/Printer");
                    foreach (XmlNode xprinter in printers)
                        if (xprinter.SelectSingleNode("WindowsName").InnerText.ToLower() == name.ToLower())
                        {
                            printer2 = xprinter;
                            this.printer.Filepath = fi.FullName;
                            break;
                        }
                    if (printer2 != null)
                        break;


                }
                if (printer2 == null)
                {
                    this.printerNotInXML = true;
                    return String.Empty;
                }
            }
            else
            {
                xmlfile.Load(printer.Filepath);
                XmlNodeList printers = xmlfile.SelectNodes("//Groups/Group/Printers/Printer");
                foreach (XmlNode xprinter in printers)
                    if (xprinter.SelectSingleNode("WindowsName").InnerText.ToLower() == name.ToLower())
                    {
                        printer2 = xprinter;
                        break;
                    }

                if (printer2 == null)
                {
                    this.printerNotInXML = true;
                    return String.Empty;
                }
            }

            

            XmlNode node = xmlfile.SelectSingleNode("//Printers/Printer[@ID='" + model + "']");
            if (node == null)
            {
                this.printerNotInXML = true;
                return String.Empty;
            }
            
            XmlNode nod=null;
            if(binCode==String.Empty)
                nod = node.SelectSingleNode("Trays/Tray[BinCode='" + this.binID + "']");
            else
            {
                nod = node.SelectSingleNode("Trays/Tray[@ID='" + trayName + "']/BinCode");
                nod.InnerText = binCode;
                this.BinID = Convert.ToInt16(binCode);
            }



            if (nod != null)
            {
                if(trayName!=String.Empty)
                    this.trayName = trayName;
            }
            else
                TrayType = String.Empty;

            XmlNode trayType = printer2.SelectSingleNode("MediaTypes/MediaType[Tray='" + this.trayName + "']");
            if (trayType == null)
            {
                this.printerNotInXML = true;
                return String.Empty;
            }
            else
            {
                if (paperType == String.Empty)
                {
                    TrayType = trayType.SelectSingleNode("@ID").InnerText;
                    this.printerNotInXML = false;
                }
                else
                {
                    trayType.SelectSingleNode("@ID").InnerText = paperType;
                    this.TrayType = paperType;
                }
            }

            if(this.printer.Filepath!= String.Empty || this.printer.Filepath != null)
                xmlfile.Save(this.printer.Filepath);
            return TrayType;

        }
        
    }

    /// <summary>
    /// A list of print trays
    /// </summary>
    public class PrintTrays : List<PrintTray>
    {
        /// <summary>
        /// Constructors
        /// </summary>
        public PrintTrays()
        {

        }

        public PrintTrays(IEnumerable<PrintTray> printTrays)
            : base (printTrays)
        {

        }

         
        /// <summary>
        /// Gets the tray matching the paper type provided
        /// </summary>
        /// <param name="sType">The paper type</param>
        /// <returns>a PrintTray object matching the requirements</returns>
        public PrintTray GetAppropriateTray(String sType)
        {
            IEnumerable<PrintTray> result =
             (from c in this
              where c.TrayType == sType
              select c);
            
                foreach (PrintTray t in result)
                    return t;
                return this[0];
        }
    }
}
