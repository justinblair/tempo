﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Templates1
{
    public class clsRibbonOptions
    {
        private int _office;
        private int _lang;
        private int _PrintRange;
        private Boolean filePrint;
        private Boolean waterMark;
        private Boolean retainWatermark;
        private String customRange;
        private Boolean letterHead;
        private Boolean engrossment;
        private Boolean accounts;
        private String customRangeEngross;
        private String customRangeAccounts;
        private int engrossmentDuplex;
        private int final35narrative;
        private int fileCopyDuplex;
        private Boolean isLabel;
        private int conversionTemplate;
        private int duplex;

        public int Duplex
        {
            get { return duplex; }
            set { duplex = value; }
        }



        public int ConversionTemplate
        {
            get { return conversionTemplate; }
            set { conversionTemplate = value; }
        }



        public Boolean IsLabel
        {
            get { return isLabel; }
            set { isLabel = value; }
        }



        public int FileCopyDuplex
        {
            get { return fileCopyDuplex; }
            set { fileCopyDuplex = value; }
        }

        public int Final35Narrative
        {
            get { return final35narrative; }
            set { final35narrative = value; }
        }

        public int EngrossmentDuplex
        {
            get { return engrossmentDuplex; }
            set { engrossmentDuplex = value; }
        }

        public String CustomRangeAccounts
        {
            get { return customRangeAccounts; }
            set { customRangeAccounts = value; }
        }


        public Boolean Accounts
        {
            get { return accounts; }
            set { accounts = value; }
        }
       

        public String CustomRangeEngross
        {
            get { return customRangeEngross; }
            set { customRangeEngross = value; }
        }

        public Boolean Engrossment
        {
            get { return engrossment; }
            set { engrossment = value; }
        }

        public Boolean LetterHead
        {
            get { return letterHead; }
            set { letterHead = value; }
        }
        

        public String CustomRange
        {
            get { return customRange; }
            set { customRange = value; }
        }

        public Boolean RetainWatermark
        {
            get { return retainWatermark; }
            set { retainWatermark = value; }
        }

        public Boolean WaterMark
        {
            get { return waterMark; }
            set { waterMark = value; }
        }

        public Boolean FilePrint
        {
            get { return filePrint; }
            set { filePrint = value; }
        }

        public int PrintRange
        {
            get { return _PrintRange; }
            set { _PrintRange = value; }
        }

        public int Lang
        {
            get { return _lang; }
            set { _lang = value; }
        }
        public int Office
        {
            get { return _office; }
            set { _office = value; }
        }


       
        public clsRibbonOptions()
        {
            _office = 0;
            _lang = 0;
            _PrintRange = 0;
            filePrint = true;
            waterMark = false;
            retainWatermark = false;
            letterHead = false;
            engrossment = false;
            accounts = false;
            conversionTemplate = 0;
            
        }

        public void GetDuplex()
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            this.duplex = ds.GetPrinterDuplex(Globals.ThisAddIn.Application.ActivePrinter, out oErr);
        }



    }
}
