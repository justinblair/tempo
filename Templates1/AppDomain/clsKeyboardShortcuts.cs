﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using stdole;

namespace Templates1
{
    /// <summary>
    /// An interface containing the method stubs of the MacroMessages class.  Used for shortcuts in Word which are stored in a template file.
    /// </summary>
    [ComVisible(true)]
    [Guid("B523844E-1A41-4118-A0F0-FDFA7BCD77C9")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IMacroMessages
    {
        void ApplyStyle(string styleName);
        void Indent(Word.Range Sel, Word.Document doc, int direction);
        void KeepWithNext(object Sel);
    }

    /// <summary>Implementation of the COM-interface</summary>
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    public class MacroMessages : StandardOleMarshalObject, IMacroMessages
    {
        /// <summary>
        /// Applies a style to the selected text.
        /// </summary>
        /// <param name="styleName">The name of the style</param>
        public void ApplyStyle(string styleName)
        {
            try
            {
                Word.Style stl = Globals.ThisAddIn.Application.ActiveDocument.Styles[styleName];
                Globals.ThisAddIn.Application.Selection.Range.set_Style(stl);
            }
            catch { }
        }

        /// <summary>
        /// Indents or outdents the current paragraph and moves it down the list.
        /// </summary>
        /// <param name="Sel">A selected range</param>
        /// <param name="doc">The owner document</param>
        /// <param name="direction">1 for indent, -1 for outdent</param>
        public void Indent(Word.Range Sel, Word.Document doc, int direction)
        {
                    
            Word.Range rngRange = (Word.Range)Sel;
            Word.Style style = rngRange.Paragraphs[1].Range.get_Style();
            Word.Style stlBullet = (Word.Style)rngRange.ListStyle;
            String newstyle = style.NameLocal.Substring(0, style.NameLocal.Length - 1) + (Convert.ToInt16(style.NameLocal.Substring(style.NameLocal.Length - 1)) + direction);
            style = doc.Styles[newstyle];
            foreach (Word.Paragraph para in rngRange.Paragraphs)
            {
                para.Range.set_Style(style);
                if (stlBullet.NameLocal != "No List")
                    para.Range.set_Style(Globals.ThisAddIn.Application.ActiveDocument.Styles[stlBullet]);
            }
        }

        /// <summary>
        /// Toggles Keep with Next to the selected paragraph.  If more than one paragraph is selected only the first will be updated.
        /// </summary>
        /// <param name="Sel">A selected range</param>
        public void KeepWithNext(object Sel)
        {
            Word.Range sel = (Word.Range)Sel;
            if (sel.Paragraphs[1].KeepWithNext == -1)
                sel.Paragraphs[1].KeepWithNext = 0;
            else
                sel.Paragraphs[1].KeepWithNext = -1;
        }
    }
    
}
