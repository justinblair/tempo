﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Diagnostics;

using InterAction;

namespace Templates1
{
    /// <summary>
    /// Wrapper for the Word Application object
    /// </summary>
    
    public class clsWordApp
    {
        /// <summary>
        /// Properties
        /// </summary>
        
        private Word.Application host;
        private Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        private Boolean bNoEvents;
        private Word.Document doc;
        
        private Documents documents;
        private Document lastDocument;
        private Document currentDocument;
        private Printer lastPrinter;
        private PrintJob pj;
        private PrintMonitor mPrintMonitor;
        private System.Timers.Timer tTimer;
        private Printer printerForReset;
        private Microsoft.Office.Tools.Word.Document activeDocument;

        public Document CurrentDocument
        {
            get { return currentDocument; }
            set { currentDocument = value; }
        }
        

        public Document LastDocument
        {
            get { return lastDocument; }
            set { lastDocument = value; }
        }
        

        public Printer LastPrinter
        {
            get { return lastPrinter; }
            set { lastPrinter = value; }
        }

        public Documents Documents
        {
            get { return documents; }
            set { documents = value; }
        }

        public Word.Application Application
        {
            get { return host; }
            set { host = value; }
        }

        public Word.Document WordDocument
        {
            get { return host.ActiveDocument; }
        }

        public Boolean InContactTable
        {
            get
            {
                Word.Table tbl = null;
                try
                {  tbl = host.Selection.Tables[1]; }
                catch { return false;}
                if (tbl != null)
                    if (getContentControl("name", true) != null)
                        return true;
                    else
                        return false;
                else
                    return false;
            }
        }

        /// <summary>
        /// Custom events
        /// </summary>
        public delegate void RogueStyleEventHandler();
        public event RogueStyleEventHandler RogueStylesDetected;
        public delegate void NewWordDocumentEventHandler(Word.Document doc);
        public event NewWordDocumentEventHandler NewDocumentCreated;
        public delegate void AfterPrintEventHandler(Printer printer, Document doc, PrintJob printJob);
        public event AfterPrintEventHandler AfterPrint;
        public delegate void AfterSaveEventHandler(Word.Document doc);
        public event AfterSaveEventHandler AfterSave;
        public delegate void ErrorLoggedEventHandler(Exception e);
        public event ErrorLoggedEventHandler ErrorLogged;
        public delegate void RecentDocumentsAddedEventHandler(RecentDocument rd);
        public event RecentDocumentsAddedEventHandler RecentDocumentAdded;
             
        /// <summary>
        /// Consdtructor called by Globals.ThisAddin
        /// </summary>
        /// <param name="Host"></param>
        
        public clsWordApp(Word.Application Host)
        {
            host = Host;
            documents = new Documents(host);
            
            
        }
        /// <summary>
        /// Event setup
        /// </summary>
        public void CreateEvents()
        {
            host.DocumentChange += new Word.ApplicationEvents4_DocumentChangeEventHandler(WordDocument_Change);
            host.DocumentOpen += new Word.ApplicationEvents4_DocumentOpenEventHandler(WordDocument_Open);
            host.WindowSelectionChange += new Word.ApplicationEvents4_WindowSelectionChangeEventHandler(host_WindowSelectionChange);
            host.DocumentBeforeSave += new Word.ApplicationEvents4_DocumentBeforeSaveEventHandler(host_DocumentBeforeSave);
            host.DocumentBeforeClose += new Word.ApplicationEvents4_DocumentBeforeCloseEventHandler(host_DocumentBeforeClose);
            FullTemplateSuite fs = new FullTemplateSuite();
            if(fs.LoadFullTemplateSuite)
                host.DocumentBeforePrint += new Word.ApplicationEvents4_DocumentBeforePrintEventHandler(host_DocumentBeforePrint);
        }

        /// <summary>
        /// Triggered by the Document Before CLose event
        /// </summary>
        /// <param name="Doc">The document being closed</param>
        /// <param name="Cancel">True if the operation is cancelled</param>
        void host_DocumentBeforeClose(Word.Document Doc, ref bool Cancel)
        {
            documents.Remove(Doc);
            if(!Cancel)
            System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                host_DocumentAfterClose()),
                System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
        }

        /// <summary>
        /// Called by the Document before close on the same thread but after the document has closed.
        /// </summary>
        void host_DocumentAfterClose()
        {
            Globals.ThisAddIn.RefreshRibbon();
        }
        /// <summary>
        /// Triggered by the Document Before Save event
        /// </summary>
        /// <param name="Doc">The document being saved</param>
        /// <param name="SaveAsUI">True if the save as dialog is to be shown</param>
        /// <param name="Cancel">True if the save is cancelled</param>
        void host_DocumentBeforeSave(Word.Document Doc, ref bool SaveAsUI, ref bool Cancel)
        {
            System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                host_DocumentAfterSave(Doc)),
                System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
        }

        /// <summary>
        /// Caled by the Document Before Save routine.  Operates on the same thread but after the document is saved
        /// </summary>
        /// <param name="doc"></param>
        void host_DocumentAfterSave(Word.Document doc)
        {
            // Call to the AfterSave custom event
            this.AfterSave(doc);
        }
        /// <summary>
        /// Called by the DocumentAfterSave routine
        /// </summary>
        /// <param name="doc">The Word document that has been saved</param>
        public void wordApp_AfterSave(Word.Document doc)
        {
            try
            {
                // Updates the FileSite reference number in places like the footer
                clsDynamicInserts.UpdateDocumentReference(doc);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        /// <summary>
        /// Triggered by the document before print event
        /// </summary>
        /// <param name="Doc">The document to be printed</param>
        /// <param name="Cancel">True if the print operation is cancelled</param>
        void host_DocumentBeforePrint(Word.Document Doc, ref bool Cancel)
        {
            Forms.frmProgress frm = new Forms.frmProgress();
            try
            {
                if (bNoEvents)
                    return;
                // Progress bar
                
                frm.Show();
                bNoEvents = true;
                frm.SetProgBar("Connecting to printer", 10);
                ///host.ActivePrinter is the printer selected in the Print tab on the backstage
                Printer printer = new Printer(host.ActivePrinter);
                clsRibbonOptions ribbonOpts = Globals.ThisAddIn.Ribbonopts;
                Document ActiveDocument = this.documents.ActiveDocument(false);
                this.pj = new PrintJob();
                mPrintMonitor = new PrintMonitor(ref pj);
                frm.SetProgBar("Saving current state", 20);
                //Load the starting trays into memory so that they can be reset afterwards
                ActiveDocument.StartTrays = pj.GetTrays(Doc);

                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                   PrintOut(printer,ActiveDocument,this.pj, Doc, frm)),
                   System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);


                if (ribbonOpts.IsLabel)
                    Cancel = true;
                

                if (!ribbonOpts.FilePrint)
                    //The prining of plain documents is handled by Word itself.   If a plain copy isn't required then the print job is cancelled
                    Cancel = true;
                else
                {
                    if (!Cancel)
                    {
                        frm.SetProgBar("Preparing File copy", 20);
                        pj = new FileCopy(printer, ActiveDocument);
                        //Initialise the JobPrinted custom event
                        this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                        pj.Jobtype = "File copy";
                        pj.papertype = "Plain";
                        DuplexSettings ds = new DuplexSettings();
                        String oErr;
                        ds.SetPrinterDuplex(printer.Name, pj.Duplex, out oErr);
                        pj.selectedTray = pj.MatchTray();
                        if (pj.selectedTray != null)
                        {
                            pj.SetTray(host.ActiveDocument, pj.selectedTray);
                            
                            //pj.PrintOut(); //a dummy method to call the JobPrinted event and write a history to the DMS.
                        }
                        else
                        {
                            // Launches a dialog with a tray selection combo so the user can decide which tray to print to.
                            
                            pj.FallBackPrintTray();
                            if (pj.JobCancelled)
                                Cancel = true;
                        }

                    }
                }
                
                
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                   host_DocumentAfterPrint(printer, ActiveDocument, this.pj, frm)),
                   System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
                
            }
            catch (Exception e) {
                frm.Close();
                this.ErrorLogged(e); }
        }

        private void PrintOut(Printer printer, Document ActiveDocument, PrintJob pj, Word.Document Doc, Forms.frmProgress frm)
        {
            clsRibbonOptions ribbonOpts = Globals.ThisAddIn.Ribbonopts;
            mPrintMonitor = new PrintMonitor(ref pj);
            if (ribbonOpts.LetterHead)
            {
                this.pj = new Letterhead(printer, ActiveDocument, ribbonOpts.CustomRange);
                //Initialise the JobPrinted custom event so that it adds an event to FileSite after the document has printed
                this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                frm.SetProgBar("Printing letterhead", 30);
                this.pj.PrintOut(Doc);
            }
            if (ribbonOpts.Engrossment)
            {
                pj = new Engrossment(printer, ActiveDocument, ribbonOpts.CustomRangeEngross);
                //Initialise the JobPrinted custom event
                this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                frm.SetProgBar("Printing engrossment", 40);
                pj.PrintOut(Doc);
            }

            if (ribbonOpts.Accounts)
            {
                if (Globals.ThisAddIn.Officedata.IsNYOffice)
                {
                    pj = new Letterhead(printer, ActiveDocument, String.Empty);
                    //Initialise the JobPrinted custom event
                    this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                    frm.SetProgBar("Printing bill as letterhead", 50);
                    pj.PrintOut(Doc);
                }
                else
                {
                    FinalThreeFive threefive;
                    switch (ribbonOpts.Final35Narrative)
                    {
                        case 0:
                            frm.SetProgBar("Printing Final Three", 50);
                            threefive = new FinalThreeFive(printer, ActiveDocument, ribbonOpts.CustomRangeAccounts, false);
                            //Printing of a FinalThreeFive document in handled in the constructor of said class
                            break;
                        case 1:
                            frm.SetProgBar("Printing Final Five", 60);
                            threefive = new FinalThreeFive(printer, ActiveDocument, ribbonOpts.CustomRangeAccounts, true);
                            break;
                        case 2:
                            pj = new LetterheadWithNotes(printer, ActiveDocument, "");
                            //Initialise the JobPrinted custom event
                            this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                            frm.SetProgBar("Printing Letterhead with Notes", 70);
                            pj.PrintOut(Doc);
                            break;

                    }
                }


            }

            if (ribbonOpts.IsLabel)
            {
                pj = new Labels(printer, ActiveDocument);
                //Initialise the JobPrinted custom event
                this.pj.JobPrinted += new PrintJob.PrintJobEventHandler(Globals.ThisAddIn.DMS.templates_DocumentAfterPrint);
                
                pj.PrintOut();
            }


        }

       /// <summary>
       /// Called after the last document has printed
       /// </summary>
       /// <param name="printer">The printer which was printed to</param>
       /// <param name="doc">THe document that was printed</param>
        /// <param name="printJob">The print job base class itself</param>
        void host_DocumentAfterPrint(Printer printer, Document doc, PrintJob printJob, Forms.frmProgress frm)
        {
            frm.SetProgBar("Cleaning up", 90);
            if (Globals.ThisAddIn.Ribbonopts.FilePrint)
                printJob.PrintOut(); //a dummy method to call the JobPrinted event and write a history to the DMS.
            this.lastPrinter = printer;
            this.lastDocument = doc;
            this.AfterPrint(printer, doc, printJob);
            frm.Close();
        }

        /// <summary>
        /// Triggered by the AfterPrint event from Globals.ThisAddin
        /// </summary>
        /// <param name="printer">the printer which was printed to</param>
        /// <param name="doc">the document that was printed</param>
        public void wordApp_AfterPrint(Printer printer, Document doc, PrintJob printJob)
       {
           try
           {
               bNoEvents = false;
               // Remove the watermark if required
               if (!Globals.ThisAddIn.Ribbonopts.RetainWatermark)
               {
                   if (doc.HasWatermark())
                   {
                       doc.Watermark.Remove();
                       doc.Watermark = null;
                   }
               }

               foreach (PrintTray tray in doc.StartTrays)
               {
                  //Resets the trays as they were before printing
                   if (tray.IsFirstPageTray)
                       doc.WordDocument.Sections[tray.Section].PageSetup.FirstPageTray = (Word.WdPaperTray)tray.BinID;
                   else
                       doc.WordDocument.Sections[tray.Section].PageSetup.OtherPagesTray = (Word.WdPaperTray)tray.BinID;
               }
               //Resets the printer's duplex settings
               tTimer = new System.Timers.Timer(5000);
               printerForReset = printer;
               tTimer.Elapsed += PrinterResetDuplex;
               tTimer.Enabled = true;

               //printer.ResetDuplex();
           }
           catch (Exception e) { this.ErrorLogged(e); }
       }

        private void PrinterResetDuplex(Object source, System.Timers.ElapsedEventArgs e)
        {
            System.Timers.Timer pt = (System.Timers.Timer)source;
            pt.Enabled = false;
            printerForReset.ResetDuplex();
        }

        /// <summary>
        /// Triggered when the mouse is moved and a range in the document selected
        /// </summary>
        /// <param name="Sel"></param>
        void host_WindowSelectionChange(Word.Selection Sel)
        {
            try
            {
                try
                {
                    if (GetTemplateName(Sel.Document).ToLower().Contains("mdrlegal.dotx") || GetTemplateName(Sel.Document).ToLower().Contains("mdrarticles"))
                    {
                        CustomTaskPanes.ctpLegal ctp = GetActiveControl() as CustomTaskPanes.ctpLegal;
                        if (ctp != null)
                            ctp.EnableControls();

                    }
                
                

                    if (GetTemplateName(Sel.Document).ToLower().Contains("mdrcourt"))
                    {
                        CustomTaskPanes.ctpCourt ctp = GetActiveControl() as CustomTaskPanes.ctpCourt;
                        if (ctp != null)
                            ctp.EnableControls();

                    }
                }
                catch { }
            }
            catch (Exception e) { this.ErrorLogged(e); }
            try
            {
                Document document = this.documents.ActiveDocument(true);
                if (document!=null)
                if (document.Styles.RoguesExist(document))
                    this.RogueStylesDetected();
            }
            catch (Exception e) { this.ErrorLogged(e); }
            //    System.Windows.Forms.MessageBox.Show("You appear to be importing rogue styles from an external source." + Environment.NewLine +
            //        "Use Undo or Ctrl + Z until the rogue styles are removed and then paste text as text.", "MdR Templates");
        }

        /// <summary>
        /// Adds a document to the Recently used list
        /// </summary>
        /// <param name="doc"></param>
        private void AddRecentDocument(Document doc)
        {
            RecentDocument rd = new RecentDocument(doc);
            Boolean match = false;
            foreach(RecentDocument r in Globals.ThisAddIn.PersonalSettings.RecentDocs)
                if(rd.Number == r.Number && rd.Version == r.Version)
                {
                    match = true;
                    break;
                }
            if (match == false)
            { 
                Globals.ThisAddIn.PersonalSettings.RecentDocs.AddItem(rd);
                Globals.ThisAddIn.PersonalSettings.Save();
                this.RecentDocumentAdded(rd);
            }
        }

       /// <summary>
       /// Triggered when a document is opened
       /// </summary>
       /// <param name="doc">The Word document that was opened</param>
         
        public void WordDocument_Open(Word.Document doc)
        {
                       
            
            try
            {
                //If active window isn't active and invisible then it's a FileSite document being processed from Outlook
                if (!doc.ActiveWindow.Active && !doc.ActiveWindow.Visible)
                    return;
                // Don't want routines to run if I'm developing the template files themselves
                if (doc.FullName.ToLower().Contains(".dotx") || doc.FullName.ToLower().Contains(".dotn"))
                    return;
                
                
                Document d = new Document(doc);
                documents.Add(d);
                AddRecentDocument(d);
                #region Taskpane Generators
                if (GetTemplateName(doc).ToLower().Contains("mdrbillus"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpUSBill billUS = GenerateUSBillPane();
                        billUS.RepopulateUSBillPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrlabel"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpLabels labels = GenerateLabelsPane();
                        //labels.RepopulateUSBillPane();
                        goto ending;
                    }
                }


                if (GetTemplateName(doc).ToLower().Contains("mdrlegalus"))
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl("type", false);
                    CustomTaskPanes.ctpBlueBack usu;
                    if (ctl != null)
                    {
                        if (ctl.Range.Text.ToLower().Contains("affidavit of service"))
                            usu = GenerateBlueBackPane("Affidavit of Service");
                        else if (ctl.Range.Text.ToLower().Contains("affirmation of service"))
                            usu = GenerateBlueBackPane("Affirmation of Service");
                        else
                            usu = GenerateBlueBackPane("Blue Back");
                        usu.RepopulateBlueBack();
                    }
                    CustomTaskPanes.ctpLegal lgl = GenerateLegalPane();
                    goto ending;
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrletterus"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpUSLetter letterUS = GenerateUSLetterPane();
                        letterUS.RepopulateLetterUSTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrpleadingus"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpPleadingUS pleadingUS = GenerateUSPleadingPane();
                        pleadingUS.RepopulatePleadingUSTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrfaxus"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpUSFax faxUS = GenerateUSFaxPane();
                        faxUS.RepopulateFaxUSTaskPane();
                        goto ending;
                    }
                }



                if (GetTemplateName(doc).ToLower().Contains("mdrletter"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        ctpLetter letter = GenerateLetterPane();
                        letter.RepopulateLetterTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrfax"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpFax fax = GenerateFaxPane();
                        fax.RepopulateFaxTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdragenda"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpAgenda agenda = GenerateAgendaPane();
                        agenda.RepopulateAgendaTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrattendance"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpAttendance att = GenerateAtendancePane();
                        att.RepopulateAgendaTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrmemorandumus"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpMemoUS memo = GenerateMemoUSPane();
                        memo.RepopulateMemoUSPane();

                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrmemo"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpMemo memo = GenerateMemoPane();
                        memo.RepopulateMemoTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrlegal.dotx") || GetTemplateName(doc).ToLower().Contains("mdrarticles"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpLegal legal = GenerateLegalPane();
                        legal.RepopulateLegalTaskPane();
                        legal.RepopulateTOC();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrcourt"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpCourt court = GenerateCourtPane();
                        court.RepopulateCourtPane();
                        court.EnableControls();
                        goto ending;
                    }
                }

                

                if (GetTemplateName(doc).ToLower().Contains("mdrinvoice"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpInvoice invoice = GenerateInvoicePane();
                        invoice.RepopulateInvoiceTaskPane();
                        goto ending;
                        //invoice.EnableControls();
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrproforma"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpProforma proforma = GenerateProformaPane();
                        proforma.RepopulateProformaTaskPane();
                        goto ending;
                    }
                }

                if (GetTemplateName(doc).ToLower().Contains("mdrnonlegal"))
                {
                    if (WindowHasTaskPane() == false)
                    {
                        CustomTaskPanes.ctpNonLegal nonlegal = GenerateNonLegalPane();
                        nonlegal.RepopulateNonLegalTaskPane();
                        goto ending;
                    }
                }
                #endregion
            ending:
                RemoveOrphanedTaskPanes();
                //Ensure that the office and language combos on the ribbon are in sync with the class that saves the information
                Globals.ThisAddIn.Ribbonopts.Office = d.Office.Index;
                Globals.ThisAddIn.Ribbonopts.Lang = d.Language.Index;
                Globals.ThisAddIn.RefreshRibbon();
                Globals.ThisAddIn.SetOptions();
                try
                {
                    clsDynamicInserts.UpdateDocumentReference(doc);
                }
                catch { }
                doc.ContentControlOnEnter += new Word.DocumentEvents2_ContentControlOnEnterEventHandler(WordDocument_ContentControlOnEnter);
                doc.BuildingBlockInsert += new Word.DocumentEvents2_BuildingBlockInsertEventHandler(doc_BuildingBlockInsert);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        /// <summary>
        /// Triggered when a building block is inserted
        /// </summary>
        /// <param name="Range">The range of the building block</param>
        /// <param name="Name">THe name odf the building block</param>
        /// <param name="Category">The category of the building block</param>
        /// <param name="BlockType">The type of building block it is</param>
        /// <param name="Template">THe filepath of the template being used</param>
        
        void doc_BuildingBlockInsert(Word.Range Range, string Name, string Category, string BlockType, string Template)
        {
            
            try
            {
                Word.Document doc = Range.Parent;
                
                if (!doc.FullName.ToLower().Contains(".dot"))
                    switch (Name)
                    {
                        case "Legal cover sheet":  //Legal cover sheet
                            clsDynamicInserts.OnBuildingBlockLegalCover(doc);
                            CustomTaskPanes.ctpLegal ctp = GetActiveControl() as CustomTaskPanes.ctpLegal;
                            ctp.RepopulateLegalTaskPane();
                            break;

                        case "House style": //TOC.  No longer used.  THe TOC is now generated programmatically as it kept corrupting as a building block
                            CustomTaskPanes.ctpLegal ctp2 = GetActiveControl() as CustomTaskPanes.ctpLegal;
                            ctp2.RepopulateLegalTaskPane();
                            ctp2.RepopulateTOC();
                            break;

                        case "Court back sheet":
                            clsDynamicInserts.OnBuildingBlockCourtCover(doc, Range);
                            break;

                        case "Back page":
                            // clsDynamicInserts.DropAddressBlock(Globals.ThisAddIn.Officedata.ActiveOffice, Globals.ThisAddIn.Officedata.Officedata);
                            break;

                        case "Blue Back":
                            if(GetTemplateName(doc).Contains("mdrLegalUS"))
                            { 
                                doc.Sections[1].PageSetup.DifferentFirstPageHeaderFooter = 0;
                                CustomTaskPanes.ctpBlueBack usu = GenerateBlueBackPane("Blue Back");
                                clsDynamicInserts.DropAddressBlock();
                            }
                            break;

                        case "Affidavit of Service":
                            CustomTaskPanes.ctpBlueBack afds = GenerateBlueBackPane("Affidavit of Service");
                            afds.SetAffidavit();
                            break;

                        case "Affirmation of Service":
                            CustomTaskPanes.ctpBlueBack affs = GenerateBlueBackPane("Affirmation of Service");
                            affs.SetAffirmationOfService();
                            Word.Section sect = Globals.ThisAddIn.Application.ActiveDocument.Sections[Range.Sections[1].Index + 1];
                            sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                            sect.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                            break;

                        case "Certificate of Service":
                            Word.Section section = Globals.ThisAddIn.Application.ActiveDocument.Sections[Range.Sections[1].Index + 1];
                            section.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                            section.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].LinkToPrevious = false;
                            break;

                        case "Tombstones two across":
                            clsBuildingBlocks bb = new clsBuildingBlocks();
                            try
                            {
                                bb.NormaliseTombstones(Range);
                            }
                            catch { }
                            break;

                        case "Tombstones three across":
                            clsBuildingBlocks bb2 = new clsBuildingBlocks();
                            try
                            {
                                bb2.NormaliseTombstones(Range);
                            }
                            catch { }
                            break;
                    }

                if (Category.ToLower() == "_mdr" && (Name != "Legal cover sheet" && Name != "Blue Back") && BlockType == "Cover Pages" && !GetTemplateName(doc).Contains("NonLegal"))
                    clsDynamicInserts.OnBuildingBlockNonLegalCover(doc, Range);

            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        /// <summary>
        /// Triggered when a user changes document windows
        /// </summary>
        private void WordDocument_Change()
        {
            System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new System.Action(() =>
               PostDocumentChange()),
               System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
            
        }

        /// <summary>
        /// Runs after the change event once documents have been opened or closed
        /// and the documents.count property is returning the correct value
        /// </summary>

        private void PostDocumentChange()
        {
            try
            {
                if (host.Documents.Count > 0)
                    activeDocument = Globals.Factory.GetVstoObject(this.Application.ActiveDocument);
                if (activeDocument != null)
                    activeDocument.WindowSize += new Microsoft.Office.Tools.Word.WindowEventHandler(activeDocument_WindowSize);
            }
            catch { return; }
            try
            {
                this.lastDocument = this.currentDocument;
                RemoveOrphanedTaskPanes();
                //Globals.ThisAddIn.RefreshRibbon();
                //The identifies if a new document has been created from the Word UI or programmatcially
                if (Globals.ThisAddIn.DMS.CancelPressed)
                {
                    Globals.ThisAddIn.DMS.CancelPressed = false;
                    return;
                }
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                {
                    try
                    {
                        Word.Document doc = host.ActiveDocument;
                    }
                    catch { }
                    try
                    {
                        // Try block required because saving an attachment as a new version causes the document to close (FileSite) part way through this routine.

                        // if (Globals.ThisAddIn.DMS.IsOnline)
                        //if (!Globals.ThisAddIn.DMS.IsProfiled(doc) && !doc.FullName.Contains(@"\"))
                        // this.NewDocumentCreated(doc);
                        Globals.ThisAddIn.SetOptions();
                    }
                    catch { }
                    try
                    {
                        String s = doc.FullName;  // will return an error if document has been closed as part of DMS File Save As Dialog: Cancel pressed.
                        if (this.documents.Count == 0 && host.Documents.Count > 0)
                            this.documents.Add(new Document(doc));
                    }
                    catch { }
                    if (host.Documents.Count > 0)
                        this.currentDocument = this.documents.ActiveDocument(false);
                }
                //Globals.ThisAddIn.RefreshRibbon();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        private void activeDocument_WindowSize(object sender, Microsoft.Office.Tools.Word.WindowEventArgs e)
        {
            CustomTaskPanes.clsHelper cp = new CustomTaskPanes.clsHelper();
            Microsoft.Office.Tools.CustomTaskPane ctp = clsWordApp.GetActivePane();
            if(ctp!=null)
                cp.ModifyForScreenResolution(ctp, 1);
        }
        
        /// <summary>
        /// Launches the contacts search dialog box
        /// </summary>
        public void LaunchSearchDialog()
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }
        
        /// <summary>
        /// Triggered when the mouse is placed into a content control
        /// </summary>
        /// <param name="ctl"></param>
        private void WordDocument_ContentControlOnEnter(Word.ContentControl ctl)
        {
            try
            {
                switch (ctl.Tag)
                {
                    case "name":

                        if (MessageBox.Show("Do you wish to search for a contact?", Globals.ThisAddIn.APP_NAME1, MessageBoxButtons.YesNo) == DialogResult.Yes)
                            LaunchSearchDialog();
                        break;
                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        /// <summary>
        /// The closing event of the search contacts dialog box so as to keep the ADUser(s) in memory
        /// </summary>
        /// <param name="sender">The search contacts dialog box</param>
        /// <param name="e">Form closing event handler</param>
 
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
                if (frm.user != null)
                    clsDynamicInserts.CreateCVContact(frm.user);
            }
            catch (Exception ex) { this.ErrorLogged(ex); }
        }
        
        /// <summary>
        /// Creats a new document based on the MdR Templates
        /// </summary>
        /// <param name="template">The document type with its template's filepath</param>
        /// <param name="language">The language selected in the language dropdown on the riibon</param>
        /// <param name="office">The office selected in the office dropdown on the riibon</param>
        /// <param name="launchPane">True if the document's taskpane is to be shown</param>
        /// <returns>A document object representing the created document</returns>
        public Document CreateDocument (Template template, Language language, XmlOffice office, Boolean launchPane)
        {
            try
            {
                try
                {
                    doc = host.Documents.Add(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.TEMPLATES, template.Filename));
                }
                catch { }
                try
                {
                    if (office.Logo != null)
                        clsDynamicInserts.SetLogo(doc, office.Logo);
                }
                catch { }
                if(!Globals.ThisAddIn.DMS.IsProfiled(doc))
                    this.NewDocumentCreated(doc); //Profiles the document
                if (Globals.ThisAddIn.DMS.CancelPressed)
                {
                    doc.Saved = true;
                    doc.Close(false);
                    Globals.ThisAddIn.DMS.CancelPressed = false;

                    return null;
                }

                Document wordAppDoc = new Document(doc, template, language, office);
                 if(Globals.ThisAddIn.DMS.IsProfiled((Object)doc)==false)
                    Globals.ThisAddIn.DMS.CreateNewProfileFromDoc((Object)doc);
                
                if (launchPane)
                    LaunchTaskPane(doc, template);
                if (wordAppDoc != null)
                {
                    documents.Add(wordAppDoc);
                    AddRecentDocument(wordAppDoc);
                }
                clsDynamicInserts.DropAddressBlock();
                //These two EventSchemaTraceListener are set at the document level.  The Word.Document object is stored with Document in this.documents
                doc.ContentControlOnEnter += new Word.DocumentEvents2_ContentControlOnEnterEventHandler(WordDocument_ContentControlOnEnter);
                doc.BuildingBlockInsert += new Word.DocumentEvents2_BuildingBlockInsertEventHandler(doc_BuildingBlockInsert);

                clsDynamicInserts.UpdateDocumentReference(doc);
                doc.Saved = false;
                doc.Save();
                return wordAppDoc;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return null;
            
        }



   /// <summary>
   /// Launches the correct task pane for the document specified
   /// </summary>
   /// <param name="doc">THe Word Document which requires a taskpane</param>
   public UserControl LaunchTaskPane(Word.Document doc, Template template)
        #region Taskpane Generators
        {
    try
    {
        
        if (doc != null)
        {
            //return GenerateGenericPane(template, doc);


            if (GetTemplateName(doc).ToLower().Contains("mdrlabel"))
            {
                CustomTaskPanes.ctpLabels usu = GenerateLabelsPane();
                //usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrbillus"))
            {
                CustomTaskPanes.ctpUSBill usu = GenerateUSBillPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrletterus"))
            {
                CustomTaskPanes.ctpUSLetter usu = GenerateUSLetterPane();
                usu.Fetch();
                return usu;
                
            }

            if(GetTemplateName(doc).ToLower().Contains("mdrlegalus"))
            { 
                CustomTaskPanes.ctpLegal usu = GenerateLegalPane();
                return usu;
                
            }
            
            if (GetTemplateName(doc).ToLower().Contains("mdrpleadingus"))
            {
                CustomTaskPanes.ctpPleadingUS usu = GenerateUSPleadingPane();
                //usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrfaxus"))
            {
                CustomTaskPanes.ctpUSFax usu = GenerateUSFaxPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrletter"))
            {
                ctpLetter usu = GenerateLetterPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdragenda"))
            {
                CustomTaskPanes.ctpAgenda usu = GenerateAgendaPane();
                //usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrfax"))
            {
                CustomTaskPanes.ctpFax usu = GenerateFaxPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrmemorandumus"))
            {
                CustomTaskPanes.ctpMemoUS usu = GenerateMemoUSPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrmemo"))
            {
                CustomTaskPanes.ctpMemo usu = GenerateMemoPane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrattendance"))
            {
                CustomTaskPanes.ctpAttendance usu = GenerateAtendancePane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrlegal.dotx") || GetTemplateName(doc).ToLower().Contains("mdrarticles"))
            {
                CustomTaskPanes.ctpLegal usu = GenerateLegalPane();
                //usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrcourt"))
            {
                CustomTaskPanes.ctpCourt usu = GenerateCourtPane();
                //usu.Fetch();
                return usu;
                
            }

            

            if (GetTemplateName(doc).ToLower().Contains("mdrinvoice"))
            {
                CustomTaskPanes.ctpInvoice usu = GenerateInvoicePane();
                usu.Fetch();
                return usu;
                
            }

            if (GetTemplateName(doc).ToLower().Contains("mdrproforma"))
            {
                CustomTaskPanes.ctpProforma usu = GenerateProformaPane();
                usu.Fetch();
                return usu;
                
            }


            if (GetTemplateName(doc).ToLower().Contains("mdrnonlegal"))
            {
                CustomTaskPanes.ctpNonLegal usu = GenerateNonLegalPane();
                //usu.Fetch();
                return usu;
                
            }

        }
        #endregion
    
        return null;
    }
    catch (Exception e) { this.ErrorLogged(e);
    return null;
    }
}

       /// <summary>
       /// Gets the Word template of a document
       /// </summary>
       /// <param name="doc">The document being interrogated</param>
       /// <returns></returns>
        public static String GetTemplateName(Word.Document doc)
        {
            Word.Template strAttTemp = doc.get_AttachedTemplate();
            return strAttTemp.FullName;
        }
        
        /// <summary>
        /// Adds the custom taskpane to the window.  There is a method for each taskpane.
        /// </summary>
        /// <returns></returns>
        #region Taskpane Generators
        public ctpLetter GenerateLetterPane()
        {
            
            if (!WindowHasTaskPane())
            {
                ctpLetter UserControl1 = new ctpLetter();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(UserControl1, "Letter");
             return UserControl1;
            }
            return null;
        }

        public CustomTaskPanes.ctpAgenda GenerateAgendaPane()
        {
           
            if (!WindowHasTaskPane())
            {
                CustomTaskPanes.ctpAgenda UserControl1 = new CustomTaskPanes.ctpAgenda();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(UserControl1, "Agenda/Minutes");
                return UserControl1;
            }

            return null;

        }

        public CustomTaskPanes.ctpMemo GenerateMemoPane()
        {
            
            if (!WindowHasTaskPane())
            {
                CustomTaskPanes.ctpMemo UserControl1 = new CustomTaskPanes.ctpMemo();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(UserControl1, "Memorandum");
                return UserControl1;
            }
            return null;


        }

        public CustomTaskPanes.ctpAttendance GenerateAtendancePane()
        {
            
            if (!WindowHasTaskPane())
            {
                CustomTaskPanes.ctpAttendance UserControl1 = new CustomTaskPanes.ctpAttendance();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(UserControl1, "Attendance Note");
                return UserControl1;
            }
            return null;


        }

        public CustomTaskPanes.ctpFax GenerateFaxPane()
        {
            CustomTaskPanes.ctpFax ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpFax();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Fax");
                
                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpLegal GenerateLegalPane()
        {
            CustomTaskPanes.ctpLegal ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpLegal();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Legal/Articles of Association");
                                
                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpCourt GenerateCourtPane()
        {
            CustomTaskPanes.ctpCourt ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpCourt();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Court");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpMemoUS GenerateMemoUSPane()
        {
            CustomTaskPanes.ctpMemoUS ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpMemoUS();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "US Memo");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpInvoice GenerateInvoicePane()
        {
            CustomTaskPanes.ctpInvoice ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpInvoice();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Invoice");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpProforma GenerateProformaPane()
        {
            CustomTaskPanes.ctpProforma ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpProforma();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Pro Forma");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpUSBill GenerateUSBillPane()
        {
            CustomTaskPanes.ctpUSBill ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpUSBill();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "US Bill");

                return ctp;
            }
            return null;


        }

        public UserControl GenerateGenericPane(Template template, Word.Document doc)
        {
            return LaunchTaskPane(doc, template);
            
            //if(!WindowHasTaskPane())
            //{
            //    CustomTaskPanes.clsPaneBuilder builder = new CustomTaskPanes.clsPaneBuilder(doc,template);
            //    builder.CreatePane();
            //    return builder.Control;
            //}
            //return null;
        }

        public CustomTaskPanes.ctpLabels GenerateLabelsPane()
        {
            CustomTaskPanes.ctpLabels ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpLabels();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Labels");

                return ctp;
            }
            return null;


        }
        
        
        public CustomTaskPanes.ctpUSLetter GenerateUSLetterPane()
        {
            CustomTaskPanes.ctpUSLetter ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpUSLetter();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "US Letter");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpPleadingUS GenerateUSPleadingPane()
        {
            CustomTaskPanes.ctpPleadingUS ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpPleadingUS();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "US Pleading");

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpBlueBack GenerateBlueBackPane(String text)
        {
            CustomTaskPanes.ctpBlueBack ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpBlueBack();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, text);

                return ctp;
            }
            return null;


        }

        public CustomTaskPanes.ctpUSFax GenerateUSFaxPane()
        {
            CustomTaskPanes.ctpUSFax ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpUSFax();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "US Fax");

                return ctp;
            }
            return (CustomTaskPanes.ctpUSFax)GetActiveControl();


        }

        public CustomTaskPanes.ctpNonLegal GenerateNonLegalPane()
        {
            CustomTaskPanes.ctpNonLegal ctp;
            if (!WindowHasTaskPane())
            {
                ctp = new CustomTaskPanes.ctpNonLegal();
                myCustomTaskPane = Globals.ThisAddIn.GenerateTaskpane(ctp, "Non-Legal");

                return ctp;
            }
            return null;


        }

        #endregion

        /// <summary>
        /// Checks whether or not the window has a custom taskpane
        /// </summary>
        /// <returns>True if it has</returns>
        internal Boolean WindowHasTaskPane()
        {
            foreach (Microsoft.Office.Tools.CustomTaskPane _ctp in Globals.ThisAddIn.CustomTaskPanes)
            {
                Word.Window ctpWindow = (Word.Window)_ctp.Window;
                if (ctpWindow == Globals.ThisAddIn.Application.ActiveWindow)
                    return true;
            }
            return false;
        }

        
        /// <summary>
        /// Retrieves the custom taskpane as a user control in the active window
        /// </summary>
        /// <returns><Either a user control or a taskpane for the active window/returns>
        public static UserControl GetActiveControl()
        {
            foreach (Microsoft.Office.Tools.CustomTaskPane _ctp in Globals.ThisAddIn.CustomTaskPanes)
            {
                Word.Window ctpWindow = (Word.Window)_ctp.Window;
                if (ctpWindow == Globals.ThisAddIn.Application.ActiveWindow)
                {
                    return _ctp.Control;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the task pane of the active window
        /// </summary>
        /// <returns>A custom taskpane or null</returns>
        public static Microsoft.Office.Tools.CustomTaskPane GetActivePane()
        {
            foreach (Microsoft.Office.Tools.CustomTaskPane _ctp in Globals.ThisAddIn.CustomTaskPanes)
            {
                Word.Window ctpWindow = (Word.Window)_ctp.Window;
                if (ctpWindow == Globals.ThisAddIn.Application.ActiveWindow)
                {
                    return _ctp;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Reactivates the taskpane if it has been closed.
        /// </summary>
        /// <returns></returns>
        public Boolean ShowHiddenPane()
        {
            foreach (Microsoft.Office.Tools.CustomTaskPane _ctp in Globals.ThisAddIn.CustomTaskPanes)
            {
                Word.Window ctpWindow = (Word.Window)_ctp.Window;
                if (ctpWindow == Globals.ThisAddIn.Application.ActiveWindow)
                {
                    _ctp.Visible = true;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Removes any taskpanes which haven't been disposed of properly
        /// </summary>
        private void RemoveOrphanedTaskPanes()
        {
            for (int i = Globals.ThisAddIn.CustomTaskPanes.Count; i > 0; i--)
            {
                Microsoft.Office.Tools.CustomTaskPane ctp = Globals.ThisAddIn.CustomTaskPanes[i - 1];
                if (ctp.Window == null)
                {
                    Globals.ThisAddIn.CustomTaskPanes.Remove(ctp);
                }
            }
        }

        
        /// <summary>
        /// Method to retrieve content controls by tag ID
        /// </summary>
        /// <param name="name">The tag to search for</param>
        /// <param name="doc">The Word document with the content controls</param>
        /// <returns></returns>
        public static List<Word.ContentControl> getContentControls(string name, Word.Document doc)
        {
            List<Word.ContentControl> list = new List<Word.ContentControl>();
            //foreach (Word.ContentControl ctl in Globals.ThisAddIn.Application.ActiveDocument.ContentControls)
            //    if (ctl.Tag == name)
            //    {
            //        list.Add(ctl);
            //    }
            foreach (Word.Range sect in doc.StoryRanges)
                foreach (Word.ContentControl ctl in sect.ContentControls)
                    if (ctl.Tag == name)
                    {
                        list.Add(ctl);
                    }
            return list;
        }
        
        /// <summary>
        /// Gets a single control by tag ID
        /// </summary>
        /// <param name="name">The tag ID to search for</param>
        /// <param name="activeTable">Limits the search to the active table if there is one</param>
        /// <returns>A content control</returns>
        public static Word.ContentControl getContentControl(string name, Boolean activeTable)
        {
            if (activeTable)
            {
                foreach (Word.ContentControl ctl in Globals.ThisAddIn.Application.Selection.Tables[1].Range.ContentControls)
                    if (ctl.Tag == name)
                        return ctl;
            }
            else
            {
                foreach (Word.ContentControl ctl in Globals.ThisAddIn.Application.ActiveDocument.ContentControls)
                    if (ctl.Tag == name)
                    {
                        return ctl;
                    }
                foreach(Word.Range sect in Globals.ThisAddIn.Application.ActiveDocument.StoryRanges )
                         foreach(Word.ContentControl ctl in sect.ContentControls)
                             if (ctl.Tag == name)
                             {
                                 return ctl;
                             }
            }
            return null;
        }
        /// <summary>
        /// Retrieves a content control by its reference number
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Word.ContentControl getContentControlByID(string id)
        {
            if (ContentControlExistsByID(id))
                return Globals.ThisAddIn.Application.ActiveDocument.ContentControls[id];
            else
                return null;
        }

        /// <summary>
        /// Checks to see if a particular content control exists
        /// </summary>
        /// <param name="tag"></param>
        /// <returns><True if the content control is found with a matching tag ID/returns>
        public static Boolean ContentControlExists(string tag)
        {
            return getContentControl(tag, false) != null;
        }

        /// <summary>
        /// Checks to see if a content control identified by its ID exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the conent control exists</returns>
        public static Boolean ContentControlExistsByID(string id)
        {
            try{
            return Globals.ThisAddIn.Application.ActiveDocument.ContentControls[id] != null;
            }
            catch{return false;}
        }

        
    }

    /// <summary>
    /// A list of documents
    /// </summary>
    public class Documents : List<Document>
    {

        private Word.Application application;
/// <summary>
/// Constructor
/// </summary>
/// <param name="host">Word.Application</param>

        public Documents(Word.Application host)
        {
            application = host;
        }
        
        /// <summary>
        /// Removes a document from the list
        /// </summary>
        /// <param name="doc">The Word document to be removed</param>
        public void Remove(Word.Document doc)
        {
            Word.Document document = application.ActiveDocument;
            IEnumerable<Document> documents =
                (from d in this
                 where d.FullName == document.FullName
                 select d);
            if(documents.Count<Document>()>0)
            this.Remove(documents.ToList()[0]);
        }
        
        
        /// <summary>
        /// The active document as a Document object.  Adds a document if there are none opened.
        /// </summary>
        public Document ActiveDocument(Boolean suppressNew)
        {            
            
                Word.Document doc = null;
                if (application.Documents.Count == 0)
                    return null;
                try
                {
                    doc = application.ActiveDocument;
                }
                catch { }
                if (doc == null)
                    return null;
                IEnumerable<Document> documents =
                    (from d in this
                     where d.FullName == doc.FullName
                     select d);
                if (documents.Count<Document>() > 0)
                    return documents.ToList()[0];
                else
                {
                    if (suppressNew)
                        return null;
                    Document d = new Document(doc);
                    Globals.ThisAddIn.WordApp.Documents.Add(d);
                    return d;
                }
            
        }
    }

    /// <summary>
    /// A class to store extra information about a document
    /// </summary>
    public class Document 
    {
        /// <summary>
        /// Properties
        /// </summary>
        private Styles styles;
        private Word.Document wordDocument;
        private String fullName;
        private Template template;
        private Language language;
        private Word.Application wordapp;
        private XmlOffice office;
        private Watermark watermark;
        private PrintTrays startTrays;
        private XMLLabel label;

        internal XMLLabel Label
        {
            get { return label; }
            set { label = value; }
        }

        public PrintTrays StartTrays
        {
            get { return startTrays; }
            set { startTrays = value; }
        }

        internal Watermark Watermark
        {
            get { return watermark; }
            set { watermark = value; }
        }

        internal Boolean HasWatermark()
       
            {
                int i = 0;
            if (this.watermark != null)
                    return true;
             foreach (Word.Section sect in this.WordDocument.Sections)
                foreach (Word.HeaderFooter hdr in sect.Headers)
                    foreach (Word.Shape shp in hdr.Shapes)
                        if (shp.Name.Contains("Watermark")||shp.Rotation>270)
                        {
                            i++;
                            if (i==1)
                            {
                                this.watermark = new Watermark(this.WordDocument);
                            }
                            this.watermark.Shapes.Add(shp);
                            shp.Name = "Watermark" + i;
                            }
            
            return this.watermark != null;
            }
        

        public XmlOffice Office
        {
            get 
            
            { 
                if (office!=null)
                return office;
                String soffice = this.wordDocument.BuiltInDocumentProperties["Company"].Value;
                foreach(XmlOffice off in Globals.ThisAddIn.Officedata.Offices)
                    if(off.Shortname == soffice)
                      office = off;
                if(office==null)
                {
                    office = new XmlOffice();
                    office.Index =0;
                    office.Shortname = "Office not found";
                }
                return office;            
            }
            set 
            { 
                office = value;
                this.wordDocument.BuiltInDocumentProperties("Company").Value = office.Shortname;
            }
        }

        public Language Language
        {
            get 
            { 
                if (language!=null)
                    return language;
                foreach (Language lang in Globals.ThisAddIn.Officedata.Languages)
                    if (Convert.ToInt16(this.wordDocument.Styles[Word.WdBuiltinStyle.wdStyleNormal].LanguageID) == lang.Langid)
                        language = lang;
                if (language == null)
                {
                    language = new Language();
                    language.Langid = 0;
                    language.Name = "Language not found";
                    language.Index = 0;
                }
                return language;

            }
            set 
            { 
                language = value;
                if (wordapp.Documents.Count == 0)
                    return;
                Word.Document doc = this.wordDocument;
                foreach (Word.Style style in doc.Styles)
                {
                    try
                    {
                        style.LanguageID = (Word.WdLanguageID)language.Langid;
                    }
                    catch { }
                }
                doc.ShowSpellingErrors = true;
            }
        }

        public Template Template
        {
            get 
            { 
                if(template!=null)
                return template;
                Word.Template tmp = this.wordDocument.get_AttachedTemplate();
                foreach (Template temp in Globals.ThisAddIn.Officedata.Templates)
                {
                    String np = temp.Np;
                    if (np == String.Empty)
                        np = "dummy";
                    if (tmp.FullName.ToLower().Contains(temp.Filename.ToLower()) || tmp.FullName.ToLower().Contains(np.ToLower()))
                        template = temp;
                }
               if(template==null)
                {
                    template = new Template();
                    template.Name = tmp.Name;
                    template.Filename = tmp.Name;
                    template.HasContactBoxFunctionality = false;
                }
                return template;
            }
            set { template = value; }
        }

        public String FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        public Word.Document WordDocument
        {
            get { return wordDocument; }
            set { wordDocument = value; }
        }
        public Styles Styles
        {
            get { return styles; }
            set { styles = value; }
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="doc">The Word document</param>
        /// <param name="template">The document's template as a Template object</param>
        /// <param name="language">The language of the document</param>
        /// <param name="office">The office of the document</param>
        public Document(Word.Document doc, Template template, Language language, XmlOffice office)
        {
            this.wordDocument = doc;
            this.wordapp = doc.Application;
            this.Styles = clsStyles.ProcessStyles(doc);
            this.template = template;
            this.Language = language;
            this.Office = office;
            this.fullName = doc.FullName;
            
           
        }
        
        
        
        /// <summary>
        /// Constructor if only the Word document object is available
        /// </summary>
        /// <param name="doc">The Word document requiring a Document object</param>
        
        public Document(Word.Document doc)
        {
            this.wordDocument = doc;
            this.wordapp = doc.Application;
            this.Styles = clsStyles.ProcessStyles(doc);
            template = GetTemplate(doc);
            fullName = doc.FullName;
            language = this.Language;
            office = GetOffice(doc);
        }

        public Document() {}

        
        
        /// <summary>
        /// Gets the template object for a Word Document
        /// </summary>
        /// <param name="doc">The Word documnent with a template</param>
        /// <returns></returns>
        private Template GetTemplate(Word.Document doc)
        {
            Word.Template tmp = doc.get_AttachedTemplate();
            foreach (Template t in Globals.ThisAddIn.Officedata.Templates)
                if (tmp.FullName.ToLower().Contains(t.Filename.ToLower()))
                    return t;
            return null;
        }

        /// <summary>
        /// Gets the office for a Word document
        /// </summary>
        /// <param name="doc">The Word document with an office</param>
        /// <returns></returns>
        private XmlOffice GetOffice(Word.Document doc)
        {
            IEnumerable<XmlOffice> result = from d in Globals.ThisAddIn.Officedata.Offices where d.Shortname == doc.BuiltInDocumentProperties["Company"].Value select d;
            return result.Count() > 0 ? result.ToList()[0] : null;
                
        }
    }


}

