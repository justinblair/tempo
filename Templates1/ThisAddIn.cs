﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using Com.iManage.WorkSiteOfficeAddins;

namespace Templates1
{
    /// <summary>
    /// Class which processes the addin itself
    /// </summary>
    public partial class ThisAddIn
    {
        /// <summary>
        /// Properties
        /// </summary>

        internal Microsoft.Office.Tools.CustomTaskPane myCustomTaskPane;
        internal clsWordApp WordApp;
        internal WorkSite8Application DMS;
        private clsOfficeData officedata;
        private clsRibbonOptions ribbonopts;
        private LabelBuilder lblBuilder;
        private Ribbon ribbon;
        private clsErrorHandling errHandler;
        private PersonalSettings personalSettings;
        private Word.Document _pleadingDocument;
        private MacroMessages MacroMessages;
        private ShortcutManager shortcuts;
        internal List<Templates1.CustomTaskPanes.ControlCollection> ControlsCollection = null;
        internal Boolean SuppressEvents { get; set; }
        internal const String PROGRAMDATA = @"C:\ProgramData\Tempo";
        internal const String DATA = "Data";
        internal const String TEMPLATES = "Templates";
        internal const String BUILDINGBLOCKS = "Document Parts";
        internal const String BUILDINGBLOCKFILE = "Building Blocks.dotx";
        internal const String STYLESETS = "StyleSets";
        internal const Boolean FIXJBTONY = true;
        internal const Boolean SHOWMESSAGES = false;
        
        private SyncManager syncmanager;
        

        public Word.Document PleadingDocument

            /// summary
        {
            get { return _pleadingDocument; }
            set { _pleadingDocument = value; }
        }

        private String APP_NAME = "Tempo";

        public String APP_NAME1
        {
            get { return APP_NAME; }
            set { APP_NAME = value; }
        }

        public String APPDATA
        {
            get { return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Mishcon de Reya\Tempo";}
        }

        internal PersonalSettings PersonalSettings
        {
            get { return personalSettings; }
            set { personalSettings = value; }
        }



        internal LabelBuilder LblBuilder

        {
            get { return lblBuilder; }
            set { lblBuilder = value; }
        }



        public clsRibbonOptions Ribbonopts
        {
            get { return ribbonopts; }
            set { ribbonopts = value; }
        }

        public clsOfficeData Officedata
        {
            get { return officedata; }
            set { officedata = value; }
        }
        
        /// <summary>
        /// Loads the ribbon in Word
        /// </summary>
        /// <returns>An IRibbonExtensibilty object representing the ribbon</returns>
        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
         {
             clsErrorHandling errHandler = new clsErrorHandling();
             try
             {
                 //if (FileSiteCheck.IsInstalled())
                 //{
                 //    syncmanager = new SyncManager(DMS);
                 //    DownloadPrimaryFiles();
                 //    ReplaceBuildingBlocks();
                 //}
                 
                 ribbonopts = new clsRibbonOptions();

                 officedata = new clsOfficeData(ribbonopts);
                 personalSettings = new PersonalSettings();

                 ribbon = new Ribbon(ribbonopts, officedata);
                 return ribbon;
             }
             catch (Exception e) {
                 System.Windows.Forms.MessageBox.Show("An error occurred: " + e.Message + " " + e.StackTrace);
                 errHandler.ProcessError(e);
                if(ribbon == null)
                    ribbon = new Ribbon(ribbonopts, officedata);
                 return ribbon; 
             }
         }

        

        /// <summary>
        /// Registers the addin as COM enabled, required for keyboard shortcuts
        /// </summary>
        /// <returns>An object pointing to the MacroMessages class</returns>

        protected override object RequestComAddInAutomationService()
        {
            if (MacroMessages == null)
            {
                MacroMessages = new MacroMessages();
            }
            return MacroMessages;
        }
        /// <summary>
        /// Runs when the addin is started in the Word application.  Loads clsWordApp and its events, 
        /// ErrorHandling, DMS, Label data, ribbon error events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {

            errHandler = new clsErrorHandling();
            try
            {
                try
                {
                    Globals.ThisAddIn.Application.AddIns.Add(System.IO.Path.Combine(PROGRAMDATA, BUILDINGBLOCKS, BUILDINGBLOCKFILE), true);
                }
                catch (Exception err) { errHandler.ProcessError(err); }
                WordApp = new clsWordApp(Globals.ThisAddIn.Application);
                
                DMS = new WorkSite8Application(Application);
                WordApp.RogueStylesDetected += new clsWordApp.RogueStyleEventHandler(RefreshRogueStyleCheck);
                WordApp.NewDocumentCreated += new clsWordApp.NewWordDocumentEventHandler(SaveDocumentToDMS);
                WordApp.AfterPrint += new clsWordApp.AfterPrintEventHandler(WordApp.wordApp_AfterPrint);
                WordApp.AfterSave += new clsWordApp.AfterSaveEventHandler(WordApp.wordApp_AfterSave);
                WordApp.ErrorLogged += new clsWordApp.ErrorLoggedEventHandler(errHandler.ProcessError);
                WordApp.RecentDocumentAdded += new clsWordApp.RecentDocumentsAddedEventHandler(RefreshRecentDocs);
                ribbon.ErrorLogged += new Ribbon.ErrorLoggedEventHandler(errHandler.ProcessError);
                lblBuilder = new LabelBuilder();
                this.ribbonopts.GetDuplex();
                
                SetOptions();
                shortcuts = new ShortcutManager();
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new System.Action(() =>
                    CloseNormalDocuments()),
                    System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
                if (!System.IO.Directory.Exists(PROGRAMDATA))
                    System.IO.Directory.CreateDirectory(PROGRAMDATA);
                //System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke(new System.Action(() =>
                    //RunSynchManager()),
                    //System.Windows.Threading.DispatcherPriority.ApplicationIdle, null);
            }
            catch (Exception serr) { errHandler.ProcessError(serr); }
 
        }

        internal void DownloadPrimaryFiles()
        {

            //WorkSite8Application wstemp = DMS;
            syncmanager.Connect();
            if (syncmanager.NoConnecction)
            {
                if(ThisAddIn.SHOWMESSAGES)
                System.Windows.Forms.MessageBox.Show("You are not connected to FileSite.");
                //throw new InvalidOperationException();
                return;
            }
            if (syncmanager.DMS.OfficeLocation == WorkSite8Application.DMSOfficeLocation.None)
            {
                if (ThisAddIn.SHOWMESSAGES)
                System.Windows.Forms.MessageBox.Show("Unable to ascertain which office you are in.");
               // throw new InvalidOperationException();
                return;
            }
            
            syncmanager.DownloadOfficeData();
            syncmanager.DownloadOtherDataFiles();
            if(syncmanager.CatastrophicError)
            {
                System.Windows.Forms.MessageBox.Show("Tempo has not been able to initialise correctly.  An error occurred downloading data files.");
                return;
            }
            syncmanager.DownloadBuildingBlocks();

        }
        
        internal void ReplaceBuildingBlocks()
        {
            String path = System.IO.Path.Combine(PROGRAMDATA, BUILDINGBLOCKS, "Building Blocks.dotx");
            String path2 = System.IO.Path.Combine(PROGRAMDATA, BUILDINGBLOCKS, "Building Blocks.dotx^2");
            if(System.IO.File.Exists(path2))
                if(System.IO.File.Exists(path))
            {
                try
                {
                    String temp = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetTempFileName());
                    if(System.IO.File.Exists(temp))
                        System.IO.File.Delete(temp);
                    System.IO.File.Replace(path2,path, temp);
                }
                catch{}
            }
        }

        /// <summary>
        /// Checks to see if there have been template updates in FileSite when the application is idle
        /// </summary>
        internal void RunSynchManager()
        {
            if (syncmanager.NoConnecction)
                return;
            Boolean stylesetsDownloaded = syncmanager.DownloadStyleSets(this.officedata.StyleSets, false);
            Boolean downloaded = syncmanager.DownloadTemplates(this.officedata.Templates, stylesetsDownloaded);
                          

            if (downloaded || stylesetsDownloaded)
            {
                syncmanager.MergeStyles(this.officedata.Templates);
                //System.Windows.Forms.MessageBox.Show("There have been updates to Tempo." + Environment.NewLine +
                //    "Please close and restart Word for the changes to take effect.", APP_NAME1);
            }
            syncmanager.Disconnect();

            

        }
/// <summary>
/// Refreshes the recent documents list in the Tempo backstage tab
/// </summary>
/// <param name="rd">A recent document - not used</param>
        public void RefreshRecentDocs(RecentDocument rd)
        {
            ribbon.RefreshRecentDocs();
        }
        
        /// <summary>
        /// Runs when the addin is shutdown, typically when Word is exited
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        /// <summary>
        /// Saves a document to FileSite
        /// </summary>
        /// <param name="doc">The Word document to be saved</param>
        public void SaveDocumentToDMS(Word.Document doc)
        {
          //  WSCommandManager wSCommandManager = new WSCommandManager();
            
          //  ConnectWord connectWord = new ConnectWord(wSCommandManager);


            //this.DMS.CreateNewProfileFromDoc(doc);
        }

        /// <summary>
        /// Closes all unsaved documents based on Normal.dotm then invokes the Document_Open event for
        /// opening documents from FileSite while Word is closed.
        /// </summary>
        public void CloseNormalDocuments()
        {
            
            for(int i= WordApp.Application.Documents.Count; i>0; i--)
                if (clsWordApp.GetTemplateName(WordApp.Application.Documents[i]).Contains("Normal.dotm") &&
                    !WordApp.Application.Documents[i].FullName.Contains(@"\"))
                    WordApp.Application.Documents[i].Close(false);

            if (WordApp.Application.Documents.Count > 0)
                try
                {
                    WordApp.WordDocument_Open(WordApp.Application.ActiveDocument);
                }
                catch { }

            WordApp.CreateEvents();
        }
        /// <summary>
        /// Generates the appropriate task pane using the CustomTaskPanes collection which is only availble in ThisAddin.  Sets the colour and the tab-order.
        /// </summary>
        /// <param name="usercontrol">The task pane object to be loaded and displayed</param>
        /// <param name="title">The title/caption to show on the task pane</param>
        /// <returns>A CustomTaskPane object</returns>
        public  Microsoft.Office.Tools.CustomTaskPane GenerateTaskpane(System.Windows.Forms.UserControl usercontrol, String title)
        {
            Microsoft.Office.Tools.CustomTaskPane pane = null;
            foreach (Microsoft.Office.Tools.CustomTaskPane ctp in this.CustomTaskPanes)
                if (ctp.Window == this.Application.ActiveWindow)
                    if (ctp.Control.GetType().ToString() == usercontrol.GetType().ToString())
                        pane = ctp;
            
            if (pane == null)
                myCustomTaskPane = this.CustomTaskPanes.Add(usercontrol, title, Globals.ThisAddIn.Application.ActiveWindow);
            else
                myCustomTaskPane = pane;

            try
            {
                myCustomTaskPane.Visible = true;
                myCustomTaskPane.Width = (int)GetWidth(usercontrol);
            }
            catch { }
            CustomTaskPanes.clsHelper ch;
                ch = new Templates1.CustomTaskPanes.clsHelper();
                if (this.personalSettings.ColourPreference == 1 && pane == null)
                {
                    ch.SetColour(usercontrol);
                }
                if (pane == null)
                    ch.SetTabOrder(usercontrol);
           
            ActivateTemplatesTab();
            Templates1.CustomTaskPanes.ControlCollection collection = new Templates1.CustomTaskPanes.ControlCollection(myCustomTaskPane.Control.Name);
            foreach(System.Windows.Forms.Control ctl in myCustomTaskPane.Control.Controls)
            {
                Templates1.CustomTaskPanes.StoredControl sc = new CustomTaskPanes.StoredControl(ctl.Name, ctl.Location, ctl.Size);
                collection.Add(sc);
                //try
                //{
                //    Templates1.CustomTaskPanes.CustomControl con = (Templates1.CustomTaskPanes.CustomControl)ctl;
                //    con.Repaint();
                //}
                //catch { }

            }
            if (ControlsCollection == null)
                ControlsCollection = new List<Templates1.CustomTaskPanes.ControlCollection>();
            ControlsCollection.Add(collection);
            ch.ModifyForScreenResolution(myCustomTaskPane,1);
            return myCustomTaskPane;
        }

        public Microsoft.Office.Tools.CustomTaskPane GenerateGenericTaskpane(Template template, Word.Document doc)
        {
            Microsoft.Office.Tools.CustomTaskPane pane = null;
            foreach (Microsoft.Office.Tools.CustomTaskPane ctp in this.CustomTaskPanes)
                if (ctp.Window == this.Application.ActiveWindow)
                    pane = ctp;

            
            Templates1.CustomTaskPanes.clsPaneBuilder panebuilder = new CustomTaskPanes.clsPaneBuilder(doc,template);
            if (pane == null)
                myCustomTaskPane = panebuilder.CreatePane();//this.CustomTaskPanes.Add(usercontrol, title, Globals.ThisAddIn.Application.ActiveWindow);
            else
                myCustomTaskPane = pane;

            myCustomTaskPane.Visible = true;
            System.Windows.Forms.UserControl usercontrol = pane.Control;
            myCustomTaskPane.Width = (int)GetWidth(usercontrol);
            CustomTaskPanes.clsHelper ch;
            ch = new Templates1.CustomTaskPanes.clsHelper();
            if (this.personalSettings.ColourPreference == 1 && pane == null)
            {
                ch.SetColour(usercontrol);
            }
            if (pane == null)
                ch.SetTabOrder(usercontrol);
            ActivateTemplatesTab();
            Templates1.CustomTaskPanes.ControlCollection collection = new Templates1.CustomTaskPanes.ControlCollection(myCustomTaskPane.Control.Name);
            foreach (System.Windows.Forms.Control ctl in myCustomTaskPane.Control.Controls)
            {
                Templates1.CustomTaskPanes.StoredControl sc = new CustomTaskPanes.StoredControl(ctl.Name, ctl.Location, ctl.Size);
                collection.Add(sc);
                //try
                //{
                //    Templates1.CustomTaskPanes.CustomControl con = (Templates1.CustomTaskPanes.CustomControl)ctl;
                //    con.Repaint();
                //}
                //catch { }

            }
            if (ControlsCollection == null)
                ControlsCollection = new List<Templates1.CustomTaskPanes.ControlCollection>();
            ControlsCollection.Add(collection);
            ch.ModifyForScreenResolution(myCustomTaskPane, 1);
            return myCustomTaskPane;
        }
        
        private float GetWidth(System.Windows.Forms.UserControl usercontrol)
        {
            float maxWidth = 0F;
            foreach (System.Windows.Forms.Control ctl in usercontrol.Controls)
                if (ctl.Location.X + ctl.Width > maxWidth)
                    maxWidth = ctl.Location.X + ctl.Width;
            return maxWidth+10;
        }
        
        public void RefreshRibbon()
        {
            ribbon.RefreshRibbon();
            
        }

       

        public void RefreshRogueStyleCheck()
          {
              ribbon.RefreshRogueStyleCheck();
          }

        public void ActivateTemplatesTab()
          {
              ribbon.ActivateTab();
          }

        public void SetOptions()
        {
            try
            {
                Globals.ThisAddIn.Application.ActiveWindow.View.ShowHiddenText = true;
                Globals.ThisAddIn.Application.Options.AutoFormatReplaceQuotes = officedata.UseCurlyQuotes;
                Globals.ThisAddIn.Application.Options.AutoFormatAsYouTypeReplaceQuotes = officedata.UseCurlyQuotes;
                Globals.ThisAddIn.Application.Options.MeasurementUnit = officedata.MeasurementUnits;
            }
            catch { }
        }
       
        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
