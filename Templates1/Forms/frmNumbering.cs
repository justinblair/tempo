﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.Forms
{
    public partial class frmNumbering : Form
    {
        private Boolean bNoUpdating = false;
        public frmNumbering()
        {
            InitializeComponent();
        }

        private void frmNumbering_Load(object sender, EventArgs e)
        {
            LoadStyles();
            LoadControls();
            LoadPresets();
            CreateEvents();
           
        }

        private void LoadStyles()
        {
            bNoUpdating = true;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            foreach (Word.Style style in doc.Styles)
                if (style.ListTemplate != null)
                    cboStyles.Items.Add(style.NameLocal);

            cboStyles.Text = "MdR Level 1";
            bNoUpdating = false;
        }

        private void LoadPresets()
        {
            cboPreset.Items.Add("London Legal");
            cboPreset.Items.Add("London Court");
            cboPreset.Items.Add("New York Legal");
        }
        
        private void CreateEvents()
        {
            foreach(Control ctl in this.Controls)
            { 
                if(ctl.GetType().ToString().Contains("Panel"))
                    foreach(RadioButton rbn in ctl.Controls)
                        rbn.CheckedChanged += new EventHandler(rbn_Click);
                
                if (ctl.GetType().ToString().Contains("CheckBox"))
                {
                    CheckBox chk = (CheckBox)ctl;
                    chk.CheckStateChanged += new EventHandler(chk_Click);
                }

                if(ctl.GetType().ToString().Contains("TextBox"))
                {
                    TextBox tbx = (TextBox)ctl;
                    tbx.TextChanged += new EventHandler(tbx_TextChanged);
                }
            }
        }

        private void tbx_TextChanged(object sender, EventArgs e)
        {
            TextBox tbx = (TextBox)sender;
            int level = Convert.ToInt16(tbx.Name.Substring(tbx.Name.Length - 1, 1));
            Word.ListTemplate lst = Globals.ThisAddIn.Application.ActiveDocument.Styles[cboStyles.Text].ListTemplate;
            Word.ListLevel lvl = lst.ListLevels[level];
            String[] numberFormat = lvl.NumberFormat.Split(new char[] { '%' });
            numberFormat[0] = tbx.Text;
            String buffer = String.Empty;
            for(int i = 0; i<numberFormat.Length; i++)
                if(i==numberFormat.Length-1)
                    buffer += numberFormat[i];
                else
                    buffer += numberFormat[i] + "%";
            lvl.NumberFormat = buffer;
        }   
        
        private void rbn_Click(object sender, EventArgs e)
        {
            if (bNoUpdating) return;
            RadioButton rbn = (RadioButton)sender;
            if (!rbn.Checked) return;
            int level = Convert.ToInt16(rbn.Name.Substring(4, 1));
            int style = Convert.ToInt16(rbn.Name.Substring(rbn.Name.Length - 1, 1));
            Word.ListTemplate lst = Globals.ThisAddIn.Application.ActiveDocument.Styles[cboStyles.Text].ListTemplate;
            Word.ListLevel lvl = lst.ListLevels[level];
            switch(style)
            {
                case 1:
                    lvl.NumberStyle = Word.WdListNumberStyle.wdListNumberStyleUppercaseLetter;
                    break;
                case 2:
                    lvl.NumberStyle = Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman;
                    break;
                case 3:
                    lvl.NumberStyle = Word.WdListNumberStyle.wdListNumberStyleArabic;
                    break;
                case 4:
                    lvl.NumberStyle = Word.WdListNumberStyle.wdListNumberStyleLowercaseLetter;
                    break;
                case 5:
                    lvl.NumberStyle = Word.WdListNumberStyle.wdListNumberStyleLowercaseRoman;
                    break;
            }
        }

        private void chk_Click(object sender, EventArgs e)
        {
            if (bNoUpdating) return;
            CheckBox chk = (CheckBox)sender;
            int level = Convert.ToInt16(chk.Name.Substring(4, 1));
            Boolean isCombo = chk.Name.Contains("Com");
            Boolean isReset = chk.Name.Contains("Reset");
            Word.ListTemplate lst = Globals.ThisAddIn.Application.ActiveDocument.Styles[cboStyles.Text].ListTemplate;
            Word.ListLevel lvl = lst.ListLevels[level];
            String buffer = "";
            if (isCombo && chk.Checked && !isReset)
                for (int i = 1; i < level + 1; i++)
                    buffer += "%" + i + ".";
            else if (isCombo && !chk.Checked && !isReset)
                buffer = "%" + level + ".";
 
            if (!isCombo && !isReset && chk.Checked)
                buffer = "(" + lvl.NumberFormat.Replace(".","") + ")";
            else if (!isCombo && !chk.Checked && !isReset)
            { 
                buffer = lvl.NumberFormat.Replace("(", "").Replace(")", "");
                if(buffer.Length!=0)
                    if (buffer.Last().ToString() != ".")
                        buffer += ".";
             }

            if (isReset && level > 1)
                if (chk.Checked)
                    lvl.ResetOnHigher = level - 1;
                else
                    lvl.ResetOnHigher = 0;
            
            if(buffer.Length>0)
                lvl.NumberFormat = buffer;

        }
        
        private void LoadControls()
        {
            bNoUpdating = true;
            Word.ListTemplate lst = Globals.ThisAddIn.Application.ActiveDocument.Styles[cboStyles.Text].ListTemplate;
            for(int i = 1; i<6; i++)
            {
                if (i > lst.ListLevels.Count) 
                {
                    bNoUpdating = false;
                    return;
                }
                Word.ListLevel lvl = lst.ListLevels[i];
                switch(lvl.NumberStyle)
                {
                    case Word.WdListNumberStyle.wdListNumberStyleUppercaseLetter:
                        var clt = this.Controls;
                        RadioButton rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C1",true)[0];
                        rbn.Checked = true;
                        break;
                    case Word.WdListNumberStyle.wdListNumberStyleUppercaseRoman:
                        rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C2",true)[0];
                        rbn.Checked = true;
                        break;
                    case Word.WdListNumberStyle.wdListNumberStyleArabic:
                        rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C3", true)[0];
                        rbn.Checked = true;
                        break;
                    case Word.WdListNumberStyle.wdListNumberStyleLowercaseLetter:
                        rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C4",true)[0];
                        rbn.Checked = true;
                        break;
                    case Word.WdListNumberStyle.wdListNumberStyleLowercaseRoman:
                        rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C5",true)[0];
                        rbn.Checked = true;
                        break;
                    case Word.WdListNumberStyle.wdListNumberStyleLegal:
                        rbn = (RadioButton)this.Controls.Find("rbnH"+i+"C3", true)[0];
                        rbn.Checked = true;
                        break;
                }
                
                    CheckBox chk = (CheckBox)this.Controls.Find("chkH" + i, true)[0];
                    chk.Checked = lvl.NumberFormat.Contains("(");

                    chk = (CheckBox)this.Controls.Find("chkH" + i + "Com",true)[0];
                    chk.Checked = lvl.NumberFormat.Contains("%" + (i - 1));

                    TextBox tbx = (TextBox)this.Controls.Find("tbxH" + i, true)[0];
                    tbx.Text = lvl.NumberFormat.Split(new char[] { '%' })[0].Replace("(","");

                    chk = (CheckBox)this.Controls.Find("chkH" + i + "Reset", true)[0];
                    chk.Checked = lvl.ResetOnHigher != 0;
                
            }
            bNoUpdating = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cboStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (bNoUpdating) return;
            LoadControls();
        }

        private void cboPreset_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach(Control ctl in this.Controls)
                if(ctl.GetType().ToString().Contains("CheckBox"))
                {
                    CheckBox cbx = (CheckBox)ctl;
                    cbx.Checked = false;
                }
            
            switch(cboPreset.SelectedIndex)
            {
                case 0:
                    this.rbnH1C3.Checked = true;
                    this.rbnH2C3.Checked = true;
                    this.chkH2Com.Checked = true;
                    this.rbnH3C3.Checked = true;
                    this.chkH3Com.Checked = true;
                    this.rbnH4C4.Checked = true;
                    this.chkH4.Checked = true;
                    this.rbnH5C5.Checked = true;
                    this.chkH5.Checked = true;
                    this.chkH2Reset.Checked = true;
                    this.chkH3Reset.Checked = true;
                    this.chkH4Reset.Checked = true;
                    this.chkH5Reset.Checked = true;
                    break;

                case 1:
                    this.rbnH1C3.Checked = true;
                    this.rbnH2C4.Checked = true;
                    this.chkH2.Checked = true;
                    this.rbnH3C5.Checked = true;
                    this.chkH3.Checked = true;
                    this.rbnH4C3.Checked = true;
                    this.chkH4.Checked = true;
                    this.chkH2Reset.Checked = true;
                    this.chkH3Reset.Checked = true;
                    this.chkH4Reset.Checked = true;
                    this.chkH5Reset.Checked = true;
                    break;

                case 2:
                    this.rbnH1C2.Checked = true;
                    this.rbnH2C1.Checked = true;
                    this.rbnH3C3.Checked = true;
                    this.rbnH4C4.Checked = true;
                    this.chkH4.Checked = true;
                    this.chkH2Reset.Checked = true;
                    this.chkH3Reset.Checked = true;
                    this.chkH4Reset.Checked = true;
                    this.chkH5Reset.Checked = true;
                    break;
            }
        }
    }
}
