﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Templates1.Forms
{
    public partial class frmProgress : Form
    {
        public frmProgress()
        {
            InitializeComponent();
        }

        private void frmProgress_Load(object sender, EventArgs e)
        {
            this.lblProg.Text = "";
            this.newProgressBar1.Value = 0;
        }

        public void SetProgBar(String text, double value)
        {
            this.lblProg.Text = text;
            this.newProgressBar1.Value = (int)value;
            this.Refresh();
        }
    }
}
