﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Templates1.Forms
{
    public partial class frmPDF : Form
    {
        public frmPDF()
        {
            InitializeComponent();
        }

        private string _file;

        public string File
        {
            get { return _file; }
            set { _file = value; }
        }

        
        private void frmPDF_Load(object sender, EventArgs e)
        {
            this.Resize += new EventHandler(frmPDF_Resize);
        }

        void frmPDF_Resize(object sender, EventArgs e)
        {
            webBrowser.Height = this.Height;
            webBrowser.Width = this.Width;
        }

        public void LoadPDF()
        {
            webBrowser.Navigate(_file);
        }

        private void axAcroPDF_OnError(object sender, EventArgs e)
        {

        }
    }
}
