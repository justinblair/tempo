﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.Forms
{
    public partial class frmHouseStyle : Form
    {
        private Word.Document doc;
        public frmHouseStyle()
        {
            InitializeComponent();
        }

        private void frmHouseStyle_Load(object sender, EventArgs e)
        {
            ScanDocument();
            //Disable the All Caps option for New York
            this.chbLevel1AC.Enabled = Globals.ThisAddIn.Officedata.IsNotNYOffice;
        }

        private void ScanDocument()
        {
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            this.doc = Globals.ThisAddIn.Application.ActiveDocument;
            foreach(Word.Paragraph para in doc.Paragraphs)
            {
                Word.Style stl = para.get_Style();
                if(stl.NameLocal.ToLower().Contains("mdr heading"))
                {
                    String i = stl.NameLocal.Substring(stl.NameLocal.Length - 1, 1);
                    CheckBox chk = (CheckBox)this.Controls["chkLevel" + i];
                    chk.Checked = true;
                }
            }
            if (clsStyles.StyleExists("MdR Heading 1", doc))
                chbLevel1AC.Checked = doc.Styles["MdR Heading 1"].Font.AllCaps == 0 ? false : true;
            else
                System.Windows.Forms.MessageBox.Show("This document has either not been converted or re-styled to become an MdR " + Globals.ThisAddIn.APP_NAME1 + " document.  Re-style or convert from the backstage.", Globals.ThisAddIn.APP_NAME1);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            
            foreach(Word.Paragraph para in doc.Paragraphs)
                foreach(Control ctl in this.Controls)
                    if(ctl.Name.Contains("chk"))
                    {
                        CheckBox chk = (CheckBox)ctl;
                        if(chk.Checked)
                        {
                            Word.Style sourceStyle = para.get_Style();
                            if(sourceStyle.NameLocal.ToLower().Contains("level " + chk.Name.Substring(chk.Name.Length - 1,1)))
                            {
                                Word.Style destStyle = doc.Styles["MdR Heading " + chk.Name.Substring(chk.Name.Length - 1, 1)];
                                para.set_Style(destStyle);
                            }
                        }
                        else
                        {
                            Word.Style sourceStyle = para.get_Style();
                            if (sourceStyle.NameLocal.ToLower().Contains("heading " + chk.Name.Substring(chk.Name.Length - 1, 1)))
                            {
                                Word.Style destStyle = doc.Styles["MdR Level " + chk.Name.Substring(chk.Name.Length - 1, 1)];
                                para.set_Style(destStyle);
                            }
                        }
                    }
            doc.Styles["MdR Heading 1"].Font.AllCaps = chbLevel1AC.Checked ? -1 : 0;
            this.Close();
        }

        private void chkLevel1_CheckedChanged(object sender, EventArgs e)
        {
            chbLevel1AC.Enabled = chkLevel1.Checked;
            if (!chkLevel1.Checked) chbLevel1AC.Checked = false;
            if (chkLevel1.Checked)
            { 
                rtbx1.SelectAll();
                rtbx1.SelectionFont = new Font(rtbx1.Font, FontStyle.Bold);
                rtbx1.Select();
            }
            else
            {
                rtbx1.SelectAll();
                rtbx1.SelectionFont = new Font(rtbx1.Font, FontStyle.Regular);
                rtbx1.Select();
            }
        }

        private void rbnStyleA_CheckedChanged(object sender, EventArgs e)
        {
            if(chkCourtDocument.Checked)
            { 
                this.chbLevel1AC.Checked = false;
                this.chkLevel1.Checked = this.rbnStyleA.Checked;
                this.chkLevel2.Checked = this.rbnStyleA.Checked;
            }
            else
            {
                this.chbLevel1AC.Checked = this.rbnStyleA.Checked;
                this.chkLevel1.Checked = this.rbnStyleA.Checked;
                this.chkLevel2.Checked = this.rbnStyleA.Checked;
            }
            this.chkLevel3.Checked = false;
            this.chkLevel4.Checked = false;
            this.chkLevel5.Checked = false;
        }

        private void rbnStyleB_CheckedChanged(object sender, EventArgs e)
        {
            if(chkCourtDocument.Checked)
            { 
                this.chbLevel1AC.Checked = false;
                this.chkLevel1.Checked = this.rbnStyleB.Checked;
                this.chkLevel2.Checked = false;
            }
            else
            {
                this.chbLevel1AC.Checked = this.rbnStyleB.Checked;
                this.chkLevel1.Checked = this.rbnStyleB.Checked;
                this.chkLevel2.Checked = false;
            }
            this.chkLevel3.Checked = false;
            this.chkLevel4.Checked = false;
            this.chkLevel5.Checked = false;
        }

        private void rbnStyleC_CheckedChanged(object sender, EventArgs e)
        {
            
            this.chbLevel1AC.Checked = false;
            this.chkLevel1.Checked = false;
            this.chkLevel2.Checked = false;
           
            this.chkLevel3.Checked = false;
            this.chkLevel4.Checked = false;
            this.chkLevel5.Checked = false;
        }

        private void chkLevel2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLevel2.Checked)
            {
                rtbx2.SelectAll();
                rtbx2.SelectionFont = new Font(rtbx2.Font, FontStyle.Bold);
                rtbx2.Select();
            }
            else
            {
                rtbx2.SelectAll();
                rtbx2.SelectionFont = new Font(rtbx2.Font, FontStyle.Regular);
                rtbx2.Select();
            }
        }

        private void chkLevel3_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLevel3.Checked)
            {
                rtbx3.SelectAll();
                rtbx3.SelectionFont = new Font(rtbx3.Font, FontStyle.Bold);
                rtbx3.Select();
            }
            else
            {
                rtbx3.SelectAll();
                rtbx3.SelectionFont = new Font(rtbx3.Font, FontStyle.Regular);
                rtbx3.Select();
            }
        }

        private void chkLevel4_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLevel4.Checked)
            {
                rtbx4.SelectAll();
                rtbx4.SelectionFont = new Font(rtbx4.Font, FontStyle.Bold);
                rtbx4.Select();
            }
            else
            {
                rtbx4.SelectAll();
                rtbx4.SelectionFont = new Font(rtbx4.Font, FontStyle.Regular);
                rtbx4.Select();
            }
        }

        private void chkLevel5_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLevel5.Checked)
            {
                rtbx5.SelectAll();
                rtbx5.SelectionFont = new Font(rtbx5.Font, FontStyle.Bold);
                rtbx5.Select();
            }
            else
            {
                rtbx5.SelectAll();
                rtbx5.SelectionFont = new Font(rtbx5.Font, FontStyle.Regular);
                rtbx5.Select();
            }
        }

        private void chbLevel1AC_CheckedChanged(object sender, EventArgs e)
        {
            if (chbLevel1AC.Checked)
                this.rtbx1.Text = this.rtbx1.Text.ToUpper();
            else
            { 
                this.rtbx1.Text = this.rtbx1.Text.ToLower();
                this.rtbx1.Text = this.rtbx1.Text.Replace("l", "L");
            }
        }







    }
}
