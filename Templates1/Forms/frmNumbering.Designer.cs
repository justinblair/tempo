﻿namespace Templates1.Forms
{
    partial class frmNumbering
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.rbnH1C1 = new System.Windows.Forms.RadioButton();
            this.rbnH1C2 = new System.Windows.Forms.RadioButton();
            this.rbnH1C3 = new System.Windows.Forms.RadioButton();
            this.rbnH1C4 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbnH1C5 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbnH2C5 = new System.Windows.Forms.RadioButton();
            this.rbnH2C1 = new System.Windows.Forms.RadioButton();
            this.rbnH2C4 = new System.Windows.Forms.RadioButton();
            this.rbnH2C3 = new System.Windows.Forms.RadioButton();
            this.rbnH2C2 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbnH3C5 = new System.Windows.Forms.RadioButton();
            this.rbnH3C1 = new System.Windows.Forms.RadioButton();
            this.rbnH3C4 = new System.Windows.Forms.RadioButton();
            this.rbnH3C3 = new System.Windows.Forms.RadioButton();
            this.rbnH3C2 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rbnH4C5 = new System.Windows.Forms.RadioButton();
            this.rbnH4C1 = new System.Windows.Forms.RadioButton();
            this.rbnH4C4 = new System.Windows.Forms.RadioButton();
            this.rbnH4C3 = new System.Windows.Forms.RadioButton();
            this.rbnH4C2 = new System.Windows.Forms.RadioButton();
            this.chkH1 = new System.Windows.Forms.CheckBox();
            this.chkH2 = new System.Windows.Forms.CheckBox();
            this.chkH3 = new System.Windows.Forms.CheckBox();
            this.chkH4 = new System.Windows.Forms.CheckBox();
            this.tbxH1 = new CustomTaskPanes.CustomTextBox();
            this.tbxH2 = new CustomTaskPanes.CustomTextBox();
            this.tbxH3 = new CustomTaskPanes.CustomTextBox();
            this.tbxH4 = new CustomTaskPanes.CustomTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkH1Com = new System.Windows.Forms.CheckBox();
            this.chkH2Com = new System.Windows.Forms.CheckBox();
            this.chkH3Com = new System.Windows.Forms.CheckBox();
            this.chkH4Com = new System.Windows.Forms.CheckBox();
            this.chkH5Com = new System.Windows.Forms.CheckBox();
            this.tbxH5 = new CustomTaskPanes.CustomTextBox();
            this.chkH5 = new System.Windows.Forms.CheckBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rbnH5C5 = new System.Windows.Forms.RadioButton();
            this.rbnH5C1 = new System.Windows.Forms.RadioButton();
            this.rbnH5C4 = new System.Windows.Forms.RadioButton();
            this.rbnH5C3 = new System.Windows.Forms.RadioButton();
            this.rbnH5C2 = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.cboStyles = new CustomTaskPanes.CustomComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.chkH5Reset = new System.Windows.Forms.CheckBox();
            this.chkH4Reset = new System.Windows.Forms.CheckBox();
            this.chkH3Reset = new System.Windows.Forms.CheckBox();
            this.chkH2Reset = new System.Windows.Forms.CheckBox();
            this.chkH1Reset = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboPreset = new CustomTaskPanes.CustomComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Level 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Level 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Level 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Level 4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(169, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "A.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "I.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(249, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "1.";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "a..";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(326, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "i.";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(364, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "( )";
            // 
            // rbnH1C1
            // 
            this.rbnH1C1.AutoSize = true;
            this.rbnH1C1.Location = new System.Drawing.Point(6, 10);
            this.rbnH1C1.Name = "rbnH1C1";
            this.rbnH1C1.Size = new System.Drawing.Size(14, 13);
            this.rbnH1C1.TabIndex = 0;
            this.rbnH1C1.TabStop = true;
            this.rbnH1C1.UseVisualStyleBackColor = true;
            // 
            // rbnH1C2
            // 
            this.rbnH1C2.AutoSize = true;
            this.rbnH1C2.Location = new System.Drawing.Point(44, 10);
            this.rbnH1C2.Name = "rbnH1C2";
            this.rbnH1C2.Size = new System.Drawing.Size(14, 13);
            this.rbnH1C2.TabIndex = 1;
            this.rbnH1C2.TabStop = true;
            this.rbnH1C2.UseVisualStyleBackColor = true;
            // 
            // rbnH1C3
            // 
            this.rbnH1C3.AutoSize = true;
            this.rbnH1C3.Location = new System.Drawing.Point(82, 10);
            this.rbnH1C3.Name = "rbnH1C3";
            this.rbnH1C3.Size = new System.Drawing.Size(14, 13);
            this.rbnH1C3.TabIndex = 2;
            this.rbnH1C3.TabStop = true;
            this.rbnH1C3.UseVisualStyleBackColor = true;
            // 
            // rbnH1C4
            // 
            this.rbnH1C4.AutoSize = true;
            this.rbnH1C4.Location = new System.Drawing.Point(120, 10);
            this.rbnH1C4.Name = "rbnH1C4";
            this.rbnH1C4.Size = new System.Drawing.Size(14, 13);
            this.rbnH1C4.TabIndex = 3;
            this.rbnH1C4.TabStop = true;
            this.rbnH1C4.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbnH1C5);
            this.panel1.Controls.Add(this.rbnH1C1);
            this.panel1.Controls.Add(this.rbnH1C4);
            this.panel1.Controls.Add(this.rbnH1C3);
            this.panel1.Controls.Add(this.rbnH1C2);
            this.panel1.Location = new System.Drawing.Point(165, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(184, 34);
            this.panel1.TabIndex = 10;
            // 
            // rbnH1C5
            // 
            this.rbnH1C5.AutoSize = true;
            this.rbnH1C5.Location = new System.Drawing.Point(158, 10);
            this.rbnH1C5.Name = "rbnH1C5";
            this.rbnH1C5.Size = new System.Drawing.Size(14, 13);
            this.rbnH1C5.TabIndex = 4;
            this.rbnH1C5.TabStop = true;
            this.rbnH1C5.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbnH2C5);
            this.panel2.Controls.Add(this.rbnH2C1);
            this.panel2.Controls.Add(this.rbnH2C4);
            this.panel2.Controls.Add(this.rbnH2C3);
            this.panel2.Controls.Add(this.rbnH2C2);
            this.panel2.Location = new System.Drawing.Point(165, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(184, 34);
            this.panel2.TabIndex = 11;
            // 
            // rbnH2C5
            // 
            this.rbnH2C5.AutoSize = true;
            this.rbnH2C5.Location = new System.Drawing.Point(158, 10);
            this.rbnH2C5.Name = "rbnH2C5";
            this.rbnH2C5.Size = new System.Drawing.Size(14, 13);
            this.rbnH2C5.TabIndex = 4;
            this.rbnH2C5.TabStop = true;
            this.rbnH2C5.UseVisualStyleBackColor = true;
            // 
            // rbnH2C1
            // 
            this.rbnH2C1.AutoSize = true;
            this.rbnH2C1.Location = new System.Drawing.Point(6, 10);
            this.rbnH2C1.Name = "rbnH2C1";
            this.rbnH2C1.Size = new System.Drawing.Size(14, 13);
            this.rbnH2C1.TabIndex = 0;
            this.rbnH2C1.TabStop = true;
            this.rbnH2C1.UseVisualStyleBackColor = true;
            // 
            // rbnH2C4
            // 
            this.rbnH2C4.AutoSize = true;
            this.rbnH2C4.Location = new System.Drawing.Point(120, 10);
            this.rbnH2C4.Name = "rbnH2C4";
            this.rbnH2C4.Size = new System.Drawing.Size(14, 13);
            this.rbnH2C4.TabIndex = 3;
            this.rbnH2C4.TabStop = true;
            this.rbnH2C4.UseVisualStyleBackColor = true;
            // 
            // rbnH2C3
            // 
            this.rbnH2C3.AutoSize = true;
            this.rbnH2C3.Location = new System.Drawing.Point(82, 10);
            this.rbnH2C3.Name = "rbnH2C3";
            this.rbnH2C3.Size = new System.Drawing.Size(14, 13);
            this.rbnH2C3.TabIndex = 2;
            this.rbnH2C3.TabStop = true;
            this.rbnH2C3.UseVisualStyleBackColor = true;
            // 
            // rbnH2C2
            // 
            this.rbnH2C2.AutoSize = true;
            this.rbnH2C2.Location = new System.Drawing.Point(44, 10);
            this.rbnH2C2.Name = "rbnH2C2";
            this.rbnH2C2.Size = new System.Drawing.Size(14, 13);
            this.rbnH2C2.TabIndex = 1;
            this.rbnH2C2.TabStop = true;
            this.rbnH2C2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.rbnH3C5);
            this.panel3.Controls.Add(this.rbnH3C1);
            this.panel3.Controls.Add(this.rbnH3C4);
            this.panel3.Controls.Add(this.rbnH3C3);
            this.panel3.Controls.Add(this.rbnH3C2);
            this.panel3.Location = new System.Drawing.Point(165, 96);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(184, 34);
            this.panel3.TabIndex = 12;
            // 
            // rbnH3C5
            // 
            this.rbnH3C5.AutoSize = true;
            this.rbnH3C5.Location = new System.Drawing.Point(158, 10);
            this.rbnH3C5.Name = "rbnH3C5";
            this.rbnH3C5.Size = new System.Drawing.Size(14, 13);
            this.rbnH3C5.TabIndex = 4;
            this.rbnH3C5.TabStop = true;
            this.rbnH3C5.UseVisualStyleBackColor = true;
            // 
            // rbnH3C1
            // 
            this.rbnH3C1.AutoSize = true;
            this.rbnH3C1.Location = new System.Drawing.Point(6, 10);
            this.rbnH3C1.Name = "rbnH3C1";
            this.rbnH3C1.Size = new System.Drawing.Size(14, 13);
            this.rbnH3C1.TabIndex = 0;
            this.rbnH3C1.TabStop = true;
            this.rbnH3C1.UseVisualStyleBackColor = true;
            // 
            // rbnH3C4
            // 
            this.rbnH3C4.AutoSize = true;
            this.rbnH3C4.Location = new System.Drawing.Point(120, 10);
            this.rbnH3C4.Name = "rbnH3C4";
            this.rbnH3C4.Size = new System.Drawing.Size(14, 13);
            this.rbnH3C4.TabIndex = 3;
            this.rbnH3C4.TabStop = true;
            this.rbnH3C4.UseVisualStyleBackColor = true;
            // 
            // rbnH3C3
            // 
            this.rbnH3C3.AutoSize = true;
            this.rbnH3C3.Location = new System.Drawing.Point(82, 10);
            this.rbnH3C3.Name = "rbnH3C3";
            this.rbnH3C3.Size = new System.Drawing.Size(14, 13);
            this.rbnH3C3.TabIndex = 2;
            this.rbnH3C3.TabStop = true;
            this.rbnH3C3.UseVisualStyleBackColor = true;
            // 
            // rbnH3C2
            // 
            this.rbnH3C2.AutoSize = true;
            this.rbnH3C2.Location = new System.Drawing.Point(44, 10);
            this.rbnH3C2.Name = "rbnH3C2";
            this.rbnH3C2.Size = new System.Drawing.Size(14, 13);
            this.rbnH3C2.TabIndex = 1;
            this.rbnH3C2.TabStop = true;
            this.rbnH3C2.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.rbnH4C5);
            this.panel4.Controls.Add(this.rbnH4C1);
            this.panel4.Controls.Add(this.rbnH4C4);
            this.panel4.Controls.Add(this.rbnH4C3);
            this.panel4.Controls.Add(this.rbnH4C2);
            this.panel4.Location = new System.Drawing.Point(165, 125);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(184, 34);
            this.panel4.TabIndex = 13;
            // 
            // rbnH4C5
            // 
            this.rbnH4C5.AutoSize = true;
            this.rbnH4C5.Location = new System.Drawing.Point(158, 10);
            this.rbnH4C5.Name = "rbnH4C5";
            this.rbnH4C5.Size = new System.Drawing.Size(14, 13);
            this.rbnH4C5.TabIndex = 4;
            this.rbnH4C5.TabStop = true;
            this.rbnH4C5.UseVisualStyleBackColor = true;
            // 
            // rbnH4C1
            // 
            this.rbnH4C1.AutoSize = true;
            this.rbnH4C1.Location = new System.Drawing.Point(6, 10);
            this.rbnH4C1.Name = "rbnH4C1";
            this.rbnH4C1.Size = new System.Drawing.Size(14, 13);
            this.rbnH4C1.TabIndex = 0;
            this.rbnH4C1.TabStop = true;
            this.rbnH4C1.UseVisualStyleBackColor = true;
            // 
            // rbnH4C4
            // 
            this.rbnH4C4.AutoSize = true;
            this.rbnH4C4.Location = new System.Drawing.Point(120, 10);
            this.rbnH4C4.Name = "rbnH4C4";
            this.rbnH4C4.Size = new System.Drawing.Size(14, 13);
            this.rbnH4C4.TabIndex = 3;
            this.rbnH4C4.TabStop = true;
            this.rbnH4C4.UseVisualStyleBackColor = true;
            // 
            // rbnH4C3
            // 
            this.rbnH4C3.AutoSize = true;
            this.rbnH4C3.Location = new System.Drawing.Point(82, 10);
            this.rbnH4C3.Name = "rbnH4C3";
            this.rbnH4C3.Size = new System.Drawing.Size(14, 13);
            this.rbnH4C3.TabIndex = 2;
            this.rbnH4C3.TabStop = true;
            this.rbnH4C3.UseVisualStyleBackColor = true;
            // 
            // rbnH4C2
            // 
            this.rbnH4C2.AutoSize = true;
            this.rbnH4C2.Location = new System.Drawing.Point(44, 10);
            this.rbnH4C2.Name = "rbnH4C2";
            this.rbnH4C2.Size = new System.Drawing.Size(14, 13);
            this.rbnH4C2.TabIndex = 1;
            this.rbnH4C2.TabStop = true;
            this.rbnH4C2.UseVisualStyleBackColor = true;
            // 
            // chkH1
            // 
            this.chkH1.AutoSize = true;
            this.chkH1.Location = new System.Drawing.Point(365, 47);
            this.chkH1.Name = "chkH1";
            this.chkH1.Size = new System.Drawing.Size(15, 14);
            this.chkH1.TabIndex = 14;
            this.chkH1.UseVisualStyleBackColor = true;
            // 
            // chkH2
            // 
            this.chkH2.AutoSize = true;
            this.chkH2.Location = new System.Drawing.Point(365, 77);
            this.chkH2.Name = "chkH2";
            this.chkH2.Size = new System.Drawing.Size(15, 14);
            this.chkH2.TabIndex = 15;
            this.chkH2.UseVisualStyleBackColor = true;
            // 
            // chkH3
            // 
            this.chkH3.AutoSize = true;
            this.chkH3.Location = new System.Drawing.Point(365, 107);
            this.chkH3.Name = "chkH3";
            this.chkH3.Size = new System.Drawing.Size(15, 14);
            this.chkH3.TabIndex = 16;
            this.chkH3.UseVisualStyleBackColor = true;
            // 
            // chkH4
            // 
            this.chkH4.AutoSize = true;
            this.chkH4.Location = new System.Drawing.Point(365, 137);
            this.chkH4.Name = "chkH4";
            this.chkH4.Size = new System.Drawing.Size(15, 14);
            this.chkH4.TabIndex = 17;
            this.chkH4.UseVisualStyleBackColor = true;
            // 
            // tbxH1
            // 
            this.tbxH1.Location = new System.Drawing.Point(62, 42);
            this.tbxH1.Name = "tbxH1";
            this.tbxH1.Size = new System.Drawing.Size(90, 20);
            this.tbxH1.TabIndex = 18;
            // 
            // tbxH2
            // 
            this.tbxH2.Location = new System.Drawing.Point(62, 72);
            this.tbxH2.Name = "tbxH2";
            this.tbxH2.Size = new System.Drawing.Size(90, 20);
            this.tbxH2.TabIndex = 19;
            // 
            // tbxH3
            // 
            this.tbxH3.Location = new System.Drawing.Point(62, 102);
            this.tbxH3.Name = "tbxH3";
            this.tbxH3.Size = new System.Drawing.Size(90, 20);
            this.tbxH3.TabIndex = 20;
            // 
            // tbxH4
            // 
            this.tbxH4.Location = new System.Drawing.Point(62, 131);
            this.tbxH4.Name = "tbxH4";
            this.tbxH4.Size = new System.Drawing.Size(90, 20);
            this.tbxH4.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(398, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "1.1.";
            // 
            // chkH1Com
            // 
            this.chkH1Com.AutoSize = true;
            this.chkH1Com.Enabled = false;
            this.chkH1Com.Location = new System.Drawing.Point(401, 47);
            this.chkH1Com.Name = "chkH1Com";
            this.chkH1Com.Size = new System.Drawing.Size(15, 14);
            this.chkH1Com.TabIndex = 23;
            this.chkH1Com.UseVisualStyleBackColor = true;
            // 
            // chkH2Com
            // 
            this.chkH2Com.AutoSize = true;
            this.chkH2Com.Location = new System.Drawing.Point(401, 77);
            this.chkH2Com.Name = "chkH2Com";
            this.chkH2Com.Size = new System.Drawing.Size(15, 14);
            this.chkH2Com.TabIndex = 24;
            this.chkH2Com.UseVisualStyleBackColor = true;
            // 
            // chkH3Com
            // 
            this.chkH3Com.AutoSize = true;
            this.chkH3Com.Location = new System.Drawing.Point(401, 107);
            this.chkH3Com.Name = "chkH3Com";
            this.chkH3Com.Size = new System.Drawing.Size(15, 14);
            this.chkH3Com.TabIndex = 25;
            this.chkH3Com.UseVisualStyleBackColor = true;
            // 
            // chkH4Com
            // 
            this.chkH4Com.AutoSize = true;
            this.chkH4Com.Location = new System.Drawing.Point(401, 137);
            this.chkH4Com.Name = "chkH4Com";
            this.chkH4Com.Size = new System.Drawing.Size(15, 14);
            this.chkH4Com.TabIndex = 26;
            this.chkH4Com.UseVisualStyleBackColor = true;
            // 
            // chkH5Com
            // 
            this.chkH5Com.AutoSize = true;
            this.chkH5Com.Location = new System.Drawing.Point(401, 166);
            this.chkH5Com.Name = "chkH5Com";
            this.chkH5Com.Size = new System.Drawing.Size(15, 14);
            this.chkH5Com.TabIndex = 31;
            this.chkH5Com.UseVisualStyleBackColor = true;
            // 
            // tbxH5
            // 
            this.tbxH5.Location = new System.Drawing.Point(62, 160);
            this.tbxH5.Name = "tbxH5";
            this.tbxH5.Size = new System.Drawing.Size(90, 20);
            this.tbxH5.TabIndex = 30;
            // 
            // chkH5
            // 
            this.chkH5.AutoSize = true;
            this.chkH5.Location = new System.Drawing.Point(365, 166);
            this.chkH5.Name = "chkH5";
            this.chkH5.Size = new System.Drawing.Size(15, 14);
            this.chkH5.TabIndex = 29;
            this.chkH5.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rbnH5C5);
            this.panel5.Controls.Add(this.rbnH5C1);
            this.panel5.Controls.Add(this.rbnH5C4);
            this.panel5.Controls.Add(this.rbnH5C3);
            this.panel5.Controls.Add(this.rbnH5C2);
            this.panel5.Location = new System.Drawing.Point(165, 154);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(184, 34);
            this.panel5.TabIndex = 28;
            // 
            // rbnH5C5
            // 
            this.rbnH5C5.AutoSize = true;
            this.rbnH5C5.Location = new System.Drawing.Point(158, 10);
            this.rbnH5C5.Name = "rbnH5C5";
            this.rbnH5C5.Size = new System.Drawing.Size(14, 13);
            this.rbnH5C5.TabIndex = 4;
            this.rbnH5C5.TabStop = true;
            this.rbnH5C5.UseVisualStyleBackColor = true;
            // 
            // rbnH5C1
            // 
            this.rbnH5C1.AutoSize = true;
            this.rbnH5C1.Location = new System.Drawing.Point(6, 10);
            this.rbnH5C1.Name = "rbnH5C1";
            this.rbnH5C1.Size = new System.Drawing.Size(14, 13);
            this.rbnH5C1.TabIndex = 0;
            this.rbnH5C1.TabStop = true;
            this.rbnH5C1.UseVisualStyleBackColor = true;
            // 
            // rbnH5C4
            // 
            this.rbnH5C4.AutoSize = true;
            this.rbnH5C4.Location = new System.Drawing.Point(120, 10);
            this.rbnH5C4.Name = "rbnH5C4";
            this.rbnH5C4.Size = new System.Drawing.Size(14, 13);
            this.rbnH5C4.TabIndex = 3;
            this.rbnH5C4.TabStop = true;
            this.rbnH5C4.UseVisualStyleBackColor = true;
            // 
            // rbnH5C3
            // 
            this.rbnH5C3.AutoSize = true;
            this.rbnH5C3.Location = new System.Drawing.Point(82, 10);
            this.rbnH5C3.Name = "rbnH5C3";
            this.rbnH5C3.Size = new System.Drawing.Size(14, 13);
            this.rbnH5C3.TabIndex = 2;
            this.rbnH5C3.TabStop = true;
            this.rbnH5C3.UseVisualStyleBackColor = true;
            // 
            // rbnH5C2
            // 
            this.rbnH5C2.AutoSize = true;
            this.rbnH5C2.Location = new System.Drawing.Point(44, 10);
            this.rbnH5C2.Name = "rbnH5C2";
            this.rbnH5C2.Size = new System.Drawing.Size(14, 13);
            this.rbnH5C2.TabIndex = 1;
            this.rbnH5C2.TabStop = true;
            this.rbnH5C2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Level 5";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(380, 213);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(73, 33);
            this.btnClose.TabIndex = 32;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cboStyles
            // 
            this.cboStyles.FormattingEnabled = true;
            this.cboStyles.Location = new System.Drawing.Point(20, 225);
            this.cboStyles.Name = "cboStyles";
            this.cboStyles.Size = new System.Drawing.Size(132, 21);
            this.cboStyles.TabIndex = 33;
            this.cboStyles.SelectedIndexChanged += new System.EventHandler(this.cboStyles_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 206);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Styles with list templates";
            // 
            // chkH5Reset
            // 
            this.chkH5Reset.AutoSize = true;
            this.chkH5Reset.Location = new System.Drawing.Point(438, 166);
            this.chkH5Reset.Name = "chkH5Reset";
            this.chkH5Reset.Size = new System.Drawing.Size(15, 14);
            this.chkH5Reset.TabIndex = 44;
            this.chkH5Reset.UseVisualStyleBackColor = true;
            // 
            // chkH4Reset
            // 
            this.chkH4Reset.AutoSize = true;
            this.chkH4Reset.Location = new System.Drawing.Point(438, 136);
            this.chkH4Reset.Name = "chkH4Reset";
            this.chkH4Reset.Size = new System.Drawing.Size(15, 14);
            this.chkH4Reset.TabIndex = 43;
            this.chkH4Reset.UseVisualStyleBackColor = true;
            // 
            // chkH3Reset
            // 
            this.chkH3Reset.AutoSize = true;
            this.chkH3Reset.Location = new System.Drawing.Point(438, 106);
            this.chkH3Reset.Name = "chkH3Reset";
            this.chkH3Reset.Size = new System.Drawing.Size(15, 14);
            this.chkH3Reset.TabIndex = 42;
            this.chkH3Reset.UseVisualStyleBackColor = true;
            // 
            // chkH2Reset
            // 
            this.chkH2Reset.AutoSize = true;
            this.chkH2Reset.Location = new System.Drawing.Point(438, 76);
            this.chkH2Reset.Name = "chkH2Reset";
            this.chkH2Reset.Size = new System.Drawing.Size(15, 14);
            this.chkH2Reset.TabIndex = 41;
            this.chkH2Reset.UseVisualStyleBackColor = true;
            // 
            // chkH1Reset
            // 
            this.chkH1Reset.AutoSize = true;
            this.chkH1Reset.Enabled = false;
            this.chkH1Reset.Location = new System.Drawing.Point(438, 46);
            this.chkH1Reset.Name = "chkH1Reset";
            this.chkH1Reset.Size = new System.Drawing.Size(15, 14);
            this.chkH1Reset.TabIndex = 40;
            this.chkH1Reset.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(430, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "Reset";
            // 
            // cboPreset
            // 
            this.cboPreset.FormattingEnabled = true;
            this.cboPreset.Location = new System.Drawing.Point(178, 225);
            this.cboPreset.Name = "cboPreset";
            this.cboPreset.Size = new System.Drawing.Size(158, 21);
            this.cboPreset.TabIndex = 46;
            this.cboPreset.SelectedIndexChanged += new System.EventHandler(this.cboPreset_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(179, 206);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 47;
            this.label15.Text = "Presets";
            // 
            // frmNumbering
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 262);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cboPreset);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.chkH5Reset);
            this.Controls.Add(this.chkH4Reset);
            this.Controls.Add(this.chkH3Reset);
            this.Controls.Add(this.chkH2Reset);
            this.Controls.Add(this.chkH1Reset);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboStyles);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.chkH5Com);
            this.Controls.Add(this.tbxH5);
            this.Controls.Add(this.chkH5);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.chkH4Com);
            this.Controls.Add(this.chkH3Com);
            this.Controls.Add(this.chkH2Com);
            this.Controls.Add(this.chkH1Com);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbxH4);
            this.Controls.Add(this.tbxH3);
            this.Controls.Add(this.tbxH2);
            this.Controls.Add(this.tbxH1);
            this.Controls.Add(this.chkH4);
            this.Controls.Add(this.chkH3);
            this.Controls.Add(this.chkH2);
            this.Controls.Add(this.chkH1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmNumbering";
            this.Text = "Numbering";
            this.Load += new System.EventHandler(this.frmNumbering_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton rbnH1C1;
        private System.Windows.Forms.RadioButton rbnH1C2;
        private System.Windows.Forms.RadioButton rbnH1C3;
        private System.Windows.Forms.RadioButton rbnH1C4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbnH1C5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbnH2C5;
        private System.Windows.Forms.RadioButton rbnH2C1;
        private System.Windows.Forms.RadioButton rbnH2C4;
        private System.Windows.Forms.RadioButton rbnH2C3;
        private System.Windows.Forms.RadioButton rbnH2C2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbnH3C5;
        private System.Windows.Forms.RadioButton rbnH3C1;
        private System.Windows.Forms.RadioButton rbnH3C4;
        private System.Windows.Forms.RadioButton rbnH3C3;
        private System.Windows.Forms.RadioButton rbnH3C2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RadioButton rbnH4C5;
        private System.Windows.Forms.RadioButton rbnH4C1;
        private System.Windows.Forms.RadioButton rbnH4C4;
        private System.Windows.Forms.RadioButton rbnH4C3;
        private System.Windows.Forms.RadioButton rbnH4C2;
        private System.Windows.Forms.CheckBox chkH1;
        private System.Windows.Forms.CheckBox chkH2;
        private System.Windows.Forms.CheckBox chkH3;
        private System.Windows.Forms.CheckBox chkH4;
        private CustomTaskPanes.CustomTextBox tbxH1;
        private CustomTaskPanes.CustomTextBox tbxH2;
        private CustomTaskPanes.CustomTextBox tbxH3;
        private CustomTaskPanes.CustomTextBox tbxH4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkH1Com;
        private System.Windows.Forms.CheckBox chkH2Com;
        private System.Windows.Forms.CheckBox chkH3Com;
        private System.Windows.Forms.CheckBox chkH4Com;
        private System.Windows.Forms.CheckBox chkH5Com;
        private CustomTaskPanes.CustomTextBox tbxH5;
        private System.Windows.Forms.CheckBox chkH5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rbnH5C5;
        private System.Windows.Forms.RadioButton rbnH5C1;
        private System.Windows.Forms.RadioButton rbnH5C4;
        private System.Windows.Forms.RadioButton rbnH5C3;
        private System.Windows.Forms.RadioButton rbnH5C2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnClose;
        private CustomTaskPanes.CustomComboBox cboStyles;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkH5Reset;
        private System.Windows.Forms.CheckBox chkH4Reset;
        private System.Windows.Forms.CheckBox chkH3Reset;
        private System.Windows.Forms.CheckBox chkH2Reset;
        private System.Windows.Forms.CheckBox chkH1Reset;
        private System.Windows.Forms.Label label14;
        private CustomTaskPanes.CustomComboBox cboPreset;
        private System.Windows.Forms.Label label15;
    }
}