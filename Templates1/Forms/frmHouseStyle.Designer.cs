﻿namespace Templates1.Forms
{
    partial class frmHouseStyle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkLevel1 = new System.Windows.Forms.CheckBox();
            this.chkLevel2 = new System.Windows.Forms.CheckBox();
            this.chkLevel3 = new System.Windows.Forms.CheckBox();
            this.chkLevel4 = new System.Windows.Forms.CheckBox();
            this.chkLevel5 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbLevel1AC = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbnStyleA = new System.Windows.Forms.RadioButton();
            this.rbnStyleB = new System.Windows.Forms.RadioButton();
            this.rbnStyleC = new System.Windows.Forms.RadioButton();
            this.rtbx1 = new CustomTaskPanes.CustomRichTextBox();
            this.rtbx2 = new CustomTaskPanes.CustomRichTextBox();
            this.rtbx3 = new CustomTaskPanes.CustomRichTextBox();
            this.rtbx4 = new CustomTaskPanes.CustomRichTextBox();
            this.rtbx5 = new CustomTaskPanes.CustomRichTextBox();
            this.chkCourtDocument = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkLevel1
            // 
            this.chkLevel1.AutoSize = true;
            this.chkLevel1.Location = new System.Drawing.Point(58, 56);
            this.chkLevel1.Name = "chkLevel1";
            this.chkLevel1.Size = new System.Drawing.Size(61, 17);
            this.chkLevel1.TabIndex = 0;
            this.chkLevel1.Text = "Level 1";
            this.chkLevel1.UseVisualStyleBackColor = true;
            this.chkLevel1.CheckedChanged += new System.EventHandler(this.chkLevel1_CheckedChanged);
            // 
            // chkLevel2
            // 
            this.chkLevel2.AutoSize = true;
            this.chkLevel2.Location = new System.Drawing.Point(58, 79);
            this.chkLevel2.Name = "chkLevel2";
            this.chkLevel2.Size = new System.Drawing.Size(61, 17);
            this.chkLevel2.TabIndex = 1;
            this.chkLevel2.Text = "Level 2";
            this.chkLevel2.UseVisualStyleBackColor = true;
            this.chkLevel2.CheckedChanged += new System.EventHandler(this.chkLevel2_CheckedChanged);
            // 
            // chkLevel3
            // 
            this.chkLevel3.AutoSize = true;
            this.chkLevel3.Location = new System.Drawing.Point(58, 102);
            this.chkLevel3.Name = "chkLevel3";
            this.chkLevel3.Size = new System.Drawing.Size(61, 17);
            this.chkLevel3.TabIndex = 2;
            this.chkLevel3.Text = "Level 3";
            this.chkLevel3.UseVisualStyleBackColor = true;
            this.chkLevel3.CheckedChanged += new System.EventHandler(this.chkLevel3_CheckedChanged);
            // 
            // chkLevel4
            // 
            this.chkLevel4.AutoSize = true;
            this.chkLevel4.Location = new System.Drawing.Point(58, 125);
            this.chkLevel4.Name = "chkLevel4";
            this.chkLevel4.Size = new System.Drawing.Size(61, 17);
            this.chkLevel4.TabIndex = 3;
            this.chkLevel4.Text = "Level 4";
            this.chkLevel4.UseVisualStyleBackColor = true;
            this.chkLevel4.CheckedChanged += new System.EventHandler(this.chkLevel4_CheckedChanged);
            // 
            // chkLevel5
            // 
            this.chkLevel5.AutoSize = true;
            this.chkLevel5.Location = new System.Drawing.Point(58, 148);
            this.chkLevel5.Name = "chkLevel5";
            this.chkLevel5.Size = new System.Drawing.Size(61, 17);
            this.chkLevel5.TabIndex = 4;
            this.chkLevel5.Text = "Level 5";
            this.chkLevel5.UseVisualStyleBackColor = true;
            this.chkLevel5.CheckedChanged += new System.EventHandler(this.chkLevel5_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Bold";
            // 
            // chbLevel1AC
            // 
            this.chbLevel1AC.AutoSize = true;
            this.chbLevel1AC.Location = new System.Drawing.Point(23, 57);
            this.chbLevel1AC.Name = "chbLevel1AC";
            this.chbLevel1AC.Size = new System.Drawing.Size(15, 14);
            this.chbLevel1AC.TabIndex = 6;
            this.chbLevel1AC.UseVisualStyleBackColor = true;
            this.chbLevel1AC.CheckedChanged += new System.EventHandler(this.chbLevel1AC_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "All caps";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(361, 214);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(77, 28);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbnStyleC);
            this.panel1.Controls.Add(this.rbnStyleB);
            this.panel1.Controls.Add(this.rbnStyleA);
            this.panel1.Location = new System.Drawing.Point(15, 199);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 43);
            this.panel1.TabIndex = 9;
            // 
            // rbnStyleA
            // 
            this.rbnStyleA.AutoSize = true;
            this.rbnStyleA.Location = new System.Drawing.Point(4, 14);
            this.rbnStyleA.Name = "rbnStyleA";
            this.rbnStyleA.Size = new System.Drawing.Size(58, 17);
            this.rbnStyleA.TabIndex = 0;
            this.rbnStyleA.TabStop = true;
            this.rbnStyleA.Text = "Style A";
            this.rbnStyleA.UseVisualStyleBackColor = true;
            this.rbnStyleA.CheckedChanged += new System.EventHandler(this.rbnStyleA_CheckedChanged);
            // 
            // rbnStyleB
            // 
            this.rbnStyleB.AutoSize = true;
            this.rbnStyleB.Location = new System.Drawing.Point(78, 14);
            this.rbnStyleB.Name = "rbnStyleB";
            this.rbnStyleB.Size = new System.Drawing.Size(58, 17);
            this.rbnStyleB.TabIndex = 1;
            this.rbnStyleB.TabStop = true;
            this.rbnStyleB.Text = "Style B";
            this.rbnStyleB.UseVisualStyleBackColor = true;
            this.rbnStyleB.CheckedChanged += new System.EventHandler(this.rbnStyleB_CheckedChanged);
            // 
            // rbnStyleC
            // 
            this.rbnStyleC.AutoSize = true;
            this.rbnStyleC.Location = new System.Drawing.Point(154, 14);
            this.rbnStyleC.Name = "rbnStyleC";
            this.rbnStyleC.Size = new System.Drawing.Size(58, 17);
            this.rbnStyleC.TabIndex = 2;
            this.rbnStyleC.TabStop = true;
            this.rbnStyleC.Text = "Style C";
            this.rbnStyleC.UseVisualStyleBackColor = true;
            this.rbnStyleC.CheckedChanged += new System.EventHandler(this.rbnStyleC_CheckedChanged);
            // 
            // rtbx1
            // 
            this.rtbx1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbx1.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbx1.Location = new System.Drawing.Point(139, 52);
            this.rtbx1.Name = "rtbx1";
            this.rtbx1.Size = new System.Drawing.Size(272, 25);
            this.rtbx1.TabIndex = 10;
            this.rtbx1.Text = "1.  Lorem ipsum doror set amet qui non.";
            // 
            // rtbx2
            // 
            this.rtbx2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbx2.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbx2.Location = new System.Drawing.Point(139, 76);
            this.rtbx2.Name = "rtbx2";
            this.rtbx2.Size = new System.Drawing.Size(272, 25);
            this.rtbx2.TabIndex = 11;
            this.rtbx2.Text = "1.1.  Lorem ipsum deror set amet qui non.";
            // 
            // rtbx3
            // 
            this.rtbx3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbx3.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbx3.Location = new System.Drawing.Point(139, 99);
            this.rtbx3.Name = "rtbx3";
            this.rtbx3.Size = new System.Drawing.Size(272, 25);
            this.rtbx3.TabIndex = 12;
            this.rtbx3.Text = "     1.1.1  Lorem ipsum doror set amet qui.";
            // 
            // rtbx4
            // 
            this.rtbx4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbx4.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbx4.Location = new System.Drawing.Point(139, 122);
            this.rtbx4.Name = "rtbx4";
            this.rtbx4.Size = new System.Drawing.Size(272, 25);
            this.rtbx4.TabIndex = 13;
            this.rtbx4.Text = "          (a.)  Lorem ipsum doror set amet.";
            // 
            // rtbx5
            // 
            this.rtbx5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbx5.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbx5.Location = new System.Drawing.Point(139, 145);
            this.rtbx5.Name = "rtbx5";
            this.rtbx5.Size = new System.Drawing.Size(272, 25);
            this.rtbx5.TabIndex = 14;
            this.rtbx5.Text = "               (i.)  Lorem ipsum doror set amet.";
            // 
            // chkCourtDocument
            // 
            this.chkCourtDocument.AutoSize = true;
            this.chkCourtDocument.Location = new System.Drawing.Point(19, 178);
            this.chkCourtDocument.Name = "chkCourtDocument";
            this.chkCourtDocument.Size = new System.Drawing.Size(101, 17);
            this.chkCourtDocument.TabIndex = 15;
            this.chkCourtDocument.Text = "Court document";
            this.chkCourtDocument.UseVisualStyleBackColor = true;
            // 
            // frmHouseStyle
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 254);
            this.Controls.Add(this.chkCourtDocument);
            this.Controls.Add(this.rtbx5);
            this.Controls.Add(this.rtbx4);
            this.Controls.Add(this.rtbx3);
            this.Controls.Add(this.rtbx2);
            this.Controls.Add(this.rtbx1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chbLevel1AC);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkLevel5);
            this.Controls.Add(this.chkLevel4);
            this.Controls.Add(this.chkLevel3);
            this.Controls.Add(this.chkLevel2);
            this.Controls.Add(this.chkLevel1);
            this.Name = "frmHouseStyle";
            this.Text = "Re-style";
            this.Load += new System.EventHandler(this.frmHouseStyle_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkLevel1;
        private System.Windows.Forms.CheckBox chkLevel2;
        private System.Windows.Forms.CheckBox chkLevel3;
        private System.Windows.Forms.CheckBox chkLevel4;
        private System.Windows.Forms.CheckBox chkLevel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbLevel1AC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbnStyleC;
        private System.Windows.Forms.RadioButton rbnStyleB;
        private System.Windows.Forms.RadioButton rbnStyleA;
        private CustomTaskPanes.CustomRichTextBox rtbx1;
        private CustomTaskPanes.CustomRichTextBox rtbx2;
        private CustomTaskPanes.CustomRichTextBox rtbx3;
        private CustomTaskPanes.CustomRichTextBox rtbx4;
        private CustomTaskPanes.CustomRichTextBox rtbx5;
        private System.Windows.Forms.CheckBox chkCourtDocument;
    }
}