﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace Templates1.Forms
{
    public partial class frmPrinters : Form
    {
        private ADSearcher adsearcher;
        private ADPrinters adprinters;
        private ADPrinter adprinter;
        private ListViewItem listviewitem;
        private Color colour;
        private PrintTray printtray;
        public frmPrinters()
        {
            InitializeComponent();
            this.Shown += new EventHandler(frm_Shown);

        }

        private void frm_Shown(object sender, EventArgs e)
        {
            this.adprinters = adsearcher.GetPrinters();
        }
        
        private void frmPrinters_Load(object sender, EventArgs e)
        {
            adsearcher = new ADSearcher("");
            adsearcher.PrinterAdded += new ADSearcher.PrinterAddedHandler(ProcessPrinter);
            SetColumns();
            SetComboBox();
        }

        private void SetComboBox()
        {
            foreach (MediaType m in new MediaTypes())
                cboPaperType.Items.Add(m.Type);
        }
        
        private void ProcessPrinter(ADPrinter printer)
        {
            //Boolean HasTrayType = false;
            ListViewItem lvi = new ListViewItem(printer.Name);
            if(printer.PrintTrays != null)
            { 
                foreach(PrintTray tray in printer.PrintTrays)
                    if (tray.TrayType != String.Empty)
                    {
                        //HasTrayType = true;
                        break;
                    }
            }
            if (!printer.Error)
                if (printer.IsNotInXMLFile)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = Color.Olive;
            else
                lvi.BackColor = Color.PaleVioletRed;
            
            lvwPrinters.Items.Add(lvi);
            this.Refresh();
        }

        private void SetColumns()
        {
            lvwPrinters.Columns.Add("Name");
            lvwPrinters.Columns[0].Width = lvwPrinters.Width - 20;
            lvwPrintersXML.Columns.Add("Tray name");
            lvwPrintersXML.Columns.Add("Bin code");
            lvwPrintersXML.Columns.Add("Paper type");
            foreach (ColumnHeader col in lvwPrintersXML.Columns)
                col.Width = (lvwPrintersXML.Width - 20) / lvwPrintersXML.Columns.Count;
            lvwPrintersActual.Columns.Add("Tray name");
            lvwPrintersActual.Columns.Add("Bin code");
            lvwPrintersActual.Columns.Add("Paper type");
            foreach (ColumnHeader col in lvwPrintersActual.Columns)
                col.Width = (lvwPrintersActual.Width - 20) / lvwPrintersActual.Columns.Count;
        }

        private void lvwPrinters_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvwPrinters.SelectedItems.Count == 0)
            {
                return;
            }
            if (listviewitem != null)
                listviewitem.BackColor = colour;
            listviewitem = lvwPrinters.SelectedItems[0];
            colour = listviewitem.BackColor;
            listviewitem.BackColor = Color.LightBlue;
            lvwPrintersXML.Items.Clear();
            
            IEnumerable<ADPrinter> results = from d in this.adprinters where d.Name == lvwPrinters.SelectedItems[0].Text select d;
            if (results.Count() == 0)
            {
                this.adprinter = new ADPrinter(lvwPrinters.SelectedItems[0].Text);
                return;
            }
            this.adprinter = results.ToList()[0];
            PresentTrays(lvwPrintersXML, false);
            if (adprinter.IsConnected())
                PresentTrays(lvwPrintersActual, true);
            tbxDeviceInfo.Text = this.adprinter.DeviceInfo;
            lblPrinterName.Text = this.adprinter.Name;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            lblConnecting.Text = "Connecting...";
            Boolean isConnected = false;
            if(!adprinter.IsConnected())
                isConnected = adprinter.Connect();
            adprinter = (ADPrinter)adprinter.GetPrinterDetails(isConnected);
            //adprinter.PrintTrays = adprinter.GetTraysFromSource(new System.Drawing.Printing.PrintDocument(), isConnected);
            PresentTrays(lvwPrintersActual, true);
            lblConnecting.Text = isConnected ?  "Connection success": "Connection failed";
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            if (adprinter.IsConnected())
            {
                lvwPrintersActual.Items.Clear();
                adprinter.RemovePrinter();
            }
        }

        private void PresentTrays(ListView lvw, Boolean useSource)
        {
            lvw.Items.Clear();
            foreach (PrintTray tray in this.adprinter.PrintTrays)
            {
                ListViewItem lvi = new ListViewItem(tray.TrayName);
                if(useSource)
                { 
                    lvi.SubItems.Add(tray.Source.RawKind.ToString());
                    lvi.SubItems.Add(tray.TrayType);
                }
                else
                {
                    lvi.SubItems.Add(tray.BinID.ToString());
                    lvi.SubItems.Add(tray.TrayType);
                }
                lvw.Items.Add(lvi);
            }

            cboTrayID.Items.Clear();
            foreach(PrintTray tray in this.adprinter.PrintTrays)
            {
                cboTrayID.Items.Add(tray.TrayName);
            }
        }

        private void cboTrayID_SelectedIndexChanged(object sender, EventArgs e)
        {
            IEnumerable<PrintTray> trays = from d in this.adprinter.PrintTrays where d.TrayName == cboTrayID.Text select d;
            if (trays.Count() == 0)
                return;
            this.printtray = trays.ToList()[0];
            tbxBinCode.Text = this.printtray.BinID.ToString();
            cboPaperType.Text = this.printtray.TrayType;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            IEnumerable<PrintTray> trays = from d in this.adprinter.PrintTrays where d.TrayName == cboTrayID.Text select d;
            if (trays.Count() == 0)
                return;
            PrintTray tray = trays.ToList()[0];
            tray.GetSetTrayType(adprinter.DeviceInfo, adprinter.Name, tbxBinCode.Text, cboPaperType.Text, tray.TrayName);
            tray.BinID = Convert.ToInt16(tbxBinCode.Text);
            tray.TrayType = cboPaperType.Text;
            PresentTrays(lvwPrintersXML, false);
            if (adprinter.IsConnected())
                PresentTrays(lvwPrintersActual, true);
            else
                lvwPrintersActual.Items.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (listviewitem.BackColor == Color.Olive)
            {
                System.Windows.Forms.MessageBox.Show("That printer already exists in the XML file.", Globals.ThisAddIn.APP_NAME1);
                return;
            }
            listviewitem.BackColor = Color.Olive;
            PrintingData pd = new PrintingData();
            pd.AddPrinterToXML(this.adprinter);
            PresentTrays(lvwPrintersXML, false);
            if (adprinter.IsConnected())
                PresentTrays(lvwPrintersActual, true);
            else
                lvwPrintersActual.Items.Clear();
        }

        private void lvwPrintersActual_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cboTrayID.Text = lvwPrintersActual.SelectedItems[0].SubItems[0].Text;
            this.tbxBinCode.Text = lvwPrintersActual.SelectedItems[0].SubItems[1].Text;
            this.cboPaperType.Text = lvwPrintersActual.SelectedItems[0].SubItems[2].Text;
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            int i = 0;
            clsExcel xl = new clsExcel();
            xl.LoadExcel();
            Excel.Worksheet wks = xl.CreateWorkBookWithSheet();
            foreach(ADPrinter adp in this.adprinters)
            {
                Printer p = (Printer)adp;
                foreach(PrintTray t in p.PrintTrays)
                {
                    try
                    {
                        i++;
                        wks.Cells[i, 1] = adp.Name;
                        wks.Cells[i, 2] = adp.IsNotInXMLFile ? "Not in XML" : "Is in XML";
                        wks.Cells[i, 3] = t.TrayName;
                        if (t.TrayType != null)
                            wks.Cells[i, 4] = t.TrayType;
                        Excel.Range rng = wks.Rows[i];
                        if (adp.IsNotInXMLFile)
                            rng.DisplayFormat.Interior.Color = Color.Orange;
                        else
                            rng.DisplayFormat.Interior.Color = Color.Olive;
                    }
                    catch { }
                }
            }
        }
    }
}
