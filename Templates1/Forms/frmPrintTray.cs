﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Templates1.Forms
{
    public partial class frmPrintTray : Form
    {
        private Printer printer;
        private String jobType;
        public PrintTray Printtray {get; set;}
        public Boolean JobCancelled { get; set; }
        public frmPrintTray(Printer printer, String jobType)
        {
            this.printer = printer;
            this.jobType = jobType;
            InitializeComponent();
            
        }

        private void frmPrintTray_Load(object sender, EventArgs e)
        {
            foreach (PrintTray tray in printer.PrintTrays)
                this.cboTrays.Items.Add(tray.TrayName);
            this.lblJobType.Text = jobType;
        }

        private void cboTrays_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboTrays.SelectedItem == null)
                return;
            IEnumerable<PrintTray> result = from d in printer.PrintTrays where d.TrayName == cboTrays.Text select d;
            if (result.Count() > 0)
                Printtray = result.ToList()[0];
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            JobCancelled = true;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            JobCancelled = false;
            this.Close();
        }
    }
}
