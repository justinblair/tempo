﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;


namespace Templates1.Forms
{
    class StylesUITreeView: TreeView
    {
        private Document document;
        private StyleUI styleUI;
        private StyleUIClone prefStyleUI = null;

        internal StylesUITreeView()
        {
            this.AfterCheck += new TreeViewEventHandler(StylesUITreeView_AfterCheck);
            this.CheckBoxes = true;
        }

        void StylesUITreeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            StylesUITreeNode node = (StylesUITreeNode)e.Node;
            if (node.Nodes.Count > 0)
                foreach (StylesUITreeNode nod in node.Nodes)
                {
                    nod.Checked = node.Checked;
                }
            else
                node.SetVisibility();
        }

        internal void LoadStyles(Document document)
        {
            this.document = document;
            
            if (this.styleUI == null)
            {
                StyleUI styleui = new StyleUI();
                styleui.Document = document;
                styleui.PrefStyleUI = prefStyleUI;
                styleui.BuildStyleGroups();
                this.styleUI = styleui;
            }
            ApplyGroupings();
            
        }

        internal void LoadStyles()
        {
            ApplyGroupings();
        }

        internal void RefreshStyles()
        {
            StyleUI styleui = new StyleUI();
            styleui.Document = document;
            styleui.PrefStyleUI = prefStyleUI;
            styleui.BuildStyleGroups();
            this.styleUI = styleui;
            ApplyGroupings();
        }

        private void ApplyGroupings()
        {
            this.Nodes.Clear();
            styleUI.StyleGroupings = styleUI.StyleGroupings.OrderBy(c => c.Priority).ToList();
            foreach (StyleGrouping grouping in this.styleUI.StyleGroupings)
            {
                StylesUITreeNode node = new StylesUITreeNode(grouping);
                node.Text = grouping.Name;
                node = node.LoadStyles(document);
                this.Nodes.Add(node);
            }
            this.SelectedNode = this.Nodes[0];
        }

        internal void Save()
        {
            String path = System.IO.Path.Combine(Globals.ThisAddIn.APPDATA, document.Template.Filename + ".xml");
            if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);
            StyleUIClone clone = new StyleUIClone();
            clone.LoadStyleUI(styleUI);

            System.Xml.Serialization.XmlSerializer writer =
            new System.Xml.Serialization.XmlSerializer(typeof(StyleUIClone));
            System.IO.StreamWriter file = new System.IO.StreamWriter(
                path);
            writer.Serialize(file, clone);
            file.Close();

        }

        internal void Load()
        {
            String path = System.IO.Path.Combine(Globals.ThisAddIn.APPDATA, document.Template.Filename + ".xml");
            if (!System.IO.File.Exists(path))
                return;
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(StyleUIClone));
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            this.prefStyleUI = (StyleUIClone)reader.Deserialize(file);
            file.Close();
            foreach (StyleGroupingClone group in this.prefStyleUI.StyleGroupingsClone)
            {
                StyleGrouping uigroup = styleUI.StyleGroupings.FindLast(c => c.Name == group.Name);
                uigroup.Priority = group.Priority;
                foreach (StyleClone style in group.Styles)
                {
                    Style uistyle = uigroup.Styles.FindLast(c => c.Name == style.Name);
                    uistyle.Priority = style.Priority;
                    uistyle.Visibility = style.Visibility;
                    try
                    {
                        Word.Style wdStyle = document.WordDocument.Styles[style.Name];
                        wdStyle.Priority = style.Priority;
                        wdStyle.Visibility = style.Visibility;
                    }
                    catch { }
                }
            }
            
            this.LoadStyles();
        }
    }

    class StylesUITreeNode: TreeNode
    {
        private StyleGrouping grouping;
        private Style style;
        private Document document;
        internal StylesUITreeNode(StyleGrouping grouping)
        {
            this.grouping = grouping;
        }

        internal StylesUITreeNode(Style style)
        {
            this.style = style;
        }

        internal StylesUITreeNode LoadStyles(Document document)
        {
            this.document = document;
            foreach(Style style in grouping.Styles)
            {
                StylesUITreeNode node = new StylesUITreeNode(style);
                node.Text = style.Name;
                node.Checked = !style.Visibility;
                
                this.Nodes.Add(node);
            }

            if (grouping.Custom)
                this.Expand();
            else
                this.Collapse();

            return this;
        }

        internal void ChangePriority(Boolean up)
        {
            Boolean change = false;
            if (up && this.grouping.Priority > 1)
            {
                this.grouping.Priority = this.grouping.Priority - 1;
                change = true;
            }
            if(!up && this.grouping.Priority<100)
            {
                this.grouping.Priority = this.grouping.Priority + 1;
                change = true;
            }

            if (!change)
                return;

            foreach (Style style in this.grouping.Styles)
            {
                style.Priority = grouping.Priority;
                style.WordStyle.Priority = grouping.Priority;
            }

         }

        internal void SetVisibility()
        {
            try
            {
                this.style.WordStyle.Visibility = !this.Checked;
                this.style.Visibility = !this.Checked;
            }
            catch { }
        }
    }
}
