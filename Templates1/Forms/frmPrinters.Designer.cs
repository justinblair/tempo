﻿namespace Templates1.Forms
{
    partial class frmPrinters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvwPrinters = new System.Windows.Forms.ListView();
            this.lvwPrintersXML = new System.Windows.Forms.ListView();
            this.lvwPrintersActual = new System.Windows.Forms.ListView();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.lblConnecting = new System.Windows.Forms.Label();
            this.cboPaperType = new CustomTaskPanes.CustomComboBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cboTrayID = new CustomTaskPanes.CustomComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxBinCode = new CustomTaskPanes.CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxDeviceInfo = new CustomTaskPanes.CustomTextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblPrinterName = new System.Windows.Forms.Label();
            this.btnExcel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvwPrinters
            // 
            this.lvwPrinters.FullRowSelect = true;
            this.lvwPrinters.Location = new System.Drawing.Point(25, 40);
            this.lvwPrinters.MultiSelect = false;
            this.lvwPrinters.Name = "lvwPrinters";
            this.lvwPrinters.Size = new System.Drawing.Size(328, 393);
            this.lvwPrinters.TabIndex = 0;
            this.lvwPrinters.UseCompatibleStateImageBehavior = false;
            this.lvwPrinters.View = System.Windows.Forms.View.Details;
            this.lvwPrinters.SelectedIndexChanged += new System.EventHandler(this.lvwPrinters_SelectedIndexChanged);
            // 
            // lvwPrintersXML
            // 
            this.lvwPrintersXML.FullRowSelect = true;
            this.lvwPrintersXML.Location = new System.Drawing.Point(384, 40);
            this.lvwPrintersXML.Name = "lvwPrintersXML";
            this.lvwPrintersXML.Size = new System.Drawing.Size(328, 162);
            this.lvwPrintersXML.TabIndex = 1;
            this.lvwPrintersXML.UseCompatibleStateImageBehavior = false;
            this.lvwPrintersXML.View = System.Windows.Forms.View.Details;
            // 
            // lvwPrintersActual
            // 
            this.lvwPrintersActual.FullRowSelect = true;
            this.lvwPrintersActual.Location = new System.Drawing.Point(384, 268);
            this.lvwPrintersActual.Name = "lvwPrintersActual";
            this.lvwPrintersActual.Size = new System.Drawing.Size(328, 165);
            this.lvwPrintersActual.TabIndex = 2;
            this.lvwPrintersActual.UseCompatibleStateImageBehavior = false;
            this.lvwPrintersActual.View = System.Windows.Forms.View.Details;
            this.lvwPrintersActual.SelectedIndexChanged += new System.EventHandler(this.lvwPrintersActual_SelectedIndexChanged);
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(385, 441);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(100, 28);
            this.btnConnect.TabIndex = 3;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(493, 441);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(100, 28);
            this.btnDisconnect.TabIndex = 4;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // lblConnecting
            // 
            this.lblConnecting.AutoSize = true;
            this.lblConnecting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblConnecting.Location = new System.Drawing.Point(607, 443);
            this.lblConnecting.Name = "lblConnecting";
            this.lblConnecting.Size = new System.Drawing.Size(0, 13);
            this.lblConnecting.TabIndex = 5;
            // 
            // cboPaperType
            // 
            this.cboPaperType.FormattingEnabled = true;
            this.cboPaperType.Location = new System.Drawing.Point(581, 226);
            this.cboPaperType.Name = "cboPaperType";
            this.cboPaperType.Size = new System.Drawing.Size(131, 21);
            this.cboPaperType.TabIndex = 6;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(724, 225);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(26, 20);
            this.btnSubmit.TabIndex = 7;
            this.btnSubmit.Text = "!";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Printers in Active Directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Trays in XML file";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(381, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Trays at source";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(581, 208);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Paper type";
            // 
            // cboTrayID
            // 
            this.cboTrayID.FormattingEnabled = true;
            this.cboTrayID.Location = new System.Drawing.Point(387, 224);
            this.cboTrayID.Name = "cboTrayID";
            this.cboTrayID.Size = new System.Drawing.Size(106, 21);
            this.cboTrayID.TabIndex = 12;
            this.cboTrayID.SelectedIndexChanged += new System.EventHandler(this.cboTrayID_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(387, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Tray ID";
            // 
            // tbxBinCode
            // 
            this.tbxBinCode.Location = new System.Drawing.Point(506, 226);
            this.tbxBinCode.Name = "tbxBinCode";
            this.tbxBinCode.Size = new System.Drawing.Size(62, 20);
            this.tbxBinCode.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(506, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Bin ID";
            // 
            // tbxDeviceInfo
            // 
            this.tbxDeviceInfo.Location = new System.Drawing.Point(25, 440);
            this.tbxDeviceInfo.Name = "tbxDeviceInfo";
            this.tbxDeviceInfo.Size = new System.Drawing.Size(327, 20);
            this.tbxDeviceInfo.TabIndex = 16;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(358, 107);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(24, 24);
            this.btnAdd.TabIndex = 17;
            this.btnAdd.Text = ">";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblPrinterName
            // 
            this.lblPrinterName.AutoSize = true;
            this.lblPrinterName.Location = new System.Drawing.Point(29, 472);
            this.lblPrinterName.Name = "lblPrinterName";
            this.lblPrinterName.Size = new System.Drawing.Size(35, 13);
            this.lblPrinterName.TabIndex = 18;
            this.lblPrinterName.Text = "label7";
            // 
            // btnExcel
            // 
            this.btnExcel.Location = new System.Drawing.Point(610, 441);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(100, 28);
            this.btnExcel.TabIndex = 19;
            this.btnExcel.Text = "Output to Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // frmPrinters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 539);
            this.Controls.Add(this.btnExcel);
            this.Controls.Add(this.lblPrinterName);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbxDeviceInfo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxBinCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboTrayID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.cboPaperType);
            this.Controls.Add(this.lblConnecting);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.lvwPrintersActual);
            this.Controls.Add(this.lvwPrintersXML);
            this.Controls.Add(this.lvwPrinters);
            this.Name = "frmPrinters";
            this.Text = "Printer settings";
            this.Load += new System.EventHandler(this.frmPrinters_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvwPrinters;
        private System.Windows.Forms.ListView lvwPrintersXML;
        private System.Windows.Forms.ListView lvwPrintersActual;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Label lblConnecting;
        private CustomTaskPanes.CustomComboBox cboPaperType;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private CustomTaskPanes.CustomComboBox cboTrayID;
        private System.Windows.Forms.Label label5;
        private CustomTaskPanes.CustomTextBox tbxBinCode;
        private System.Windows.Forms.Label label6;
        private CustomTaskPanes.CustomTextBox tbxDeviceInfo;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblPrinterName;
        private System.Windows.Forms.Button btnExcel;
    }
}