﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Templates1.Forms
{
    public partial class fmrSearchContact : Form
    {

        private ADUsers users;
        public ADUser user;
        public fmrSearchContact()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Search();
           
        }

        private void lbxOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADUser user = users[this.lbxOptions.SelectedIndex];
            lblName.Text = user.FullName;
            lblRole.Text = user.Title;
            lblTel.Text = user.TelephoneNumber;
            lblEmail.Text = user.Mail;
            try
            {
                String photoPath = @"\\Dc1-iaapp01\Employee Pics\" + user.Login + ".jpg";
                if (!System.IO.File.Exists(photoPath))
                    //photoPath = photoPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), 
                    // @"Images\photonotavailable.jpg");
                    photoPath = System.Environment.GetEnvironmentVariable("PROGRAMFILES") + @"\Mishcon De Reya\Mishcon de Reya Templates\Images\photonotavailable.jpg";
                pbxPhoto.Load(photoPath);
                   
            }
            catch { }
        }

        private void fmrSearchContact_Load(object sender, EventArgs e)
        {
            lblName.Text = "";
            lblRole.Text = "";
            lblTel.Text = "";
            lblEmail.Text = "";
            this.tbxParams.PreviewKeyDown += tbxParams_PreviewKeyDown;
        }

        void tbxParams_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                Search();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if(users!=null)
            { 
            user = users[this.lbxOptions.SelectedIndex];
             this.Close();
            }
        }
        

        private void btnClose_Click(object sender, EventArgs e)
        {
            user = null;
            this.Close();
        }

      private void Search()
        {
            this.lblSearching.Text = "Searching...";
            this.Refresh();
            this.lbxOptions.Items.Clear();
            ADSearcher ads = new ADSearcher(this.tbxParams.Text);
            users = ads.GetUsers();
            foreach (ADUser user in users)
                this.lbxOptions.Items.Add(user.FullName);
            this.lblSearching.Text = "";
            this.Refresh();
        }

      private void tbxParams_TextChanged(object sender, EventArgs e)
      {

      }

        
    }
}
