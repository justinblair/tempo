﻿namespace Templates1.Forms
{
    partial class fmrSearchContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxParams = new CustomTaskPanes.CustomTextBox();
            this.lbxOptions = new System.Windows.Forms.ListBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblTel = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.pbxPhoto = new System.Windows.Forms.PictureBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblSearching = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxParams
            // 
            this.tbxParams.Location = new System.Drawing.Point(20, 47);
            this.tbxParams.Name = "tbxParams";
            this.tbxParams.Size = new System.Drawing.Size(192, 20);
            this.tbxParams.TabIndex = 0;
            this.tbxParams.TextChanged += new System.EventHandler(this.tbxParams_TextChanged);
            // 
            // lbxOptions
            // 
            this.lbxOptions.FormattingEnabled = true;
            this.lbxOptions.Location = new System.Drawing.Point(19, 81);
            this.lbxOptions.Name = "lbxOptions";
            this.lbxOptions.Size = new System.Drawing.Size(192, 147);
            this.lbxOptions.TabIndex = 1;
            this.lbxOptions.SelectedIndexChanged += new System.EventHandler(this.lbxOptions_SelectedIndexChanged);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(333, 101);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(233, 22);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "label1";
            // 
            // lblRole
            // 
            this.lblRole.Location = new System.Drawing.Point(333, 123);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(233, 19);
            this.lblRole.TabIndex = 3;
            this.lblRole.Text = "label1";
            // 
            // lblTel
            // 
            this.lblTel.Location = new System.Drawing.Point(333, 163);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(233, 22);
            this.lblTel.TabIndex = 4;
            this.lblTel.Text = "label1";
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(333, 142);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(233, 21);
            this.lblEmail.TabIndex = 5;
            this.lblEmail.Text = "label1";
            // 
            // pbxPhoto
            // 
            this.pbxPhoto.Location = new System.Drawing.Point(226, 82);
            this.pbxPhoto.Name = "pbxPhoto";
            this.pbxPhoto.Size = new System.Drawing.Size(101, 103);
            this.pbxPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxPhoto.TabIndex = 6;
            this.pbxPhoto.TabStop = false;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(226, 42);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(67, 29);
            this.btnSearch.TabIndex = 7;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(381, 214);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(83, 29);
            this.btnInsert.TabIndex = 8;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(474, 215);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 27);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblSearching
            // 
            this.lblSearching.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblSearching.Location = new System.Drawing.Point(308, 50);
            this.lblSearching.Name = "lblSearching";
            this.lblSearching.Size = new System.Drawing.Size(143, 17);
            this.lblSearching.TabIndex = 10;
            // 
            // fmrSearchContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(567, 262);
            this.Controls.Add(this.lblSearching);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.pbxPhoto);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblTel);
            this.Controls.Add(this.lblRole);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lbxOptions);
            this.Controls.Add(this.tbxParams);
            this.Name = "fmrSearchContact";
            this.Text = "Search for a contact";
            this.Load += new System.EventHandler(this.fmrSearchContact_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTaskPanes.CustomTextBox tbxParams;
        private System.Windows.Forms.ListBox lbxOptions;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.PictureBox pbxPhoto;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblSearching;
    }
}