﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Stylick
{
    internal class XMLHandler
    {
        internal XmlDocument XmlStyles { get; set; }
        internal XmlDocument XmlListTemplates { get; set; }
        internal XmlNamespaceManager NMSStyles { get; set; }
        internal XmlNamespaceManager NMSLists { get; set; }
        
        internal XMLHandler(DocumentHandler doc)
        {
            XmlDocument styles = new XmlDocument();
            styles.Load(doc.StylesFile);
            
            XmlNamespaceManager ns = new XmlNamespaceManager(styles.NameTable);
            ns.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            NMSStyles = ns;

            XmlStyles = styles;

            XmlDocument numbering = new XmlDocument();
            numbering.Load(doc.ListTemplatesFile);
            XmlListTemplates = numbering;

            ns = new XmlNamespaceManager(numbering.NameTable);
            ns.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            NMSLists = ns;

        }

        internal void RemoveStyle(Style sourceStyle, DocumentHandler destDocument)
        {
            XmlNode style = destDocument.XML.XmlStyles.SelectSingleNode("//w:style[w:name/@w:val='" + sourceStyle.Name + "']", NMSStyles);
            if (style != null)
                style.ParentNode.RemoveChild(style);
        }

        internal void CopyStyle(Style sourceStyle, DocumentHandler destDocument, Boolean replace, Boolean includeBaseStyle)
        {
           if(destDocument.Styles.Exists(sourceStyle.Name))
               if(replace)
                    RemoveStyle(sourceStyle, destDocument);
               else
                   return;
            XmlNode newNode = null;
            String newID = String.Empty;
            XmlNode lastNode = null;
            if (sourceStyle.Number != null)
                if (sourceStyle.Number.AbstractNumber != null)
                    if (!destDocument.AbstractNums.Exists(sourceStyle.Number.AbstractNumber.TMPL))
                    {
                        if (sourceStyle.Number != null)
                        {
                            if (sourceStyle.Number.AbstractNumber != null)
                            {
                                newNode = CopyNode(sourceStyle.Number.AbstractNumber.Node, destDocument.XML.XmlListTemplates);
                                lastNode = destDocument.XML.XmlListTemplates.SelectNodes("//w:abstractNum", NMSLists)[destDocument.XML.XmlListTemplates.SelectNodes("//w:abstractNum", NMSLists).Count - 1];
                                newID = (Convert.ToInt16(lastNode.SelectSingleNode("@w:abstractNumId", NMSLists).InnerText) + 1).ToString();
                                newNode.SelectSingleNode("@w:abstractNumId", NMSLists).InnerText = newID;
                                lastNode.ParentNode.InsertAfter(newNode, lastNode);
                            }


                            newNode = CopyNode(sourceStyle.Number.Node, destDocument.XML.XmlListTemplates);
                            lastNode = destDocument.XML.XmlListTemplates.SelectNodes("//w:num", NMSLists)[destDocument.XML.XmlListTemplates.SelectNodes("//w:num", NMSLists).Count - 1];
                            if (newID != String.Empty)
                            {
                                newNode.SelectSingleNode("w:abstractNumId/@w:val", NMSLists).InnerText = newID;
                                newID = (Convert.ToInt16(lastNode.SelectSingleNode("@w:numId", NMSLists).InnerText) + 1).ToString();
                                newNode.SelectSingleNode("@w:numId", NMSLists).InnerText = newID;
                            }
                            lastNode.ParentNode.InsertAfter(newNode, lastNode);
                        }
                    }
                    else
                        newID = destDocument.GetExistingListTemplate(sourceStyle.Number.AbstractNumber.TMPL).NumID;
                
                newNode = CopyNode(sourceStyle.Node, destDocument.XML.XmlStyles);
                XmlNode lastStyleNode = destDocument.XML.XmlStyles.SelectNodes("//w:style", NMSStyles)[destDocument.XML.XmlStyles.SelectNodes("//w:style", NMSStyles).Count - 1];
                XmlNode num = newNode.SelectSingleNode("w:pPr/w:numPr/w:numId", NMSStyles);
                if (num != null && newID != String.Empty)
                    num.SelectSingleNode("@w:val", NMSStyles).InnerText = newID;

                XmlNode priority = newNode.SelectSingleNode("w:uiPriority/@w:val", NMSStyles);
                if(priority!=null)
                {
                    Style pStyle = destDocument.Styles.GetStyle(sourceStyle.Name);
                    if (pStyle != null)
                        priority.InnerText = pStyle.Priority.ToString();
                }
                lastStyleNode.ParentNode.InsertAfter(newNode, lastStyleNode);
            
        }

                
        internal XmlNode CopyNode(XmlNode node, XmlDocument destXML)
        {
            XmlNode newNode = destXML.CreateElement(node.Name, node.NamespaceURI);
            newNode.Prefix = node.Prefix;
            
            newNode.InnerText = node.InnerText;
            foreach(XmlAttribute att in node.Attributes)
            {
                XmlAttribute newAtt = destXML.CreateAttribute(att.Name, att.NamespaceURI);
                att.Prefix = att.Prefix;
                newAtt.InnerText = att.InnerText;
                newNode.Attributes.Append(newAtt);
            }
            foreach (XmlNode child in node.ChildNodes)
                newNode.AppendChild(CopyNode(child, destXML));

            return newNode;
        }
        
        
        internal AbstractNums GetListTemplates()
        {
            AbstractNums absNums = new AbstractNums();
            XmlNodeList ndl = XmlListTemplates.SelectNodes("//w:abstractNum", NMSLists);
            foreach(XmlNode node in ndl)
            {
                AbstractNum num = new AbstractNum(node);
                num.TMPL = node.SelectSingleNode("w:nsid/@w:val", NMSLists).InnerText;
                String absNum = node.SelectSingleNode("@w:abstractNumId", NMSLists).InnerText;
                XmlNode numNode = node.OwnerDocument.SelectSingleNode("//w:num[w:abstractNumId/@w:val='" + absNum + "']/@w:numId", NMSLists);
                if(numNode!=null)
                {
                    num.NumID = numNode.InnerText;
                    absNums.Add(num);
                }
            }
            return absNums;

        }

        internal void GetBaseStyles(ref Styles styles)
        {
            foreach (Style style in styles)
            {
                XmlNode baseStyle = style.Node.SelectSingleNode("w:basedOn", NMSStyles);
                if (baseStyle != null)
                {
                    String baseStyleName = baseStyle.SelectSingleNode("@w:val", NMSStyles).InnerText;
                    IEnumerable<Style> collection = from d in styles where d.Name == baseStyleName select d;
                    foreach (Style baseStl in collection)
                        style.BaseStyle = baseStl;
                }
            }
        }
        
        internal Styles GetStyles()
        {
            Styles styles = new Styles();
            
            XmlNodeList ndl = XmlStyles.SelectNodes("//w:style", NMSStyles);
            foreach(XmlNode node in ndl)
            {
                Style style = new Style(node);
                style.Name = node.SelectSingleNode("w:name/@w:val", NMSStyles).InnerText;

                XmlNode custom = node.SelectSingleNode("@w:customStyle", NMSStyles);
                if (custom != null)
                    style.Custom = custom.InnerText == "1";
                else
                    style.Custom = false;

                XmlNode priority = node.SelectSingleNode("w:uiPriority/@w:val", NMSStyles);
                if (node != null)
                    if (node.InnerText != String.Empty)
                        style.Priority = Convert.ToInt16(node.InnerText);
                    else
                        if (style.Custom)
                            style.Priority = 40;
                        else
                            style.Priority = 99;

                XmlNode num = node.SelectSingleNode("w:pPr/w:numPr", NMSStyles);
                if (num != null)
                {
                    XmlNode level = num.SelectSingleNode("w:ilvl/@w:val", NMSStyles);
                    if(level != null)
                        style.Level = Convert.ToInt16(level.InnerText);
                    XmlNode numNode = num.SelectSingleNode("w:numId/@w:val", NMSStyles);
                    if (numNode != null)
                    {
                        style.NumID = Convert.ToInt16(numNode.InnerText);

                        num = XmlListTemplates.SelectSingleNode("//w:num[@w:numId='" + style.NumID + "']", NMSLists);
                        if (num != null)
                        {
                            Num numberList = new Num(num);

                            numberList.AbstractID = Convert.ToInt16(num.SelectSingleNode("w:abstractNumId/@w:val", NMSLists).InnerText);

                            style.Number = numberList;

                            num = XmlListTemplates.SelectSingleNode("//w:abstractNum[@w:abstractNumId='" + style.Number.AbstractID + "']", NMSLists);

                            AbstractNum abs = new AbstractNum(num);
                            abs.TMPL = num.SelectSingleNode("w:nsid/@w:val", NMSLists).InnerText;

                            abs.NumID = style.NumID.ToString();
               
                            style.Number.AbstractNumber = abs;
                        }
                        
                    }

                }

  
                styles.Add(style);

            }

            
            return styles;

        }
    }
}
