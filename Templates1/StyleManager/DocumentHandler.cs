﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml;


namespace Stylick
{
    
    internal class StyleManger
    {
        private DocumentHandler sourceDocument;
        private static DocumentHandler destDocument;
        private Templates1.StyleSet styleSet;
        private Boolean bNoEvents;

        internal StyleManger(String sourceFile, String destFile, Templates1.StyleSet styleSet)
        {
            sourceDocument = new DocumentHandler(sourceFile);
            destDocument = new DocumentHandler(destFile);
        }

    }
    
    internal class DocumentHandler
    {
        internal String StylesFile { get; set; }
        internal String ListTemplatesFile { get; set; }
        internal Styles Styles { get; set; }
        internal XMLHandler XML { get; set; }
        internal AbstractNums AbstractNums { get; set; }
        internal String FilePath { get; set; }
        
        internal DocumentHandler(String file)
        {
            FilePath = file;
            FileStream zipFile = new FileStream(file, FileMode.Open);
            ZipArchive zip = new ZipArchive(zipFile, ZipArchiveMode.Read);
            ZipArchiveEntry styles = zip.GetEntry(@"word/styles.xml");
            String stylesFile = System.IO.Path.Combine(System.IO.Path.GetTempPath(),System.IO.Path.GetTempFileName());
            if (File.Exists(stylesFile))
                File.Delete(stylesFile);
            styles.ExtractToFile(stylesFile);
            StylesFile = stylesFile;

            ZipArchiveEntry listTemplates = zip.GetEntry(@"word/numbering.xml");
            String listTemplateFile = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetTempFileName());
            if (File.Exists(listTemplateFile))
                File.Delete(listTemplateFile);
            listTemplates.ExtractToFile(listTemplateFile);
            ListTemplatesFile = listTemplateFile;

            XML = new XMLHandler(this);
            Styles stylesCollection = XML.GetStyles();
            AbstractNums = XML.GetListTemplates();
            XML.GetBaseStyles(ref stylesCollection);
            Styles = stylesCollection;

            zip.Dispose();
            

        }

        internal void Save()
        {
            XML.XmlStyles.Save(StylesFile);
            XML.XmlListTemplates.Save(ListTemplatesFile);
            FileStream zipFile = new FileStream(FilePath, FileMode.Open);
            ZipArchive zip = new ZipArchive(zipFile, ZipArchiveMode.Update);
            ZipArchiveEntry styles = zip.GetEntry("word/styles.xml");
            styles.Delete();
            zip.CreateEntryFromFile(StylesFile, "word/styles.xml");
            

            ZipArchiveEntry listTemplates = zip.GetEntry("word/numbering.xml");
            listTemplates.Delete();
            zip.CreateEntryFromFile(ListTemplatesFile, "word/numbering.xml");

            zip.Dispose();


        }

        internal AbstractNum GetExistingListTemplate(String nsid)
        {
            IEnumerable<AbstractNum> collection = from d in AbstractNums where d.TMPL == nsid select d;
            if (collection != null)
                foreach (AbstractNum abs in collection)
                    return abs;
            return null;
        }

        //internal void CopyStyles(DocumentHandler destDoc, Boolean replace)
        //{
        //    foreach (Style style in this.Styles)
        //        if (!destDoc.Styles.Exists(style.Name))
        //            this.XML.CopyStyle(style, destDoc);
        //        else if (replace)
        //        {
        //            this.XML.RemoveStyle(style, destDoc);
        //            this.XML.CopyStyle(style, destDoc);
        //        }
        //}

        internal Styles GetCustomStyles()
        {
            IEnumerable<Style> collection = from d in this.Styles where d.Custom == true select d;
            Styles styles = new Styles(collection);
            return styles;

        }
    }

    internal class Style
    {
        internal Num Number { get; set; }
        internal XmlNode Node { get; set; }
        internal String Name { get; set; }
        internal int Level { get; set; }
        internal int NumID { get; set; }
        internal Boolean Custom { get; set; }
        internal Style BaseStyle { get; set; }
        internal int Priority { get; set; }
        internal Boolean Visibility { get; set; }
        internal Boolean QuickStyle { get; set; }

        internal Style(XmlNode node)
        {
            Node = node;
        }
    }

    internal class Styles: List<Style>
    {
        internal Styles()
        { }

        internal Styles(IEnumerable<Style> styles): base(styles)
        {}

        internal Boolean Exists(String name)
        {
            IEnumerable<Style> collection = from d in this where d.Name == name select d;
            return collection.Count() > 0;

        }

        internal Style GetStyle(String styleName)
        {
            IEnumerable<Style> collection = from d in this where d.Name == styleName select d;
            foreach (Style style in collection)
                return style;
            return null;
        }
    }

    internal class Num
    {
        internal AbstractNum AbstractNumber { get; set; }
        internal XmlNode Node { get; set; }
        internal int AbstractID { get; set; }

        internal Num(XmlNode node)
        {
            Node = node;
        }
    }

    internal class AbstractNum
    {
        internal XmlNode Node { get; set; }
        internal String TMPL { get; set; }
        internal String NumID { get; set; }

        internal AbstractNum(XmlNode node)
        {
            Node = node;
        }
    }

    internal class AbstractNums: List<AbstractNum>
    {
        internal AbstractNums() { }

        internal Boolean Exists(String tmpl)
        {
            IEnumerable<AbstractNum> collection = from d in this where d.TMPL == tmpl select d;
            return collection.Count() > 0;
        }
        
    }
}
