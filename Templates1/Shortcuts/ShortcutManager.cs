﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class ShortcutManager
    {
        private GlobalEventProvider eventProvider;
        internal ShortcutManager()
        {
            eventProvider = new GlobalEventProvider();
            eventProvider.KeyPress += new System.Windows.Forms.KeyPressEventHandler(eventProvider_KeyPress);
            eventProvider.KeyDown += new System.Windows.Forms.KeyEventHandler(eventProvider_KeyDown);
        }

        void eventProvider_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return;

            if(e.Alt && e.KeyCode==Keys.H)
            {
                clsStyles.ApplyAutoHeading();
                return;
            }

            if(e.Alt && e.KeyCode == Keys.B)
            {
                clsStyles.ApplyAutoBody(Globals.ThisAddIn.Application.Selection.Range, false, null, false);
                return;
            }

            if(e.Alt && e.KeyCode == Keys.L)
            {
                clsStyles.ApplyAutoNumbering();
                return;
            }

            int level = 0;
            String style = String.Empty;

            switch (e.KeyCode)
            {
                case Keys.D1:
                    level = 1;
                    break;
                case Keys.D2:
                    level = 2;
                    break;
                case Keys.D3:
                    level = 3;
                    break;
                case Keys.D4:
                    level = 4;
                    break;
                case Keys.D5:
                    level = 5;
                    break;
            }

            if (e.Control && !e.Shift)
                style = "MdR Level " + level;
            if (e.Control && e.Shift)
                style = "MdR Heading " + level;
            if (e.Alt && !e.Shift)
                if (clsStyles.StyleExists("MdR Text " + level, Globals.ThisAddIn.Application.ActiveDocument))
                    style = "MdR Text " + level;
                else
                    style = "MdR Body " + level;
            if(level!=0 && style!=String.Empty)
                ApplyStyle(style);
        }

        void eventProvider_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            
        }

        private void ApplyStyle(String style)
        {
            if(Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if(clsStyles.StyleExists(style, doc))
            {
                Word.Style styleWord = doc.Styles[style];
                Globals.ThisAddIn.Application.Selection.Range.set_Style(styleWord);
            }
        }
    }
}
