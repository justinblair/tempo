﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpPleadingUS : UserControl
    {
        
        private Boolean bUpdate = true;
        private Word.Document _pleadingDocument;
        
        public ctpPleadingUS()
        {
            bUpdate = false;
            InitializeComponent();
        }

        private void ctpPleadingUS_Load(object sender, EventArgs e)
        {
            bUpdate = false;
            Globals.ThisAddIn.Application.StatusBar = "Loading task pane..";
            SetEvents();
            SetRadioTextBoxes();
            SetIndexCombo();
            SetPlaintiffCombo();
            SetVersusCombo();
            SetSubmittedBy();
            if(Globals.ThisAddIn.DMS.IsOnline)
            { 
                Globals.ThisAddIn.Application.StatusBar = "Loading staff details..";
                LoadAttorneys();
                SetListOtherAttorneys();
            }
            Globals.ThisAddIn.Application.StatusBar = "";
            bUpdate = true;
        }

        public void RepopulatePleadingUSTaskPane()
        {

            bUpdate = false;
            if(!clsWordApp.GetTemplateName(Globals.ThisAddIn.Application.ActiveDocument).Contains("California"))
                DetermineStateFederalOther();
            RepopulateCheckBoxes();
            RepopulateListView();
            bUpdate = false;
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }
            bUpdate = true;
        }

        private void RepopulateListView()
        {
            
            ListView lvw = this.lvwIncluded;
            Word.ContentControl ctl = clsWordApp.getContentControl("attorney2", false);
            if (ctl.ShowingPlaceholderText)
                return;
            String contents = ctl.Range.Text;
            String[] splitContents = contents.Split(Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray());
            String[] splitItems;
            char[] i = new char[] { '\u00A0' };
            foreach(String item in splitContents)
            {
                splitItems = item.Split(i);
                if (splitItems.Length == 3)
                {
                ListViewItem lvi = new ListViewItem(splitItems[0]);
                lvi.SubItems.Add(splitItems[1]);
                lvi.SubItems.Add(splitItems[2].Replace("(","").Replace(")",""));
                lvw.Items.Add(lvi);
                }
            }

        }


        private void RepopulateCheckBoxes()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            chkDraft.Checked = !clsWordApp.getContentControl("draft", false).ShowingPlaceholderText;
            chkS.Checked = !clsWordApp.getContentControl("s", false).ShowingPlaceholderText;
            chkInclude.Checked = !clsWordApp.getContentControl("attorney1Include", false).ShowingPlaceholderText;
            chkBelow.Checked = clsWordApp.getContentControl("type", false).Range.Tables.Count == 0;
            //bUpdate = false;
            chkTOC.Checked = doc.TablesOfContents.Count > 0;
            if(chkTOC.Checked)
                chkIncHeading3.Checked = doc.TablesOfContents[1].Range.Fields[1].Code.Text.ToLower().Contains("heading 3");
            
            chkCertOfService.Checked = doc.Range().Text.ToLower().Contains("certificate of service");
            //bUpdate = true;
        }
        
        private void DetermineStateFederalOther()
        {
            //bUpdate = false;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.ContentControl ctlDistrict = clsWordApp.getContentControl("districtCounty", false);
            Word.ContentControl ctlCounty = clsWordApp.getContentControl("county", false);
            Word.ContentControl ctlCourt = clsWordApp.getContentControl("court", false);
            if (doc.Styles["CourtDetails_MdR"].ParagraphFormat.Alignment == Word.WdParagraphAlignment.wdAlignParagraphLeft)
            {
                rbnState.Checked = true;
                tbxCounty.Text = ctlCounty.Range.Text;
                
            }
            else if (ctlDistrict.ShowingPlaceholderText && ctlCounty.ShowingPlaceholderText)
            {
                rbnOther.Checked = true;
                tbxOther.Text = ctlCourt.Range.Text;
            }
            else
            { 
                rbnFederal.Checked = true;
                tbxFederal.Text = ctlDistrict.Range.Text.ToLower().Replace("for the ", "").Replace(" district of", "").ToUpper();
                tbxDistrict.Text = ctlCounty.Range.Text;
            }
            //bUpdate = true;
        }
        
        private void SetEvents()
        {
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);
           
        }
        

        private void ctl_TextChanged(object sender, EventArgs e)
        {
            if (!bUpdate || Globals.ThisAddIn.SuppressEvents)
                return;

            Control ctl = (Control)sender;
            foreach (Word.ContentControl cf in clsWordApp.getContentControls(ctl.Tag.ToString(), Globals.ThisAddIn.Application.ActiveDocument))
                if (ctl.Name == "tbxDefendant" || ctl.Name == "tbxPlaintiff")
                    cf.Range.Text = ctl.Text + ",";
                else
                    cf.Range.Text = ctl.Text;
        }


        private void LoadAttorneys()
        {
            ArrayList arrList = new ArrayList();
            
            ADSearcher ads = new ADSearcher("");
            ADUsers users = ads.GetNYUsers();
            AutoCompleteStringCollection combData = new AutoCompleteStringCollection();
            foreach(ADUser user in users)
            {
                String extendedName = user.ExtendedFullName;
                combData.Add(extendedName);
                arrList.Add(extendedName);
            }
            arrList.Sort();
            this.cboPrincipalAttorney.Items.AddRange(arrList.ToArray());
            this.cboOthers.Items.AddRange(arrList.ToArray());
            this.cboPrincipalAttorney.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cboOthers.AutoCompleteMode = AutoCompleteMode.Suggest;
            this.cboPrincipalAttorney.AutoCompleteSource = AutoCompleteSource.CustomSource;
            this.cboOthers.AutoCompleteSource = AutoCompleteSource.CustomSource;
            this.cboPrincipalAttorney.AutoCompleteCustomSource = combData;
            this.cboOthers.AutoCompleteCustomSource = combData;
            
            
            //lvwSorter = new ListViewColumnSorter();
            //lvwOtherAttorneys.ListViewItemSorter = lvwSorter;
            //lvwSorter.SortColumn = 0;
            //lvwSorter.Order = SortOrder.Ascending;
            //lvwOtherAttorneys.Sort();
        }
        
        private void SetSubmittedBy()
        {
            cboSubmittedBy.Items.Add("Mishcon de Reya New York LLP");
            cboSubmittedBy.Items.Add("Other");
            //cboSubmittedBy.SelectedIndex = 0;
            SetAddress();
        }
        
        private void SetVersusCombo()
        {
            cboVersus.Items.Add("v.");
            cboVersus.Items.Add("against");
            //cboVersus.SelectedIndex = 0;
        }
        
        private void SetPlaintiffCombo()
        {
            cboPlaintiff.Items.Add("Plaintiff");
            cboPlaintiff.Items.Add("Plaintiffs");
            cboDefendant.Items.Add("Defendant");
            cboDefendant.Items.Add("Defendants");
            cboFor.Items.Add("Plaintiff");
            cboFor.Items.Add("Plaintiffs");
            cboFor.Items.Add("Defendant");
            cboFor.Items.Add("Defendants");
            cboProHac.Items.Add("Pro hac vice");
            cboProHac.Items.Add("Pro hac vice pending");
            //cboPlaintiff.SelectedIndex = 0;
            //cboDefendant.SelectedIndex = 0;
        }
        
        private void SetIndexCombo()
        {
            cboIndex.Items.Add("Index No.");
            cboIndex.Items.Add("Case No.");
            cboIndex.Items.Add("Civil Action No.");
            //cboIndex.SelectedIndex = 0;
        }

        private void SetListOtherAttorneys()
        {
            this.lvwIncluded.Columns.Add("Attorney");
            lvwIncluded.Columns.Add("Bar admission");
            lvwIncluded.Columns.Add("Pro hoc vice");
        }
        
        private void SetRadioTextBoxes()
        {
            this.tbxCounty.Enabled = this.rbnState.Checked;
            this.tbxFederal.Enabled = this.rbnFederal.Checked;
            this.tbxDistrict.Enabled = this.rbnFederal.Checked;
            this.tbxOther.Enabled = this.rbnOther.Checked;
            if(bUpdate)
            { 
                this.tbxCounty.Text = "";
                this.tbxFederal.Text = "";
                this.tbxDistrict.Text = "";
                this.tbxOther.Text = "";
            }
            this.tbxHeading.Text = "Complaint";
        }

        private void rbnState_CheckedChanged(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.SuppressEvents)
                return;
            SetRadioTextBoxes();
            if (rbnState.Checked)
            { 
                clsWordApp.getContentControl("court", false).Range.Text = "Supreme Court of the state of New York";
                clsWordApp.getContentControl("districtCounty", false).Range.Text = "County of";
                Word.Style stl = Globals.ThisAddIn.Application.ActiveDocument.Styles["CourtDetails_MdR"];
                stl.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
            }
        }

        private void rbnFederal_CheckedChanged(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.SuppressEvents)
                return;
            SetRadioTextBoxes();
            if(rbnFederal.Checked)
            {
                clsWordApp.getContentControl("court", false).Range.Text = "United States District Court";
                if(bUpdate)
                    clsWordApp.getContentControl("districtCounty", false).Range.Text = "for the " + tbxFederal.Text + " District of";
                Word.Style stl = Globals.ThisAddIn.Application.ActiveDocument.Styles["CourtDetails_MdR"];
                stl.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            }
        }

        private void tbxFederal_TextChanged(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.SuppressEvents)
                return;
            clsWordApp.getContentControl("districtCounty", false).Range.Text = "for the " + tbxFederal.Text + " District of";
        }

        private void rbnOther_CheckedChanged(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.SuppressEvents)
                return;
            SetRadioTextBoxes();
            if(rbnOther.Checked)
            {
                clsWordApp.getContentControl("court", false).Range.Text = tbxOther.Text;
                clsWordApp.getContentControl("districtCounty", false).Range.Text = "";
                Word.Style stl = Globals.ThisAddIn.Application.ActiveDocument.Styles["CourtDetails_MdR"];
                stl.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            }
        }

        private void cboSubmittedBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetAddress();  
        }

        private void SetAddress()
        {
            if (cboSubmittedBy.SelectedIndex == 0)
            {
                clsOfficeData officedata = Globals.ThisAddIn.Officedata;
                tbxAddress.Text = officedata.NYAddress;
                tbxPhone.Text = officedata.NYPhoneNumber;
                tbxFax.Text = officedata.NYFaxNumber;
                tbxAddress.Enabled = false;
                tbxPhone.Enabled = false;
                tbxFax.Enabled = false;
            }
            else
            { 
                tbxAddress.Enabled = true;
                tbxPhone.Enabled = true;
                tbxFax.Enabled = true;
                tbxAddress.Text = "";
                tbxPhone.Text = "";
                tbxFax.Text = "";
                cboSubmittedBy.Select();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (cboOthers.Text == String.Empty)
                return;
            ListViewItem lvi = new ListViewItem(cboOthers.Text);
            lvi.SubItems.Add(tbxBarOther.Text);
            lvi.SubItems.Add(cboProHac.SelectedItem.ToString());
            lvwIncluded.Items.Add(lvi);
            SetAttorney2();
        }

        private void btnTake_Click(object sender, EventArgs e)
        {
            if (lvwIncluded.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = lvwIncluded.SelectedItems[0];
            lvi.Remove();
            SetAttorney2();

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvwIncluded.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = lvwIncluded.SelectedItems[0];
            int index = lvi.Index;
            lvi.Remove();
            lvi = new ListViewItem(cboOthers.Text);
            lvi.SubItems.Add(tbxBarOther.Text);
            lvi.SubItems.Add(cboProHac.Text);
            lvwIncluded.Items.Insert(index, lvi);
            SetAttorney2();

        }

        private void SetAttorney2()
        {
            String buffer = "";
            foreach(ListViewItem lvi in lvwIncluded.Items)
                buffer = buffer + lvi.SubItems[0].Text + "\u00A0" + lvi.SubItems[1].Text + "\u00A0" + "(" + lvi.SubItems[2].Text + ")" + Microsoft.VisualBasic.Constants.vbCrLf;
            clsWordApp.getContentControl("attorney2", false).Range.Text = buffer;

        }

        private void lvwIncluded_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvwIncluded.SelectedItems.Count == 0)
                return;

            foreach (ListViewItem lvi in lvwIncluded.Items)
                if (lvi.Selected)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = Color.White;

            cboOthers.Text = lvwIncluded.SelectedItems[0].Text;

            
            tbxBarOther.Text = lvwIncluded.SelectedItems[0].SubItems[1].Text;
            cboProHac.Text = lvwIncluded.SelectedItems[0].SubItems[2].Text;

        }

        private void cboPrincipalAttorney_SelectedIndexChanged(object sender, EventArgs e)
        {
            IncludeName();
        }

        private void IncludeName()
        {
            if (chkInclude.Checked)
                clsWordApp.getContentControl("attorney1Include", false).Range.Text = cboPrincipalAttorney.Text;
            else
                clsWordApp.getContentControl("attorney1Include", false).Range.Text = "";
        }

        private void chkInclude_CheckedChanged(object sender, EventArgs e)
        {
            IncludeName();
        }

        private void chkS_CheckedChanged(object sender, EventArgs e)
        {
            if(chkS.Checked)
                clsWordApp.getContentControl("s",false).Range.Text = "/s/";
            else
                clsWordApp.getContentControl("s",false).Range.Text = "";
        }

        private void chkDraft_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDraft.Checked)
                clsWordApp.getContentControl("draft", false).Range.Text = "Draft";
            else
                clsWordApp.getContentControl("draft", false).Range.Text = "";
        }

        private void chkBelow_CheckedChanged(object sender, EventArgs e)
        {
            SetHeadingBelow(chkBelow.Checked);
        }

        private void SetHeadingBelow(Boolean below)
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Table tbl = doc.Tables[1];
            Word.Range rng;
            Word.Range rng2;
            Word.ContentControl ctl = clsWordApp.getContentControl("type", false);
            Word.ContentControl ctl2;
            if(below)
            {
                rng = tbl.Range;
                rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                rng.set_Style(doc.Styles["OptionalType_MdR"]);
                doc.Styles["OptionalType_MdR"].ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            }
            else
            {
                rng = tbl.Rows[4].Cells[3].Range;
                rng2 = tbl.Range;
                rng2.MoveEnd(Word.WdUnits.wdParagraph, 1);
                rng2.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                rng2.set_Style(doc.Styles[Word.WdBuiltinStyle.wdStyleNormal]);
            }
            ctl2 = doc.ContentControls.Add(Word.WdContentControlType.wdContentControlRichText, rng);
            ctl2.Title = ctl.Title;
            ctl2.Tag = ctl.Tag;
            ctl2.Range.Text = ctl.Range.Text;
            ctl2.SetPlaceholderText(ctl.PlaceholderText);
            ctl.Range.Text = "";
            ctl.Delete();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbxPlaintiff_TextChanged(object sender, EventArgs e)
        {
            if (!bUpdate) return;
            clsWordApp.getContentControl("plaintiff", false).Range.Text = tbxPlaintiff.Text + ",";
        }

        private void tbxDefendant_TextChanged(object sender, EventArgs e)
        {
            if (!bUpdate) return;
            clsWordApp.getContentControl("defendant", false).Range.Text = tbxDefendant.Text + ",";
        }

        private void chkTOC_CheckedChanged(object sender, EventArgs e)
        {
            if(bUpdate)
            DrawTOC();
        }

        private void DrawTOC()
        {
            clsTOC.DeleteTableOfContents();
            if (chkTOC.Checked)
                if (chkIncHeading3.Checked)
                    clsTOC.InsertTOC(2, "", "Heading 1,1;Heading 2,2;Heading 3,3");
                else
                    clsTOC.InsertTOC(2, "", "Heading 1,1;Heading 2,2");
            else
                clsTOC.DeleteTableOfContents();
        }

        private void chkIncHeading3_CheckedChanged(object sender, EventArgs e)
        {
            if(bUpdate)
            DrawTOC();
        }

        private void chkCertOfService_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCertOfService.Checked)
                clsDynamicInserts.InsertCertificateOfService();
            else
                clsDynamicInserts.RemoveCertificateOfService();
        }

        private void chkAffidavit_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAffidavit.Checked)
            {
                Globals.ThisAddIn.PleadingDocument = Globals.ThisAddIn.Application.ActiveDocument;
                Word.Document doc = clsDynamicInserts.CreateAffidavitOfService();
                doc.ActiveWindow.Activate();
            }
            else
                clsDynamicInserts.DeleteAffidavitOfService();
        }

        private void chkAffirmation_CheckedChanged(object sender, EventArgs e)
        {
            if(chkAffirmation.Checked)
            {
                Globals.ThisAddIn.PleadingDocument = Globals.ThisAddIn.Application.ActiveDocument;
                Word.Document doc = clsDynamicInserts.CreateAffidavitOfService();
                doc.ActiveWindow.Activate();
            }
            else
                clsDynamicInserts.DeleteAffidavitOfService();
        }

        private void tbxCounty_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
