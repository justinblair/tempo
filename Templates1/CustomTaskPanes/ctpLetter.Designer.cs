﻿namespace Templates1
{
    partial class ctpLetter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkListConfidentiality = new CustomTaskPanes.CustomCheckedListBox();
            this.chkListDelivery = new CustomTaskPanes.CustomCheckedListBox();
            this.tbxClientMatter = new CustomTaskPanes.CustomTextBox();
            this.btnFetch = new System.Windows.Forms.Button();
            this.tbxAuthorTel = new CustomTaskPanes.CustomTextBox();
            this.tbxMail = new CustomTaskPanes.CustomTextBox();
            this.tbxFax = new CustomTaskPanes.CustomTextBox();
            this.chkListLegalInstructions = new CustomTaskPanes.CustomComboBox();
            this.chkListPrejudice = new CustomTaskPanes.CustomComboBox();
            this.chkListSpecDelivery = new CustomTaskPanes.CustomComboBox();
            this.cboRecipient = new CustomTaskPanes.CustomComboBox();
            this.btnLookup = new System.Windows.Forms.Button();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.cboSalutation = new CustomTaskPanes.CustomComboBox();
            this.cboSignOff = new CustomTaskPanes.CustomComboBox();
            this.tbxSubject = new CustomTaskPanes.CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new CustomTaskPanes.CustomDateTimePicker();
            this.tbxAuthor = new CustomTaskPanes.CustomTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbxTitle = new CustomTaskPanes.CustomTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbxOurRef = new CustomTaskPanes.CustomTextBox();
            this.tbxYourRef = new CustomTaskPanes.CustomTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.chkCC = new System.Windows.Forms.CheckBox();
            this.chkEnc = new System.Windows.Forms.CheckBox();
            this.tbxCCs = new CustomTaskPanes.CustomTextBox();
            this.tbxEncs = new CustomTaskPanes.CustomTextBox();
            this.btnAuthor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkListConfidentiality
            // 
            this.chkListConfidentiality.CheckOnClick = true;
            this.chkListConfidentiality.FormattingEnabled = true;
            this.chkListConfidentiality.Location = new System.Drawing.Point(98, 92);
            this.chkListConfidentiality.Name = "chkListConfidentiality";
            this.chkListConfidentiality.Size = new System.Drawing.Size(238, 79);
            this.chkListConfidentiality.TabIndex = 4;
            this.chkListConfidentiality.Tag = "privacy";
            this.chkListConfidentiality.SelectedIndexChanged += new System.EventHandler(this.chkListConfidentiality_SelectedIndexChanged);
            // 
            // chkListDelivery
            // 
            this.chkListDelivery.CheckOnClick = true;
            this.chkListDelivery.FormattingEnabled = true;
            this.chkListDelivery.Location = new System.Drawing.Point(98, 363);
            this.chkListDelivery.Name = "chkListDelivery";
            this.chkListDelivery.Size = new System.Drawing.Size(238, 109);
            this.chkListDelivery.TabIndex = 9;
            this.chkListDelivery.Tag = "delivery";
            this.chkListDelivery.SelectedIndexChanged += new System.EventHandler(this.chkListDelivery_SelectedIndexChanged);
            // 
            // tbxClientMatter
            // 
            this.tbxClientMatter.Enabled = false;
            this.tbxClientMatter.Location = new System.Drawing.Point(98, 507);
            this.tbxClientMatter.Name = "tbxClientMatter";
            this.tbxClientMatter.Size = new System.Drawing.Size(238, 20);
            this.tbxClientMatter.TabIndex = 12;
            this.tbxClientMatter.TextChanged += new System.EventHandler(this.tbxClientMatter_TextChanged);
            // 
            // btnFetch
            // 
            this.btnFetch.Location = new System.Drawing.Point(16, 819);
            this.btnFetch.Name = "btnFetch";
            this.btnFetch.Size = new System.Drawing.Size(97, 30);
            this.btnFetch.TabIndex = 6;
            this.btnFetch.Text = "Reset";
            this.btnFetch.UseVisualStyleBackColor = true;
            this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
            // 
            // tbxAuthorTel
            // 
            this.tbxAuthorTel.Location = new System.Drawing.Point(98, 685);
            this.tbxAuthorTel.Name = "tbxAuthorTel";
            this.tbxAuthorTel.Size = new System.Drawing.Size(238, 20);
            this.tbxAuthorTel.TabIndex = 18;
            this.tbxAuthorTel.Tag = "authorTelephone";
            this.tbxAuthorTel.TextChanged += new System.EventHandler(this.tbxAuthorTel_TextChanged);
            // 
            // tbxMail
            // 
            this.tbxMail.Location = new System.Drawing.Point(98, 736);
            this.tbxMail.Name = "tbxMail";
            this.tbxMail.Size = new System.Drawing.Size(238, 20);
            this.tbxMail.TabIndex = 20;
            this.tbxMail.Tag = "authorEmail";
            this.tbxMail.TextChanged += new System.EventHandler(this.tbxMail_TextChanged);
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(98, 710);
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(238, 20);
            this.tbxFax.TabIndex = 19;
            this.tbxFax.Tag = "authorFax";
            this.tbxFax.TextChanged += new System.EventHandler(this.tbxFax_TextChanged);
            // 
            // chkListLegalInstructions
            // 
            this.chkListLegalInstructions.FormattingEnabled = true;
            this.chkListLegalInstructions.Location = new System.Drawing.Point(98, 309);
            this.chkListLegalInstructions.Name = "chkListLegalInstructions";
            this.chkListLegalInstructions.Size = new System.Drawing.Size(238, 21);
            this.chkListLegalInstructions.TabIndex = 7;
            this.chkListLegalInstructions.Tag = "legal";
            this.chkListLegalInstructions.SelectedIndexChanged += new System.EventHandler(this.chkListLegalInstructions_SelectedIndexChanged_1);
            // 
            // chkListPrejudice
            // 
            this.chkListPrejudice.FormattingEnabled = true;
            this.chkListPrejudice.Location = new System.Drawing.Point(98, 336);
            this.chkListPrejudice.Name = "chkListPrejudice";
            this.chkListPrejudice.Size = new System.Drawing.Size(238, 21);
            this.chkListPrejudice.TabIndex = 8;
            this.chkListPrejudice.Tag = "prejudice";
            this.chkListPrejudice.SelectedIndexChanged += new System.EventHandler(this.chkListPrejudice_SelectedIndexChanged_1);
            // 
            // chkListSpecDelivery
            // 
            this.chkListSpecDelivery.FormattingEnabled = true;
            this.chkListSpecDelivery.Location = new System.Drawing.Point(98, 480);
            this.chkListSpecDelivery.Name = "chkListSpecDelivery";
            this.chkListSpecDelivery.Size = new System.Drawing.Size(238, 21);
            this.chkListSpecDelivery.TabIndex = 10;
            this.chkListSpecDelivery.Tag = "specialDelivery";
            this.chkListSpecDelivery.SelectedIndexChanged += new System.EventHandler(this.chkListSpecDelivery_SelectedIndexChanged_1);
            // 
            // cboRecipient
            // 
            this.cboRecipient.FormattingEnabled = true;
            this.cboRecipient.Location = new System.Drawing.Point(98, 178);
            this.cboRecipient.Name = "cboRecipient";
            this.cboRecipient.Size = new System.Drawing.Size(212, 21);
            this.cboRecipient.TabIndex = 5;
            this.cboRecipient.Tag = "recipient";
            this.cboRecipient.SelectedIndexChanged += new System.EventHandler(this.cboRecipient_SelectedIndexChanged);
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(316, 180);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(19, 18);
            this.btnLookup.TabIndex = 14;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(98, 207);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(238, 96);
            this.tbxAddress.TabIndex = 6;
            this.tbxAddress.Tag = "addressee";
            this.tbxAddress.Text = "";
            this.tbxAddress.TextChanged += new System.EventHandler(this.tbxAddress_TextChanged);
            // 
            // cboSalutation
            // 
            this.cboSalutation.FormattingEnabled = true;
            this.cboSalutation.Location = new System.Drawing.Point(98, 548);
            this.cboSalutation.Name = "cboSalutation";
            this.cboSalutation.Size = new System.Drawing.Size(238, 21);
            this.cboSalutation.TabIndex = 13;
            this.cboSalutation.Tag = "salutation";
            this.cboSalutation.SelectedIndexChanged += new System.EventHandler(this.cboSalutation_SelectedIndexChanged);
            // 
            // cboSignOff
            // 
            this.cboSignOff.FormattingEnabled = true;
            this.cboSignOff.Location = new System.Drawing.Point(98, 602);
            this.cboSignOff.Name = "cboSignOff";
            this.cboSignOff.Size = new System.Drawing.Size(238, 21);
            this.cboSignOff.TabIndex = 16;
            this.cboSignOff.Tag = "signoff";
            this.cboSignOff.SelectedIndexChanged += new System.EventHandler(this.cboSignOff_SelectedIndexChanged);
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(98, 576);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(238, 20);
            this.tbxSubject.TabIndex = 14;
            this.tbxSubject.Tag = "subject";
            this.tbxSubject.TextChanged += new System.EventHandler(this.tbxSubject_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 342);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Prejudice";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 486);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Special delivery";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 554);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Salutation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 608);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Sign-off";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 582);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Subject";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Recipient";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 315);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 18);
            this.label1.TabIndex = 26;
            this.label1.Text = "Subject to...";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 66);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(238, 20);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.Tag = "date";
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(98, 635);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(212, 20);
            this.tbxAuthor.TabIndex = 16;
            this.tbxAuthor.Tag = "authorName";
            this.tbxAuthor.TextChanged += new System.EventHandler(this.tbxAuthor_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 641);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Author";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(16, 690);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Author tel";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 716);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Author fax";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 742);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Author email";
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(98, 661);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(238, 20);
            this.tbxTitle.TabIndex = 17;
            this.tbxTitle.Tag = "role";
            this.tbxTitle.TextChanged += new System.EventHandler(this.tbxTitle_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 667);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Author role";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 67);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Date";
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(98, 8);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(238, 20);
            this.tbxOurRef.TabIndex = 1;
            this.tbxOurRef.Tag = "ourRef";
            this.tbxOurRef.TextChanged += new System.EventHandler(this.tbxOurRef_TextChanged);
            // 
            // tbxYourRef
            // 
            this.tbxYourRef.Location = new System.Drawing.Point(98, 36);
            this.tbxYourRef.Name = "tbxYourRef";
            this.tbxYourRef.Size = new System.Drawing.Size(238, 20);
            this.tbxYourRef.TabIndex = 2;
            this.tbxYourRef.Tag = "yourRef";
            this.tbxYourRef.TextChanged += new System.EventHandler(this.tbxYourRef_TextChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(16, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 38;
            this.label14.Text = "Our ref";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 39);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Your ref";
            // 
            // chkCC
            // 
            this.chkCC.AutoSize = true;
            this.chkCC.Location = new System.Drawing.Point(16, 776);
            this.chkCC.Name = "chkCC";
            this.chkCC.Size = new System.Drawing.Size(40, 17);
            this.chkCC.TabIndex = 21;
            this.chkCC.Text = "CC";
            this.chkCC.UseVisualStyleBackColor = true;
            this.chkCC.CheckedChanged += new System.EventHandler(this.chkCC_CheckedChanged);
            // 
            // chkEnc
            // 
            this.chkEnc.AutoSize = true;
            this.chkEnc.Location = new System.Drawing.Point(16, 796);
            this.chkEnc.Name = "chkEnc";
            this.chkEnc.Size = new System.Drawing.Size(50, 17);
            this.chkEnc.TabIndex = 22;
            this.chkEnc.Text = "Encs";
            this.chkEnc.UseVisualStyleBackColor = true;
            this.chkEnc.CheckedChanged += new System.EventHandler(this.chkEnc_CheckedChanged);
            // 
            // tbxCCs
            // 
            this.tbxCCs.Enabled = false;
            this.tbxCCs.Location = new System.Drawing.Point(98, 770);
            this.tbxCCs.Name = "tbxCCs";
            this.tbxCCs.Size = new System.Drawing.Size(238, 20);
            this.tbxCCs.TabIndex = 21;
            this.tbxCCs.Tag = "ccs";
            this.tbxCCs.TextChanged += new System.EventHandler(this.tbxCCs_TextChanged);
            // 
            // tbxEncs
            // 
            this.tbxEncs.Enabled = false;
            this.tbxEncs.Location = new System.Drawing.Point(98, 790);
            this.tbxEncs.Name = "tbxEncs";
            this.tbxEncs.Size = new System.Drawing.Size(238, 20);
            this.tbxEncs.TabIndex = 43;
            this.tbxEncs.Tag = "encs";
            this.tbxEncs.TextChanged += new System.EventHandler(this.tbxEncs_TextChanged);
            // 
            // btnAuthor
            // 
            this.btnAuthor.Location = new System.Drawing.Point(316, 637);
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(19, 18);
            this.btnAuthor.TabIndex = 44;
            this.btnAuthor.Text = "...";
            this.btnAuthor.UseVisualStyleBackColor = true;
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // ctpLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.btnAuthor);
            this.Controls.Add(this.tbxEncs);
            this.Controls.Add(this.tbxCCs);
            this.Controls.Add(this.chkEnc);
            this.Controls.Add(this.chkCC);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbxYourRef);
            this.Controls.Add(this.tbxOurRef);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.cboSignOff);
            this.Controls.Add(this.cboSalutation);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.cboRecipient);
            this.Controls.Add(this.chkListSpecDelivery);
            this.Controls.Add(this.chkListPrejudice);
            this.Controls.Add(this.chkListLegalInstructions);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.tbxMail);
            this.Controls.Add(this.tbxAuthorTel);
            this.Controls.Add(this.btnFetch);
            this.Controls.Add(this.tbxClientMatter);
            this.Controls.Add(this.chkListDelivery);
            this.Controls.Add(this.chkListConfidentiality);
            this.Name = "ctpLetter";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.UserControl1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTaskPanes.CustomCheckedListBox chkListConfidentiality;
        private CustomTaskPanes.CustomCheckedListBox chkListDelivery;
        private CustomTaskPanes.CustomTextBox tbxClientMatter;
        private System.Windows.Forms.Button btnFetch;
        private CustomTaskPanes.CustomTextBox tbxAuthorTel;
        private CustomTaskPanes.CustomTextBox tbxMail;
        private CustomTaskPanes.CustomTextBox tbxFax;
        private CustomTaskPanes.CustomComboBox chkListLegalInstructions;
        private CustomTaskPanes.CustomComboBox chkListPrejudice;
        private CustomTaskPanes.CustomComboBox chkListSpecDelivery;
        private CustomTaskPanes.CustomComboBox cboRecipient;
        private System.Windows.Forms.Button btnLookup;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private CustomTaskPanes.CustomComboBox cboSalutation;
        private CustomTaskPanes.CustomComboBox cboSignOff;
        private CustomTaskPanes.CustomTextBox tbxSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker1;
        private CustomTaskPanes.CustomTextBox tbxAuthor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private CustomTaskPanes.CustomTextBox tbxTitle;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private CustomTaskPanes.CustomTextBox tbxOurRef;
        private CustomTaskPanes.CustomTextBox tbxYourRef;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox chkCC;
        private System.Windows.Forms.CheckBox chkEnc;
        private CustomTaskPanes.CustomTextBox tbxCCs;
        private CustomTaskPanes.CustomTextBox tbxEncs;
        private System.Windows.Forms.Button btnAuthor;
    }
}
