﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpNonLegal : UserControl
    {
        private Boolean bNoUpdating = false;
        public ctpNonLegal()
        {
            InitializeComponent();
        }

        public void RepopulateNonLegalTaskPane()
        {
            Word.Section sect = Globals.ThisAddIn.Application.ActiveDocument.Sections[1];
            Word.Style stl = sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.get_Style();
            bNoUpdating = true;
            chkHeader.Checked = stl.NameLocal == "Header";
            tbxHeader.Text = sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text.Replace("\u000D","");
            bNoUpdating = false;
            tbxHeader.Enabled = chkHeader.Checked;
        }

        private void ctpNonLegal_Load(object sender, EventArgs e)
        {
            chkHeader.Checked = HasHeader();
            tbxHeader.Enabled = chkHeader.Checked;
        }

        private void chkHeader_CheckedChanged(object sender, EventArgs e)
        {
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            if (bNoUpdating)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (!chkHeader.Checked)
            {
                foreach (Word.Section sect in doc.Sections)
                {
                    sect.PageSetup.TopMargin = 28.34646F;
                    sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.set_Style(doc.Styles[Word.WdBuiltinStyle.wdStyleNormal]);
                    sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = "";
                }
            }
            else
            {
                foreach (Word.Section sect in doc.Sections)
                { 
                    sect.PageSetup.TopMargin = 69.44882F;
                    sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.set_Style(doc.Styles[Word.WdBuiltinStyle.wdStyleHeader]);
                    Word.Range rng = sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                    
                    rng.MoveStart(Word.WdUnits.wdParagraph);
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                    rng.Text = tbxHeader.Text;
                }
            }
            tbxHeader.Enabled = chkHeader.Checked;
        }

        private void tbxHeader_TextChanged(object sender, EventArgs e)
        {
            if (bNoUpdating)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            foreach (Word.Section sect in doc.Sections)
            {
                
                sect.Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text = tbxHeader.Text;
            }
        }

        private Boolean HasHeader()
        {
            return Globals.ThisAddIn.Application.ActiveDocument.Sections[1].Headers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.get_Style().NameLocal == "Header";
        }
    }
}
