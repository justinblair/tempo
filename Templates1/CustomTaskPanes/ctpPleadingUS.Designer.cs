﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpPleadingUS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDraft = new System.Windows.Forms.CheckBox();
            this.rbnState = new System.Windows.Forms.RadioButton();
            this.rbnFederal = new System.Windows.Forms.RadioButton();
            this.rbnOther = new System.Windows.Forms.RadioButton();
            this.pnlCourtType = new System.Windows.Forms.Panel();
            this.tbxCounty = new CustomTextBox();
            this.tbxFederal = new CustomTextBox();
            this.tbxDistrict = new CustomTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxOther = new CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxHeading = new CustomTextBox();
            this.chkBelow = new System.Windows.Forms.CheckBox();
            this.cboIndex = new CustomTaskPanes.CustomComboBox();
            this.cboPlaintiff = new CustomTaskPanes.CustomComboBox();
            this.cboVersus = new CustomTaskPanes.CustomComboBox();
            this.tbxIndex = new CustomTextBox();
            this.tbxPlaintiff = new CustomTaskPanes.CustomRichTextBox();
            this.tbxDefendant = new CustomTaskPanes.CustomRichTextBox();
            this.cboDefendant = new CustomTaskPanes.CustomComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxCityDate = new CustomTextBox();
            this.dtpDate = new CustomTaskPanes.CustomDateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.cboSubmittedBy = new CustomTaskPanes.CustomComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.tbxPhone = new CustomTextBox();
            this.tbxFax = new CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chkS = new System.Windows.Forms.CheckBox();
            this.chkInclude = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxBarAdmission = new CustomTextBox();
            this.cboPrincipalAttorney = new CustomTaskPanes.CustomComboBox();
            this.cboProHac = new CustomTaskPanes.CustomComboBox();
            this.tbxBarOther = new CustomTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lvwIncluded = new System.Windows.Forms.ListView();
            this.richTextBox2 = new CustomTaskPanes.CustomRichTextBox();
            this.cboFor = new CustomTaskPanes.CustomComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnTake = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cboOthers = new CustomTaskPanes.CustomComboBox();
            this.chkTOC = new System.Windows.Forms.CheckBox();
            this.chkCertOfService = new System.Windows.Forms.CheckBox();
            this.chkIncHeading3 = new System.Windows.Forms.CheckBox();
            this.chkAffidavit = new System.Windows.Forms.CheckBox();
            this.chkAffirmation = new System.Windows.Forms.CheckBox();
            this.pnlCourtType.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkDraft
            // 
            this.chkDraft.AutoSize = true;
            this.chkDraft.Location = new System.Drawing.Point(21, 8);
            this.chkDraft.Name = "chkDraft";
            this.chkDraft.Size = new System.Drawing.Size(49, 17);
            this.chkDraft.TabIndex = 62;
            this.chkDraft.Text = "Draft";
            this.chkDraft.UseVisualStyleBackColor = true;
            this.chkDraft.CheckedChanged += new System.EventHandler(this.chkDraft_CheckedChanged);
            // 
            // rbnState
            // 
            this.rbnState.AutoSize = true;
            this.rbnState.Location = new System.Drawing.Point(11, 3);
            this.rbnState.Name = "rbnState";
            this.rbnState.Size = new System.Drawing.Size(50, 17);
            this.rbnState.TabIndex = 63;
            this.rbnState.TabStop = true;
            this.rbnState.Text = "State";
            this.rbnState.UseVisualStyleBackColor = true;
            this.rbnState.CheckedChanged += new System.EventHandler(this.rbnState_CheckedChanged);
            // 
            // rbnFederal
            // 
            this.rbnFederal.AutoSize = true;
            this.rbnFederal.Location = new System.Drawing.Point(11, 27);
            this.rbnFederal.Name = "rbnFederal";
            this.rbnFederal.Size = new System.Drawing.Size(60, 17);
            this.rbnFederal.TabIndex = 64;
            this.rbnFederal.TabStop = true;
            this.rbnFederal.Text = "Federal";
            this.rbnFederal.UseVisualStyleBackColor = true;
            this.rbnFederal.CheckedChanged += new System.EventHandler(this.rbnFederal_CheckedChanged);
            // 
            // rbnOther
            // 
            this.rbnOther.AutoSize = true;
            this.rbnOther.Location = new System.Drawing.Point(11, 78);
            this.rbnOther.Name = "rbnOther";
            this.rbnOther.Size = new System.Drawing.Size(51, 17);
            this.rbnOther.TabIndex = 65;
            this.rbnOther.TabStop = true;
            this.rbnOther.Text = "Other";
            this.rbnOther.UseVisualStyleBackColor = true;
            this.rbnOther.CheckedChanged += new System.EventHandler(this.rbnOther_CheckedChanged);
            // 
            // pnlCourtType
            // 
            this.pnlCourtType.Controls.Add(this.rbnState);
            this.pnlCourtType.Controls.Add(this.rbnOther);
            this.pnlCourtType.Controls.Add(this.rbnFederal);
            this.pnlCourtType.Location = new System.Drawing.Point(10, 29);
            this.pnlCourtType.Name = "pnlCourtType";
            this.pnlCourtType.Size = new System.Drawing.Size(76, 105);
            this.pnlCourtType.TabIndex = 66;
            // 
            // tbxCounty
            // 
            this.tbxCounty.Location = new System.Drawing.Point(137, 31);
            this.tbxCounty.Name = "tbxCounty";
            this.tbxCounty.Size = new System.Drawing.Size(179, 20);
            this.tbxCounty.TabIndex = 67;
            this.tbxCounty.Tag = "county";
            this.tbxCounty.TextChanged += new System.EventHandler(this.tbxCounty_TextChanged);
            // 
            // tbxFederal
            // 
            this.tbxFederal.Location = new System.Drawing.Point(91, 56);
            this.tbxFederal.Name = "tbxFederal";
            this.tbxFederal.Size = new System.Drawing.Size(225, 20);
            this.tbxFederal.TabIndex = 68;
            this.tbxFederal.TextChanged += new System.EventHandler(this.tbxFederal_TextChanged);
            // 
            // tbxDistrict
            // 
            this.tbxDistrict.Location = new System.Drawing.Point(137, 80);
            this.tbxDistrict.Name = "tbxDistrict";
            this.tbxDistrict.Size = new System.Drawing.Size(179, 20);
            this.tbxDistrict.TabIndex = 69;
            this.tbxDistrict.Tag = "county";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 70;
            this.label1.Text = "District";
            // 
            // tbxOther
            // 
            this.tbxOther.Location = new System.Drawing.Point(91, 107);
            this.tbxOther.Name = "tbxOther";
            this.tbxOther.Size = new System.Drawing.Size(225, 20);
            this.tbxOther.TabIndex = 71;
            this.tbxOther.Tag = "court";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "County";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 73;
            this.label3.Text = "Heading";
            // 
            // tbxHeading
            // 
            this.tbxHeading.Location = new System.Drawing.Point(91, 135);
            this.tbxHeading.Name = "tbxHeading";
            this.tbxHeading.Size = new System.Drawing.Size(170, 20);
            this.tbxHeading.TabIndex = 74;
            this.tbxHeading.Tag = "type";
            // 
            // chkBelow
            // 
            this.chkBelow.AutoSize = true;
            this.chkBelow.Location = new System.Drawing.Point(266, 138);
            this.chkBelow.Name = "chkBelow";
            this.chkBelow.Size = new System.Drawing.Size(55, 17);
            this.chkBelow.TabIndex = 75;
            this.chkBelow.Text = "Below";
            this.chkBelow.UseVisualStyleBackColor = true;
            this.chkBelow.CheckedChanged += new System.EventHandler(this.chkBelow_CheckedChanged);
            // 
            // cboIndex
            // 
            this.cboIndex.FormattingEnabled = true;
            this.cboIndex.Location = new System.Drawing.Point(21, 164);
            this.cboIndex.Name = "cboIndex";
            this.cboIndex.Size = new System.Drawing.Size(117, 21);
            this.cboIndex.TabIndex = 76;
            this.cboIndex.Tag = "caseType";
            // 
            // cboPlaintiff
            // 
            this.cboPlaintiff.FormattingEnabled = true;
            this.cboPlaintiff.Location = new System.Drawing.Point(199, 215);
            this.cboPlaintiff.Name = "cboPlaintiff";
            this.cboPlaintiff.Size = new System.Drawing.Size(117, 21);
            this.cboPlaintiff.TabIndex = 78;
            this.cboPlaintiff.Tag = "plaintiffs";
            // 
            // cboVersus
            // 
            this.cboVersus.FormattingEnabled = true;
            this.cboVersus.Location = new System.Drawing.Point(21, 243);
            this.cboVersus.Name = "cboVersus";
            this.cboVersus.Size = new System.Drawing.Size(117, 21);
            this.cboVersus.TabIndex = 80;
            this.cboVersus.Tag = "vAgainst";
            // 
            // tbxIndex
            // 
            this.tbxIndex.Location = new System.Drawing.Point(153, 165);
            this.tbxIndex.Name = "tbxIndex";
            this.tbxIndex.Size = new System.Drawing.Size(163, 20);
            this.tbxIndex.TabIndex = 77;
            this.tbxIndex.Tag = "caseNumber";
            // 
            // tbxPlaintiff
            // 
            this.tbxPlaintiff.Location = new System.Drawing.Point(21, 191);
            this.tbxPlaintiff.Name = "tbxPlaintiff";
            this.tbxPlaintiff.Size = new System.Drawing.Size(165, 45);
            this.tbxPlaintiff.TabIndex = 81;
            this.tbxPlaintiff.Tag = "plaintiff";
            this.tbxPlaintiff.Text = "";
            this.tbxPlaintiff.TextChanged += new System.EventHandler(this.tbxPlaintiff_TextChanged);
            // 
            // tbxDefendant
            // 
            this.tbxDefendant.Location = new System.Drawing.Point(21, 270);
            this.tbxDefendant.Name = "tbxDefendant";
            this.tbxDefendant.Size = new System.Drawing.Size(165, 45);
            this.tbxDefendant.TabIndex = 83;
            this.tbxDefendant.Tag = "defendant";
            this.tbxDefendant.Text = "";
            this.tbxDefendant.TextChanged += new System.EventHandler(this.tbxDefendant_TextChanged);
            // 
            // cboDefendant
            // 
            this.cboDefendant.FormattingEnabled = true;
            this.cboDefendant.Location = new System.Drawing.Point(199, 294);
            this.cboDefendant.Name = "cboDefendant";
            this.cboDefendant.Size = new System.Drawing.Size(117, 21);
            this.cboDefendant.TabIndex = 82;
            this.cboDefendant.Tag = "defendants";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 323);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 84;
            this.label4.Text = "Dated";
            // 
            // tbxCityDate
            // 
            this.tbxCityDate.Location = new System.Drawing.Point(77, 320);
            this.tbxCityDate.Name = "tbxCityDate";
            this.tbxCityDate.Size = new System.Drawing.Size(239, 20);
            this.tbxCityDate.TabIndex = 85;
            this.tbxCityDate.Tag = "cityState";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMMM d, yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(78, 346);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(238, 20);
            this.dtpDate.TabIndex = 86;
            this.dtpDate.Tag = "date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 376);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Submitted by";
            // 
            // cboSubmittedBy
            // 
            this.cboSubmittedBy.FormattingEnabled = true;
            this.cboSubmittedBy.Location = new System.Drawing.Point(122, 372);
            this.cboSubmittedBy.Name = "cboSubmittedBy";
            this.cboSubmittedBy.Size = new System.Drawing.Size(194, 21);
            this.cboSubmittedBy.TabIndex = 88;
            this.cboSubmittedBy.Tag = "practice";
            this.cboSubmittedBy.SelectedIndexChanged += new System.EventHandler(this.cboSubmittedBy_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 402);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 89;
            this.label6.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 455);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 90;
            this.label7.Text = "Phone";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 479);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 91;
            this.label8.Text = "Fax";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(124, 399);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(192, 45);
            this.tbxAddress.TabIndex = 92;
            this.tbxAddress.Tag = "firmAddress";
            this.tbxAddress.Text = "";
            // 
            // tbxPhone
            // 
            this.tbxPhone.Location = new System.Drawing.Point(124, 450);
            this.tbxPhone.Name = "tbxPhone";
            this.tbxPhone.Size = new System.Drawing.Size(192, 20);
            this.tbxPhone.TabIndex = 93;
            this.tbxPhone.Tag = "telephone";
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(124, 474);
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(192, 20);
            this.tbxFax.TabIndex = 94;
            this.tbxFax.Tag = "fax";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 502);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 13);
            this.label9.TabIndex = 95;
            this.label9.Text = "Principal attorney";
            // 
            // chkS
            // 
            this.chkS.AutoSize = true;
            this.chkS.Location = new System.Drawing.Point(17, 527);
            this.chkS.MinimumSize = new System.Drawing.Size(100, 0);
            this.chkS.Name = "chkS";
            this.chkS.Size = new System.Drawing.Size(100, 17);
            this.chkS.TabIndex = 97;
            this.chkS.Text = "/s/";
            this.chkS.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkS.UseVisualStyleBackColor = true;
            this.chkS.CheckedChanged += new System.EventHandler(this.chkS_CheckedChanged);
            // 
            // chkInclude
            // 
            this.chkInclude.AutoSize = true;
            this.chkInclude.Location = new System.Drawing.Point(126, 526);
            this.chkInclude.Name = "chkInclude";
            this.chkInclude.Size = new System.Drawing.Size(90, 17);
            this.chkInclude.TabIndex = 98;
            this.chkInclude.Text = "Include name";
            this.chkInclude.UseVisualStyleBackColor = true;
            this.chkInclude.CheckedChanged += new System.EventHandler(this.chkInclude_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 550);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 99;
            this.label10.Text = "Bar admission";
            // 
            // tbxBarAdmission
            // 
            this.tbxBarAdmission.Location = new System.Drawing.Point(124, 547);
            this.tbxBarAdmission.Name = "tbxBarAdmission";
            this.tbxBarAdmission.Size = new System.Drawing.Size(192, 20);
            this.tbxBarAdmission.TabIndex = 100;
            this.tbxBarAdmission.Tag = "attorney1BarAdmission";
            // 
            // cboPrincipalAttorney
            // 
            this.cboPrincipalAttorney.FormattingEnabled = true;
            this.cboPrincipalAttorney.Location = new System.Drawing.Point(126, 499);
            this.cboPrincipalAttorney.Name = "cboPrincipalAttorney";
            this.cboPrincipalAttorney.Size = new System.Drawing.Size(190, 21);
            this.cboPrincipalAttorney.TabIndex = 101;
            this.cboPrincipalAttorney.Tag = "attorney1";
            this.cboPrincipalAttorney.SelectedIndexChanged += new System.EventHandler(this.cboPrincipalAttorney_SelectedIndexChanged);
            // 
            // cboProHac
            // 
            this.cboProHac.FormattingEnabled = true;
            this.cboProHac.Location = new System.Drawing.Point(126, 626);
            this.cboProHac.Name = "cboProHac";
            this.cboProHac.Size = new System.Drawing.Size(190, 21);
            this.cboProHac.TabIndex = 104;
            // 
            // tbxBarOther
            // 
            this.tbxBarOther.Location = new System.Drawing.Point(126, 600);
            this.tbxBarOther.Name = "tbxBarOther";
            this.tbxBarOther.Size = new System.Drawing.Size(190, 20);
            this.tbxBarOther.TabIndex = 105;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 606);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 106;
            this.label12.Text = "Bar admission";
            // 
            // lvwIncluded
            // 
            this.lvwIncluded.FullRowSelect = true;
            this.lvwIncluded.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvwIncluded.Location = new System.Drawing.Point(127, 653);
            this.lvwIncluded.Name = "lvwIncluded";
            this.lvwIncluded.Size = new System.Drawing.Size(189, 82);
            this.lvwIncluded.TabIndex = 107;
            this.lvwIncluded.UseCompatibleStateImageBehavior = false;
            this.lvwIncluded.View = System.Windows.Forms.View.Details;
            this.lvwIncluded.SelectedIndexChanged += new System.EventHandler(this.lvwIncluded_SelectedIndexChanged);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(151, 741);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(165, 45);
            this.richTextBox2.TabIndex = 112;
            this.richTextBox2.Tag = "defendantPlaintiffName";
            this.richTextBox2.Text = "";
            // 
            // cboFor
            // 
            this.cboFor.FormattingEnabled = true;
            this.cboFor.Location = new System.Drawing.Point(16, 741);
            this.cboFor.Name = "cboFor";
            this.cboFor.Size = new System.Drawing.Size(105, 21);
            this.cboFor.TabIndex = 111;
            this.cboFor.Tag = "defendantPlaintiff";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 634);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 13);
            this.label13.TabIndex = 108;
            this.label13.Text = "Pro hac vice";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 724);
            this.label11.MinimumSize = new System.Drawing.Size(100, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 13);
            this.label11.TabIndex = 115;
            this.label11.Text = "Attorneys for";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 577);
            this.label14.MinimumSize = new System.Drawing.Size(100, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 116;
            this.label14.Text = "Other attorneys";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(320, 658);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(21, 21);
            this.btnAdd.TabIndex = 117;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnTake
            // 
            this.btnTake.Location = new System.Drawing.Point(320, 680);
            this.btnTake.Name = "btnTake";
            this.btnTake.Size = new System.Drawing.Size(21, 21);
            this.btnTake.TabIndex = 118;
            this.btnTake.Text = "-";
            this.btnTake.UseVisualStyleBackColor = true;
            this.btnTake.Click += new System.EventHandler(this.btnTake_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(320, 702);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(21, 21);
            this.btnUpdate.TabIndex = 119;
            this.btnUpdate.Text = "!";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // cboOthers
            // 
            this.cboOthers.FormattingEnabled = true;
            this.cboOthers.Location = new System.Drawing.Point(126, 573);
            this.cboOthers.Name = "cboOthers";
            this.cboOthers.Size = new System.Drawing.Size(190, 21);
            this.cboOthers.TabIndex = 120;
            this.cboOthers.Tag = "";
            // 
            // chkTOC
            // 
            this.chkTOC.AutoSize = true;
            this.chkTOC.Location = new System.Drawing.Point(18, 797);
            this.chkTOC.Name = "chkTOC";
            this.chkTOC.Size = new System.Drawing.Size(110, 17);
            this.chkTOC.TabIndex = 121;
            this.chkTOC.Text = "Table of Contents";
            this.chkTOC.UseVisualStyleBackColor = true;
            this.chkTOC.CheckedChanged += new System.EventHandler(this.chkTOC_CheckedChanged);
            // 
            // chkCertOfService
            // 
            this.chkCertOfService.AutoSize = true;
            this.chkCertOfService.Location = new System.Drawing.Point(18, 814);
            this.chkCertOfService.Name = "chkCertOfService";
            this.chkCertOfService.Size = new System.Drawing.Size(124, 17);
            this.chkCertOfService.TabIndex = 122;
            this.chkCertOfService.Text = "Certificate of Service";
            this.chkCertOfService.UseVisualStyleBackColor = true;
            this.chkCertOfService.CheckedChanged += new System.EventHandler(this.chkCertOfService_CheckedChanged);
            // 
            // chkIncHeading3
            // 
            this.chkIncHeading3.AutoSize = true;
            this.chkIncHeading3.Location = new System.Drawing.Point(147, 797);
            this.chkIncHeading3.Name = "chkIncHeading3";
            this.chkIncHeading3.Size = new System.Drawing.Size(113, 17);
            this.chkIncHeading3.TabIndex = 123;
            this.chkIncHeading3.Text = "Include Heading 3";
            this.chkIncHeading3.UseVisualStyleBackColor = true;
            this.chkIncHeading3.CheckedChanged += new System.EventHandler(this.chkIncHeading3_CheckedChanged);
            // 
            // chkAffidavit
            // 
            this.chkAffidavit.AutoSize = true;
            this.chkAffidavit.Location = new System.Drawing.Point(147, 814);
            this.chkAffidavit.Name = "chkAffidavit";
            this.chkAffidavit.Size = new System.Drawing.Size(115, 17);
            this.chkAffidavit.TabIndex = 124;
            this.chkAffidavit.Text = "Affidavit of Service";
            this.chkAffidavit.UseVisualStyleBackColor = true;
            this.chkAffidavit.CheckedChanged += new System.EventHandler(this.chkAffidavit_CheckedChanged);
            // 
            // chkAffirmation
            // 
            this.chkAffirmation.AutoSize = true;
            this.chkAffirmation.Location = new System.Drawing.Point(18, 832);
            this.chkAffirmation.Name = "chkAffirmation";
            this.chkAffirmation.Size = new System.Drawing.Size(126, 17);
            this.chkAffirmation.TabIndex = 125;
            this.chkAffirmation.Text = "Affirmation of Service";
            this.chkAffirmation.UseVisualStyleBackColor = true;
            this.chkAffirmation.CheckedChanged += new System.EventHandler(this.chkAffirmation_CheckedChanged);
            // 
            // ctpPleadingUS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.chkAffirmation);
            this.Controls.Add(this.chkAffidavit);
            this.Controls.Add(this.chkIncHeading3);
            this.Controls.Add(this.chkCertOfService);
            this.Controls.Add(this.chkTOC);
            this.Controls.Add(this.cboOthers);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnTake);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.cboFor);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lvwIncluded);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxBarOther);
            this.Controls.Add(this.cboProHac);
            this.Controls.Add(this.cboPrincipalAttorney);
            this.Controls.Add(this.tbxBarAdmission);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.chkInclude);
            this.Controls.Add(this.chkS);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.tbxPhone);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboSubmittedBy);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.tbxCityDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxDefendant);
            this.Controls.Add(this.cboDefendant);
            this.Controls.Add(this.tbxPlaintiff);
            this.Controls.Add(this.cboVersus);
            this.Controls.Add(this.cboPlaintiff);
            this.Controls.Add(this.tbxIndex);
            this.Controls.Add(this.cboIndex);
            this.Controls.Add(this.chkBelow);
            this.Controls.Add(this.tbxHeading);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxOther);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxDistrict);
            this.Controls.Add(this.tbxFederal);
            this.Controls.Add(this.tbxCounty);
            this.Controls.Add(this.pnlCourtType);
            this.Controls.Add(this.chkDraft);
            this.Name = "ctpPleadingUS";
            this.Size = new System.Drawing.Size(351, 852);
            this.Load += new System.EventHandler(this.ctpPleadingUS_Load);
            this.pnlCourtType.ResumeLayout(false);
            this.pnlCourtType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDraft;
        private System.Windows.Forms.RadioButton rbnState;
        private System.Windows.Forms.RadioButton rbnFederal;
        private System.Windows.Forms.RadioButton rbnOther;
        private System.Windows.Forms.Panel pnlCourtType;
        private CustomTextBox tbxCounty;
        private CustomTextBox tbxFederal;
        private CustomTextBox tbxDistrict;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxOther;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxHeading;
        private System.Windows.Forms.CheckBox chkBelow;
        private CustomTaskPanes.CustomComboBox cboIndex;
        private CustomTaskPanes.CustomComboBox cboPlaintiff;
        private CustomTaskPanes.CustomComboBox cboVersus;
        private CustomTextBox tbxIndex;
        private CustomTaskPanes.CustomRichTextBox tbxPlaintiff;
        private CustomTaskPanes.CustomRichTextBox tbxDefendant;
        private CustomTaskPanes.CustomComboBox cboDefendant;
        private System.Windows.Forms.Label label4;
        private CustomTextBox tbxCityDate;
        private CustomTaskPanes.CustomDateTimePicker dtpDate;
        private System.Windows.Forms.Label label5;
        private CustomTaskPanes.CustomComboBox cboSubmittedBy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private CustomTextBox tbxPhone;
        private CustomTextBox tbxFax;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkS;
        private System.Windows.Forms.CheckBox chkInclude;
        private System.Windows.Forms.Label label10;
        private CustomTextBox tbxBarAdmission;
        private CustomTaskPanes.CustomComboBox cboPrincipalAttorney;
        private CustomTaskPanes.CustomComboBox cboProHac;
        private CustomTextBox tbxBarOther;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lvwIncluded;
        private CustomTaskPanes.CustomRichTextBox richTextBox2;
        private CustomTaskPanes.CustomComboBox cboFor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnTake;
        private System.Windows.Forms.Button btnUpdate;
        private CustomTaskPanes.CustomComboBox cboOthers;
        private System.Windows.Forms.CheckBox chkTOC;
        private System.Windows.Forms.CheckBox chkCertOfService;
        private System.Windows.Forms.CheckBox chkIncHeading3;
        private System.Windows.Forms.CheckBox chkAffidavit;
        private System.Windows.Forms.CheckBox chkAffirmation;
    }
}
