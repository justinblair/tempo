﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpStyleUI : UserControl
    {
        private Document document;
        public ctpStyleUI()
        {
            this.tvwStyleUI = new Forms.StylesUITreeView();
            InitializeComponent();
        }

        private void ctpStyleUI_Load(object sender, EventArgs e)
        {
            this.document = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(true);
            tvwStyleUI.LoadStyles(document);
        }

        private void btnMoveUp_Click(object sender, EventArgs e)
        {
            try
            {
                Forms.StylesUITreeNode node = (Forms.StylesUITreeNode)tvwStyleUI.SelectedNode;
                if (node.Parent != null)
                    node = (Forms.StylesUITreeNode)node.Parent;
                node.ChangePriority(true);
                Forms.StylesUITreeNode prevNode = (Forms.StylesUITreeNode)node.PrevNode;
                prevNode.ChangePriority(false);
                RefreshStylesPane();
                tvwStyleUI.LoadStyles();
            }
            catch { }

        }

        private void RefreshStylesPane()
        {
            Word.Style normal = document.WordDocument.Styles[Word.WdBuiltinStyle.wdStyleNormal];
            normal.Visibility = !normal.Visibility;
            normal.Visibility = !normal.Visibility;
        }

        private void btnMoveDown_Click(object sender, EventArgs e)
        {
            try
            {
                Forms.StylesUITreeNode node = (Forms.StylesUITreeNode)tvwStyleUI.SelectedNode;
                if (node.Parent != null)
                    node = (Forms.StylesUITreeNode)node.Parent;
                node.ChangePriority(false);
                Forms.StylesUITreeNode prevNode = (Forms.StylesUITreeNode)node.NextNode;
                prevNode.ChangePriority(true);
                RefreshStylesPane();
                tvwStyleUI.LoadStyles();
            }
            catch { }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            tvwStyleUI.RefreshStyles();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            tvwStyleUI.Load();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            tvwStyleUI.Save();
        }
    }
}
