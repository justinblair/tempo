﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpUSLetter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxOurRef = new CustomTextBox();
            this.dtpDate = new CustomTaskPanes.CustomDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.cboDelivery = new CustomTaskPanes.CustomComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxJobTitle = new CustomTextBox();
            this.tbxCompany = new CustomTextBox();
            this.textBox1 = new CustomTextBox();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboClosing = new CustomTaskPanes.CustomComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lvwContacts = new System.Windows.Forms.ListView();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAuthor = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbxAuthor = new CustomTextBox();
            this.tbxFax = new CustomTextBox();
            this.tbxMail = new CustomTextBox();
            this.tbxAuthorTel = new CustomTextBox();
            this.btnEnvelope = new System.Windows.Forms.Button();
            this.chkSRA = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lvwPrivacy = new System.Windows.Forms.ListView();
            this.chkDraft = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxSubject = new CustomTextBox();
            this.btnLookup = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxContact = new CustomTextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(111, 181);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(225, 20);
            this.tbxOurRef.TabIndex = 0;
            this.tbxOurRef.Tag = "ourRef";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMMM d, yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(111, 211);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(222, 20);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.Tag = "date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            // 
            // cboDelivery
            // 
            this.cboDelivery.FormattingEnabled = true;
            this.cboDelivery.Location = new System.Drawing.Point(111, 242);
            this.cboDelivery.Name = "cboDelivery";
            this.cboDelivery.Size = new System.Drawing.Size(221, 21);
            this.cboDelivery.TabIndex = 4;
            this.cboDelivery.Tag = "delivery";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Delivery";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 184);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Our reference";
            // 
            // tbxJobTitle
            // 
            this.tbxJobTitle.Location = new System.Drawing.Point(111, 401);
            this.tbxJobTitle.Name = "tbxJobTitle";
            this.tbxJobTitle.Size = new System.Drawing.Size(220, 20);
            this.tbxJobTitle.TabIndex = 10;
            this.tbxJobTitle.Tag = "jobTitle";
            // 
            // tbxCompany
            // 
            this.tbxCompany.Location = new System.Drawing.Point(111, 427);
            this.tbxCompany.Name = "tbxCompany";
            this.tbxCompany.Size = new System.Drawing.Size(220, 20);
            this.tbxCompany.TabIndex = 11;
            this.tbxCompany.Tag = "company";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(111, 563);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(221, 20);
            this.textBox1.TabIndex = 12;
            this.textBox1.Tag = "salutation";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(111, 456);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(222, 75);
            this.tbxAddress.TabIndex = 13;
            this.tbxAddress.Tag = "address";
            this.tbxAddress.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 432);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Company";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 459);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Address";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 566);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Salutation";
            // 
            // cboClosing
            // 
            this.cboClosing.FormattingEnabled = true;
            this.cboClosing.Location = new System.Drawing.Point(111, 589);
            this.cboClosing.Name = "cboClosing";
            this.cboClosing.Size = new System.Drawing.Size(221, 21);
            this.cboClosing.TabIndex = 21;
            this.cboClosing.Tag = "closing";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 594);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Closing";
            // 
            // lvwContacts
            // 
            this.lvwContacts.FullRowSelect = true;
            this.lvwContacts.GridLines = true;
            this.lvwContacts.Location = new System.Drawing.Point(110, 269);
            this.lvwContacts.Name = "lvwContacts";
            this.lvwContacts.Size = new System.Drawing.Size(221, 98);
            this.lvwContacts.TabIndex = 24;
            this.lvwContacts.UseCompatibleStateImageBehavior = false;
            this.lvwContacts.View = System.Windows.Forms.View.Details;
            this.lvwContacts.SelectedIndexChanged += new System.EventHandler(this.lvwContacts_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 269);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Outlook contacts";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 404);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Title";
            // 
            // btnAuthor
            // 
            this.btnAuthor.Location = new System.Drawing.Point(313, 620);
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(21, 21);
            this.btnAuthor.TabIndex = 55;
            this.btnAuthor.Text = "...";
            this.btnAuthor.UseVisualStyleBackColor = true;
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 707);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 53;
            this.label14.Text = "Author email";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 681);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 52;
            this.label15.Text = "Author fax";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(14, 655);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 17);
            this.label16.TabIndex = 51;
            this.label16.Text = "Author tel";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 627);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Author";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(111, 621);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(196, 20);
            this.tbxAuthor.TabIndex = 45;
            this.tbxAuthor.Tag = "authorName";
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(111, 675);
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(238, 20);
            this.tbxFax.TabIndex = 48;
            this.tbxFax.Tag = "authorFax";
            // 
            // tbxMail
            // 
            this.tbxMail.Location = new System.Drawing.Point(111, 701);
            this.tbxMail.Name = "tbxMail";
            this.tbxMail.Size = new System.Drawing.Size(238, 20);
            this.tbxMail.TabIndex = 49;
            this.tbxMail.Tag = "authorEmail";
            // 
            // tbxAuthorTel
            // 
            this.tbxAuthorTel.Location = new System.Drawing.Point(111, 649);
            this.tbxAuthorTel.Name = "tbxAuthorTel";
            this.tbxAuthorTel.Size = new System.Drawing.Size(238, 20);
            this.tbxAuthorTel.TabIndex = 47;
            this.tbxAuthorTel.Tag = "authorTelephone";
            // 
            // btnEnvelope
            // 
            this.btnEnvelope.Location = new System.Drawing.Point(14, 805);
            this.btnEnvelope.Name = "btnEnvelope";
            this.btnEnvelope.Size = new System.Drawing.Size(78, 28);
            this.btnEnvelope.TabIndex = 56;
            this.btnEnvelope.Text = "Envelope";
            this.btnEnvelope.UseVisualStyleBackColor = true;
            this.btnEnvelope.Visible = false;
            // 
            // chkSRA
            // 
            this.chkSRA.AutoSize = true;
            this.chkSRA.Location = new System.Drawing.Point(14, 734);
            this.chkSRA.Name = "chkSRA";
            this.chkSRA.Size = new System.Drawing.Size(118, 17);
            this.chkSRA.TabIndex = 57;
            this.chkSRA.Text = "Include SRA notice";
            this.chkSRA.UseVisualStyleBackColor = true;
            this.chkSRA.CheckedChanged += new System.EventHandler(this.chkSRA_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 60;
            this.label4.Text = "Privacy";
            // 
            // lvwPrivacy
            // 
            this.lvwPrivacy.CheckBoxes = true;
            this.lvwPrivacy.Location = new System.Drawing.Point(110, 46);
            this.lvwPrivacy.Name = "lvwPrivacy";
            this.lvwPrivacy.Size = new System.Drawing.Size(222, 129);
            this.lvwPrivacy.TabIndex = 59;
            this.lvwPrivacy.UseCompatibleStateImageBehavior = false;
            this.lvwPrivacy.View = System.Windows.Forms.View.Details;
            // 
            // chkDraft
            // 
            this.chkDraft.AutoSize = true;
            this.chkDraft.Location = new System.Drawing.Point(18, 15);
            this.chkDraft.Name = "chkDraft";
            this.chkDraft.Size = new System.Drawing.Size(49, 17);
            this.chkDraft.TabIndex = 61;
            this.chkDraft.Text = "Draft";
            this.chkDraft.UseVisualStyleBackColor = true;
            this.chkDraft.CheckedChanged += new System.EventHandler(this.chkDraft_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 541);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 63;
            this.label10.Text = "Subject";
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(111, 537);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(221, 20);
            this.tbxSubject.TabIndex = 62;
            this.tbxSubject.Tag = "subject";
            this.tbxSubject.TextChanged += new System.EventHandler(this.tbxSubject_TextChanged_1);
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(311, 373);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(21, 21);
            this.btnLookup.TabIndex = 67;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 377);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "Contact";
            // 
            // tbxContact
            // 
            this.tbxContact.Location = new System.Drawing.Point(111, 375);
            this.tbxContact.Name = "tbxContact";
            this.tbxContact.Size = new System.Drawing.Size(191, 20);
            this.tbxContact.TabIndex = 65;
            this.tbxContact.Tag = "contact";
            this.tbxContact.TextChanged += new System.EventHandler(this.tbxContact_TextChanged_1);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(14, 771);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(78, 28);
            this.btnReset.TabIndex = 68;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            // 
            // ctpUSLetter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxContact);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.chkDraft);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lvwPrivacy);
            this.Controls.Add(this.chkSRA);
            this.Controls.Add(this.btnEnvelope);
            this.Controls.Add(this.btnAuthor);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.tbxMail);
            this.Controls.Add(this.tbxAuthorTel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lvwContacts);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cboClosing);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tbxCompany);
            this.Controls.Add(this.tbxJobTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboDelivery);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.tbxOurRef);
            this.Name = "ctpUSLetter";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpUSLetter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTextBox tbxOurRef;
        private CustomTaskPanes.CustomDateTimePicker dtpDate;
        private System.Windows.Forms.Label label2;
        private CustomTaskPanes.CustomComboBox cboDelivery;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxJobTitle;
        private CustomTextBox tbxCompany;
        private CustomTextBox textBox1;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CustomTaskPanes.CustomComboBox cboClosing;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListView lvwContacts;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAuthor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private CustomTextBox tbxAuthor;
        private CustomTextBox tbxFax;
        private CustomTextBox tbxMail;
        private CustomTextBox tbxAuthorTel;
        private System.Windows.Forms.Button btnEnvelope;
        private System.Windows.Forms.CheckBox chkSRA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lvwPrivacy;
        private System.Windows.Forms.CheckBox chkDraft;
        private System.Windows.Forms.Label label10;
        private CustomTextBox tbxSubject;
        private System.Windows.Forms.Button btnLookup;
        private System.Windows.Forms.Label label5;
        private CustomTextBox tbxContact;
        private System.Windows.Forms.Button btnReset;
    }
}
