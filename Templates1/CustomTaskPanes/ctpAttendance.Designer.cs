﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpAttendance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxClient = new CustomTextBox();
            this.tbxMatter = new CustomTextBox();
            this.tbxAuthor = new CustomTextBox();
            this.tbxFeeEarner = new CustomTextBox();
            this.tbxMatterNo = new CustomTextBox();
            this.textBox1 = new CustomTextBox();
            this.dateTimePicker = new CustomTaskPanes.CustomDateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbxClient
            // 
            this.tbxClient.Location = new System.Drawing.Point(91, 17);
            this.tbxClient.Name = "tbxClient";
            this.tbxClient.Size = new System.Drawing.Size(244, 20);
            this.tbxClient.TabIndex = 0;
            this.tbxClient.Tag = "client";
            this.tbxClient.TextChanged += new System.EventHandler(this.tbxClient_TextChanged);
            // 
            // tbxMatter
            // 
            this.tbxMatter.Location = new System.Drawing.Point(91, 43);
            this.tbxMatter.Name = "tbxMatter";
            this.tbxMatter.Size = new System.Drawing.Size(244, 20);
            this.tbxMatter.TabIndex = 1;
            this.tbxMatter.Tag = "matter";
            this.tbxMatter.TextChanged += new System.EventHandler(this.tbxMatter_TextChanged);
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(91, 69);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(244, 20);
            this.tbxAuthor.TabIndex = 2;
            this.tbxAuthor.Tag = "author";
            this.tbxAuthor.TextChanged += new System.EventHandler(this.tbxAuthor_TextChanged);
            // 
            // tbxFeeEarner
            // 
            this.tbxFeeEarner.Location = new System.Drawing.Point(91, 121);
            this.tbxFeeEarner.Name = "tbxFeeEarner";
            this.tbxFeeEarner.Size = new System.Drawing.Size(244, 20);
            this.tbxFeeEarner.TabIndex = 3;
            this.tbxFeeEarner.Tag = "authorName";
            this.tbxFeeEarner.TextChanged += new System.EventHandler(this.tbxFeeEarner_TextChanged);
            // 
            // tbxMatterNo
            // 
            this.tbxMatterNo.Location = new System.Drawing.Point(91, 95);
            this.tbxMatterNo.Name = "tbxMatterNo";
            this.tbxMatterNo.Size = new System.Drawing.Size(244, 20);
            this.tbxMatterNo.TabIndex = 4;
            this.tbxMatterNo.Tag = "ourRef";
            this.tbxMatterNo.TextChanged += new System.EventHandler(this.tbxMatterNo_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(91, 175);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(244, 20);
            this.textBox1.TabIndex = 5;
            this.textBox1.Tag = "subjectLine";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(91, 149);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(244, 20);
            this.dateTimePicker.TabIndex = 6;
            this.dateTimePicker.Tag = "date";
            this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Client";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Matter";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Author";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Matter number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Fee earner";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Subject";
            // 
            // ctpAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tbxMatterNo);
            this.Controls.Add(this.tbxFeeEarner);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxMatter);
            this.Controls.Add(this.tbxClient);
            this.Name = "ctpAttendance";
            this.Size = new System.Drawing.Size(363, 863);
            this.Load += new System.EventHandler(this.ctpAttendance_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTextBox tbxClient;
        private CustomTextBox tbxMatter;
        private CustomTextBox tbxAuthor;
        private CustomTextBox tbxFeeEarner;
        private CustomTextBox tbxMatterNo;
        private CustomTextBox textBox1;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}
