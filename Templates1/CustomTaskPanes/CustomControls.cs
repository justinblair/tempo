﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Templates1.CustomTaskPanes
{

    public class CustomControl : Control
    {

        private Graphics gr;
        private Image img;
        private Control control;
        private ControlTag ctag;
        private ErrorProvider errP;

        public CustomControl(Control ctl)
        {
            try
            {
                this.control = ctl;
                this.control.TextChanged += new EventHandler(CustomControl_TextChanged);
                this.ctag = new ControlTag(ctl);
                errP = new ErrorProvider((ContainerControl)this.control.Parent);
                errP.SetIconAlignment(this.control, ErrorIconAlignment.TopRight);
                errP.SetIconPadding(this.control, 0);
                errP.BlinkStyle = ErrorBlinkStyle.BlinkIfDifferentError;
                errP.BlinkRate = 1000;
            }
            catch { }
            
        }

        void CustomControl_TextChanged(object sender, EventArgs e)
        {
            //Repaint();
            try
            {
                if (this.control.Tag.ToString().Length > 0)
                    errP.SetError(this.control, "");
                else
                    errP.SetError(this.control, "No tag");
            }
            catch { }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {
                this.ctag.Draw(e);
                return;
            }
            catch { }
        }

        public void Repaint()
        {
            this.img = new Bitmap(8, 8);
            this.gr = this.control.CreateGraphics();
            this.OnPaint(new PaintEventArgs(this.gr, new Rectangle(this.control.Location.X,this.control.Location.Y,30,30)));
        }
        
       
    }
    
    public interface ICustomControl
    {

        
    }
    
    
    
    public class CustomTextBox : TextBox
    {
        private ControlTag tag;
        private Control cc;
        private CustomControl con;
        public CustomTextBox()
        {
           
            this.cc = (Control)this;
            this.con = new CustomControl(this);
            
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {

                ControlTag tag = new ControlTag(this);
                tag.Draw(e);
                return;
            }
            catch { }
        }


       
    }

    public class CustomComboBox : ComboBox
    {
        private ControlTag tag;
        private Control cc;
        private CustomControl con;
        public CustomComboBox()
        {
           
            this.cc = (Control)this;
            this.con = new CustomControl(this);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {

                ControlTag tag = new ControlTag(this);
                tag.Draw(e);
                return;
            }
            catch { }
        }
       
    }

    public class CustomCheckedListBox : CheckedListBox
    {
        private ControlTag tag;
        private Control cc;
        private CustomControl con;
        public CustomCheckedListBox()
        {
           
            this.cc = (Control)this;
            this.con = new CustomControl(this);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {

                ControlTag tag = new ControlTag(this);
                tag.Draw(e);
                return;
            }
            catch { }
        }
    }

    public class CustomRichTextBox : RichTextBox
    {
        private ControlTag tag;
        private Control cc;
        private CustomControl con;
        
        public CustomRichTextBox()
        {
           
            this.cc = (Control)this;
            this.con = new CustomControl(this);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {

                ControlTag tag = new ControlTag(this);
                tag.Draw(e);
                return;
            }
            catch { }
        }
        
    }

    public class CustomDateTimePicker : DateTimePicker
    {
        private ControlTag tag;
        private Control cc;
        private CustomControl con;
        public CustomDateTimePicker()
        {
            
            this.cc = (Control)this;
            this.con = new CustomControl(this);
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            try
            {

                ControlTag tag = new ControlTag(this);
                tag.Draw(e);
                return;
            }
            catch { }
        }
        
    }

    public class ControlTag
    {
        private System.Drawing.Graphics graph;
        private System.Drawing.Image img;
        private Control control;
        public ControlTag(Control ctl)
        {
            this.control = ctl;

            
            

        }
        public void Draw(PaintEventArgs e)
           
        {
            img = new Bitmap(8, 8);
            this.graph = e.Graphics;
            this.graph.DrawRectangle(new Pen(Brushes.Orange), e.ClipRectangle);

            //Rectangle r = e.ClipRectangle;
            
            //Graphics orig = Graphics.FromImage(img);
            
            //Brush b = Brushes.Orange;
            //orig.FillRectangle(b,r);
            //graph.DrawImage(img, 0, 0);
            
        }

        public void Hide()
        {
            this.graph.Clear(Color.Transparent);
        }

        public void Show()
        {
            this.graph.Clear(Color.Orange);
            
        }
    }
}
