﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpCourt : UserControl
    {
        
        private Boolean bNoUpdate;
        public ctpCourt()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Triggered whem the court task pane loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void ctpCourt_Load(object sender, EventArgs e)
        {

            LetterParameters lp = new LetterParameters();
            
            cboClaim.Items.AddRange(lp.Claim.ToArray());
            cboCourt.Items.AddRange(lp.Court.ToArray());
            cboDivision.Items.AddRange(lp.Division.ToArray());
            cboClaimant.Items.AddRange(lp.Claimant.ToArray());
            cboDefendants.Items.AddRange(lp.Defendant.ToArray());
            cboClaimDefendant.Items.AddRange(lp.Representing.ToArray());
            cboFax.Items.Add(Globals.ThisAddIn.Officedata.Offices.ActiveOffice.Fax);
            //XmlDoc = new XmlDocument();
            //clsHelper helper = new clsHelper();
            //XmlDoc.Load(helper.Params);

            //foreach (XmlNode nodNode in XmlDoc.SelectNodes("//claim/param"))
            //    this.cboClaim.Items.Add(nodNode.InnerText);
            //foreach (XmlNode nodNode in XmlDoc.SelectNodes("//court/param"))
            //    this.cboCourt.Items.Add(nodNode.InnerText);
            //foreach (XmlNode nodNode in XmlDoc.SelectNodes("//division/param"))
            //    this.cboDivision.Items.Add(nodNode.InnerText);
            //foreach (XmlNode nodNode in XmlDoc.SelectNodes("//claimant/param"))
            //{ 
            //    this.cboClaimant.Items.Add(nodNode.InnerText);
            //    this.cboClaimDefendant.Items.Add(nodNode.InnerText.Replace("(s)", ""));
            //}
            //foreach (XmlNode nodNode in XmlDoc.SelectNodes("//defendant/param"))
            //{ 
            //    this.cboDefendants.Items.Add(nodNode.InnerText);
            //    this.cboClaimDefendant.Items.Add(nodNode.InnerText.Replace("(s)", ""));
            //}

            //foreach (XmlNode nodNode in Globals.ThisAddIn.Officedata.Officedata.SelectNodes("//office[@id='MdRLondon']/fax"))
            //    this.cboFax.Items.Add(nodNode.InnerText);
  
            foreach(Control ctl in this.Controls)
                if (ctl.GetType().ToString().ToLower().Contains("combobox") || ctl.GetType().ToString().ToLower().Contains("textbox"))
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);


        }

        /// <summary>
        /// Triggered when a control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        void ctl_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

       
        /// <summary>
        /// Updates the relevant content control in the document with data from the task pane
        /// </summary>
        /// <param name="sender"></param>
        
        private void TextBoxUpdate(object sender)
        {
            if (bNoUpdate) return;
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
           ctl = clsWordApp.getContentControl(tbx.Tag.ToString()+"B", false);
            if (ctl != null)
            { 
                ctl.Range.Text = tbx.Text;
                if (ctl.Tag == "claimB" && tbx.Text.Length > 0)
                { 
                    ctl.Range.Font.Hidden = 0;
                    ctl.Range.Font.ColorIndex = Word.WdColorIndex.wdBlack;
                    ctl.Range.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                }
            }
        }

        
        /// <summary>
        /// Updates the back page with data from the task pane controls
        /// </summary>
        
        public void FillBackPage()
        {
            foreach(Control control in this.Controls)
                if (control.Tag!=null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString()+"B", false);
                    if (ctl != null)
                        ctl.Range.Text = control.Text;
                }
        }
        
        
        
        /// <summary>
        /// Repopulates the court task pane with data from the document
        /// </summary>
        
        public void RepopulateCourtPane()
        {
            bNoUpdate = true;
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    if (ctl!=null)
                    { 
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                    }
                    ctl = clsWordApp.getContentControl(control.Tag.ToString()+"B", false);
                    if (ctl != null)
                    {
                        try
                        {
                            if (!ctl.ShowingPlaceholderText)
                                control.Text = ctl.Range.Text;
                        }
                        catch { }
                    }
                }
            }

            bNoUpdate = false;
        }

        /// <summary>
        /// Enables the controls in the task pane based on the contents of the word document
        /// </summary>
        public void EnableControls()
        {
            bNoUpdate = true;
            Boolean hasCoverPage = clsWordApp.ContentControlExistsByID("1534691451");
            Boolean hasBackPage = clsWordApp.ContentControlExistsByID("4221221192");
            chkCover.Checked = hasCoverPage;
            if (chkCover.Checked)
                this.pbCover.Value = 100;
            if (hasBackPage) hasCoverPage = true;

            foreach (Control ctl in this.Controls)
                if (ctl.Location.Y < this.pbxDivider.Location.Y)
                    ctl.Enabled = hasCoverPage;
                else
                    ctl.Enabled = hasBackPage;

            chkBack.Enabled = true;
            chkCover.Enabled = true;
            chkBack.Checked = hasBackPage;
            if (chkBack.Checked)
                this.pbBack.Value = 100;
            bNoUpdate = false;
        }

       /// <summary>
       /// Triggered when the back page check box is clicked
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        
        private void chkBack_CheckedChanged(object sender, EventArgs e)
        {
            if (bNoUpdate) return;
            if (chkBack.Checked)
            {
                clsDynamicInserts.InsertCourtBackPage();
                this.pbBack.Value = 50;
                FillBackPage();
                this.pbBack.Value = 100;
            }
            else
            { 
                clsDynamicInserts.RemoveCourtBackPage();
                this.pbBack.Value = 0;
            }
           EnableControls();
           ResetBack();
           
        }

       /// <summary>
       /// Triggered when the cover page check box is clicked
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        
        private void chkCover_CheckedChanged(object sender, EventArgs e)
       {
           if (bNoUpdate) return;
           if (chkCover.Checked)
           {
               pbCover.Value = 50;
               clsDynamicInserts.InsertFrontPage("Court front sheet");
               pbCover.Value = 100;
           }
           else
           {
               pbCover.Value = 50;
               clsDynamicInserts.RemoveFrontPage();
               pbCover.Value = 0;
           }
           EnableControls();
           ResetCover();
           
       }

       /// <summary>
       /// Resets the cover page controls in the task pane
       /// </summary>
        
        private void ResetCover()
       {
           foreach (Control ctl in this.Controls)
               if(ctl.Location.Y < this.pbxDivider.Location.Y)
               if (ctl.GetType().ToString().ToLower().Contains("combobox") || ctl.GetType().ToString().ToLower().Contains("textbox"))
                   ctl.Text = "";
       }

        /// <summary>
        /// Resets the back page controls in the task pane
        /// </summary>
        
        private void ResetBack()
        {
            foreach (Control ctl in this.Controls)
                if (ctl.Location.Y > this.pbxDivider.Location.Y)
                    if (ctl.GetType().ToString().ToLower().Contains("combobox") || ctl.GetType().ToString().ToLower().Contains("textbox"))
                        ctl.Text = "";
        }

        /// <summary>
        /// Triggered when the cover page reset button is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnResetCover_Click(object sender, EventArgs e)
        {
            ResetCover();
        }

       /// <summary>
       /// Triggered when the back page reset button is pressed
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        
        private void btnResetBack_Click(object sender, EventArgs e)
        {
            ResetBack();
        }
        
    }
}
