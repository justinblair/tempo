﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpUSFax : UserControl
    {
        private WorkSite8Application DMS;
        private clsOfficeData officedata;
        private clsOutlook ol;
        private Boolean bNoUpdating;
        
        private clsInterAction IA;
        
        public ctpUSFax()
        {
            InitializeComponent();
        }

        public void RepopulateFaxUSTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }
            RepopulateCCs();
            Word.ContentControl ctl2 = clsWordApp.getContentControl("rubric", false);
            this.chkSRA.Checked = ctl2.Range.Text.Contains("SRA");
        }

        public void RepopulateCCs()
        {
            char[] paragraph = Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray();
            String[] to;
            String[] org;
            String[] fax;

            Word.ContentControl ctl = clsWordApp.getContentControl("cc1", false);
            if (ctl.ShowingPlaceholderText)
                return;

                to = clsWordApp.getContentControl("cc1", false).Range.Text.Split(paragraph);
                org = clsWordApp.getContentControl("cc2", false).Range.Text.Split(paragraph);
                fax = clsWordApp.getContentControl("cc3", false).Range.Text.Split(paragraph);
            
            int i = -1;
            foreach (String item in to)
            {
                i++;
                if (to[i].Length > 0)
                {
                    ListViewItem lvw = new ListViewItem(item);
                    lvw.SubItems.Add(org[i]);
                    lvw.SubItems.Add(fax[i]);
                    lvwCC.Items.Add(lvw);
                }
            }
        }
        
        public void Fetch()
        {
            
                if (DMS == null)
                    DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
                this.tbxOurRef.Text = DMS.ClientMatterCode;
            if(DMS.IsOnline)
            { 
                FetchAuthor(DMS);
            }
                SetDate();
                clsWordApp.getContentControl("pagesCount", false).Range.Text = this.numPages.Value.ToString();
                this.tbxReturnNumber.Text = officedata.Offices.ActiveOffice.FaxTransError;
            
        }

        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();

            this.tbxAuthorTel.Text = adu.TelephoneNumber;
            this.tbxMail.Text = adu.Mail;
            this.tbxFax.Text = officedata.NYFaxNumber;
            this.tbxAuthor.Text = adu.ExtendedFullName;

        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dtpDate.Text;
        }

        private void LoadContacts()
        {
            ol = new clsOutlook();
            ol.GetContacts(lvwContacts);
        }

        private void SetListView()
        {
            lvwContacts.Columns.Add("Contact");
            lvwContacts.Columns.Add("Org.");
            lvwContacts.Columns[0].Width = 190 / 2;
            lvwContacts.Columns[1].Width = 190 / 2;
            lvwCC.Columns.Add("CC");
            lvwCC.Columns.Add("Org.");
            lvwCC.Columns.Add("Fax");
        }

        private void LoadCombosEtc()
        {
            LetterParameters lp = new LetterParameters();
            this.cboClosing.Items.AddRange(lp.USClosing.ToArray());
            cboClosing.Text = "Sincerely";
            ctl_TextChanged((object)cboClosing, null);
            
        }

        void ctl_TextChanged(object sender, EventArgs e)
        {
            //if (bNoUpdating)
            //    return;
            Control tbx = (Control)sender;
            List<Word.ContentControl> ctls = clsWordApp.getContentControls(tbx.Tag.ToString(), Globals.ThisAddIn.Application.ActiveDocument);
            foreach(Word.ContentControl ctl in ctls)
                ctl.Range.Text = tbx.Text;
        }

        private void ctpUSFax_Load(object sender, EventArgs e)
        {
            SetListView();
            LoadContacts();
            officedata = Globals.ThisAddIn.Officedata;
            LoadCombosEtc();
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
            btnCCLookup.Enabled = online;


        }

        private void lvwContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvwContacts.Items)
                if (lvi.Selected)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = lvwContacts.BackColor;
        }

        private void btnCCAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi;

            lvi = new ListViewItem(this.tbxCCName.Text);
            lvi.SubItems.Add(this.tbxCCOrg.Text);
            lvi.SubItems.Add(this.tbxCCFax.Text);
            this.lvwCC.Items.Add(lvi);
            this.lvwCC.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwCC.Focus();
            UpdateCCLists();
        }

        private void UpdateCCLists()
        {
            String strCC = "";
            String strCompany = "";
            String strFaxNumber = "";
            foreach(ListViewItem lvi in this.lvwCC.Items)
            {
                strCC = strCC + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                strCompany = strCompany + lvi.SubItems[1].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                strFaxNumber = strFaxNumber + lvi.SubItems[2].Text + Microsoft.VisualBasic.Constants.vbCrLf;
            }
            Word.ContentControl ctl = clsWordApp.getContentControl("cc1", false);
            if (strCC.Length > 0)
                ctl.Range.Text = strCC.Substring(0, strCC.Length - 2);
            else
                ctl.Range.Text = "";
            ctl = clsWordApp.getContentControl("cc2", false);
            if(strCompany.Length>0)
                ctl.Range.Text = strCompany.Substring(0, strCompany.Length-2);
            else
                ctl.Range.Text = "";
            ctl = clsWordApp.getContentControl("cc3", false);
            if (strFaxNumber.Length>0)
                ctl.Range.Text = strFaxNumber.Substring(0, strFaxNumber.Length-2);
            else
                ctl.Range.Text = "";
        }

        private void btnCCRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwCC.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwCC.SelectedItems)
                    this.lvwCC.Items.Remove(lvi);
            }
            UpdateCCLists();
        }

        private void btnUpdateCC_Click(object sender, EventArgs e)
        {
            if (lvwCC.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwCC.SelectedItems[0];
            int index = lvi.Index;
            this.lvwCC.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxCCName.Text);
            lvi.SubItems.Add(this.tbxCCOrg.Text);
            lvi.SubItems.Add(this.tbxCCFax.Text);
            this.lvwCC.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateCCLists();
        }

        private void chkSRA_CheckedChanged(object sender, EventArgs e)
        {
            String text;
            if (chkSRA.Checked)
                text = officedata.SRARubric;
            else
                text = officedata.CurrentRubric;

            Word.ContentControl ctl = clsWordApp.getContentControl("rubric", false);
            ctl.Range.Text = text;
        }

        private void btnAddToTo_Click(object sender, EventArgs e)
        {
            bNoUpdating = true;
            if (lvwContacts.SelectedItems.Count == 0)
                return;
            tbxContact.Text = (lvwContacts.SelectedItems[0].SubItems[4].Text + " " + lvwContacts.SelectedItems[0].SubItems[0].Text).Trim();
            tbxCompany.Text = lvwContacts.SelectedItems[0].SubItems[1].Text;
            tbxFaxNumber.Text = lvwContacts.SelectedItems[0].SubItems[5].Text;
            bNoUpdating = false;
        }

        private void btnAddToCC_Click(object sender, EventArgs e)
        {
            bNoUpdating = true;
            if (lvwContacts.SelectedItems.Count == 0)
                return;
            tbxCCName.Text = (lvwContacts.SelectedItems[0].SubItems[4].Text + " " + lvwContacts.SelectedItems[0].SubItems[0].Text).Trim();
            tbxCCOrg.Text = lvwContacts.SelectedItems[0].SubItems[1].Text;
            tbxCCFax.Text = lvwContacts.SelectedItems[0].SubItems[5].Text;
            bNoUpdating = false;
        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxContact.Text = icontact.FullName;
            this.tbxFaxNumber.Text = icontact.ContactDetail.busFax;
            this.tbxCompany.Text = icontact.CompanyName;
            
        }
        
        private InterAction.IAContact GetContact()
        {
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        private void btnCCLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxCCName.Text = icontact.FullName;
            this.tbxCCFax.Text = icontact.ContactDetail.busFax;
            this.tbxCCOrg.Text = icontact.CompanyName;
        }

        private void btnUnderline_Click(object sender, EventArgs e)
        {
            {
                Word.ContentControl ctl = clsWordApp.getContentControl("subject", false);
                int line = ctl.Range.Characters[1].Information[Word.WdInformation.wdFirstCharacterLineNumber];
                foreach (Word.Range chr in ctl.Range.Characters)
                    if (chr.Information[Word.WdInformation.wdFirstCharacterLineNumber] == line)
                        chr.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                    else
                        chr.Font.Underline = Word.WdUnderline.wdUnderlineNone;
            }
        }

        private void btnAuthor_Click(object sender, EventArgs e)
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
            if (frm.user != null)
            {
                this.tbxAuthor.Text = (frm.user.FirstName + " " + frm.user.Initial).Trim() + " " + (frm.user.Surname + " " + frm.user.Suffix).Trim();
                this.tbxAuthorTel.Text = frm.user.TelephoneNumber;
                this.tbxFax.Text = officedata.NYFaxNumber;
                this.tbxMail.Text = frm.user.Mail;
                try
                {
                    DMS.SetAuthor(frm.user.Login);
                }
                catch { }
            }
        }

        private void lvwCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem lvi in lvwCC.Items)
                if (lvi.Selected)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = Color.White;
        }

    }
}
