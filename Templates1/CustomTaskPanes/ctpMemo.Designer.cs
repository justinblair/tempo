﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpMemo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAuthor = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxAuthor = new CustomTextBox();
            this.dateTimePicker1 = new CustomTaskPanes.CustomDateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxSubject = new CustomTextBox();
            this.btnUpdateCC = new System.Windows.Forms.Button();
            this.btnUpdateTo = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvwCC = new System.Windows.Forms.ListView();
            this.btnCCRemove = new System.Windows.Forms.Button();
            this.btnCCAdd = new System.Windows.Forms.Button();
            this.btnCCLookup = new System.Windows.Forms.Button();
            this.tbxCCName = new CustomTextBox();
            this.lvwTo = new System.Windows.Forms.ListView();
            this.btnToRemove = new System.Windows.Forms.Button();
            this.btnToAdd = new System.Windows.Forms.Button();
            this.btnToLookup = new System.Windows.Forms.Button();
            this.tbxToName = new CustomTextBox();
            this.SuspendLayout();
            // 
            // btnAuthor
            // 
            this.btnAuthor.Location = new System.Drawing.Point(324, 20);
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(22, 19);
            this.btnAuthor.TabIndex = 47;
            this.btnAuthor.Text = "...";
            this.btnAuthor.UseVisualStyleBackColor = true;
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Author";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(97, 20);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(221, 20);
            this.tbxAuthor.TabIndex = 45;
            this.tbxAuthor.Tag = "authorName";
            this.tbxAuthor.TextChanged += new System.EventHandler(this.tbxAuthor_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(97, 55);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(221, 20);
            this.dateTimePicker1.TabIndex = 48;
            this.dateTimePicker1.Tag = "date";
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 60);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "Date";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "Subject";
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(97, 90);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(221, 20);
            this.tbxSubject.TabIndex = 50;
            this.tbxSubject.Tag = "subject";
            this.tbxSubject.TextChanged += new System.EventHandler(this.tbxSubject_TextChanged);
            // 
            // btnUpdateCC
            // 
            this.btnUpdateCC.Location = new System.Drawing.Point(324, 332);
            this.btnUpdateCC.Name = "btnUpdateCC";
            this.btnUpdateCC.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateCC.TabIndex = 100;
            this.btnUpdateCC.Text = "!";
            this.btnUpdateCC.UseVisualStyleBackColor = true;
            this.btnUpdateCC.Click += new System.EventHandler(this.btnUpdateCC_Click);
            // 
            // btnUpdateTo
            // 
            this.btnUpdateTo.Location = new System.Drawing.Point(324, 188);
            this.btnUpdateTo.Name = "btnUpdateTo";
            this.btnUpdateTo.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateTo.TabIndex = 99;
            this.btnUpdateTo.Text = "!";
            this.btnUpdateTo.UseVisualStyleBackColor = true;
            this.btnUpdateTo.Click += new System.EventHandler(this.btnUpdateTo_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 277);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 98;
            this.label4.Text = "CC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "To";
            // 
            // lvwCC
            // 
            this.lvwCC.Location = new System.Drawing.Point(97, 296);
            this.lvwCC.MultiSelect = false;
            this.lvwCC.Name = "lvwCC";
            this.lvwCC.Size = new System.Drawing.Size(225, 111);
            this.lvwCC.TabIndex = 96;
            this.lvwCC.UseCompatibleStateImageBehavior = false;
            this.lvwCC.SelectedIndexChanged += new System.EventHandler(this.lvwCC_SelectedIndexChanged);
            // 
            // btnCCRemove
            // 
            this.btnCCRemove.Location = new System.Drawing.Point(324, 313);
            this.btnCCRemove.Name = "btnCCRemove";
            this.btnCCRemove.Size = new System.Drawing.Size(22, 19);
            this.btnCCRemove.TabIndex = 95;
            this.btnCCRemove.Text = "-";
            this.btnCCRemove.UseVisualStyleBackColor = true;
            this.btnCCRemove.Click += new System.EventHandler(this.btnCCRemove_Click);
            // 
            // btnCCAdd
            // 
            this.btnCCAdd.Location = new System.Drawing.Point(324, 295);
            this.btnCCAdd.Name = "btnCCAdd";
            this.btnCCAdd.Size = new System.Drawing.Size(22, 19);
            this.btnCCAdd.TabIndex = 94;
            this.btnCCAdd.Text = "+";
            this.btnCCAdd.UseVisualStyleBackColor = true;
            this.btnCCAdd.Click += new System.EventHandler(this.btnCCAdd_Click);
            // 
            // btnCCLookup
            // 
            this.btnCCLookup.Location = new System.Drawing.Point(324, 270);
            this.btnCCLookup.Name = "btnCCLookup";
            this.btnCCLookup.Size = new System.Drawing.Size(22, 19);
            this.btnCCLookup.TabIndex = 93;
            this.btnCCLookup.Text = "...";
            this.btnCCLookup.UseVisualStyleBackColor = true;
            this.btnCCLookup.Click += new System.EventHandler(this.btnCCLookup_Click);
            // 
            // tbxCCName
            // 
            this.tbxCCName.Location = new System.Drawing.Point(97, 270);
            this.tbxCCName.Name = "tbxCCName";
            this.tbxCCName.Size = new System.Drawing.Size(221, 20);
            this.tbxCCName.TabIndex = 88;
            // 
            // lvwTo
            // 
            this.lvwTo.AllowDrop = true;
            this.lvwTo.Location = new System.Drawing.Point(97, 150);
            this.lvwTo.MultiSelect = false;
            this.lvwTo.Name = "lvwTo";
            this.lvwTo.Size = new System.Drawing.Size(225, 111);
            this.lvwTo.TabIndex = 92;
            this.lvwTo.UseCompatibleStateImageBehavior = false;
            this.lvwTo.View = System.Windows.Forms.View.Details;
            this.lvwTo.SelectedIndexChanged += new System.EventHandler(this.lvwTo_SelectedIndexChanged);
            // 
            // btnToRemove
            // 
            this.btnToRemove.Location = new System.Drawing.Point(324, 169);
            this.btnToRemove.Name = "btnToRemove";
            this.btnToRemove.Size = new System.Drawing.Size(22, 19);
            this.btnToRemove.TabIndex = 91;
            this.btnToRemove.Text = "-";
            this.btnToRemove.UseVisualStyleBackColor = true;
            this.btnToRemove.Click += new System.EventHandler(this.btnToRemove_Click);
            // 
            // btnToAdd
            // 
            this.btnToAdd.Location = new System.Drawing.Point(324, 150);
            this.btnToAdd.Name = "btnToAdd";
            this.btnToAdd.Size = new System.Drawing.Size(22, 19);
            this.btnToAdd.TabIndex = 90;
            this.btnToAdd.Text = "+";
            this.btnToAdd.UseVisualStyleBackColor = true;
            this.btnToAdd.Click += new System.EventHandler(this.btnToAdd_Click);
            // 
            // btnToLookup
            // 
            this.btnToLookup.Location = new System.Drawing.Point(324, 125);
            this.btnToLookup.Name = "btnToLookup";
            this.btnToLookup.Size = new System.Drawing.Size(22, 19);
            this.btnToLookup.TabIndex = 89;
            this.btnToLookup.Text = "...";
            this.btnToLookup.UseVisualStyleBackColor = true;
            this.btnToLookup.Click += new System.EventHandler(this.btnToLookup_Click);
            // 
            // tbxToName
            // 
            this.tbxToName.Location = new System.Drawing.Point(97, 125);
            this.tbxToName.Name = "tbxToName";
            this.tbxToName.Size = new System.Drawing.Size(221, 20);
            this.tbxToName.TabIndex = 87;
            // 
            // ctpMemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnUpdateCC);
            this.Controls.Add(this.btnUpdateTo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvwCC);
            this.Controls.Add(this.btnCCRemove);
            this.Controls.Add(this.btnCCAdd);
            this.Controls.Add(this.btnCCLookup);
            this.Controls.Add(this.tbxCCName);
            this.Controls.Add(this.lvwTo);
            this.Controls.Add(this.btnToRemove);
            this.Controls.Add(this.btnToAdd);
            this.Controls.Add(this.btnToLookup);
            this.Controls.Add(this.tbxToName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnAuthor);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxAuthor);
            this.Name = "ctpMemo";
            this.Size = new System.Drawing.Size(363, 863);
            this.Load += new System.EventHandler(this.ctpMemo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAuthor;
        private System.Windows.Forms.Label label8;
        private CustomTextBox tbxAuthor;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private CustomTextBox tbxSubject;
        private System.Windows.Forms.Button btnUpdateCC;
        private System.Windows.Forms.Button btnUpdateTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvwCC;
        private System.Windows.Forms.Button btnCCRemove;
        private System.Windows.Forms.Button btnCCAdd;
        private System.Windows.Forms.Button btnCCLookup;
        private CustomTextBox tbxCCName;
        private System.Windows.Forms.ListView lvwTo;
        private System.Windows.Forms.Button btnToRemove;
        private System.Windows.Forms.Button btnToAdd;
        private System.Windows.Forms.Button btnToLookup;
        private CustomTextBox tbxToName;
    }
}
