﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpLabels : UserControl
    {
        private System.Windows.Forms.Button[,] button;
        private XMLLabel label;
        private int row;
        private int col;
        public ctpLabels()
        {
            this.label = Globals.ThisAddIn.LblBuilder.ActiveLabel;
            InitializeComponent();
            BuildButtons();
        }

        private void ctpLabels_Load(object sender, EventArgs e)
        {
            
            btnc0c0.Click += new EventHandler(button_Click);
            rbnAll.Checked = true;
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnInterAction.Enabled = online  && Globals.ThisAddIn.Officedata.IsNotNYOffice;
        }

        private void BuildButtons()
        {
            button = new Button[label.RowCount, label.ColCount];
            
            for (int i = 0; i < label.RowCount; i++)
                for (int j = 0; j < label.ColCount; j++)
                {
                    
                    if (j % 2 != 1)
                        if (i == 0 && j == 0)
                            button[i,j] = btnc0c0;
                        else
                    {
                        
                        button[i,j] = new Button();
                        button[i,j].Name = "btnc" + i + "c" + j;
                        button[i,j].Text = "";
                        button[i, j].Location = new Point(btnc0c0.Location.X + (j/2) * btnc0c0.Width, btnc0c0.Location.Y + (i) * btnc0c0.Height);
                        button[i,j].Size = new Size(btnc0c0.Width, btnc0c0.Height);
                        button[i,j].Click += new EventHandler(button_Click);
                        button[i, j].UseVisualStyleBackColor = true;
                        this.Controls.Add(button[i, j]);
                        this.Refresh();
                    }
                    
                }
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            rbnOne.Checked = true;
            String[] split = button.Name.Replace("btn", "").Split(new char[] { 'c' },StringSplitOptions.RemoveEmptyEntries);
            row = Convert.ToInt16(split[0]) + 1;
            col = Convert.ToInt16(split[1]) + 1;
            PopulateTable(row, col, false);
            
        }

        private void rbnAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbnAll.Checked)
                PopulateTable(0, 0, true);

        }

        private void PopulateTable(int row, int col, Boolean all)
        {
            Word.Table table = Globals.ThisAddIn.Application.ActiveDocument.Tables[1];
            foreach (Word.Row r in table.Rows)
                foreach (Word.Cell c in r.Cells)
                    if(c.Column.Index % 2 == 1)
                        if (all)
                            c.Range.Text = tbxLabel.Text;
                        else
                            c.Range.Text = (c.Row.Index == row && c.Column.Index == col) ? tbxLabel.Text : String.Empty;
        }

        private void tbxLabel_TextChanged(object sender, EventArgs e)
        {
            PopulateTable(row, col, rbnAll.Checked);
        }

        private void btnInterAction_Click(object sender, EventArgs e)
        {
            clsInterAction IA = new clsInterAction();
            InterAction.IAContact icontact;
            InterAction.IAAddress iaddress;
            icontact = IA.GetContact();
            iaddress = IA.GetAddress(icontact);
            String buffer = (icontact.NameTitle + " " + icontact.FullName + " " + icontact.NameSuffix).Trim();
            buffer += Environment.NewLine;
            buffer += icontact.CompanyName + Environment.NewLine;
            buffer += iaddress.FormattedAddress;
            tbxLabel.Text = buffer;
            
        }
    }
}
