﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpLegal : UserControl
    {
        private Boolean bNoUpdate;
        public ctpLegal()
        {
            InitializeComponent();
        }

        public void EnableControls()
        {
            Boolean hasCoverPage = clsWordApp.ContentControlExistsByID("859163546");
            Boolean hasTOC = clsTOC.HasTOC();
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Location.Y < this.pbxDivider.Location.Y)
                    control.Enabled = hasCoverPage;

                if (control.Location.Y > this.pbxDivider.Location.Y && control.Location.Y < this.pbxDivider2.Location.Y)
                {
                    control.Enabled = hasTOC;
                    if (control.Tag!=null)
                    { 
                    string[] buffer = control.Tag.ToString().Split(",".ToCharArray());
                    control.Enabled = clsStyles.StyleExists(buffer[0], Globals.ThisAddIn.Application.ActiveDocument) && hasTOC;
                    }
                }
            } 
            this.chkCover.Enabled = (Globals.ThisAddIn.Officedata.IsNYOffice) ? false : true;
            
            this.chkTOC.Enabled = true;
            this.btnAppendix.Enabled = (Globals.ThisAddIn.Officedata.IsNYOffice) ? false : true;
            this.btnSchedule.Enabled = (Globals.ThisAddIn.Officedata.IsNYOffice) ? false : true;
            
            
            this.chkCover.Checked = hasCoverPage;
            this.chkTOC.Checked = hasTOC;
            
        }
        
        
        private Boolean hasCover()
        {
            return clsWordApp.ContentControlExistsByID("859163546");
        }

        public void RepopulateLegalTaskPane()
        {
            Boolean hasCoverPage = hasCover();
            Boolean hasTOC = clsTOC.HasTOC();
            EnableControls();
            foreach (Control control in this.Controls)
            {
             if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    if(ctl!=null)
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }

            Word.ContentControl ctl2 = clsWordApp.getContentControl("parties", false);
            if(ctl2!=null)
            if (!ctl2.ShowingPlaceholderText)
                RepopulateListView(this.lvwTo);
            this.chkCover.Enabled = (Globals.ThisAddIn.Officedata.IsNYOffice) ? false : true;
            this.chkTOC.Enabled = true;
            bNoUpdate = true;
            this.chkCover.Checked = hasCoverPage;
            this.chkTOC.Checked = hasTOC;
            bNoUpdate = false;
            
           
        }

        private void RepopulateListView(ListView lvw)
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("parties", false);
            if(ctl!=null)
            if (ctl.ShowingPlaceholderText) return;
            char[] paragraph = Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray();
            String[] to = ctl.Range.Text.Split(paragraph);
            int i = -1;
            lvw.Items.Clear();
            foreach (String item in to)
            {
                i++;
                if (to[i].Length > 0 && to[i] != "-and-")
                {
                    ListViewItem lvi = new ListViewItem(item);
                    lvw.Items.Add(lvi);
                }
            }
           
        }
        
        private void ctpLegal_Load(object sender, EventArgs e)
        {
            tbxYear.Text = System.DateTime.Now.ToString("yyyy");
            TextBoxUpdate(tbxYear);
            this.lvwTo.Columns.Add("Parties");
            this.lvwTo.View = View.Details;
            this.lvwTo.Columns[0].Width = 180;
            this.tbxParty.KeyDown += new KeyEventHandler(tbxParty_KeyDown);
            bNoUpdate = true;
            EnableControls();
            this.cboLocation.SelectedIndex = 1;
            bNoUpdate = false;
                                                
        }

        void tbxParty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                AddItem();
        }

        private void TextBoxUpdate(object sender)
        {
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            if (chkDate.Checked)
                TextBoxUpdate(sender);
        }

        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker.Enabled = chkDate.Checked;
            Word.ContentControl ctl = clsWordApp.getContentControl(chkDate.Tag.ToString(), false);
            Word.ContentControl ctl2 = clsWordApp.getContentControl(this.dateTimePicker.Tag.ToString(), false);
            if (chkDate.Checked)
            {
                ctl.DropdownListEntries[1].Select();
                ctl2.Range.Text = dateTimePicker.Text;
            }
            else
            {
                ctl.Range.Text = "";
                ctl2.Range.Text = "";
            }
        }

        private void btnToAdd_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        private void btnToRemove_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            lvwTo.Items.Remove(lvwTo.SelectedItems[0]);
            UpdatePartyList();
        }

        private void btnUpdateTo_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwTo.SelectedItems[0];
            int index = lvi.Index;
            this.lvwTo.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxParty.Text);
            this.lvwTo.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdatePartyList();
        }

        private void AddItem()
        {
            ListViewItem lvi = new ListViewItem(tbxParty.Text);
            lvwTo.Items.Add(lvi);
            lvi.Selected = true;
            UpdatePartyList();
        }
        
        
        private void UpdatePartyList()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("parties", false);
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            String buffer = "";
            
            if (ctl != null)
            { 
                if(lvwTo.Items.Count ==0)
                {
                    ctl.Range.set_Style(doc.Styles["MdR Centered Bold"]);
                    return;
                }
                foreach (ListViewItem lvi in lvwTo.Items)
                { 
                    if (lvi.Index != lvwTo.Items.Count - 1)
                        buffer = buffer + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf + "-and-" + Microsoft.VisualBasic.Constants.vbCrLf;
                    else
                        buffer = buffer + lvi.SubItems[0].Text;
                }
            ctl.Range.Text = buffer;
            ctl.Range.set_Style(doc.Styles["MdR Parties Front Sheet"]);
            Word.Find fnd = ctl.Range.Find;
            fnd.Replacement.ClearFormatting();
            fnd.Replacement.set_Style(doc.Styles["MdR Centered Bold"]);
            fnd.Text = "-and-";
            fnd.Replacement.Text = "-and-";
            fnd.Forward = true;
            fnd.Wrap = Word.WdFindWrap.wdFindContinue;
            fnd.Execute(Replace: Word.WdReplace.wdReplaceAll);

            }




        }

        private void lvwTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvwTo.SelectedItems.Count>0)
            tbxParty.Text = lvwTo.SelectedItems[0].Text;
            
        }

        private void tbxYear_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxTitle_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxRef_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxEmail_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void chkLev1_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        public void DrawTOC()
        {
            if (bNoUpdate) return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (!clsTOC.HasTOC()) return;
            Word.TableOfContents toc = doc.TablesOfContents[1];
            for (int i = toc.HeadingStyles.Count; i > 0; i--)
                toc.HeadingStyles[i].Delete();
            string[] buffer;
            foreach (System.Windows.Forms.Control control in this.Controls)
                if (control.Location.Y > this.pbxDivider.Location.Y && control.Location.Y < this.pbxDivider2.Location.Y)
                    if (control.Tag != null)
                    {
                        CheckBox chk = (CheckBox)control;
                        if (chk.Checked && chk.Enabled)
                        { 
                        buffer = control.Tag.ToString().Split(",".ToCharArray());
                        String s = buffer[0];
                        Word.Style style = doc.Styles[buffer[0]];

                        switch(s)
                        {
                            case "Heading 1":
                                toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading1], Convert.ToInt16(buffer[1]));
                                break;
                            case "Heading 2":
                                toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading2], Convert.ToInt16(buffer[1]));
                                break;
                            case "Heading 3":
                                toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading3], Convert.ToInt16(buffer[1]));
                                break;

                            default:
                                toc.HeadingStyles.Add(style, Convert.ToInt16(buffer[1]));
                                break;
                       
                            }
                        }

                    }
            toc.Update();

            
            
        }

        public void RepopulateTOC()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            
            bNoUpdate = true;
            Boolean HasTOC = doc.TablesOfContents.Count > 0;
            Word.TableOfContents toc = null;
            if (HasTOC)
            toc = doc.TablesOfContents[1];
            try
            {
                if(HasTOC)
                cboLocation.SelectedIndex = toc.Range.Information[Word.WdInformation.wdActiveEndPageNumber];
                else
                    if (!chkTOC.Checked)
                    {
                        bNoUpdate = true;
                        if (hasCover())
                            cboLocation.SelectedIndex = 2;
                        else
                            cboLocation.SelectedIndex = 1;
                        bNoUpdate = false;
                    }
            }
            catch { cboLocation.SelectedIndex = 0; }

            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Location.Y > this.pbxDivider.Location.Y)
                    if (control.Tag != null)
                    {
                        string[] buffer = control.Tag.ToString().Split(",".ToCharArray());
                        control.Enabled = (clsStyles.StyleExists(buffer[0], doc) && HasTOC);
                            if(HasTOC)
                            for (int i = toc.HeadingStyles.Count; i > 0; i--)
                            { 
                                Word.HeadingStyle style = toc.HeadingStyles[i];
                                CheckBox chk = (CheckBox)control;
                                chk.Checked = control.Tag.ToString().ToLower().Contains(style.get_Style().ToLower());
               
                                if (chk.Checked) break;
                            }
                    }
            }
            bNoUpdate = false;
        }

        private void chkLev2_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkSchedules_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkScheduleParts_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkArticles_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkAppendix_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkArticleParts_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void cboLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(!bNoUpdate)
            { 
            clsTOC.MoveTableOfContents(cboLocation.SelectedIndex, "House style", GetTOCStyles());
            DrawTOC();
            }
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
            foreach (Control ctl in this.Controls)
                if (ctl.Location.Y < this.pbxDivider.Location.Y)
                { 
                    if (ctl.GetType().ToString() == "CustomTextBox")
                        ctl.Text = "";
                    
                }
            tbxYear.Text = System.DateTime.Now.ToString("yyyy");
            lvwTo.Items.Clear();
            Word.ContentControl control = clsWordApp.getContentControl("parties", false);
            if (control != null)
                control.Range.Text = "";
            dateTimePicker.Value = System.DateTime.Now;
            UpdatePartyList();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            clsTOC.UpdateTable();
        }

        private void btnGoTo_Click(object sender, EventArgs e)
        {
            clsTOC.SelectTable();
        }

        private void chkCover_CheckedChanged(object sender, EventArgs e)
        {
            if (bNoUpdate) return;
            if (chkCover.Checked)
            {
                pbCover.Value = 30;
                clsDynamicInserts.InsertFrontPage("Legal cover sheet");
                pbCover.Value = 70;
                clsDynamicInserts.DropAddressBlock();
                RepopulateTOC();
                TextBoxUpdate(tbxYear);
                pbCover.Value = 100;
                
                
            }
            else
            {
                pbCover.Value = 50;
                clsDynamicInserts.RemoveFrontPage();
                RepopulateTOC();
                pbCover.Value = 0;
            }
        }

        private void chkTOC_CheckedChanged(object sender, EventArgs e)
        {
            if (bNoUpdate) return;
            if (chkTOC.Checked)
            {
                pbTOC.Value = 10;
                String styles = GetTOCStyles();
                pbTOC.Value = 30;
                clsTOC.InsertTOC(cboLocation.SelectedIndex, "House style", styles);
                pbTOC.Value = 50;
                EnableControls();
                pbTOC.Value = 70;
                DrawTOC();
                pbTOC.Value = 100;
                
            }
            else
            {
                pbTOC.Value = 50;
                clsTOC.DeleteTableOfContents();
                EnableControls();
                bNoUpdate = true;
                if (hasCover())
                    cboLocation.SelectedIndex = 2;
                else
                    cboLocation.SelectedIndex = 1;
                bNoUpdate = false;
                pbTOC.Value = 1;
                
            }
        }

        public string GetTOCStyles()
        {
            String buffer = "";
            foreach(Control ctl in this.Controls)
                if(ctl.GetType().ToString().ToLower().Contains("checkbox"))
                    if(ctl.Tag != null)
                {
                    CheckBox chk = ctl as CheckBox;
                        if(chk.Checked)
                        buffer = buffer + ctl.Tag.ToString() + ";";
                }
            buffer = buffer.Substring(0, buffer.Length - 2);
            return buffer;
        }

        private void chkHead1_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkHead2_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void btnSchedule_Click(object sender, EventArgs e)
        {
            Schedule schedule = new Schedule(Globals.ThisAddIn.Application.ActiveDocument);
            schedule.Create();
        }

        private void btnAppendix_Click(object sender, EventArgs e)
        {
            Appendix appendix = new Appendix(Globals.ThisAddIn.Application.ActiveDocument);
            appendix.Create();
        }

        private void chkH1_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkH2_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }

        private void chkH3_CheckedChanged(object sender, EventArgs e)
        {
            DrawTOC();
        }


    }
}
