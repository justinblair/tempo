﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpMemoUS
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxTo = new CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxCC = new CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxAuthor = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDate = new CustomTaskPanes.CustomDateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxSubject = new CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxExtension = new CustomTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "To";
            // 
            // tbxTo
            // 
            this.tbxTo.Location = new System.Drawing.Point(116, 34);
            this.tbxTo.Name = "tbxTo";
            this.tbxTo.Size = new System.Drawing.Size(225, 20);
            this.tbxTo.TabIndex = 9;
            this.tbxTo.Tag = "to";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "CC";
            // 
            // tbxCC
            // 
            this.tbxCC.Location = new System.Drawing.Point(116, 86);
            this.tbxCC.Name = "tbxCC";
            this.tbxCC.Size = new System.Drawing.Size(225, 20);
            this.tbxCC.TabIndex = 11;
            this.tbxCC.Tag = "cc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "From";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(116, 112);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(225, 20);
            this.tbxAuthor.TabIndex = 13;
            this.tbxAuthor.Tag = "authorName";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMMM d, yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(116, 60);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(225, 20);
            this.dtpDate.TabIndex = 15;
            this.dtpDate.Tag = "date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Subject";
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(116, 166);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(225, 20);
            this.tbxSubject.TabIndex = 17;
            this.tbxSubject.Tag = "subject";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Extension";
            // 
            // tbxExtension
            // 
            this.tbxExtension.Location = new System.Drawing.Point(116, 140);
            this.tbxExtension.Name = "tbxExtension";
            this.tbxExtension.Size = new System.Drawing.Size(225, 20);
            this.tbxExtension.TabIndex = 19;
            this.tbxExtension.Tag = "extension";
            // 
            // ctpMemoUS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxExtension);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxCC);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxTo);
            this.Name = "ctpMemoUS";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpMemoUS_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxTo;
        private System.Windows.Forms.Label label2;
        private CustomTextBox tbxCC;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxAuthor;
        private System.Windows.Forms.Label label4;
        private CustomTaskPanes.CustomDateTimePicker dtpDate;
        private System.Windows.Forms.Label label5;
        private CustomTextBox tbxSubject;
        private System.Windows.Forms.Label label6;
        private CustomTextBox tbxExtension;
    }
}
