﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpUSBill
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDate = new CustomTaskPanes.CustomDateTimePicker();
            this.btnLookup = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxContact = new CustomTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.tbxCompany = new CustomTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxTaxID = new CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxOurRef = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxInvoiceNumber = new CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxMatter = new CustomTextBox();
            this.cboInterimFinal = new CustomTaskPanes.CustomComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lvwContacts = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMMM d, yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(113, 24);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(222, 20);
            this.dtpDate.TabIndex = 4;
            this.dtpDate.Tag = "date";
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(313, 164);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(21, 21);
            this.btnLookup.TabIndex = 74;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 73;
            this.label5.Text = "Contact";
            // 
            // tbxContact
            // 
            this.tbxContact.Location = new System.Drawing.Point(113, 166);
            this.tbxContact.Name = "tbxContact";
            this.tbxContact.Size = new System.Drawing.Size(191, 20);
            this.tbxContact.TabIndex = 72;
            this.tbxContact.Tag = "contact";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 224);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 71;
            this.label8.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 197);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 70;
            this.label7.Text = "Company";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(113, 221);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(222, 75);
            this.tbxAddress.TabIndex = 69;
            this.tbxAddress.Tag = "address";
            this.tbxAddress.Text = "";
            // 
            // tbxCompany
            // 
            this.tbxCompany.Location = new System.Drawing.Point(113, 192);
            this.tbxCompany.Name = "tbxCompany";
            this.tbxCompany.Size = new System.Drawing.Size(225, 20);
            this.tbxCompany.TabIndex = 68;
            this.tbxCompany.Tag = "company";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 307);
            this.label1.MinimumSize = new System.Drawing.Size(45, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 76;
            this.label1.Text = "Tax ID";
            // 
            // tbxTaxID
            // 
            this.tbxTaxID.Location = new System.Drawing.Point(113, 302);
            this.tbxTaxID.Name = "tbxTaxID";
            this.tbxTaxID.Size = new System.Drawing.Size(225, 20);
            this.tbxTaxID.TabIndex = 75;
            this.tbxTaxID.Tag = "taxId";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 78;
            this.label3.Text = "Our reference";
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(113, 328);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(225, 20);
            this.tbxOurRef.TabIndex = 77;
            this.tbxOurRef.Tag = "ourRef";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 359);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 80;
            this.label4.Text = "Invoice number";
            // 
            // tbxInvoiceNumber
            // 
            this.tbxInvoiceNumber.Location = new System.Drawing.Point(113, 354);
            this.tbxInvoiceNumber.Name = "tbxInvoiceNumber";
            this.tbxInvoiceNumber.Size = new System.Drawing.Size(225, 20);
            this.tbxInvoiceNumber.TabIndex = 79;
            this.tbxInvoiceNumber.Tag = "invoiceNumber";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 385);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 82;
            this.label6.Text = "Matter";
            // 
            // tbxMatter
            // 
            this.tbxMatter.Location = new System.Drawing.Point(113, 380);
            this.tbxMatter.Name = "tbxMatter";
            this.tbxMatter.Size = new System.Drawing.Size(225, 20);
            this.tbxMatter.TabIndex = 81;
            this.tbxMatter.Tag = "matter";
            // 
            // cboInterimFinal
            // 
            this.cboInterimFinal.FormattingEnabled = true;
            this.cboInterimFinal.Location = new System.Drawing.Point(114, 414);
            this.cboInterimFinal.Name = "cboInterimFinal";
            this.cboInterimFinal.Size = new System.Drawing.Size(219, 21);
            this.cboInterimFinal.TabIndex = 83;
            this.cboInterimFinal.Tag = "interimFinal";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 418);
            this.label9.MinimumSize = new System.Drawing.Size(85, 13);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 84;
            this.label9.Text = "Interim/Final";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 86;
            this.label12.Text = "Outlook contacts";
            // 
            // lvwContacts
            // 
            this.lvwContacts.FullRowSelect = true;
            this.lvwContacts.GridLines = true;
            this.lvwContacts.Location = new System.Drawing.Point(112, 60);
            this.lvwContacts.Name = "lvwContacts";
            this.lvwContacts.Size = new System.Drawing.Size(221, 98);
            this.lvwContacts.TabIndex = 85;
            this.lvwContacts.UseCompatibleStateImageBehavior = false;
            this.lvwContacts.View = System.Windows.Forms.View.Details;
            this.lvwContacts.SelectedIndexChanged += new System.EventHandler(this.lvwContacts_SelectedIndexChanged);
            // 
            // ctpUSBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lvwContacts);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboInterimFinal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxMatter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxInvoiceNumber);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxOurRef);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxTaxID);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxContact);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.tbxCompany);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpDate);
            this.Name = "ctpUSBill";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpUSBill_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private CustomTaskPanes.CustomDateTimePicker dtpDate;
        private System.Windows.Forms.Button btnLookup;
        private System.Windows.Forms.Label label5;
        private CustomTextBox tbxContact;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private CustomTextBox tbxCompany;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxTaxID;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxOurRef;
        private System.Windows.Forms.Label label4;
        private CustomTextBox tbxInvoiceNumber;
        private System.Windows.Forms.Label label6;
        private CustomTextBox tbxMatter;
        private CustomTaskPanes.CustomComboBox cboInterimFinal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lvwContacts;
    }
}
