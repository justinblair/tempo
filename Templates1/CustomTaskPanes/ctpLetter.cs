﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    public partial class ctpLetter : UserControl
    {

        private WorkSite8Application DMS;
        private clsInterAction IA;
        private clsWordApp WordApp;
        private Boolean yoursFaithfully = false;
        private Word.ContentControl _ctl;
        
        public ctpLetter()
        {
            InitializeComponent();
        }

        private void UserControl1_Load(object sender, EventArgs e)
        {
            WordApp = Globals.ThisAddIn.WordApp;
           
            CustomTaskPanes.LetterParameters lp = new CustomTaskPanes.LetterParameters();
          
            foreach (String s in lp.Confidentiality)
                this.chkListConfidentiality.Items.Add(s);
            foreach (String s in lp.Instructions)
                this.chkListLegalInstructions.Items.Add(s);
            foreach (String s in lp.Delivery)
              this.chkListDelivery.Items.Add(s);
            foreach (String s in lp.Prejudice)
                this.chkListPrejudice.Items.Add(s);
            foreach (String s in lp.SpecialDelivery)
                this.chkListSpecDelivery.Items.Add(s);
                this.cboSignOff.Items.AddRange(lp.SignOff.ToArray());

            this.cboRecipient.TextChanged += new EventHandler(cboRecipient_TextChanged);
            DMS = Globals.ThisAddIn.DMS;
            

            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
            btnAuthor.Enabled = online;
            

        }

        private void cboRecipient_TextChanged(object sender, EventArgs e)
        {
            SetControlValue2(cboRecipient, "recipient");
        }
        
        
        private void chkListConfidentiality_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlValue(chkListConfidentiality, "privacy");
        }

        private void chkListDelivery_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            SetControlValue(chkListDelivery, "delivery");
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
           foreach(Control ctl in this.Controls)
               try
               {
                   if(ctl.Tag.ToString() != "") ctl.Text = "";
               }
               catch { }
           for (int i=0; i < this.chkListConfidentiality.Items.Count; i++)
               this.chkListConfidentiality.SetItemChecked(i, false);
           for (int i = 0; i < this.chkListDelivery.Items.Count; i++)
               this.chkListDelivery.SetItemChecked(i, false);
           this.chkEnc.Checked = false;
           this.chkCC.Checked = false;
            Fetch();
        }

        private void ReplaceFaxEmail(String replacementStub, String replacementValue)
        {
            int i = -1;
            foreach (object item in chkListDelivery.Items)
            {
                i++;
                if (item.ToString().Contains(replacementStub))
                {
                    try
                    {
                        chkListDelivery.Items.RemoveAt(i);
                        chkListDelivery.Items.Add(replacementStub + " (" + replacementValue + ")");
                        return;
                    }
                    catch (Exception) { }
                }
            }
        }

        private void SetControlValue(CheckedListBox chkList, String controlname)
        {
            Word.ContentControl ctl;
            String buffer= "";
            ctl = getContentControl(controlname);
            foreach (object item in chkList.CheckedItems)
                buffer = buffer + item.ToString() + " ";
            if(ctl!=null)
                ctl.Range.Text = buffer;
        }

        private void SetControlValue2(ComboBox chkList, String controlname)
        {
            Word.ContentControl ctl;
            ctl = getContentControl(controlname);
            if (chkList.Text == "[blank]")
                ctl.Range.Text = "";
            else
                ctl.Range.Text = chkList.Text;
            if (chkList.Name == "chkListSpecDelivery")
                ctl.Range.Text = chkList.Text + " " + this.tbxClientMatter.Text;
        }

        private Word.ContentControl getContentControl(string name)
        {
            if(_ctl!=null)
            if (_ctl.Tag == name)
                return _ctl;
            foreach (Word.ContentControl ctl in Globals.ThisAddIn.Application.ActiveDocument.ContentControls)
                if (ctl.Tag == name)
                {
                    _ctl = ctl;
                    return ctl;
                }
            return null;
        }

        private void tbxAuthorTel_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("authorTelephone");
            ctl.Range.Text = this.tbxAuthorTel.Text;
        }

        private void tbxMail_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("authorEmail");
            ctl.Range.Text = this.tbxMail.Text;
        }

        public void Fetch()
        {
            if(DMS==null)
                DMS = Globals.ThisAddIn.DMS;
            if(DMS.IsOnline)
            { 
                this.tbxClientMatter.Text = DMS.ClientMatterCode;
                this.tbxOurRef.Text = DMS.ClientMatterCode;
                FetchAuthor(DMS);
            }
            SetDate();
            FetchSubject(DMS);
            
        }

        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();
            
            this.tbxAuthorTel.Text = adu.TelephoneNumber;
            this.tbxMail.Text = adu.Mail;
            this.tbxFax.Text = adu.Fax;
            this.tbxAuthor.Text = adu.FullName;
            this.tbxTitle.Text = adu.Title;
        }
        private void FetchSubject(WorkSite8Application DMS)
        {
            this.tbxSubject.Text = DMS.MatterDescription;
        }

        private void chkListLegalInstructions_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SetControlValue2(chkListLegalInstructions, "legal");
        }

        private void chkListPrejudice_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SetControlValue2(chkListPrejudice, "prejudice");
        }

        private void chkListSpecDelivery_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SetControlValue2(chkListSpecDelivery, "specDel");
            if (chkListSpecDelivery.SelectedIndex != 0)
                this.tbxClientMatter.Enabled = true;
            else
                this.tbxClientMatter.Enabled = false;

        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            BuildLists("");
        }

        private void cboRecipient_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SetControlValue2(cboRecipient, "recipient");
        }

        private void tbxAddress_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("addressee");
            ctl.Range.Text = this.tbxAddress.Text;
        }

        private void tbxSubject_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("subject");
            ctl.Range.Text = this.tbxSubject.Text;
        }

        private void cboSignOff_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlValue2(cboSignOff, "signoff");
            if (cboSignOff.Text.ToLower().Contains("faithfully"))
                yoursFaithfully = true;
            else
                yoursFaithfully = false;
            SetAuthor();
            SetRole();
        }

        private void cboSalutation_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlValue2(cboSalutation, "salutation");
            if (cboSalutation.SelectedIndex == 4 || cboSalutation.SelectedIndex == 5)
                cboSignOff.SelectedIndex = 0;
            else
                cboSignOff.SelectedIndex = 1;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
           SetDate() ;
        }

        private void SetDate()
        {
            Word.ContentControl ctl = getContentControl("date");
            ctl.Range.Text = this.dateTimePicker1.Text;
        }

        private void tbxAuthor_TextChanged(object sender, EventArgs e)
        {
            SetAuthor();
        }

        private void SetAuthor()
        {
            Word.ContentControl ctl = getContentControl("MdR");
            if (yoursFaithfully)
                ctl.Range.Text = Globals.ThisAddIn.Officedata.Offices.ActiveOffice.Fullname;
            else
                ctl.Range.Text = this.tbxAuthor.Text;
        }

        private void SetRole()
        {
            Word.ContentControl ctl = getContentControl("role");
            if (yoursFaithfully)
                ctl.Range.Text = "";
            else
                ctl.Range.Text = this.tbxTitle.Text;
        }

        private void tbxTitle_TextChanged(object sender, EventArgs e)
        {
            SetRole();
        }

        private void tbxFax_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("authorFax");
            ctl.Range.Text = this.tbxFax.Text;
        }

        private void tbxOurRef_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("ourRef");
            ctl.Range.Text = this.tbxOurRef.Text;
        }

        private void tbxYourRef_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("yourRef");
            ctl.Range.Text = this.tbxYourRef.Text;
        }

        public void RepopulateLetterTaskPane()
        {
            try
            {
                Word.Document wordDoc = Globals.ThisAddIn.Application.ActiveDocument;
                BuildLists(wordDoc.Variables["iFindAddressee"].Value);
            }
            catch { }
            foreach(System.Windows.Forms.Control control in this.Controls)
            { 
                if(control.Tag!=null)
                { 
                Word.ContentControl ctl = getContentControl(control.Tag.ToString());
                try
                {
                    if(!ctl.ShowingPlaceholderText)
                    control.Text = ctl.Range.Text;
                }
                catch { }
                }
            }

            Word.ContentControl ctl2 = clsWordApp.getContentControl("privacy", false);
            for (int i = 0; i < this.chkListConfidentiality.Items.Count; i++)
            {
                string lvw = this.chkListConfidentiality.Items[i].ToString();
                this.chkListConfidentiality.SetItemChecked(i,ctl2.Range.Text.ToLower().Contains(lvw.ToLower()));
            }

            ctl2 = clsWordApp.getContentControl("delivery", false);
            for (int i = 0; i < this.chkListDelivery.Items.Count; i++)
            {
                string lvw = this.chkListDelivery.Items[i].ToString();
                this.chkListDelivery.SetItemChecked(i, ctl2.Range.Text.ToLower().Contains(lvw.ToLower()));
            }

            ctl2 = clsWordApp.getContentControl("cc", false);
            this.chkCC.Checked = (ctl2.Range.Text != ctl2.Title);

            ctl2 = clsWordApp.getContentControl("enclosures", false);
            this.chkEnc.Checked = (ctl2.Range.Text != ctl2.Title);


                
                    
            
        }

        private void BuildLists(String id)
        {
            if (IA==null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            InterAction.IAAddress iaddress;

            try
            {

                if (id.Length == 0)
                {
                    icontact = IA.GetContact();
                    iaddress = IA.GetAddress(icontact);
                }
                else
                {
                    icontact = IA.GetContactByIALID(id);
                    iaddress = null;
                }
                this.cboRecipient.Items.Clear();
                if (icontact == null)
                    return;
                this.cboRecipient.Items.Add((icontact.NameTitle + " " + icontact.FirstName.Substring(0, 1) + " " + icontact.LastName).Trim());
                this.cboRecipient.Items.Add((icontact.NameTitle + " " + icontact.FirstName + " " + icontact.LastName).Trim());
                if (icontact.NameSuffix != null)
                {
                    this.cboRecipient.Items.Add((icontact.NameTitle + " " + icontact.FirstName + " " + icontact.LastName + " " + icontact.NameSuffix).Trim());
                    this.cboRecipient.Items.Add((icontact.NameTitle + " " + icontact.LastName + " " + icontact.NameSuffix).Trim());
                }
                this.cboRecipient.Items.Add((icontact.NameTitle + " " + icontact.LastName).Trim());
                this.cboRecipient.SelectedIndex = 0;
                if (iaddress != null)
                {
                    if (icontact.JobTitle != null)
                        if (icontact.JobTitle.Length > 0)
                            this.tbxAddress.Text = icontact.JobTitle + Microsoft.VisualBasic.Constants.vbCrLf + icontact.CompanyName + Microsoft.VisualBasic.Constants.vbCrLf +
                                iaddress.FormattedAddress;
                        else
                            this.tbxAddress.Text = icontact.CompanyName + Microsoft.VisualBasic.Constants.vbCrLf + iaddress.FormattedAddress;
                    else
                        this.tbxAddress.Text = icontact.CompanyName + Microsoft.VisualBasic.Constants.vbCrLf + iaddress.FormattedAddress;
                }

                CustomTaskPanes.LetterParameters lp = new CustomTaskPanes.LetterParameters();
                foreach (String s in lp.Salutation)
                    this.cboSalutation.Items.Add(s.Replace("[Title]", icontact.NameTitle).Replace("[Surname]", icontact.LastName).Replace("[Firstname]", icontact.FirstName).Trim());

                String faxNumber = icontact.ContactDetail.busFax;
                String email = icontact.ContactDetail.busEmail;
                ReplaceFaxEmail("By Fax", faxNumber);
                ReplaceFaxEmail("By EMail", email);
            }
            catch { }
        }

        private void chkCC_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.tbxCCs.Enabled = this.chkCC.Checked;

                if (this.chkCC.Checked)
                {
                    Word.ContentControl ctl = getContentControl("cc");
                    ctl.Range.Text = "CC:";
                    ctl.Range.Font.Hidden = 0;
                    ctl = getContentControl("ccs");
                    ctl.Range.Text = this.tbxCCs.Text;
                    ctl.Range.Font.Hidden = 0;
                }
                else
                {
                    Word.ContentControl ctl = getContentControl("cc");
                    ctl.Range.Text = "";
                    ctl.Range.Font.Hidden = -1;
                    ctl = getContentControl("ccs");
                    ctl.Range.Text = "";
                    ctl.Range.Font.Hidden = -1;
                }
            }
            catch { }
        }

        private void tbxCCs_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("ccs");
            if(ctl!=null)
                ctl.Range.Text = this.tbxCCs.Text;
        }

        private void chkEnc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                this.tbxEncs.Enabled = this.chkEnc.Checked;

                if (this.chkEnc.Checked)
                {
                    Word.ContentControl ctl = getContentControl("enclosures");
                    ctl.Range.Font.Hidden = 0;
                    ctl.Range.Text = "Encs:";
                    ctl = getContentControl("encs");
                    ctl.Range.Text = this.tbxEncs.Text;
                    ctl.Range.Font.Hidden = 0;
                }
                else
                {
                    Word.ContentControl ctl = getContentControl("enclosures");
                    ctl.Range.Text = "";
                    ctl.Range.Font.Hidden = -1;
                    ctl = getContentControl("encs");
                    ctl.Range.Text = "";
                    ctl.Range.Font.Hidden = -1;
                }
            }
            catch { }
        }

        private void tbxEncs_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = getContentControl("encs");
            ctl.Range.Text = this.tbxEncs.Text;
        }

        private void btnAuthor_Click(object sender, EventArgs e)
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e )
        {
            Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
            if (frm.user!=null)
            { 
            this.tbxAuthor.Text = frm.user.FullName;
            this.tbxAuthorTel.Text = frm.user.TelephoneNumber;
            this.tbxTitle.Text = frm.user.Title;
            this.tbxFax.Text = frm.user.Fax;
            this.tbxMail.Text = frm.user.Mail;
            DMS.SetAuthor(frm.user.Login);
            }
        }

        private void tbxClientMatter_TextChanged(object sender, EventArgs e)
        {
            SetControlValue2(chkListSpecDelivery, "specDel");
        }
    }
}
