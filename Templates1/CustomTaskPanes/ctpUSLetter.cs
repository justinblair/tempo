﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpUSLetter : UserControl
    {

        private clsOutlook ol;
        private Boolean bNoUpdating;
        private WorkSite8Application DMS;
        private clsOfficeData officedata;
        private clsInterAction IA;
        
        
        
        public ctpUSLetter()
        {
            InitializeComponent();
        }

        private void LoadCombosEtc()
        {
            LetterParameters lp = new LetterParameters();
            
            this.cboDelivery.Items.AddRange(lp.USDelivery.ToArray());
            this.cboClosing.Items.AddRange(lp.USClosing.ToArray());
            cboClosing.Text = "Sincerely";
            ctl_TextChanged((object)cboClosing, null);
            foreach (String s in lp.USPrivacy)
            {
                ListViewItem lvi = new ListViewItem(s);
                lvwPrivacy.Items.Add(lvi);
            }
            
            
                
        }

        private void ctpUSLetter_Load(object sender, EventArgs e)
        {
            SetListView();
            LoadCombosEtc();
            officedata = Globals.ThisAddIn.Officedata;
            lvwPrivacy.ItemChecked += new ItemCheckedEventHandler(lvwPrivacy_ItemChecked);
            LoadContacts();
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
        }

        void ctl_TextChanged(object sender, EventArgs e)
        {
            //if (bNoUpdating)
            //    return;
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }
        
        
        void lvwPrivacy_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (lvwPrivacy.CheckedItems.Count > 3)
                e.Item.Checked = false;
            List<Word.ContentControl> ctl = clsWordApp.getContentControls("privacy", Globals.ThisAddIn.Application.ActiveDocument);
            String combined = "";
            int count = 0;
            foreach (ListViewItem lvi in lvwPrivacy.CheckedItems)
            {
                count++;
                if (count < lvwPrivacy.CheckedItems.Count)
                    combined += lvi.SubItems[0].Text + " AND ";
                else
                    combined += lvi.SubItems[0].Text;
            }

            foreach(Word.ContentControl c in ctl)
                c.Range.Text = combined;
        }


               
        private void SetListView()
        {
            lvwPrivacy.Columns.Add("Privacy");
            lvwPrivacy.HeaderStyle = ColumnHeaderStyle.None;
            lvwPrivacy.Columns[0].Width = 190;
            lvwContacts.Columns.Add("Contact");
            lvwContacts.Columns.Add("Org.");
            lvwContacts.Columns[0].Width = 190 / 2;
            lvwContacts.Columns[1].Width = 190 / 2;

        }
        
        public void RepopulateLetterUSTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(),false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }

            Word.ContentControl ctl2 = clsWordApp.getContentControl("draft", false);
            this.chkDraft.Checked = ctl2.Range.Text.ToLower().Contains("draft");
            ctl2 = clsWordApp.getContentControl("rubric", false);
            this.chkSRA.Checked = ctl2.Range.Text.Contains("SRA");
            ctl2 = clsWordApp.getContentControl("privacy", false);
            foreach (ListViewItem lvw in this.lvwPrivacy.Items)
                lvw.Checked = ctl2.Range.Text.ToLower().Contains(lvw.SubItems[0].Text.ToLower());
  
        }

        private void LoadContacts()
        {
            ol = new clsOutlook();
            ol.GetContacts(lvwContacts);
        }


        public void Fetch()
        {
            if (DMS == null)
                DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
            if (DMS.IsOnline)
            { 
            this.tbxOurRef.Text = DMS.ClientMatterCode;         
            FetchAuthor(DMS);
            }
            SetDate();
                
            
        }

        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();

            this.tbxAuthorTel.Text = adu.TelephoneNumber;
            this.tbxMail.Text = adu.Mail;
            this.tbxFax.Text = officedata.NYFaxNumber;
            this.tbxAuthor.Text = adu.FullName;
           
        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dtpDate.Text;
        }

        private void lvwContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            bNoUpdating = true;
            if (lvwContacts.SelectedItems.Count == 0)
                return;
            tbxContact.Text = (lvwContacts.SelectedItems[0].SubItems[4].Text + " " + lvwContacts.SelectedItems[0].SubItems[0].Text).Trim();
            tbxCompany.Text = lvwContacts.SelectedItems[0].SubItems[1].Text;
            tbxJobTitle.Text = lvwContacts.SelectedItems[0].SubItems[2].Text;
            tbxAddress.Text = lvwContacts.SelectedItems[0].SubItems[3].Text;

            foreach (ListViewItem lvi in lvwContacts.Items)
                if (lvi.Selected)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = lvwContacts.BackColor;

            bNoUpdating = false;
        }

        private void tbxContact_TextChanged(object sender, EventArgs e)
        {

            if (bNoUpdating)
                return;
            ol.GetContactsFiltered(lvwContacts, tbxContact.Text);
        }

        private void btnAuthor_Click(object sender, EventArgs e)
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
            if (frm.user != null)
            {
                this.tbxAuthor.Text = frm.user.ExtendedFullName;
                this.tbxAuthorTel.Text = frm.user.TelephoneNumber;
                this.tbxFax.Text = officedata.NYFaxNumber;
                this.tbxMail.Text = frm.user.Mail;
                try
                {
                    if(DMS.IsOnline)
                        DMS.SetAuthor(frm.user.Login);
                }
                catch { }
            }
        }

        private void chkSRA_CheckedChanged(object sender, EventArgs e)
        {
            String text;
            if (chkSRA.Checked)
                text = officedata.SRARubric;
            else
                text = officedata.CurrentRubric;

            Word.ContentControl ctl = clsWordApp.getContentControl("rubric", false);
            ctl.Range.Text = text;
        }

        private void tbxSubject_TextChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("subject", false);
            ctl.Range.Text = tbxSubject.Text;
            
        }

        private void btnUnderline_Click(object sender, EventArgs e)  //No longer required
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("subject", false);
            int line = ctl.Range.Characters[1].Information[Word.WdInformation.wdFirstCharacterLineNumber];
            foreach (Word.Range chr in ctl.Range.Characters)
                if (chr.Information[Word.WdInformation.wdFirstCharacterLineNumber] == line)
                    chr.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                else
                    chr.Font.Underline = Word.WdUnderline.wdUnderlineNone;
        }

        private void chkDraft_CheckedChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("draft", false);
            if (chkDraft.Checked)
                ctl.Range.Text = "DRAFT (New York): " + DateTime.Now.ToString("MMMM d, yyyy");
            else
                ctl.Range.Text = "";
        }

        private void btnUnderline_Click_1(object sender, EventArgs e) //No longer required
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("subject", false);
            int line = ctl.Range.Characters[1].Information[Word.WdInformation.wdFirstCharacterLineNumber];
            foreach (Word.Range chr in ctl.Range.Characters)
                if (chr.Information[Word.WdInformation.wdFirstCharacterLineNumber] == line)
                    chr.Font.Underline = Word.WdUnderline.wdUnderlineSingle;
                else
                    chr.Font.Underline = Word.WdUnderline.wdUnderlineNone;
        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxContact.Text = icontact.FullName;
            this.tbxCompany.Text = icontact.CompanyName;
            this.tbxAddress.Text = icontact.SelectedAddress.FormattedAddress;
            this.tbxJobTitle.Text = icontact.JobTitle;
        }

        private InterAction.IAContact GetContact()
        {
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        private void tbxSubject_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void tbxContact_TextChanged_1(object sender, EventArgs e)
        {

        }
    }
}
