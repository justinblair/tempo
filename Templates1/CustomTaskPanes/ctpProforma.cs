﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpProforma : UserControl

        
    {

        private XmlDocument XmlDoc;
        private Boolean bNoUpdating;
        private clsInterAction IA;
        private WorkSite8Application DMS;
        private XmlDocument XmlSave;



        public ctpProforma()
        {
            InitializeComponent();
        }

        private void ctpProforma_Load(object sender, EventArgs e)
        {
            XmlDoc = new XmlDocument();
            CustomTaskPanes.clsHelper helper = new CustomTaskPanes.clsHelper();
            XmlDoc.Load(helper.Params);

            foreach (XmlNode nodNode in XmlDoc.SelectNodes("//title/param"))
                this.cboTitle.Items.Add(nodNode.InnerText);

            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);

            SetListView();
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
        }

        void ctl_TextChanged(object sender, EventArgs e)
        {
            if (bNoUpdating)
                return;
            Control tbx = (Control)sender;
            if (tbx.Name.ToLower() == "tbxcompany" || tbx.Name.ToLower() == "cboaddressee")
            {
                AddresseeOrCompany();
            }
            else
            {
                Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
                if (ctl != null)
                    ctl.Range.Text = tbx.Text;
            }
            SetParagraphSpacingOnPBandFAO();
            BuildSaveTree();

            //if (tbx.Name.ToLower() == "tbxmonies")
            //    ProcessBalance();

            //BuildSaveTree();
        }

        private void AddresseeOrCompany()
        {
            if (chkIndividual.Checked)
            {
                tbxCompany.Tag = "addressee";
                cboAddressee.Tag = "fao";
            }
            else
            {
                tbxCompany.Tag = "fao";
                cboAddressee.Tag = "addressee";
            }

            Word.ContentControl ctl = clsWordApp.getContentControl(tbxCompany.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbxCompany.Text;

            ctl = clsWordApp.getContentControl(cboAddressee.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = cboAddressee.Text;
        }

        private void SetParagraphSpacingOnPBandFAO()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("payableBy", false);
            if (tbxPayableBy.Text.Length == 0)
            {
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceExactly;
                ctl.Range.ParagraphFormat.LineSpacing = 1;
                ctl = clsWordApp.getContentControl("payableByLabel", false);
                if (ctl != null)
                    ctl.Range.Text = "";
            }
            else
            {
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                ctl = clsWordApp.getContentControl("payableByLabel", false);
                if (ctl != null)
                    ctl.Range.Text = "PAYABLE BY:";
            }

            ctl = clsWordApp.getContentControl("fao", false);

            if (chkIndividual.Checked)
            {
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceExactly;
                ctl.Range.ParagraphFormat.LineSpacing = 1;
            }
            else
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;

        }
        
        
        private void BuildLists(String id)
        {
            bNoUpdating = true;
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            InterAction.IAAddress iaddress;
            if (id.Length == 0)
            {
                icontact = IA.GetContact(tbxForename.Text, tbxSurname.Text, tbxCompany.Text);
                iaddress = IA.GetAddress(icontact);
            }
            else
            {
                icontact = IA.GetContactByIALID(id);
                iaddress = null;
            }

            if (icontact.NameTitle.Length > 0)
                cboTitle.Text = icontact.NameTitle;

            tbxForename.Text = icontact.FirstName;
            tbxSurname.Text = icontact.LastName;
            tbxSuffix.Text = icontact.NameSuffix;
            tbxCompany.Text = icontact.CompanyName;
            bNoUpdating = false;
            tbxAddress.Text = icontact.SelectedAddress.FormattedAddress;
            BuildRecipientList();

        }

        private void BuildRecipientList()
        {
            if (bNoUpdating)
                return;
            cboAddressee.Items.Clear();
            String recip = "";
            if (!chkIndividual.Checked)
            { 
                foreach (XmlNode node in XmlDoc.SelectNodes("//naming/param"))
                {
                    try
                    {
                        recip = node.InnerText.Replace("[Title]", cboTitle.Text).Replace("[Initial]", tbxForename.Text.Substring(0, 1)).Replace("[Forename]", tbxForename.Text);
                        recip = recip.Replace("[Surname]", tbxSurname.Text).Replace("[Suffix]", tbxSuffix.Text).Trim();
                        cboAddressee.Items.Add(recip);
                    }
                    catch { }
                }
                cboAddressee.SelectedIndex = 0;
            }

        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            BuildLists("");
            BuildSaveTree();
        }

        public void Fetch()
        {
            if (DMS == null)
                DMS = Globals.ThisAddIn.DMS;
            this.tbxFileNumber.Text = DMS.ClientMatterCode;
            this.tbxMatter.Text = DMS.MatterDescription;
            //this.tbxOurRef.Text = DMS.ClientMatterCode;
            //FetchAuthor(DMS);
            //SetDate();
            //FetchSubject(DMS);

        }

        private void chkIndividual_CheckedChanged(object sender, EventArgs e)
        {
            cboTitle.Enabled = !chkIndividual.Checked;
            tbxForename.Enabled = !chkIndividual.Checked;
            tbxSurname.Enabled = !chkIndividual.Checked;
            tbxSuffix.Enabled = !chkIndividual.Checked;
            cboAddressee.Enabled = !chkIndividual.Checked;
            cboAddressee.Text = "";
            AddresseeOrCompany();
            SetParagraphSpacingOnPBandFAO();
            BuildSaveTree();
        }

        private void chkAddAddress_CheckedChanged(object sender, EventArgs e)
        {
            SetExtraAddressEnabled();
            BuildSaveTree();
        }

        private void SetExtraAddressEnabled()
        {
            tbxExtraAddress.Enabled = chkAddAddress.Checked;
        }

        private void SetListView()
        {
            lvwServices.Columns.Add("Description");
            lvwServices.Columns.Add("Net");
            lvwServices.Columns[0].Width = 215;
        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(tbxDescription.Text);
            double netfloat = Convert.ToDouble(tbxNet.Text);
            lvi.SubItems.Add(netfloat.ToString("0.00"));
            lvwServices.Items.Add(lvi);
            ClearTable();
            BuildTable();
            AddTotalToTable();
            BuildSaveTree();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = lvwServices.SelectedItems[0];
            lvi.Remove();
            ClearTable();
            BuildTable();
            AddTotalToTable();
            BuildSaveTree();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lvwServices.SelectedItems.Count == 0)
            {
                MessageBox.Show("You need to select an item in the list below before you can update it.");
                return;
            }
            ListViewItem lvi = lvwServices.SelectedItems[0];
            lvi.SubItems[0].Text = tbxDescription.Text;
            lvi.SubItems[1].Text = Convert.ToDouble(tbxNet.Text).ToString("0.00");
            ClearTable();
            BuildTable();
            AddTotalToTable();
            BuildSaveTree();
        }

        private void ClearTable()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("title", false);
            Word.Table tbl = ctl.Range.Tables[1];
            for (int i = tbl.Rows.Count; i>1 ; i--)
                tbl.Rows[i].Delete();
        }

        private void BuildTable()
        {
            //Word.UndoRecord ur = Globals.ThisAddIn.Application.UndoRecord;
            //ur.StartCustomRecord("Added data");
            Word.ContentControl ctl = clsWordApp.getContentControl("title", false);
            Word.Table tbl = ctl.Range.Tables[1];
            
            foreach(ListViewItem lvi in lvwServices.Items)
            {
                Word.Row row = tbl.Rows.Add();
                row.Cells[1].Range.Text = lvi.SubItems[0].Text;
                row.Cells[1].Range.Font.SmallCaps = 0;
                row.Cells[1].Range.Font.Bold = 0;

                row.Cells[2].Range.Text = lvi.SubItems[1].Text;
                row.Cells[2].Range.Font.Reset();
                row.Cells[2].Range.ParagraphFormat.Reset();
                row.Cells[2].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
            }
            //ur.EndCustomRecord();
        }

        private void AddTotalToTable()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("title", false);
            Word.Table tbl = ctl.Range.Tables[1];
            double total = 0;

            foreach (ListViewItem lvi in lvwServices.Items)
                total += Convert.ToDouble(lvi.SubItems[1].Text);

            Word.Row row = tbl.Rows.Add();
            row.Cells[2].Range.Text = total.ToString("0.00");
            row.Cells[2].Range.Font.Reset();
            row.Cells[2].Range.ParagraphFormat.Reset();
            row.Cells[2].Range.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphRight;
        }

        private void BuildSaveTree()
        {
            if (bNoUpdating)
                return;
            XmlSave = new XmlDocument();
            XmlNode topItem = XmlSave.CreateElement(string.Empty, "savings", string.Empty);
            XmlSave.AppendChild(topItem);
            foreach (Control ctl in this.Controls)
            {
                XmlNode node = XmlSave.CreateElement(string.Empty, ctl.Name, string.Empty);
                topItem.AppendChild(node);
                XmlAttribute att = XmlSave.CreateAttribute("type");
                att.InnerText = ctl.GetType().ToString();
                node.Attributes.Append(att);

                if (ctl.GetType().ToString().ToLower().Contains("checkbox"))
                {
                    CheckBox chk = (CheckBox)ctl;
                    if (chk.Checked)
                        node.InnerText = "-1";
                    else
                        node.InnerText = "0";
                }

                if (ctl.GetType().ToString().ToLower().Contains("forms.textbox"))
                {
                    TextBox tbx = (TextBox)ctl;
                    node.InnerText = tbx.Text;
                }

                if (ctl.GetType().ToString().ToLower().Contains("richtextbox"))
                {
                    RichTextBox tbx = (RichTextBox)ctl;
                    node.InnerText = tbx.Text;
                }

                if (ctl.GetType().ToString().ToLower().Contains("datetime"))
                {
                    DateTimePicker tbx = (DateTimePicker)ctl;
                    node.InnerText = tbx.Text;
                }

                if (ctl.GetType().ToString().ToLower().Contains("listview"))
                {
                    ListView lvw = (ListView)ctl;
                    foreach (ListViewItem lvi in lvw.Items)
                    {
                        XmlNode subnode = XmlSave.CreateElement(string.Empty, "subnode", string.Empty);
                        node.AppendChild(subnode);
                        XmlNode column1 = XmlSave.CreateElement(string.Empty, "column1", string.Empty);
                        column1.InnerText = lvi.SubItems[0].Text;
                        subnode.AppendChild(column1);
                        XmlNode column2 = XmlSave.CreateElement(string.Empty, "column2", string.Empty);
                        column2.InnerText = lvi.SubItems[1].Text;
                        subnode.AppendChild(column2);
  
                    }
                }

                if (ctl.GetType().ToString().ToLower().Contains("combobox"))
                {
                    ComboBox cbo = (ComboBox)ctl;
                    node.InnerText = cbo.Text;
                }

                try
                {
                    Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["invoiceDetails"];
                    vbl.Value = XmlSave.OuterXml;
                }
                catch
                {
                    Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("invoiceDetails", XmlSave.OuterXml);
                }
            }
        }

        public void RepopulateProformaTaskPane()
        {
            bNoUpdating = true;
            try
            {
                Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["invoiceDetails"];
                XmlSave = new XmlDocument();
                XmlSave.LoadXml(vbl.Value);
            }
            catch
            {
                bNoUpdating = false;
                return;
            }

            foreach (XmlNode node in XmlSave.DocumentElement.ChildNodes)
            {
                String type = node.SelectSingleNode("@type").InnerText;
                switch (type)
                {
                    case "System.Windows.Forms.CheckBox":
                        CheckBox chk = (CheckBox)this.Controls[node.Name];
                        if (node.InnerText == "-1")
                            chk.Checked = true;
                        else
                            chk.Checked = false;
                        break;

                    case "CustomTextBox":
                        TextBox tbx = (TextBox)this.Controls[node.Name];
                        tbx.Text = node.InnerText;
                        break;

                    case "CustomTaskPanes.CustomComboBox":
                        ComboBox cbo = (ComboBox)this.Controls[node.Name];
                        cbo.Text = node.InnerText;
                        break;

                    case "CustomTaskPanes.CustomRichTextBox":
                        RichTextBox rtbx = (RichTextBox)this.Controls[node.Name];
                        rtbx.Text = node.InnerText;
                        break;

                    case "System.Windows.Forms.ListView":
                        ListView lvw = (ListView)this.Controls[node.Name];
                        foreach (XmlNode subnode in node.ChildNodes)
                        {
                            ListViewItem lvi = new ListViewItem(subnode.ChildNodes[0].InnerText);
                            lvw.Items.Add(lvi);
                            foreach (XmlNode column in subnode.SelectNodes("column2|column3|column4"))
                            {
                                lvi.SubItems.Add(column.InnerText);
                            }
                        }
                        break;
                }
            }
           
            bNoUpdating = false;
            BuildRecipientList();
            bNoUpdating = true;
            cboAddressee.Text = XmlSave.SelectSingleNode("//cboAddressee").InnerText;
            bNoUpdating = false;
        }

        private void lvwServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvwServices.SelectedItems.Count>0)
            { 
                tbxDescription.Text = lvwServices.SelectedItems[0].SubItems[0].Text;
                tbxNet.Text = lvwServices.SelectedItems[0].SubItems[1].Text;
            }
        }

        private void cboTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxForename_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxSurname_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxSuffix_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

    }
}
