﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpUSFax
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxOurRef = new CustomTextBox();
            this.btnLookup = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxContact = new CustomTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lvwContacts = new System.Windows.Forms.ListView();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxCompany = new CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxFaxNumber = new CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDate = new CustomTaskPanes.CustomDateTimePicker();
            this.chkSRA = new System.Windows.Forms.CheckBox();
            this.btnAuthor = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tbxAuthor = new CustomTextBox();
            this.tbxFax = new CustomTextBox();
            this.tbxMail = new CustomTextBox();
            this.tbxAuthorTel = new CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxReturnNumber = new CustomTextBox();
            this.btnUnderline = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxSubject = new CustomTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboClosing = new CustomTaskPanes.CustomComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox1 = new CustomTextBox();
            this.btnUpdateCC = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lvwCC = new System.Windows.Forms.ListView();
            this.btnCCRemove = new System.Windows.Forms.Button();
            this.btnCCAdd = new System.Windows.Forms.Button();
            this.btnCCLookup = new System.Windows.Forms.Button();
            this.tbxCCFax = new CustomTextBox();
            this.tbxCCOrg = new CustomTextBox();
            this.tbxCCName = new CustomTextBox();
            this.btnAddToTo = new System.Windows.Forms.Button();
            this.btnAddToCC = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.numPages = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Our reference";
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(115, 21);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(226, 20);
            this.tbxOurRef.TabIndex = 9;
            this.tbxOurRef.Tag = "ourRef";
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(322, 212);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(21, 21);
            this.btnLookup.TabIndex = 74;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 73;
            this.label5.Text = "To";
            // 
            // tbxContact
            // 
            this.tbxContact.Location = new System.Drawing.Point(116, 214);
            this.tbxContact.Name = "tbxContact";
            this.tbxContact.Size = new System.Drawing.Size(200, 20);
            this.tbxContact.TabIndex = 72;
            this.tbxContact.Tag = "toList";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 71;
            this.label12.Text = "Outlook contacts";
            // 
            // lvwContacts
            // 
            this.lvwContacts.FullRowSelect = true;
            this.lvwContacts.GridLines = true;
            this.lvwContacts.Location = new System.Drawing.Point(115, 108);
            this.lvwContacts.Name = "lvwContacts";
            this.lvwContacts.Size = new System.Drawing.Size(226, 98);
            this.lvwContacts.TabIndex = 70;
            this.lvwContacts.UseCompatibleStateImageBehavior = false;
            this.lvwContacts.View = System.Windows.Forms.View.Details;
            this.lvwContacts.SelectedIndexChanged += new System.EventHandler(this.lvwContacts_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 245);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 69;
            this.label7.Text = "Organisation";
            // 
            // tbxCompany
            // 
            this.tbxCompany.Location = new System.Drawing.Point(116, 240);
            this.tbxCompany.Name = "tbxCompany";
            this.tbxCompany.Size = new System.Drawing.Size(225, 20);
            this.tbxCompany.TabIndex = 68;
            this.tbxCompany.Tag = "company";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 76;
            this.label2.Text = "Fax number";
            // 
            // tbxFaxNumber
            // 
            this.tbxFaxNumber.Location = new System.Drawing.Point(116, 266);
            this.tbxFaxNumber.Name = "tbxFaxNumber";
            this.tbxFaxNumber.Size = new System.Drawing.Size(225, 20);
            this.tbxFaxNumber.TabIndex = 75;
            this.tbxFaxNumber.Tag = "fax";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 78;
            this.label3.Text = "Date";
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "MMMM d, yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(114, 47);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(227, 20);
            this.dtpDate.TabIndex = 77;
            this.dtpDate.Tag = "date";
            // 
            // chkSRA
            // 
            this.chkSRA.AutoSize = true;
            this.chkSRA.Location = new System.Drawing.Point(18, 756);
            this.chkSRA.Name = "chkSRA";
            this.chkSRA.Size = new System.Drawing.Size(118, 17);
            this.chkSRA.TabIndex = 88;
            this.chkSRA.Text = "Include SRA notice";
            this.chkSRA.UseVisualStyleBackColor = true;
            this.chkSRA.CheckedChanged += new System.EventHandler(this.chkSRA_CheckedChanged);
            // 
            // btnAuthor
            // 
            this.btnAuthor.Location = new System.Drawing.Point(312, 642);
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(21, 21);
            this.btnAuthor.TabIndex = 87;
            this.btnAuthor.Text = "...";
            this.btnAuthor.UseVisualStyleBackColor = true;
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 729);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 13);
            this.label14.TabIndex = 86;
            this.label14.Text = "Author email";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 703);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 85;
            this.label15.Text = "Author fax";
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(18, 677);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 17);
            this.label16.TabIndex = 84;
            this.label16.Text = "Author tel";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 649);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 13);
            this.label17.TabIndex = 83;
            this.label17.Text = "Author";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(115, 643);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(191, 20);
            this.tbxAuthor.TabIndex = 79;
            this.tbxAuthor.Tag = "authorName";
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(115, 697);
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(225, 20);
            this.tbxFax.TabIndex = 81;
            this.tbxFax.Tag = "authorFax";
            // 
            // tbxMail
            // 
            this.tbxMail.Location = new System.Drawing.Point(115, 723);
            this.tbxMail.Name = "tbxMail";
            this.tbxMail.Size = new System.Drawing.Size(225, 20);
            this.tbxMail.TabIndex = 82;
            this.tbxMail.Tag = "authorEmail";
            // 
            // tbxAuthorTel
            // 
            this.tbxAuthorTel.Location = new System.Drawing.Point(115, 671);
            this.tbxAuthorTel.Name = "tbxAuthorTel";
            this.tbxAuthorTel.Size = new System.Drawing.Size(225, 20);
            this.tbxAuthorTel.TabIndex = 80;
            this.tbxAuthorTel.Tag = "authorTelephone";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 532);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 96;
            this.label9.Text = "Return number";
            // 
            // tbxReturnNumber
            // 
            this.tbxReturnNumber.Location = new System.Drawing.Point(115, 527);
            this.tbxReturnNumber.Name = "tbxReturnNumber";
            this.tbxReturnNumber.Size = new System.Drawing.Size(225, 20);
            this.tbxReturnNumber.TabIndex = 95;
            this.tbxReturnNumber.Tag = "faxTransError";
            // 
            // btnUnderline
            // 
            this.btnUnderline.Location = new System.Drawing.Point(317, 555);
            this.btnUnderline.Name = "btnUnderline";
            this.btnUnderline.Size = new System.Drawing.Size(21, 21);
            this.btnUnderline.TabIndex = 103;
            this.btnUnderline.Text = "__";
            this.btnUnderline.UseVisualStyleBackColor = true;
            this.btnUnderline.Click += new System.EventHandler(this.btnUnderline_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 559);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 102;
            this.label10.Text = "Subject";
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(115, 555);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(196, 20);
            this.tbxSubject.TabIndex = 101;
            this.tbxSubject.Tag = "subject";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 612);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 100;
            this.label11.Text = "Closing";
            // 
            // cboClosing
            // 
            this.cboClosing.FormattingEnabled = true;
            this.cboClosing.Location = new System.Drawing.Point(115, 607);
            this.cboClosing.Name = "cboClosing";
            this.cboClosing.Size = new System.Drawing.Size(221, 21);
            this.cboClosing.TabIndex = 99;
            this.cboClosing.Tag = "closing";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 584);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(54, 13);
            this.label13.TabIndex = 98;
            this.label13.Text = "Salutation";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(115, 581);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(225, 20);
            this.textBox1.TabIndex = 97;
            this.textBox1.Tag = "salutation";
            // 
            // btnUpdateCC
            // 
            this.btnUpdateCC.Location = new System.Drawing.Point(322, 357);
            this.btnUpdateCC.Name = "btnUpdateCC";
            this.btnUpdateCC.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateCC.TabIndex = 114;
            this.btnUpdateCC.Text = "!";
            this.btnUpdateCC.UseVisualStyleBackColor = true;
            this.btnUpdateCC.Click += new System.EventHandler(this.btnUpdateCC_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 360);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 13);
            this.label4.TabIndex = 113;
            this.label4.Text = "Fax number";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 112;
            this.label6.Text = "Organisation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 111;
            this.label8.Text = "CC";
            // 
            // lvwCC
            // 
            this.lvwCC.GridLines = true;
            this.lvwCC.Location = new System.Drawing.Point(116, 383);
            this.lvwCC.MultiSelect = false;
            this.lvwCC.Name = "lvwCC";
            this.lvwCC.Size = new System.Drawing.Size(225, 111);
            this.lvwCC.TabIndex = 110;
            this.lvwCC.UseCompatibleStateImageBehavior = false;
            this.lvwCC.View = System.Windows.Forms.View.Details;
            this.lvwCC.SelectedIndexChanged += new System.EventHandler(this.lvwCC_SelectedIndexChanged);
            // 
            // btnCCRemove
            // 
            this.btnCCRemove.Location = new System.Drawing.Point(322, 338);
            this.btnCCRemove.Name = "btnCCRemove";
            this.btnCCRemove.Size = new System.Drawing.Size(22, 19);
            this.btnCCRemove.TabIndex = 109;
            this.btnCCRemove.Text = "-";
            this.btnCCRemove.UseVisualStyleBackColor = true;
            this.btnCCRemove.Click += new System.EventHandler(this.btnCCRemove_Click);
            // 
            // btnCCAdd
            // 
            this.btnCCAdd.Location = new System.Drawing.Point(322, 320);
            this.btnCCAdd.Name = "btnCCAdd";
            this.btnCCAdd.Size = new System.Drawing.Size(22, 19);
            this.btnCCAdd.TabIndex = 108;
            this.btnCCAdd.Text = "+";
            this.btnCCAdd.UseVisualStyleBackColor = true;
            this.btnCCAdd.Click += new System.EventHandler(this.btnCCAdd_Click);
            // 
            // btnCCLookup
            // 
            this.btnCCLookup.Location = new System.Drawing.Point(322, 301);
            this.btnCCLookup.Name = "btnCCLookup";
            this.btnCCLookup.Size = new System.Drawing.Size(22, 19);
            this.btnCCLookup.TabIndex = 107;
            this.btnCCLookup.Text = "...";
            this.btnCCLookup.UseVisualStyleBackColor = true;
            this.btnCCLookup.Click += new System.EventHandler(this.btnCCLookup_Click);
            // 
            // tbxCCFax
            // 
            this.tbxCCFax.Location = new System.Drawing.Point(116, 357);
            this.tbxCCFax.Name = "tbxCCFax";
            this.tbxCCFax.Size = new System.Drawing.Size(199, 20);
            this.tbxCCFax.TabIndex = 106;
            // 
            // tbxCCOrg
            // 
            this.tbxCCOrg.Location = new System.Drawing.Point(116, 329);
            this.tbxCCOrg.Name = "tbxCCOrg";
            this.tbxCCOrg.Size = new System.Drawing.Size(200, 20);
            this.tbxCCOrg.TabIndex = 105;
            // 
            // tbxCCName
            // 
            this.tbxCCName.Location = new System.Drawing.Point(116, 301);
            this.tbxCCName.Name = "tbxCCName";
            this.tbxCCName.Size = new System.Drawing.Size(200, 20);
            this.tbxCCName.TabIndex = 104;
            // 
            // btnAddToTo
            // 
            this.btnAddToTo.Location = new System.Drawing.Point(66, 134);
            this.btnAddToTo.Name = "btnAddToTo";
            this.btnAddToTo.Size = new System.Drawing.Size(41, 21);
            this.btnAddToTo.TabIndex = 115;
            this.btnAddToTo.Text = "To";
            this.btnAddToTo.UseVisualStyleBackColor = true;
            this.btnAddToTo.Click += new System.EventHandler(this.btnAddToTo_Click);
            // 
            // btnAddToCC
            // 
            this.btnAddToCC.Location = new System.Drawing.Point(66, 161);
            this.btnAddToCC.Name = "btnAddToCC";
            this.btnAddToCC.Size = new System.Drawing.Size(41, 21);
            this.btnAddToCC.TabIndex = 116;
            this.btnAddToCC.Text = "CC";
            this.btnAddToCC.UseVisualStyleBackColor = true;
            this.btnAddToCC.Click += new System.EventHandler(this.btnAddToCC_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 503);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(62, 13);
            this.label18.TabIndex = 118;
            this.label18.Text = "Page count";
            // 
            // numPages
            // 
            this.numPages.Location = new System.Drawing.Point(114, 500);
            this.numPages.Name = "numPages";
            this.numPages.Size = new System.Drawing.Size(48, 20);
            this.numPages.TabIndex = 117;
            this.numPages.Tag = "pagesCount";
            this.numPages.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ctpUSFax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.label18);
            this.Controls.Add(this.numPages);
            this.Controls.Add(this.btnAddToCC);
            this.Controls.Add(this.btnAddToTo);
            this.Controls.Add(this.btnUpdateCC);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lvwCC);
            this.Controls.Add(this.btnCCRemove);
            this.Controls.Add(this.btnCCAdd);
            this.Controls.Add(this.btnCCLookup);
            this.Controls.Add(this.tbxCCFax);
            this.Controls.Add(this.tbxCCOrg);
            this.Controls.Add(this.tbxCCName);
            this.Controls.Add(this.btnUnderline);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cboClosing);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxReturnNumber);
            this.Controls.Add(this.chkSRA);
            this.Controls.Add(this.btnAuthor);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.tbxMail);
            this.Controls.Add(this.tbxAuthorTel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxFaxNumber);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxContact);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lvwContacts);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxCompany);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxOurRef);
            this.Name = "ctpUSFax";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpUSFax_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxOurRef;
        private System.Windows.Forms.Button btnLookup;
        private System.Windows.Forms.Label label5;
        private CustomTextBox tbxContact;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListView lvwContacts;
        private System.Windows.Forms.Label label7;
        private CustomTextBox tbxCompany;
        private System.Windows.Forms.Label label2;
        private CustomTextBox tbxFaxNumber;
        private System.Windows.Forms.Label label3;
        private CustomTaskPanes.CustomDateTimePicker dtpDate;
        private System.Windows.Forms.CheckBox chkSRA;
        private System.Windows.Forms.Button btnAuthor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private CustomTextBox tbxAuthor;
        private CustomTextBox tbxFax;
        private CustomTextBox tbxMail;
        private CustomTextBox tbxAuthorTel;
        private System.Windows.Forms.Label label9;
        private CustomTextBox tbxReturnNumber;
        private System.Windows.Forms.Button btnUnderline;
        private System.Windows.Forms.Label label10;
        private CustomTextBox tbxSubject;
        private System.Windows.Forms.Label label11;
        private CustomTaskPanes.CustomComboBox cboClosing;
        private System.Windows.Forms.Label label13;
        private CustomTextBox textBox1;
        private System.Windows.Forms.Button btnUpdateCC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView lvwCC;
        private System.Windows.Forms.Button btnCCRemove;
        private System.Windows.Forms.Button btnCCAdd;
        private System.Windows.Forms.Button btnCCLookup;
        private CustomTextBox tbxCCFax;
        private CustomTextBox tbxCCOrg;
        private CustomTextBox tbxCCName;
        private System.Windows.Forms.Button btnAddToTo;
        private System.Windows.Forms.Button btnAddToCC;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numPages;
    }
}
