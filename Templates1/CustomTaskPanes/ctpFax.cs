﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;
using System.Xml;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpFax : UserControl
    {
        private WorkSite8Application DMS;
        private clsInterAction IA;
        private Boolean yoursFaithfully = false;
        
        public ctpFax()
        {
            InitializeComponent();
        }

        
        /// <summary>
        /// Triggered when the fax task pane is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void ctpFax_Load(object sender, EventArgs e)
        {
            LetterParameters lp = new LetterParameters();

            DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
           
            this.cboSignOff.Items.AddRange(lp.SignOff.ToArray());
            this.lvwTo.Columns.Add("To");
            this.lvwTo.Columns.Add("Org");
            this.lvwTo.Columns.Add("Fax");
            this.lvwTo.View = View.Details;
            
           
            this.lvwCC.Columns.Add("CC");
            this.lvwCC.Columns.Add("Org");
            this.lvwCC.Columns.Add("Fax");
            this.lvwCC.View = View.Details;
           
            this.numPages.Value = 1;
            this.cboSignOff.TextChanged += new EventHandler(cboSignOff_TextChanged);
            SizeColumns(lvwTo);
            SizeColumns(lvwCC);
            
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnCCLookup.Enabled = online;
            btnToLookup.Enabled = online;

        }

        /// <summary>
        /// Triggered when the sign-off text box is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        void cboSignOff_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }
  
        
        /// <summary>
        /// Sizes the columns of a list view control
        /// </summary>
        /// <param name="lvw"></param>
        
        private void SizeColumns(ListView lvw)
        {
            foreach (ColumnHeader col in lvw.Columns)
                col.Width = lvw.Width / 3;
        }
        
        
        /// <summary>
        /// Updates the to fields
        /// </summary>
        
        private void UpdateToLists()
        {
            string to = "";
            string org = "";
            string fax = "";
            foreach (ListViewItem lvi in this.lvwTo.Items)
            {
                to = to + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                org = org + lvi.SubItems[1].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                fax = fax + lvi.SubItems[2].Text + Microsoft.VisualBasic.Constants.vbCrLf;
            }
            Word.ContentControl ctl = clsWordApp.getContentControl("toList", false);
            if (ctl != null) ctl.Range.Text = to;
            ctl = clsWordApp.getContentControl("toOrganisations", false);
            if (ctl != null) ctl.Range.Text = org;
            ctl = clsWordApp.getContentControl("toFaxNumbers", false);
            if (ctl != null) ctl.Range.Text = fax;
        }

        /// <summary>
        /// Updates the cc fields
        /// </summary>
        
        private void UpdateCCLists()
        {
            string to = "";
            string org = "";
            string fax = "";
            foreach (ListViewItem lvi in this.lvwCC.Items)
            {
                to = to + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                org = org + lvi.SubItems[1].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                fax = fax + lvi.SubItems[2].Text + Microsoft.VisualBasic.Constants.vbCrLf;
            }
            Word.ContentControl ctl = clsWordApp.getContentControl("ccList", false);
            if (ctl != null) ctl.Range.Text = to;
            ctl = clsWordApp.getContentControl("ccOrganisations", false);
            if (ctl != null) ctl.Range.Text = org;
            ctl = clsWordApp.getContentControl("ccFaxNumbers", false);
            if (ctl != null) ctl.Range.Text = fax;
        }
        
        /// <summary>
        /// Repopulates the fax task pane with data from the document
        /// </summary>
        
        public void RepopulateFaxTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if(!ctl.ShowingPlaceholderText)
                        control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }

            Word.ContentControl ctl2 = clsWordApp.getContentControl("toList", false);
                if(ctl2.Range.Text != ctl2.Title)
                    RepopulateListView(this.lvwTo, true);
            ctl2 = clsWordApp.getContentControl("ccList", false);
                if(ctl2.Range.Text != ctl2.Title)
                     RepopulateListView(this.lvwCC, false);
        }
        
        /// <summary>
        /// Fetches the author and subject from the DMS
        /// </summary>
        
        
        public void Fetch()
        {
            if (DMS == null)
                DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
           
            if(DMS.IsOnline)
            { 
            this.tbxOurRef.Text = DMS.ClientMatterCode;
            FetchAuthor(DMS);
            FetchSubject(DMS);
            }
            SetDate();
            

        }

        /// <summary>
        /// Fetches the author details from the DMS
        /// </summary>
        /// <param name="DMS"></param>
        
        
        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author=String.Empty;
            if (DMS.IsOnline)
                author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            else
                return;
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();
            this.tbxAuthorTel.Text = adu.TelephoneNumber;
            this.tbxMail.Text = adu.Mail;
            this.tbxFax.Text = adu.Fax;
            this.tbxAuthor.Text = adu.FullName;
            this.tbxTitle.Text = adu.Title;
        }
        
        /// <summary>
        /// Fetches the subject from the DMS
        /// </summary>
        /// <param name="DMS"></param>
        
        private void FetchSubject(WorkSite8Application DMS)
        {
            if(DMS.IsOnline)
            this.tbxSubject.Text = DMS.MatterDescription;
        }

        /// <summary>
        /// Sets the date in the document based on the value in the datetime picker
        /// </summary>
        
        
        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date",false);
            ctl.Range.Text = this.dateTimePicker1.Text;
        }

        
        /// <summary>
        /// Triggered when the our ref text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxOurRef_TextChanged(object sender, EventArgs e)
           {
               TextBoxUpdate(sender);
        }

        
        /// <summary>
        /// Updates the control in the word document with the value from a text box
        /// </summary>
        /// <param name="sender"></param>
        
        private void TextBoxUpdate(object sender)
        {
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if(ctl!=null)
                ctl.Range.Text = tbx.Text;
        }

        
        /// <summary>
        /// Triggered when the your ref text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxYourRef_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Updates the content control in the word document when the date time picker is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        
        /// <summary>
        /// Triggered when the subject control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxSubject_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxAuthor_TextChanged(object sender, EventArgs e)
        {
            SetAuthor();
        }

        private void SetAuthor()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("MdR", false);
            if (yoursFaithfully)
                ctl.Range.Text = Globals.ThisAddIn.Officedata.Offices.ActiveOffice.Fullname;
            else
                ctl.Range.Text = tbxAuthor.Text;
        }

        private void tbxTitle_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
            SetRole();
        }

        private void tbxAuthorTel_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxFax_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxMail_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void cboSignOff_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboSignOff.Text.ToLower().Contains("faithfully"))
                yoursFaithfully = true;
            else
                yoursFaithfully = false;
            TextBoxUpdate(sender);
            SetAuthor();
            SetRole();
        }

        private void SetRole()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("role", false);
            if(ctl!=null)
                if (yoursFaithfully)
                    ctl.Range.Text = "";
                else
                    ctl.Range.Text = this.tbxTitle.Text;
        }
        
        private void btnToLookup_Click(object sender, EventArgs e)
        {
            try
            {
                InterAction.IAContact icontact = GetContact();
                if (icontact == null) return;
                this.tbxToName.Text = icontact.FullName;
                this.tbxToFax.Text = icontact.ContactDetail.busFax;
                this.tbxToOrg.Text = icontact.CompanyName;
                this.lvwTo.SelectedItems.Clear();
            }
            catch { }
        }

        private InterAction.IAContact GetContact()
        {
            if (IA==null)
            IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        private void btnToAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(this.tbxToName.Text);
            lvi.SubItems.Add(this.tbxToOrg.Text);
            lvi.SubItems.Add(this.tbxToFax.Text);
            this.lvwTo.Items.Add(lvi);
            this.lvwTo.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwTo.Focus();
            UpdateToLists();
        }

        private void lvwTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.lvwTo.SelectedItems.Count>0)
            { 
            ListViewItem lvi = this.lvwTo.SelectedItems[0];
            this.tbxToName.Text = lvi.SubItems[0].Text;
            this.tbxToOrg.Text = lvi.SubItems[1].Text;
            this.tbxToFax.Text = lvi.SubItems[2].Text;
            }
        }

        private void btnToRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwTo.SelectedItems.Count > 0)
            {
                foreach(ListViewItem lvi in this.lvwTo.SelectedItems)
                    this.lvwTo.Items.Remove(lvi);
            }
            UpdateToLists();
        }

        private void RepopulateListView(ListView lbx, Boolean ToList)
        {
        char[] paragraph = Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray();
        String[] to;
        String[] org;
        String[] fax;
            if(ToList)
            { 
                to = clsWordApp.getContentControl("toList", false).Range.Text.Split(paragraph);
                org = clsWordApp.getContentControl("toOrganisations", false).Range.Text.Split(paragraph);
                fax = clsWordApp.getContentControl("toFaxNumbers", false).Range.Text.Split(paragraph);
            }
            else
            {
                to = clsWordApp.getContentControl("ccList", false).Range.Text.Split(paragraph);
                org = clsWordApp.getContentControl("ccOrganisations", false).Range.Text.Split(paragraph);
                fax = clsWordApp.getContentControl("ccFaxNumbers", false).Range.Text.Split(paragraph);
            }
        int i = -1;
            foreach(String item in to)
            {
                i++;
                if(to[i].Length>0)
                { 
                ListViewItem lvw = new ListViewItem(item);
                lvw.SubItems.Add(org[i]);
                lvw.SubItems.Add(fax[i]);
                lbx.Items.Add(lvw);
                }           
            }
        }

        private void btnCCAdd_Click(object sender, EventArgs e)
        {
             ListViewItem lvi;
            
            lvi = new ListViewItem(this.tbxCCName.Text);
            lvi.SubItems.Add(this.tbxCCOrg.Text);
            lvi.SubItems.Add(this.tbxCCFax.Text);
            this.lvwCC.Items.Add(lvi);
            this.lvwCC.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwCC.Focus();
            UpdateCCLists();
        }

        private void btnCCLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
           
            this.tbxCCName.Text = icontact.FullName;
            this.tbxCCFax.Text = icontact.ContactDetail.busFax;
            this.tbxCCOrg.Text = icontact.CompanyName;
            this.lvwCC.SelectedItems.Clear();
        }

        private void btnCCRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwCC.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwCC.SelectedItems)
                    this.lvwCC.Items.Remove(lvi);
            }
            UpdateCCLists();
        }

        private void lvwCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvwCC.SelectedItems.Count > 0)
            {
                ListViewItem lvi = this.lvwCC.SelectedItems[0];
                this.tbxCCName.Text = lvi.SubItems[0].Text;
                this.tbxCCOrg.Text = lvi.SubItems[1].Text;
                this.tbxCCFax.Text = lvi.SubItems[2].Text;
            }
        }

        private void numPages_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown tbx = (NumericUpDown)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Value.ToString();
        }

        private void btnAuthor_Click(object sender, EventArgs e)
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
            if (frm.user != null)
            {
                this.tbxAuthor.Text = frm.user.FullName;
                this.tbxAuthorTel.Text = frm.user.TelephoneNumber;
                this.tbxTitle.Text = frm.user.Title;
                this.tbxFax.Text = frm.user.Fax;
                this.tbxMail.Text = frm.user.Mail;
                if(DMS.IsOnline)
                    DMS.SetAuthor(frm.user.Login);
            }
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
            foreach (Control ctl in this.Controls)
                try
                {
                    if (ctl.Tag.ToString() != "") ctl.Text = "";
                }
                catch { }
           
                this.lvwTo.Items.Clear();
                this.lvwCC.Items.Clear();
                UpdateToLists();
                UpdateCCLists();
                this.numPages.Value = Globals.ThisAddIn.Application.ActiveDocument.Range().Information[Word.WdInformation.wdActiveEndPageNumber];
                 Control ctl2 = this.numPages;
                 ctl2.Text = this.numPages.Value.ToString();
            
            Fetch();
        }

        private void btnUpdateTo_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwTo.SelectedItems[0];
            int index = lvi.Index;
            this.lvwTo.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxToName.Text);
            lvi.SubItems.Add(this.tbxToOrg.Text);
            lvi.SubItems.Add(this.tbxToFax.Text);
            this.lvwTo.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateToLists();

        }

        private void btnUpdateCC_Click(object sender, EventArgs e)
        {
            if (lvwCC.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwCC.SelectedItems[0];
            int index = lvi.Index;
            this.lvwCC.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxCCName.Text);
            lvi.SubItems.Add(this.tbxCCOrg.Text);
            lvi.SubItems.Add(this.tbxCCFax.Text);
            this.lvwCC.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateCCLists();
        }
    }
}
