﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpUSBill : UserControl
    {
        private clsOfficeData officedata;
        private clsOutlook ol;
        private clsInterAction IA;

        public ctpUSBill()
        {
            InitializeComponent();
        }

        private void ctpUSBill_Load(object sender, EventArgs e)
        {
            SetListView();
            officedata = Globals.ThisAddIn.Officedata;
            LoadContacts();
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);
            LoadCombosEtc();
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
        }

        private void SetListView()
        {
            lvwContacts.Columns.Add("Contact");
            lvwContacts.Columns.Add("Org.");
            lvwContacts.Columns[0].Width = 190 / 2;
            lvwContacts.Columns[1].Width = 190 / 2;
        }
        
        void ctl_TextChanged(object sender, EventArgs e)
        {
            //if (bNoUpdating)
            //    return;
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        private void LoadCombosEtc()
        {
            cboInterimFinal.Items.Add("Interim");
            cboInterimFinal.Items.Add("Final");
            tbxMatter.Text = Globals.ThisAddIn.DMS.MatterDescription;
        }

        private void LoadContacts()
        {
            ol = new clsOutlook();
            ol.GetContacts(lvwContacts);
        }

        public void RepopulateUSBillPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }
        }

        public void Fetch()
        {
            WorkSite8Application DMS = Globals.ThisAddIn.DMS;
            if (DMS.IsOnline)
                tbxMatter.Text = DMS.ClientMatterCode;
            SetDate();
            
        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dtpDate.Text;
        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxContact.Text = icontact.FullName;
            this.tbxCompany.Text = icontact.CompanyName;
            this.tbxAddress.Text = icontact.SelectedAddress.FormattedAddress;
        }

        private InterAction.IAContact GetContact()
        {
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        private void lvwContacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (lvwContacts.SelectedItems.Count == 0)
                return;
            tbxContact.Text = (lvwContacts.SelectedItems[0].SubItems[4].Text + " " + lvwContacts.SelectedItems[0].SubItems[0].Text).Trim();
            tbxCompany.Text = lvwContacts.SelectedItems[0].SubItems[1].Text;
            
            tbxAddress.Text = lvwContacts.SelectedItems[0].SubItems[3].Text;

            foreach (ListViewItem lvi in lvwContacts.Items)
                if (lvi.Selected)
                    lvi.BackColor = Color.Orange;
                else
                    lvi.BackColor = lvwContacts.BackColor;
            
        }
    }
}