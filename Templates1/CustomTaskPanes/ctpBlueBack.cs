﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpBlueBack : UserControl
    {
        private Boolean bUpdate = true;
        public ctpBlueBack()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the heading text box to read "Affidavit of Service"
        /// </summary>
        public void SetAffidavit()
        {
            this.tbxHeading.Text = "Affidavit of Service";
        }

        /// <summary>
        /// As above but sets the index combo as well
        /// </summary>
        
        public void SetAffirmationOfService()
        {
            this.rbnState.Checked = true;
            this.tbxHeading.Text = "Affirmation of Service";
            this.cboIndex.Text = "Index No.";
        }
        
        /// <summary>
        /// Sets the appropriate radio button control based on the jurisdiction in the document
        /// </summary>
        
        private void SetStateFederalOtherCheck()
        {
            if(clsWordApp.getContentControl("districtCounty", false).Range.Text == "")
            {
                rbnOther.Checked = true;
                return;
            }

            if(clsWordApp.getContentControl("court", false).Range.Text.ToLower() == "supreme court of the state of new york")
            {
                rbnState.Checked = true;
                return;
            }

            rbnFederal.Checked = true;
        }
        
        
        /// <summary>
        /// Triggered when the State radio button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void rbnState_CheckedChanged(object sender, EventArgs e)
        {
            SetRadioTextBoxes();
            if (rbnState.Checked)
            {
                clsWordApp.getContentControl("court", false).Range.Text = "Supreme Court of the State of New York";
                clsWordApp.getContentControl("districtCounty", false).Range.Text = "County of";
            }
        }

        /// <summary>
        /// Sets the radio button text boxes
        /// </summary>
        
        private void SetRadioTextBoxes()
        {
            this.tbxCounty.Enabled = this.rbnState.Checked;
            this.tbxFederal.Enabled = this.rbnFederal.Checked;
            this.tbxDistrict.Enabled = this.rbnFederal.Checked;
            this.tbxOther.Enabled = this.rbnOther.Checked;
            if (bUpdate)
            {
                this.tbxCounty.Text = "";
                this.tbxFederal.Text = "";
                this.tbxDistrict.Text = "";
                this.tbxOther.Text = "";
            }
            this.tbxHeading.Text = "Complaint";
        }

        
        /// <summary>
        /// Triggered when the federal radio button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbnFederal_CheckedChanged(object sender, EventArgs e)
        {
            SetRadioTextBoxes();
            if (rbnFederal.Checked)
            {
                clsWordApp.getContentControl("court", false).Range.Text = "United States District Court";
                if (bUpdate)
                    clsWordApp.getContentControl("districtCounty", false).Range.Text = "for the " + tbxFederal.Text + " District of";
            }
        }

        /// <summary>
        /// Triggered when the other radio button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void rbnOther_CheckedChanged(object sender, EventArgs e)
        {
            SetRadioTextBoxes();
            if (rbnOther.Checked)
            {
                clsWordApp.getContentControl("court", false).Range.Text = tbxOther.Text;
                clsWordApp.getContentControl("districtCounty", false).Range.Text = "";
            }
        }

        /// <summary>
        /// Triggered when the federal text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxFederal_TextChanged(object sender, EventArgs e)
        {
            clsWordApp.getContentControl("districtCounty", false).Range.Text = "for the " + tbxFederal.Text + " District of";
        }

        /// <summary>
        /// Sets the changed event on textboxes with a tag assigned
        /// </summary>
        
        private void SetEvents()
        {
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);

        }

        /// <summary>
        /// Triggered when a text box control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void ctl_TextChanged(object sender, EventArgs e)
        {
            Control ctl = (Control)sender;
            foreach (Word.ContentControl cf in clsWordApp.getContentControls(ctl.Tag.ToString(), Globals.ThisAddIn.Application.ActiveDocument))
                cf.Range.Text = ctl.Text;
        }

        /// <summary>
        /// Sets the index combo values
        /// </summary>
        
        private void SetIndexCombo()
        {
            cboIndex.Items.Add("Index No.");
            cboIndex.Items.Add("Case No.");
            cboIndex.Items.Add("Civil Action No.");
            //cboIndex.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets the plaintiff combo values
        /// </summary>
        
        private void SetPlaintiffCombo()
        {
            cboPlaintiff.Items.Add("Plaintiff");
            cboPlaintiff.Items.Add("Plaintiffs");
            cboDefendant.Items.Add("Defendant");
            cboDefendant.Items.Add("Defendants");
            //cboPlaintiff.SelectedIndex = 0;
            //cboDefendant.SelectedIndex = 0;
        }

        /// <summary>
        /// Sets the versus combo values
        /// </summary>
        
        private void SetVersusCombo()
        {
            cboVersus.Items.Add("v.");
            cboVersus.Items.Add("against");
            //cboVersus.SelectedIndex = 0;
        }

        /// <summary>
        /// Triggered when the blueback taskpane is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void ctpBlueBack_Load(object sender, EventArgs e)
        {
            Globals.ThisAddIn.Application.StatusBar = "Loading task pane..";
            SetEvents();
            SetRadioTextBoxes();
            SetIndexCombo();
            SetPlaintiffCombo();
            SetVersusCombo();
            
            Globals.ThisAddIn.Application.StatusBar = "";
        }

        /// <summary>
        /// Repopulates the blue back task pane with data from the document when one is openend
        /// </summary>
        
        public void RepopulateBlueBack()
        {
            SetStateFederalOtherCheck();
            chkDraft.Checked = !clsWordApp.getContentControl("draft", false).ShowingPlaceholderText;
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Triggered when the draft check box is checked/unchecked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void chkDraft_CheckedChanged(object sender, EventArgs e)
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("draft", false);
            if (chkDraft.Checked)
               ctl.Range.Text = "Draft";
            else
               ctl.Range.Text = "";
        }

        /// <summary>
        /// Triggered when the  plaintiff text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxPlaintiff_TextChanged(object sender, EventArgs e)
        {
            clsWordApp.getContentControl("plaintiff", false).Range.Text = tbxPlaintiff.Text + ",";
        }

        /// <summary>
        /// Triggered when the defendant text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxDefendant_TextChanged(object sender, EventArgs e)
        {
            clsWordApp.getContentControl("defendant", false).Range.Text = tbxDefendant.Text + ",";
        }

        /// <summary>
        /// Triggered when the affidavit check box is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnAff_Click(object sender, EventArgs e)
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Document pleading = Globals.ThisAddIn.PleadingDocument;
            String bookmarkName = String.Empty;
            if (doc.Bookmarks.Exists("bmkAffidavit"))
                bookmarkName = "bmkAffidavit";
            else if (doc.Bookmarks.Exists("bmkAffirmation"))
                bookmarkName = "bmkAffirmation";

            if(bookmarkName.Length>0)
            {
                Word.Range rng = doc.Bookmarks[bookmarkName].Range;
                rng.Copy();
                pleading.ActiveWindow.Activate();
                Word.Range destination = pleading.Range();
                destination.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                int start = destination.Start;
                destination.InsertBreak(Word.WdBreakType.wdPageBreak);
                destination.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                destination.Paste();
                int end = pleading.Range().End;
                System.Windows.Forms.Clipboard.Clear();
                destination.Start = start;
                destination.End = end;
                pleading.Bookmarks.Add(bookmarkName, destination);
            }
        }


    }
}
