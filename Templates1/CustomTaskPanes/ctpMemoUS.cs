﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpMemoUS : UserControl
    {
        private WorkSite8Application DMS;
        private Boolean bNoUpdate;
        public ctpMemoUS()
        {
            InitializeComponent();
        }

        private void ctpMemoUS_Load(object sender, EventArgs e)
        {
            foreach (Control ctl in this.Controls)
                if (ctl.Tag != null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);
        }

        void ctl_TextChanged(object sender, EventArgs e)
        {
            //if (bNoUpdating)
            //    return;
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        public void Fetch()
        {
            if (DMS == null)
                DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
            if (DMS.IsOnline)
            {
                FetchAuthor(DMS);
            }
            SetDate();


        }

        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();

            this.tbxAuthor.Text = adu.FullName;
            this.tbxExtension.Text = adu.TelephoneNumber.Substring(adu.TelephoneNumber.Length - 4, 4);

        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dtpDate.Text;
        }

        public void RepopulateMemoUSPane()
        {
            bNoUpdate = true;
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    if (ctl != null)
                    {
                        try
                        {
                            if (!ctl.ShowingPlaceholderText)
                                control.Text = ctl.Range.Text;
                        }
                        catch { }
                    }
                }
            }

            bNoUpdate = false;
        }
    }
}
