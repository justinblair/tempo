﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpNonLegal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkHeader = new System.Windows.Forms.CheckBox();
            this.tbxHeader = new CustomTextBox();
            this.SuspendLayout();
            // 
            // chkHeader
            // 
            this.chkHeader.AutoSize = true;
            this.chkHeader.Location = new System.Drawing.Point(25, 25);
            this.chkHeader.Name = "chkHeader";
            this.chkHeader.Size = new System.Drawing.Size(111, 17);
            this.chkHeader.TabIndex = 0;
            this.chkHeader.Text = "Repeating header";
            this.chkHeader.UseVisualStyleBackColor = true;
            this.chkHeader.CheckedChanged += new System.EventHandler(this.chkHeader_CheckedChanged);
            // 
            // tbxHeader
            // 
            this.tbxHeader.Location = new System.Drawing.Point(25, 50);
            this.tbxHeader.Name = "tbxHeader";
            this.tbxHeader.Size = new System.Drawing.Size(311, 20);
            this.tbxHeader.TabIndex = 1;
            this.tbxHeader.TextChanged += new System.EventHandler(this.tbxHeader_TextChanged);
            // 
            // ctpNonLegal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tbxHeader);
            this.Controls.Add(this.chkHeader);
            this.Name = "ctpNonLegal";
            this.Size = new System.Drawing.Size(339, 73);
            this.Load += new System.EventHandler(this.ctpNonLegal_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkHeader;
        private CustomTextBox tbxHeader;
    }
}
