﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpMemo : UserControl
    {
        private WorkSite8Application DMS;
        private clsInterAction IA;
        public ctpMemo()
        {
            InitializeComponent();
        }

        private void ctpMemo_Load(object sender, EventArgs e)
        {
            this.lvwTo.View = View.Details;
            this.lvwCC.View = View.Details;
            this.lvwTo.Columns.Add("To");
            this.lvwCC.Columns.Add("CC");
            this.lvwCC.Columns[0].Width = lvwCC.Width-10;
            this.lvwTo.Columns[0].Width = lvwTo.Width-10;
            
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnCCLookup.Enabled = online;
            btnToLookup.Enabled = online;
        }

        public void Fetch()
        {
            if (DMS == null)
                DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
            if(DMS.IsOnline)
            { 
                FetchAuthor(DMS);
                FetchSubject(DMS);
            }
            SetDate();
            

        }

        public void FetchAuthor(WorkSite8Application DMS)
        {
            if(DMS.IsOnline)
            { 
                String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
                ADSearcher ads = new ADSearcher(author);
                ADUser adu = ads.GetUser();
                this.tbxAuthor.Text = adu.FullName;
            }
            
        }
        private void FetchSubject(WorkSite8Application DMS)
        {
            if(DMS.IsOnline)
                this.tbxSubject.Text = DMS.MatterDescription;
        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dateTimePicker1.Text;
        }

        private void btnToLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxToName.Text = icontact.FullName;
            this.lvwTo.SelectedItems.Clear();
        }

        private InterAction.IAContact GetContact()
        {
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        private void btnToAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(this.tbxToName.Text);
            this.lvwTo.Items.Add(lvi);
            this.lvwTo.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwTo.Focus();
            UpdateToLists();
        }

        private void UpdateToLists()
        {
            string to = "";
            
            foreach (ListViewItem lvi in this.lvwTo.Items)
                to = to + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf;
           
            Word.ContentControl ctl = clsWordApp.getContentControl("toList", false);
            if (ctl != null) ctl.Range.Text = to;
            
        }

        private void btnToRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwTo.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwTo.SelectedItems)
                    this.lvwTo.Items.Remove(lvi);
            }
            UpdateToLists();
        }

        private void btnUpdateTo_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwTo.SelectedItems[0];
            int index = lvi.Index;
            this.lvwTo.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxToName.Text);
           
            this.lvwTo.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateToLists();
        }

        private void btnCCLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;

            this.tbxCCName.Text = icontact.FullName;
            this.lvwCC.SelectedItems.Clear();
        }

        private void btnCCAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi;

            lvi = new ListViewItem(this.tbxCCName.Text);
           
            this.lvwCC.Items.Add(lvi);
            this.lvwCC.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwCC.Focus();
            UpdateCCLists();
        }

        private void UpdateCCLists()
        {
            string to = "";
            foreach (ListViewItem lvi in this.lvwCC.Items)
                to = to + lvi.SubItems[0].Text + Microsoft.VisualBasic.Constants.vbCrLf;
                
            Word.ContentControl ctl = clsWordApp.getContentControl("ccList", false);
            if (ctl != null) ctl.Range.Text = to;
            
        }

        private void btnCCRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwCC.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwCC.SelectedItems)
                    this.lvwCC.Items.Remove(lvi);
            }
            UpdateCCLists();
        }

        private void btnUpdateCC_Click(object sender, EventArgs e)
        {
            if (lvwCC.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwCC.SelectedItems[0];
            int index = lvi.Index;
            this.lvwCC.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxCCName.Text);
            this.lvwCC.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateCCLists();
        }

        public void RepopulateMemoTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }

            Word.ContentControl ctl2 = clsWordApp.getContentControl("toList", false);
            if (ctl2.Range.Text != ctl2.Title)
                RepopulateListView(this.lvwTo, true);
            ctl2 = clsWordApp.getContentControl("ccList", false);
            if (ctl2.Range.Text != ctl2.Title)
                RepopulateListView(this.lvwCC, false);
        }

        private void RepopulateListView(ListView lbx, Boolean ToList)
        {
            char[] paragraph = Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray();
            String[] to;
            
            if (ToList)
                to = clsWordApp.getContentControl("toList", false).Range.Text.Split(paragraph);
            
            else
                to = clsWordApp.getContentControl("ccList", false).Range.Text.Split(paragraph);
               
            int i = -1;
            foreach (String item in to)
            {
                i++;
                if (to[i].Length > 0)
                {
                    ListViewItem lvw = new ListViewItem(item);
                    lbx.Items.Add(lvw);
                }
            }
        }

        private void TextBoxUpdate(object sender)
        {
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        private void tbxAuthor_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void tbxSubject_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void lvwTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvwTo.SelectedItems.Count > 0)
            {
                ListViewItem lvi = this.lvwTo.SelectedItems[0];
                this.tbxToName.Text = lvi.SubItems[0].Text;
            }
        }

        private void lvwCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.lvwCC.SelectedItems.Count > 0)
            {
                ListViewItem lvi = this.lvwCC.SelectedItems[0];
                this.tbxCCName.Text = lvi.SubItems[0].Text;
            }
        }

        private void btnAuthor_Click(object sender, EventArgs e)
        {
            Forms.fmrSearchContact frm = new Forms.fmrSearchContact();
            frm.Show();
            frm.FormClosing += new FormClosingEventHandler(Form_Closing);
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            Forms.fmrSearchContact frm = (Forms.fmrSearchContact)sender;
            if (frm.user != null)
            {
                this.tbxAuthor.Text = frm.user.FullName;
                if(DMS.IsOnline)
                    DMS.SetAuthor(frm.user.Login);
            }
        }
    }
}
