﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Tools = Microsoft.Office.Tools;
using System.Windows.Forms;
using System.Xml;

namespace Templates1.CustomTaskPanes
{
    internal class clsPaneBuilder: Tools.CustomTaskPane, IDisposable
    {
        private List<String> ControlNames { get; set; }
        private Word.Document Document { get; set; }
        private List<ControlObject> ControlObjects { get; set; }
        private ControlObject LastObject {get;set;}
        private Tools.CustomTaskPane Pane { get; set; }
        private Template Template { get; set; }
        
        
        internal clsPaneBuilder(Word.Document doc, Template template)
        {
            ControlNames = new List<string>();
            ControlObjects = new List<ControlObject>();
            Document = doc;
            Template = template;
        }

        

        
        internal Tools.CustomTaskPane CreatePane()
        {
            if (!Existing)
            {
                Pane = (clsPaneBuilder)Globals.ThisAddIn.GenerateGenericTaskpane(Template,Document);
                CustomTaskPanes.clsHelper help = new clsHelper();
                help.SetColour(Control);
                Width = 380;
                LastObject = null;
                foreach (Word.ContentControl ctl in Document.ContentControls)
                {
                    if (ControlNames.Select(c => c).Count() == 0)
                    {
                        Word.ContentControls controls = Document.SelectContentControlsByTag(ctl.Tag);
                        ControlNames.Add(ctl.Tag);
                        ControlObject cObj = new ControlObject(controls, this);
                        ControlObjects.Add(cObj);
                        cObj.Position(LastObject);
                        LastObject = cObj;
                    }
                }
                return Pane;
            }

            return GetExisting();
        }



        internal Boolean Existing
        {
            get
            {
                foreach (Tools.CustomTaskPane p in Globals.ThisAddIn.CustomTaskPanes)
                    if ((Word.Window)p.Window == Globals.ThisAddIn.Application.ActiveWindow)
                        return true;
                return false;
            }
        }

        internal Tools.CustomTaskPane GetExisting()
        {
            foreach (Tools.CustomTaskPane p in Globals.ThisAddIn.CustomTaskPanes)
                if ((Word.Window)p.Window == Globals.ThisAddIn.Application.ActiveWindow)
                    return p;
            return null;
        }

        public UserControl Control
        {
            get { return Pane.Control; }
        }

        public Microsoft.Office.Core.MsoCTPDockPosition DockPosition
        {
            get
            {
                return Pane.DockPosition;
            }
            set
            {
                Pane.DockPosition = value;
            }
        }

        public event EventHandler DockPositionChanged;

        public Microsoft.Office.Core.MsoCTPDockPositionRestrict DockPositionRestrict
        {
            get
            {
                return Pane.DockPositionRestrict;
            }
            set
            {
                Pane.DockPositionRestrict = value;
            }
        }

        public int Height
        {
            get
            {
                return Pane.Height;
            }
            set
            {
                Pane.Height = value;
            }
        }

        public string Title
        {
            get { return Pane.Title; }
        }

        public bool Visible
        {
            get
            {
                return Pane.Visible;
            }
            set
            {
                Pane.Visible = value;
            }
        }

        public event EventHandler VisibleChanged;

        public int Width
        {
            get
            {
                return Pane.Width;
            }
            set
            {
                Pane.Width = value;
            }
        }

        public object Window
        {
            get { return Pane.Window; }
        }

        public void Dispose()
        {
            Pane.Dispose();
        }
    }

    internal class ControlObject
    {
        internal enum ControlType {
            TextBox, 
            ComboBox, 
            ListBox, 
            CheckListBox, 
            DateTimePicker,
            IAContactPicker,
            ADContactPicker
        }
        
        internal Word.ContentControl WordControl { get; set; }
        internal UserControl FormControl { get; set; }
        internal ControlType CType { get; set; }
        internal String Source { get; set; }
        internal XmlDocument SourceXML { get; set; }
        internal List<String> ContentList { get; set; }
        internal Tools.CustomTaskPane Pane { get; set; }
        
        
        internal ControlObject(Word.ContentControls ctl, Tools.CustomTaskPane pane)
        {
            String tag = ctl[1].Tag;
            Pane = pane;
            String[] tagElements = tag.Split(new char[] {';'});
            ControlType ctype = (ControlType) Enum.Parse(typeof(ControlType),tagElements[1]);
            switch (ctype)
            {
                case (ControlType.TextBox):
                    {
                        FormControl = new CustomTaskPaneControls.CustomPaneTextBox(ctl);
                        break;
                    }
            }


                
        }
        internal ControlObject Position(ControlObject last)
        {
            this.Pane.Control.Controls.Add(this.FormControl);
            int ycoord;

            if (last == null)
                ycoord = 0;
            else
                ycoord = last.FormControl.Location.Y + last.FormControl.Size.Height + 6;

            this.FormControl.Location = new System.Drawing.Point(last.FormControl.Location.X, ycoord);
            return this;
        }
    }
}
