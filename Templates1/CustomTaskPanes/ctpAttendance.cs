﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpAttendance : UserControl
    {
        private WorkSite8Application DMS;
        public ctpAttendance()
        {
            InitializeComponent();
        }

        private void ctpAttendance_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Repopulates the task pane with data from the document
        /// </summary>
        
        public void RepopulateAgendaTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Imports data from the DMS about the folder in which the document is saved
        /// </summary>
        
        public void Fetch()
        {
            if (DMS == null)
                DMS = new WorkSite8Application(Globals.ThisAddIn.Application);
            if(DMS.IsOnline)
            { 
                FetchAuthor(DMS);
                this.tbxClient.Text = DMS.ClientDescription;
                this.tbxMatter.Text = DMS.MatterDescription;
                this.tbxMatterNo.Text = DMS.ClientMatterCode;
            }
            SetDate();
        }

        /// <summary>
        /// Sets the date in the document based on the information in the date/time picker control
        /// </summary>
        
        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            ctl.Range.Text = this.dateTimePicker.Text;
        }

        /// <summary>
        /// Imports the name of the author based on the data from the DMS by searching AD
        /// </summary>
        /// <param name="DMS"></param>
        
        public void FetchAuthor(WorkSite8Application DMS)
        {
            String author = DMS.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();

            this.tbxAuthor.Text = adu.FullName;
            this.tbxFeeEarner.Text = adu.FullName;
        }

        /// <summary>
        /// Updates the content control in the document with data from a text box
        /// </summary>
        /// <param name="sender"></param>
        
        private void TextBoxUpdate(object sender)
        {
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        /// <summary>
        /// Triggered when the client text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxClient_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the matter text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        
        /// <summary>
        /// Triggered when the matter text box is updated
        /// </summary>
        
        private void tbxMatter_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        
        /// <summary>
        /// Triggered when the author text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxAuthor_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the matter number text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void tbxMatterNo_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the fee earner text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxFeeEarner_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the date/time control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }
    }
}
