﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpLegal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDate = new System.Windows.Forms.CheckBox();
            this.dateTimePicker = new CustomTaskPanes.CustomDateTimePicker();
            this.tbxYear = new CustomTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxParty = new CustomTextBox();
            this.btnToRemove = new System.Windows.Forms.Button();
            this.btnToAdd = new System.Windows.Forms.Button();
            this.lvwTo = new System.Windows.Forms.ListView();
            this.btnUpdateTo = new System.Windows.Forms.Button();
            this.tbxTitle = new CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxRef = new CustomTextBox();
            this.tbxEmail = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkLev1 = new System.Windows.Forms.CheckBox();
            this.chkLev2 = new System.Windows.Forms.CheckBox();
            this.cboLocation = new CustomTaskPanes.CustomComboBox();
            this.pbxDivider = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.chkSchedules = new System.Windows.Forms.CheckBox();
            this.chkScheduleParts = new System.Windows.Forms.CheckBox();
            this.chkArticles = new System.Windows.Forms.CheckBox();
            this.chkAppendix = new System.Windows.Forms.CheckBox();
            this.chkArticleParts = new System.Windows.Forms.CheckBox();
            this.btnFetch = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnGoTo = new System.Windows.Forms.Button();
            this.chkCover = new System.Windows.Forms.CheckBox();
            this.chkTOC = new System.Windows.Forms.CheckBox();
            this.chkHead1 = new System.Windows.Forms.CheckBox();
            this.chkHead2 = new System.Windows.Forms.CheckBox();
            this.pbxDivider2 = new System.Windows.Forms.PictureBox();
            this.btnSchedule = new System.Windows.Forms.Button();
            this.pbTOC = new Templates1.Forms.NewProgressBar();
            this.btnAppendix = new System.Windows.Forms.Button();
            this.chkH1 = new System.Windows.Forms.CheckBox();
            this.chkH2 = new System.Windows.Forms.CheckBox();
            this.chkH3 = new System.Windows.Forms.CheckBox();
            this.pbCover = new Templates1.Forms.NewProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider2)).BeginInit();
            this.SuspendLayout();
            // 
            // chkDate
            // 
            this.chkDate.AutoSize = true;
            this.chkDate.Location = new System.Drawing.Point(21, 238);
            this.chkDate.MinimumSize = new System.Drawing.Size(80, 17);
            this.chkDate.Name = "chkDate";
            this.chkDate.Size = new System.Drawing.Size(80, 17);
            this.chkDate.TabIndex = 0;
            this.chkDate.Tag = "draft";
            this.chkDate.Text = "Draft date";
            this.chkDate.UseVisualStyleBackColor = true;
            this.chkDate.CheckedChanged += new System.EventHandler(this.chkDate_CheckedChanged);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "dd MMMM yyyy";
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.Location = new System.Drawing.Point(96, 239);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(215, 20);
            this.dateTimePicker.TabIndex = 1;
            this.dateTimePicker.Tag = "date";
            this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // tbxYear
            // 
            this.tbxYear.Location = new System.Drawing.Point(96, 45);
            this.tbxYear.Name = "tbxYear";
            this.tbxYear.Size = new System.Drawing.Size(73, 20);
            this.tbxYear.TabIndex = 2;
            this.tbxYear.Tag = "year";
            this.tbxYear.TextChanged += new System.EventHandler(this.tbxYear_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Year";
            // 
            // tbxParty
            // 
            this.tbxParty.Location = new System.Drawing.Point(96, 80);
            this.tbxParty.Name = "tbxParty";
            this.tbxParty.Size = new System.Drawing.Size(215, 20);
            this.tbxParty.TabIndex = 9;
            // 
            // btnToRemove
            // 
            this.btnToRemove.Location = new System.Drawing.Point(292, 124);
            this.btnToRemove.Name = "btnToRemove";
            this.btnToRemove.Size = new System.Drawing.Size(22, 19);
            this.btnToRemove.TabIndex = 68;
            this.btnToRemove.Text = "-";
            this.btnToRemove.UseVisualStyleBackColor = true;
            this.btnToRemove.Click += new System.EventHandler(this.btnToRemove_Click);
            // 
            // btnToAdd
            // 
            this.btnToAdd.Location = new System.Drawing.Point(292, 105);
            this.btnToAdd.Name = "btnToAdd";
            this.btnToAdd.Size = new System.Drawing.Size(22, 19);
            this.btnToAdd.TabIndex = 67;
            this.btnToAdd.Text = "+";
            this.btnToAdd.UseVisualStyleBackColor = true;
            this.btnToAdd.Click += new System.EventHandler(this.btnToAdd_Click);
            // 
            // lvwTo
            // 
            this.lvwTo.AutoArrange = false;
            this.lvwTo.Location = new System.Drawing.Point(96, 106);
            this.lvwTo.MultiSelect = false;
            this.lvwTo.Name = "lvwTo";
            this.lvwTo.Size = new System.Drawing.Size(186, 115);
            this.lvwTo.TabIndex = 69;
            this.lvwTo.UseCompatibleStateImageBehavior = false;
            this.lvwTo.SelectedIndexChanged += new System.EventHandler(this.lvwTo_SelectedIndexChanged);
            // 
            // btnUpdateTo
            // 
            this.btnUpdateTo.Location = new System.Drawing.Point(292, 143);
            this.btnUpdateTo.Name = "btnUpdateTo";
            this.btnUpdateTo.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateTo.TabIndex = 86;
            this.btnUpdateTo.Text = "!";
            this.btnUpdateTo.UseVisualStyleBackColor = true;
            this.btnUpdateTo.Click += new System.EventHandler(this.btnUpdateTo_Click);
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(96, 262);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(215, 20);
            this.tbxTitle.TabIndex = 87;
            this.tbxTitle.Tag = "title";
            this.tbxTitle.TextChanged += new System.EventHandler(this.tbxTitle_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 265);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 88;
            this.label2.Text = "Title";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 89;
            this.label3.Text = "Parties";
            // 
            // tbxRef
            // 
            this.tbxRef.Location = new System.Drawing.Point(96, 288);
            this.tbxRef.Name = "tbxRef";
            this.tbxRef.Size = new System.Drawing.Size(215, 20);
            this.tbxRef.TabIndex = 90;
            this.tbxRef.Tag = "reference";
            this.tbxRef.TextChanged += new System.EventHandler(this.tbxRef_TextChanged);
            // 
            // tbxEmail
            // 
            this.tbxEmail.Location = new System.Drawing.Point(96, 314);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(215, 20);
            this.tbxEmail.TabIndex = 91;
            this.tbxEmail.Tag = "mail";
            this.tbxEmail.TextChanged += new System.EventHandler(this.tbxEmail_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 291);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 92;
            this.label4.Text = "Reference";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 317);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 93;
            this.label5.Text = "Email";
            // 
            // chkLev1
            // 
            this.chkLev1.AutoSize = true;
            this.chkLev1.Checked = true;
            this.chkLev1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLev1.Location = new System.Drawing.Point(177, 498);
            this.chkLev1.Name = "chkLev1";
            this.chkLev1.Size = new System.Drawing.Size(61, 17);
            this.chkLev1.TabIndex = 95;
            this.chkLev1.Tag = "MDR LEVEL 1,1";
            this.chkLev1.Text = "Level 1";
            this.chkLev1.UseVisualStyleBackColor = true;
            this.chkLev1.CheckedChanged += new System.EventHandler(this.chkLev1_CheckedChanged);
            // 
            // chkLev2
            // 
            this.chkLev2.AutoSize = true;
            this.chkLev2.Checked = true;
            this.chkLev2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLev2.Location = new System.Drawing.Point(177, 517);
            this.chkLev2.Name = "chkLev2";
            this.chkLev2.Size = new System.Drawing.Size(61, 17);
            this.chkLev2.TabIndex = 96;
            this.chkLev2.Tag = "MDR LEVEL 2,3";
            this.chkLev2.Text = "Level 2";
            this.chkLev2.UseVisualStyleBackColor = true;
            this.chkLev2.CheckedChanged += new System.EventHandler(this.chkLev2_CheckedChanged);
            // 
            // cboLocation
            // 
            this.cboLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.Items.AddRange(new object[] {
            "Cursor position",
            "Page 1",
            "Page 2",
            "Page 3"});
            this.cboLocation.Location = new System.Drawing.Point(96, 449);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(186, 21);
            this.cboLocation.TabIndex = 97;
            this.cboLocation.SelectedIndexChanged += new System.EventHandler(this.cboLocation_SelectedIndexChanged);
            // 
            // pbxDivider
            // 
            this.pbxDivider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxDivider.Location = new System.Drawing.Point(29, 392);
            this.pbxDivider.Name = "pbxDivider";
            this.pbxDivider.Size = new System.Drawing.Size(293, 1);
            this.pbxDivider.TabIndex = 98;
            this.pbxDivider.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 454);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 101;
            this.label8.Text = "Location";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 497);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 102;
            this.label9.Text = "Include";
            // 
            // chkSchedules
            // 
            this.chkSchedules.AutoSize = true;
            this.chkSchedules.Checked = true;
            this.chkSchedules.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSchedules.Location = new System.Drawing.Point(96, 541);
            this.chkSchedules.Name = "chkSchedules";
            this.chkSchedules.Size = new System.Drawing.Size(76, 17);
            this.chkSchedules.TabIndex = 103;
            this.chkSchedules.Tag = "MDR SCHEDULE,2";
            this.chkSchedules.Text = "Schedules";
            this.chkSchedules.UseVisualStyleBackColor = true;
            this.chkSchedules.CheckedChanged += new System.EventHandler(this.chkSchedules_CheckedChanged);
            // 
            // chkScheduleParts
            // 
            this.chkScheduleParts.AutoSize = true;
            this.chkScheduleParts.Checked = true;
            this.chkScheduleParts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkScheduleParts.Location = new System.Drawing.Point(96, 564);
            this.chkScheduleParts.Name = "chkScheduleParts";
            this.chkScheduleParts.Size = new System.Drawing.Size(98, 17);
            this.chkScheduleParts.TabIndex = 104;
            this.chkScheduleParts.Tag = "MDR SCHEDULE PART,5";
            this.chkScheduleParts.Text = "Schedule Parts";
            this.chkScheduleParts.UseVisualStyleBackColor = true;
            this.chkScheduleParts.CheckedChanged += new System.EventHandler(this.chkScheduleParts_CheckedChanged);
            // 
            // chkArticles
            // 
            this.chkArticles.AutoSize = true;
            this.chkArticles.Location = new System.Drawing.Point(96, 587);
            this.chkArticles.Name = "chkArticles";
            this.chkArticles.Size = new System.Drawing.Size(103, 17);
            this.chkArticles.TabIndex = 105;
            this.chkArticles.Tag = "MDR ARTICLE,1";
            this.chkArticles.Text = "Article Headings";
            this.chkArticles.UseVisualStyleBackColor = true;
            this.chkArticles.CheckedChanged += new System.EventHandler(this.chkArticles_CheckedChanged);
            // 
            // chkAppendix
            // 
            this.chkAppendix.AutoSize = true;
            this.chkAppendix.Checked = true;
            this.chkAppendix.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAppendix.Location = new System.Drawing.Point(96, 632);
            this.chkAppendix.Name = "chkAppendix";
            this.chkAppendix.Size = new System.Drawing.Size(82, 17);
            this.chkAppendix.TabIndex = 106;
            this.chkAppendix.Tag = "MdR Appendix,6";
            this.chkAppendix.Text = "Appendices";
            this.chkAppendix.UseVisualStyleBackColor = true;
            this.chkAppendix.CheckedChanged += new System.EventHandler(this.chkAppendix_CheckedChanged);
            // 
            // chkArticleParts
            // 
            this.chkArticleParts.AutoSize = true;
            this.chkArticleParts.Location = new System.Drawing.Point(96, 610);
            this.chkArticleParts.Name = "chkArticleParts";
            this.chkArticleParts.Size = new System.Drawing.Size(82, 17);
            this.chkArticleParts.TabIndex = 107;
            this.chkArticleParts.Tag = "MDR ARTICLE PART,1";
            this.chkArticleParts.Text = "Article Parts";
            this.chkArticleParts.UseVisualStyleBackColor = true;
            this.chkArticleParts.CheckedChanged += new System.EventHandler(this.chkArticleParts_CheckedChanged);
            // 
            // btnFetch
            // 
            this.btnFetch.Location = new System.Drawing.Point(24, 340);
            this.btnFetch.Name = "btnFetch";
            this.btnFetch.Size = new System.Drawing.Size(97, 30);
            this.btnFetch.TabIndex = 108;
            this.btnFetch.Text = "Reset";
            this.btnFetch.UseVisualStyleBackColor = true;
            this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(12, 655);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(97, 30);
            this.btnUpdate.TabIndex = 109;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnGoTo
            // 
            this.btnGoTo.Location = new System.Drawing.Point(115, 655);
            this.btnGoTo.Name = "btnGoTo";
            this.btnGoTo.Size = new System.Drawing.Size(97, 30);
            this.btnGoTo.TabIndex = 110;
            this.btnGoTo.Text = "GoTo";
            this.btnGoTo.UseVisualStyleBackColor = true;
            this.btnGoTo.Click += new System.EventHandler(this.btnGoTo_Click);
            // 
            // chkCover
            // 
            this.chkCover.AutoSize = true;
            this.chkCover.Enabled = false;
            this.chkCover.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.chkCover.Location = new System.Drawing.Point(21, 22);
            this.chkCover.Name = "chkCover";
            this.chkCover.Size = new System.Drawing.Size(92, 17);
            this.chkCover.TabIndex = 128;
            this.chkCover.Text = "Cover Page";
            this.chkCover.UseVisualStyleBackColor = true;
            this.chkCover.CheckedChanged += new System.EventHandler(this.chkCover_CheckedChanged);
            // 
            // chkTOC
            // 
            this.chkTOC.AutoSize = true;
            this.chkTOC.BackColor = System.Drawing.Color.Transparent;
            this.chkTOC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTOC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.chkTOC.Location = new System.Drawing.Point(24, 415);
            this.chkTOC.Name = "chkTOC";
            this.chkTOC.Size = new System.Drawing.Size(127, 17);
            this.chkTOC.TabIndex = 129;
            this.chkTOC.Text = "Table of Contents";
            this.chkTOC.UseVisualStyleBackColor = false;
            this.chkTOC.CheckedChanged += new System.EventHandler(this.chkTOC_CheckedChanged);
            // 
            // chkHead1
            // 
            this.chkHead1.AutoSize = true;
            this.chkHead1.Checked = true;
            this.chkHead1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHead1.Location = new System.Drawing.Point(96, 497);
            this.chkHead1.Name = "chkHead1";
            this.chkHead1.Size = new System.Drawing.Size(75, 17);
            this.chkHead1.TabIndex = 133;
            this.chkHead1.Tag = "MDR HEADING 1,1";
            this.chkHead1.Text = "Heading 1";
            this.chkHead1.UseVisualStyleBackColor = true;
            this.chkHead1.CheckedChanged += new System.EventHandler(this.chkHead1_CheckedChanged);
            // 
            // chkHead2
            // 
            this.chkHead2.AutoSize = true;
            this.chkHead2.Checked = true;
            this.chkHead2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHead2.Location = new System.Drawing.Point(96, 518);
            this.chkHead2.Name = "chkHead2";
            this.chkHead2.Size = new System.Drawing.Size(75, 17);
            this.chkHead2.TabIndex = 134;
            this.chkHead2.Tag = "MDR HEADING 2,3";
            this.chkHead2.Text = "Heading 2";
            this.chkHead2.UseVisualStyleBackColor = true;
            this.chkHead2.CheckedChanged += new System.EventHandler(this.chkHead2_CheckedChanged);
            // 
            // pbxDivider2
            // 
            this.pbxDivider2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxDivider2.Location = new System.Drawing.Point(29, 704);
            this.pbxDivider2.Name = "pbxDivider2";
            this.pbxDivider2.Size = new System.Drawing.Size(293, 1);
            this.pbxDivider2.TabIndex = 135;
            this.pbxDivider2.TabStop = false;
            // 
            // btnSchedule
            // 
            this.btnSchedule.Enabled = false;
            this.btnSchedule.Location = new System.Drawing.Point(12, 719);
            this.btnSchedule.Name = "btnSchedule";
            this.btnSchedule.Size = new System.Drawing.Size(96, 31);
            this.btnSchedule.TabIndex = 138;
            this.btnSchedule.Text = "Schedule";
            this.btnSchedule.UseVisualStyleBackColor = true;
            this.btnSchedule.Click += new System.EventHandler(this.btnSchedule_Click);
            // 
            // pbTOC
            // 
            this.pbTOC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pbTOC.Location = new System.Drawing.Point(168, 417);
            this.pbTOC.Minimum = 1;
            this.pbTOC.Name = "pbTOC";
            this.pbTOC.Size = new System.Drawing.Size(113, 14);
            this.pbTOC.TabIndex = 136;
            this.pbTOC.Value = 1;
            // 
            // btnAppendix
            // 
            this.btnAppendix.Enabled = false;
            this.btnAppendix.Location = new System.Drawing.Point(118, 719);
            this.btnAppendix.Name = "btnAppendix";
            this.btnAppendix.Size = new System.Drawing.Size(93, 30);
            this.btnAppendix.TabIndex = 140;
            this.btnAppendix.Text = "Appendix";
            this.btnAppendix.UseVisualStyleBackColor = true;
            this.btnAppendix.Click += new System.EventHandler(this.btnAppendix_Click);
            // 
            // chkH1
            // 
            this.chkH1.AutoSize = true;
            this.chkH1.Checked = true;
            this.chkH1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkH1.Location = new System.Drawing.Point(251, 497);
            this.chkH1.Name = "chkH1";
            this.chkH1.Size = new System.Drawing.Size(40, 17);
            this.chkH1.TabIndex = 141;
            this.chkH1.Tag = "Heading 1,1";
            this.chkH1.Text = "H1";
            this.chkH1.UseVisualStyleBackColor = true;
            this.chkH1.CheckedChanged += new System.EventHandler(this.chkH1_CheckedChanged);
            // 
            // chkH2
            // 
            this.chkH2.AutoSize = true;
            this.chkH2.Checked = true;
            this.chkH2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkH2.Location = new System.Drawing.Point(251, 517);
            this.chkH2.Name = "chkH2";
            this.chkH2.Size = new System.Drawing.Size(40, 17);
            this.chkH2.TabIndex = 142;
            this.chkH2.Tag = "Heading 2,3";
            this.chkH2.Text = "H2";
            this.chkH2.UseVisualStyleBackColor = true;
            this.chkH2.CheckedChanged += new System.EventHandler(this.chkH2_CheckedChanged);
            // 
            // chkH3
            // 
            this.chkH3.AutoSize = true;
            this.chkH3.Location = new System.Drawing.Point(251, 537);
            this.chkH3.Name = "chkH3";
            this.chkH3.Size = new System.Drawing.Size(40, 17);
            this.chkH3.TabIndex = 143;
            this.chkH3.Tag = "Heading 3,3";
            this.chkH3.Text = "H3";
            this.chkH3.UseVisualStyleBackColor = true;
            this.chkH3.CheckedChanged += new System.EventHandler(this.chkH3_CheckedChanged);
            // 
            // pbCover
            // 
            this.pbCover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pbCover.Location = new System.Drawing.Point(118, 22);
            this.pbCover.Minimum = 1;
            this.pbCover.Name = "pbCover";
            this.pbCover.Size = new System.Drawing.Size(163, 14);
            this.pbCover.TabIndex = 144;
            this.pbCover.Value = 1;
            // 
            // ctpLegal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pbCover);
            this.Controls.Add(this.chkH3);
            this.Controls.Add(this.chkH2);
            this.Controls.Add(this.chkH1);
            this.Controls.Add(this.btnAppendix);
            this.Controls.Add(this.btnSchedule);
            this.Controls.Add(this.pbTOC);
            this.Controls.Add(this.pbxDivider2);
            this.Controls.Add(this.chkHead2);
            this.Controls.Add(this.chkHead1);
            this.Controls.Add(this.chkTOC);
            this.Controls.Add(this.chkCover);
            this.Controls.Add(this.btnGoTo);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnFetch);
            this.Controls.Add(this.chkArticleParts);
            this.Controls.Add(this.chkAppendix);
            this.Controls.Add(this.chkArticles);
            this.Controls.Add(this.chkScheduleParts);
            this.Controls.Add(this.chkSchedules);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pbxDivider);
            this.Controls.Add(this.cboLocation);
            this.Controls.Add(this.chkLev2);
            this.Controls.Add(this.chkLev1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxEmail);
            this.Controls.Add(this.tbxRef);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.btnUpdateTo);
            this.Controls.Add(this.lvwTo);
            this.Controls.Add(this.btnToRemove);
            this.Controls.Add(this.btnToAdd);
            this.Controls.Add(this.tbxParty);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxYear);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.chkDate);
            this.Name = "ctpLegal";
            this.Size = new System.Drawing.Size(345, 852);
            this.Load += new System.EventHandler(this.ctpLegal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDate;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker;
        private CustomTextBox tbxYear;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxParty;
        private System.Windows.Forms.Button btnToRemove;
        private System.Windows.Forms.Button btnToAdd;
        private System.Windows.Forms.ListView lvwTo;
        private System.Windows.Forms.Button btnUpdateTo;
        private CustomTextBox tbxTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxRef;
        private CustomTextBox tbxEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkLev1;
        private System.Windows.Forms.CheckBox chkLev2;
        private CustomTaskPanes.CustomComboBox cboLocation;
        private System.Windows.Forms.PictureBox pbxDivider;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkSchedules;
        private System.Windows.Forms.CheckBox chkScheduleParts;
        private System.Windows.Forms.CheckBox chkArticles;
        private System.Windows.Forms.CheckBox chkAppendix;
        private System.Windows.Forms.CheckBox chkArticleParts;
        private System.Windows.Forms.Button btnFetch;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnGoTo;
        private System.Windows.Forms.CheckBox chkCover;
        private System.Windows.Forms.CheckBox chkTOC;
        private System.Windows.Forms.CheckBox chkHead1;
        private System.Windows.Forms.CheckBox chkHead2;
        private System.Windows.Forms.PictureBox pbxDivider2;
        private Forms.NewProgressBar pbTOC;
        private System.Windows.Forms.Button btnSchedule;
        private System.Windows.Forms.Button btnAppendix;
        private System.Windows.Forms.CheckBox chkH1;
        private System.Windows.Forms.CheckBox chkH2;
        private System.Windows.Forms.CheckBox chkH3;
        private Forms.NewProgressBar pbCover;
    }
}
