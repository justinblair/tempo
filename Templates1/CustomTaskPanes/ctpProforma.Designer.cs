﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpProforma
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkIndividual = new System.Windows.Forms.CheckBox();
            this.cboTitle = new CustomTaskPanes.CustomComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxForename = new CustomTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxSurname = new CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxSuffix = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboAddressee = new CustomTaskPanes.CustomComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnLookup = new System.Windows.Forms.Button();
            this.tbxCompany = new CustomTextBox();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxFileNumber = new CustomTextBox();
            this.tbxMatter = new CustomTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new CustomTaskPanes.CustomDateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbxYourRef = new CustomTextBox();
            this.tbxOurRef = new CustomTextBox();
            this.chkAddAddress = new System.Windows.Forms.CheckBox();
            this.tbxExtraAddress = new CustomTaskPanes.CustomRichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbxPayableBy = new CustomTextBox();
            this.tbxDescription = new CustomTextBox();
            this.tbxNet = new CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lvwServices = new System.Windows.Forms.ListView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkIndividual
            // 
            this.chkIndividual.AutoSize = true;
            this.chkIndividual.Location = new System.Drawing.Point(6, 246);
            this.chkIndividual.Name = "chkIndividual";
            this.chkIndividual.Size = new System.Drawing.Size(92, 17);
            this.chkIndividual.TabIndex = 0;
            this.chkIndividual.Text = "Company only";
            this.chkIndividual.UseVisualStyleBackColor = true;
            this.chkIndividual.CheckedChanged += new System.EventHandler(this.chkIndividual_CheckedChanged);
            // 
            // cboTitle
            // 
            this.cboTitle.FormattingEnabled = true;
            this.cboTitle.Location = new System.Drawing.Point(101, 111);
            this.cboTitle.Name = "cboTitle";
            this.cboTitle.Size = new System.Drawing.Size(143, 21);
            this.cboTitle.TabIndex = 1;
            this.cboTitle.SelectedIndexChanged += new System.EventHandler(this.cboTitle_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Title";
            // 
            // tbxForename
            // 
            this.tbxForename.Location = new System.Drawing.Point(101, 139);
            this.tbxForename.Name = "tbxForename";
            this.tbxForename.Size = new System.Drawing.Size(217, 20);
            this.tbxForename.TabIndex = 3;
            this.tbxForename.TextChanged += new System.EventHandler(this.tbxForename_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 144);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Forename";
            // 
            // tbxSurname
            // 
            this.tbxSurname.Location = new System.Drawing.Point(101, 165);
            this.tbxSurname.Name = "tbxSurname";
            this.tbxSurname.Size = new System.Drawing.Size(217, 20);
            this.tbxSurname.TabIndex = 5;
            this.tbxSurname.TextChanged += new System.EventHandler(this.tbxSurname_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Surname";
            // 
            // tbxSuffix
            // 
            this.tbxSuffix.Location = new System.Drawing.Point(101, 191);
            this.tbxSuffix.Name = "tbxSuffix";
            this.tbxSuffix.Size = new System.Drawing.Size(141, 20);
            this.tbxSuffix.TabIndex = 7;
            this.tbxSuffix.TextChanged += new System.EventHandler(this.tbxSuffix_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 194);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Suffix";
            // 
            // cboAddressee
            // 
            this.cboAddressee.FormattingEnabled = true;
            this.cboAddressee.Location = new System.Drawing.Point(101, 217);
            this.cboAddressee.Name = "cboAddressee";
            this.cboAddressee.Size = new System.Drawing.Size(219, 21);
            this.cboAddressee.TabIndex = 9;
            this.cboAddressee.Tag = "addressee";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Addressee";
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(297, 190);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(21, 21);
            this.btnLookup.TabIndex = 29;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // tbxCompany
            // 
            this.tbxCompany.Location = new System.Drawing.Point(101, 244);
            this.tbxCompany.Name = "tbxCompany";
            this.tbxCompany.Size = new System.Drawing.Size(217, 20);
            this.tbxCompany.TabIndex = 30;
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(101, 273);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(215, 88);
            this.tbxAddress.TabIndex = 32;
            this.tbxAddress.Tag = "address";
            this.tbxAddress.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 276);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Address";
            // 
            // tbxFileNumber
            // 
            this.tbxFileNumber.Location = new System.Drawing.Point(101, 31);
            this.tbxFileNumber.Name = "tbxFileNumber";
            this.tbxFileNumber.Size = new System.Drawing.Size(219, 20);
            this.tbxFileNumber.TabIndex = 34;
            this.tbxFileNumber.Tag = "FileNo";
            // 
            // tbxMatter
            // 
            this.tbxMatter.Location = new System.Drawing.Point(101, 522);
            this.tbxMatter.Name = "tbxMatter";
            this.tbxMatter.Size = new System.Drawing.Size(215, 20);
            this.tbxMatter.TabIndex = 35;
            this.tbxMatter.Tag = "matter";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "File number";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 525);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 37;
            this.label9.Text = "Matter";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(101, 57);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(219, 20);
            this.dateTimePicker1.TabIndex = 38;
            this.dateTimePicker1.Tag = "date";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 500);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 54;
            this.label11.Text = "Your reference";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 474);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 53;
            this.label12.Text = "Our reference";
            // 
            // tbxYourRef
            // 
            this.tbxYourRef.Location = new System.Drawing.Point(101, 496);
            this.tbxYourRef.Name = "tbxYourRef";
            this.tbxYourRef.Size = new System.Drawing.Size(215, 20);
            this.tbxYourRef.TabIndex = 52;
            this.tbxYourRef.Tag = "yourRef";
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(101, 470);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(215, 20);
            this.tbxOurRef.TabIndex = 51;
            this.tbxOurRef.Tag = "ourRef";
            // 
            // chkAddAddress
            // 
            this.chkAddAddress.AutoSize = true;
            this.chkAddAddress.Location = new System.Drawing.Point(6, 395);
            this.chkAddAddress.Name = "chkAddAddress";
            this.chkAddAddress.Size = new System.Drawing.Size(85, 17);
            this.chkAddAddress.TabIndex = 50;
            this.chkAddAddress.Text = "Add address";
            this.chkAddAddress.UseVisualStyleBackColor = true;
            this.chkAddAddress.CheckedChanged += new System.EventHandler(this.chkAddAddress_CheckedChanged);
            // 
            // tbxExtraAddress
            // 
            this.tbxExtraAddress.Location = new System.Drawing.Point(101, 393);
            this.tbxExtraAddress.Name = "tbxExtraAddress";
            this.tbxExtraAddress.Size = new System.Drawing.Size(215, 71);
            this.tbxExtraAddress.TabIndex = 49;
            this.tbxExtraAddress.Tag = "addAddressDetails";
            this.tbxExtraAddress.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 370);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = "Payable by";
            // 
            // tbxPayableBy
            // 
            this.tbxPayableBy.Location = new System.Drawing.Point(101, 367);
            this.tbxPayableBy.Name = "tbxPayableBy";
            this.tbxPayableBy.Size = new System.Drawing.Size(215, 20);
            this.tbxPayableBy.TabIndex = 47;
            this.tbxPayableBy.Tag = "payableBy";
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(101, 563);
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.Size = new System.Drawing.Size(177, 20);
            this.tbxDescription.TabIndex = 55;
            // 
            // tbxNet
            // 
            this.tbxNet.Location = new System.Drawing.Point(101, 589);
            this.tbxNet.Name = "tbxNet";
            this.tbxNet.Size = new System.Drawing.Size(177, 20);
            this.tbxNet.TabIndex = 56;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 566);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Description";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 592);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 58;
            this.label14.Text = "Net";
            // 
            // lvwServices
            // 
            this.lvwServices.FullRowSelect = true;
            this.lvwServices.GridLines = true;
            this.lvwServices.Location = new System.Drawing.Point(10, 629);
            this.lvwServices.MultiSelect = false;
            this.lvwServices.Name = "lvwServices";
            this.lvwServices.Size = new System.Drawing.Size(302, 111);
            this.lvwServices.TabIndex = 59;
            this.lvwServices.UseCompatibleStateImageBehavior = false;
            this.lvwServices.View = System.Windows.Forms.View.Details;
            this.lvwServices.SelectedIndexChanged += new System.EventHandler(this.lvwServices_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(297, 563);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(21, 21);
            this.btnAdd.TabIndex = 60;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(297, 584);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(21, 21);
            this.btnRemove.TabIndex = 61;
            this.btnRemove.Text = "-";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(297, 605);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(21, 21);
            this.btnUpdate.TabIndex = 62;
            this.btnUpdate.Text = "!";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // ctpProforma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lvwServices);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxNet);
            this.Controls.Add(this.tbxDescription);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxYourRef);
            this.Controls.Add(this.tbxOurRef);
            this.Controls.Add(this.chkAddAddress);
            this.Controls.Add(this.tbxExtraAddress);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbxPayableBy);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxMatter);
            this.Controls.Add(this.tbxFileNumber);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.tbxCompany);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboAddressee);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxSuffix);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxSurname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxForename);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboTitle);
            this.Controls.Add(this.chkIndividual);
            this.Name = "ctpProforma";
            this.Size = new System.Drawing.Size(340, 845);
            this.Load += new System.EventHandler(this.ctpProforma_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkIndividual;
        private CustomTaskPanes.CustomComboBox cboTitle;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxForename;
        private System.Windows.Forms.Label label2;
        private CustomTextBox tbxSurname;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxSuffix;
        private System.Windows.Forms.Label label4;
        private CustomTaskPanes.CustomComboBox cboAddressee;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnLookup;
        private CustomTextBox tbxCompany;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private System.Windows.Forms.Label label7;
        private CustomTextBox tbxFileNumber;
        private CustomTextBox tbxMatter;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private CustomTextBox tbxYourRef;
        private CustomTextBox tbxOurRef;
        private System.Windows.Forms.CheckBox chkAddAddress;
        private CustomTaskPanes.CustomRichTextBox tbxExtraAddress;
        private System.Windows.Forms.Label label13;
        private CustomTextBox tbxPayableBy;
        private CustomTextBox tbxDescription;
        private CustomTextBox tbxNet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListView lvwServices;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnUpdate;
    }
}
