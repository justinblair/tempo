﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpInvoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkCredit = new System.Windows.Forms.CheckBox();
            this.tbxFileNumber = new CustomTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker = new CustomTaskPanes.CustomDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxAddress = new CustomTaskPanes.CustomRichTextBox();
            this.btnLookup = new System.Windows.Forms.Button();
            this.cboRecipient = new CustomTaskPanes.CustomComboBox();
            this.cboTitle = new CustomTaskPanes.CustomComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxForename = new CustomTextBox();
            this.tbxSurname = new CustomTextBox();
            this.tbxSuffix = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxPayableBy = new CustomTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxExtraAddress = new CustomTaskPanes.CustomRichTextBox();
            this.chkAddAddress = new System.Windows.Forms.CheckBox();
            this.tbxOurRef = new CustomTextBox();
            this.tbxYourRef = new CustomTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.chkMonies = new System.Windows.Forms.CheckBox();
            this.tbxMonies = new CustomTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbxMatter = new CustomTextBox();
            this.lvwServices = new System.Windows.Forms.ListView();
            this.tbxDescription = new CustomTextBox();
            this.tbxNet = new CustomTextBox();
            this.cboType = new CustomTaskPanes.CustomComboBox();
            this.cboVat = new CustomTaskPanes.CustomComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tbxCompany = new CustomTextBox();
            this.chkIndividual = new System.Windows.Forms.CheckBox();
            this.lvwDisbursements = new System.Windows.Forms.ListView();
            this.lvwNonVAT = new System.Windows.Forms.ListView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chkCredit
            // 
            this.chkCredit.AutoSize = true;
            this.chkCredit.Location = new System.Drawing.Point(15, 22);
            this.chkCredit.Name = "chkCredit";
            this.chkCredit.Size = new System.Drawing.Size(121, 17);
            this.chkCredit.TabIndex = 0;
            this.chkCredit.Text = "Is this a credit note?";
            this.chkCredit.UseVisualStyleBackColor = true;
            this.chkCredit.CheckedChanged += new System.EventHandler(this.chkCredit_CheckedChanged);
            // 
            // tbxFileNumber
            // 
            this.tbxFileNumber.Location = new System.Drawing.Point(107, 45);
            this.tbxFileNumber.Name = "tbxFileNumber";
            this.tbxFileNumber.Size = new System.Drawing.Size(215, 20);
            this.tbxFileNumber.TabIndex = 10;
            this.tbxFileNumber.Tag = "FileNo";
            this.tbxFileNumber.TextChanged += new System.EventHandler(this.tbxFileNumber_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "File number";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "dd/MM/yy";
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.Location = new System.Drawing.Point(107, 71);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(215, 20);
            this.dateTimePicker.TabIndex = 12;
            this.dateTimePicker.Tag = "date";
            this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Addressee";
            // 
            // tbxAddress
            // 
            this.tbxAddress.Location = new System.Drawing.Point(107, 246);
            this.tbxAddress.Name = "tbxAddress";
            this.tbxAddress.Size = new System.Drawing.Size(215, 71);
            this.tbxAddress.TabIndex = 27;
            this.tbxAddress.Tag = "address";
            this.tbxAddress.Text = "";
            // 
            // btnLookup
            // 
            this.btnLookup.Location = new System.Drawing.Point(300, 169);
            this.btnLookup.Name = "btnLookup";
            this.btnLookup.Size = new System.Drawing.Size(21, 21);
            this.btnLookup.TabIndex = 28;
            this.btnLookup.Text = "...";
            this.btnLookup.UseVisualStyleBackColor = true;
            this.btnLookup.Click += new System.EventHandler(this.btnLookup_Click);
            // 
            // cboRecipient
            // 
            this.cboRecipient.FormattingEnabled = true;
            this.cboRecipient.Location = new System.Drawing.Point(107, 194);
            this.cboRecipient.Name = "cboRecipient";
            this.cboRecipient.Size = new System.Drawing.Size(212, 21);
            this.cboRecipient.TabIndex = 26;
            this.cboRecipient.Tag = "addressee";
            this.cboRecipient.SelectedIndexChanged += new System.EventHandler(this.cboRecipient_SelectedIndexChanged);
            // 
            // cboTitle
            // 
            this.cboTitle.FormattingEnabled = true;
            this.cboTitle.Location = new System.Drawing.Point(107, 97);
            this.cboTitle.Name = "cboTitle";
            this.cboTitle.Size = new System.Drawing.Size(102, 21);
            this.cboTitle.TabIndex = 30;
            this.cboTitle.Tag = "";
            this.cboTitle.SelectedIndexChanged += new System.EventHandler(this.cboTitle_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Title";
            // 
            // tbxForename
            // 
            this.tbxForename.Location = new System.Drawing.Point(107, 122);
            this.tbxForename.Name = "tbxForename";
            this.tbxForename.Size = new System.Drawing.Size(215, 20);
            this.tbxForename.TabIndex = 32;
            this.tbxForename.Tag = "";
            this.tbxForename.TextChanged += new System.EventHandler(this.tbxForename_TextChanged);
            // 
            // tbxSurname
            // 
            this.tbxSurname.Location = new System.Drawing.Point(107, 146);
            this.tbxSurname.Name = "tbxSurname";
            this.tbxSurname.Size = new System.Drawing.Size(215, 20);
            this.tbxSurname.TabIndex = 33;
            this.tbxSurname.Tag = "";
            this.tbxSurname.TextChanged += new System.EventHandler(this.tbxSurname_TextChanged);
            // 
            // tbxSuffix
            // 
            this.tbxSuffix.Location = new System.Drawing.Point(107, 170);
            this.tbxSuffix.Name = "tbxSuffix";
            this.tbxSuffix.Size = new System.Drawing.Size(102, 20);
            this.tbxSuffix.TabIndex = 34;
            this.tbxSuffix.Tag = "";
            this.tbxSuffix.TextChanged += new System.EventHandler(this.tbxSuffix_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 35;
            this.label4.Text = "Forename";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Surname";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 177);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "Suffix";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 249);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Address";
            // 
            // tbxPayableBy
            // 
            this.tbxPayableBy.Location = new System.Drawing.Point(107, 321);
            this.tbxPayableBy.Name = "tbxPayableBy";
            this.tbxPayableBy.Size = new System.Drawing.Size(215, 20);
            this.tbxPayableBy.TabIndex = 39;
            this.tbxPayableBy.Tag = "payableBy";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 324);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Payable by";
            // 
            // tbxExtraAddress
            // 
            this.tbxExtraAddress.Location = new System.Drawing.Point(107, 347);
            this.tbxExtraAddress.Name = "tbxExtraAddress";
            this.tbxExtraAddress.Size = new System.Drawing.Size(215, 71);
            this.tbxExtraAddress.TabIndex = 41;
            this.tbxExtraAddress.Tag = "addAddressDetails";
            this.tbxExtraAddress.Text = "";
            // 
            // chkAddAddress
            // 
            this.chkAddAddress.AutoSize = true;
            this.chkAddAddress.Location = new System.Drawing.Point(15, 349);
            this.chkAddAddress.Name = "chkAddAddress";
            this.chkAddAddress.Size = new System.Drawing.Size(85, 17);
            this.chkAddAddress.TabIndex = 42;
            this.chkAddAddress.Text = "Add address";
            this.chkAddAddress.UseVisualStyleBackColor = true;
            this.chkAddAddress.CheckedChanged += new System.EventHandler(this.chkAddAddress_CheckedChanged);
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(107, 424);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(215, 20);
            this.tbxOurRef.TabIndex = 43;
            this.tbxOurRef.Tag = "ourRef";
            // 
            // tbxYourRef
            // 
            this.tbxYourRef.Location = new System.Drawing.Point(107, 450);
            this.tbxYourRef.Name = "tbxYourRef";
            this.tbxYourRef.Size = new System.Drawing.Size(215, 20);
            this.tbxYourRef.TabIndex = 44;
            this.tbxYourRef.Tag = "yourRef";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 428);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 13);
            this.label10.TabIndex = 45;
            this.label10.Text = "Our reference";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 454);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 46;
            this.label11.Text = "Your reference";
            // 
            // chkMonies
            // 
            this.chkMonies.AutoSize = true;
            this.chkMonies.Location = new System.Drawing.Point(18, 510);
            this.chkMonies.Name = "chkMonies";
            this.chkMonies.Size = new System.Drawing.Size(140, 17);
            this.chkMonies.TabIndex = 47;
            this.chkMonies.Text = "Monies held on account";
            this.chkMonies.UseVisualStyleBackColor = true;
            this.chkMonies.CheckedChanged += new System.EventHandler(this.chkMonies_CheckedChanged);
            // 
            // tbxMonies
            // 
            this.tbxMonies.Location = new System.Drawing.Point(170, 507);
            this.tbxMonies.Name = "tbxMonies";
            this.tbxMonies.Size = new System.Drawing.Size(149, 20);
            this.tbxMonies.TabIndex = 48;
            this.tbxMonies.Tag = "";
            this.tbxMonies.TextChanged += new System.EventHandler(this.tbxMonies_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 480);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 50;
            this.label12.Text = "Matter";
            // 
            // tbxMatter
            // 
            this.tbxMatter.Location = new System.Drawing.Point(107, 476);
            this.tbxMatter.Name = "tbxMatter";
            this.tbxMatter.Size = new System.Drawing.Size(215, 20);
            this.tbxMatter.TabIndex = 49;
            this.tbxMatter.Tag = "matter";
            // 
            // lvwServices
            // 
            this.lvwServices.FullRowSelect = true;
            this.lvwServices.GridLines = true;
            this.lvwServices.Location = new System.Drawing.Point(16, 634);
            this.lvwServices.Name = "lvwServices";
            this.lvwServices.Size = new System.Drawing.Size(306, 75);
            this.lvwServices.TabIndex = 51;
            this.lvwServices.UseCompatibleStateImageBehavior = false;
            this.lvwServices.View = System.Windows.Forms.View.Details;
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(104, 555);
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.Size = new System.Drawing.Size(179, 20);
            this.tbxDescription.TabIndex = 52;
            this.tbxDescription.Tag = "";
            // 
            // tbxNet
            // 
            this.tbxNet.Location = new System.Drawing.Point(104, 581);
            this.tbxNet.Name = "tbxNet";
            this.tbxNet.Size = new System.Drawing.Size(179, 20);
            this.tbxNet.TabIndex = 53;
            this.tbxNet.Tag = "";
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(104, 607);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(105, 21);
            this.cboType.TabIndex = 54;
            this.cboType.Tag = "";
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // cboVat
            // 
            this.cboVat.FormattingEnabled = true;
            this.cboVat.Location = new System.Drawing.Point(215, 607);
            this.cboVat.Name = "cboVat";
            this.cboVat.Size = new System.Drawing.Size(68, 21);
            this.cboVat.TabIndex = 55;
            this.cboVat.Tag = "";
            this.cboVat.SelectedIndexChanged += new System.EventHandler(this.cboVat_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 558);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "Description";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 588);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 13);
            this.label14.TabIndex = 57;
            this.label14.Text = "Net";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 615);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 58;
            this.label15.Text = "Type/VAT rate";
            // 
            // tbxCompany
            // 
            this.tbxCompany.Location = new System.Drawing.Point(107, 220);
            this.tbxCompany.Name = "tbxCompany";
            this.tbxCompany.Size = new System.Drawing.Size(215, 20);
            this.tbxCompany.TabIndex = 59;
            this.tbxCompany.Tag = "";
            // 
            // chkIndividual
            // 
            this.chkIndividual.AutoSize = true;
            this.chkIndividual.Location = new System.Drawing.Point(13, 223);
            this.chkIndividual.Name = "chkIndividual";
            this.chkIndividual.Size = new System.Drawing.Size(92, 17);
            this.chkIndividual.TabIndex = 60;
            this.chkIndividual.Text = "Company only";
            this.chkIndividual.UseVisualStyleBackColor = true;
            this.chkIndividual.CheckedChanged += new System.EventHandler(this.chkIndividual_CheckedChanged);
            // 
            // lvwDisbursements
            // 
            this.lvwDisbursements.FullRowSelect = true;
            this.lvwDisbursements.GridLines = true;
            this.lvwDisbursements.Location = new System.Drawing.Point(16, 710);
            this.lvwDisbursements.Name = "lvwDisbursements";
            this.lvwDisbursements.Size = new System.Drawing.Size(306, 60);
            this.lvwDisbursements.TabIndex = 61;
            this.lvwDisbursements.UseCompatibleStateImageBehavior = false;
            this.lvwDisbursements.View = System.Windows.Forms.View.Details;
            // 
            // lvwNonVAT
            // 
            this.lvwNonVAT.FullRowSelect = true;
            this.lvwNonVAT.GridLines = true;
            this.lvwNonVAT.Location = new System.Drawing.Point(16, 771);
            this.lvwNonVAT.Name = "lvwNonVAT";
            this.lvwNonVAT.Size = new System.Drawing.Size(306, 60);
            this.lvwNonVAT.TabIndex = 62;
            this.lvwNonVAT.UseCompatibleStateImageBehavior = false;
            this.lvwNonVAT.View = System.Windows.Forms.View.Details;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(289, 554);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(21, 21);
            this.btnAdd.TabIndex = 63;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(289, 580);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(21, 21);
            this.btnRemove.TabIndex = 64;
            this.btnRemove.Text = "-";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(289, 607);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(21, 21);
            this.btnUpdate.TabIndex = 65;
            this.btnUpdate.Text = "!";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // ctpInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lvwNonVAT);
            this.Controls.Add(this.lvwDisbursements);
            this.Controls.Add(this.chkIndividual);
            this.Controls.Add(this.tbxCompany);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cboVat);
            this.Controls.Add(this.cboType);
            this.Controls.Add(this.tbxNet);
            this.Controls.Add(this.tbxDescription);
            this.Controls.Add(this.lvwServices);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxMatter);
            this.Controls.Add(this.tbxMonies);
            this.Controls.Add(this.chkMonies);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxYourRef);
            this.Controls.Add(this.tbxOurRef);
            this.Controls.Add(this.chkAddAddress);
            this.Controls.Add(this.tbxExtraAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxPayableBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxSuffix);
            this.Controls.Add(this.tbxSurname);
            this.Controls.Add(this.tbxForename);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboTitle);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxAddress);
            this.Controls.Add(this.btnLookup);
            this.Controls.Add(this.cboRecipient);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxFileNumber);
            this.Controls.Add(this.chkCredit);
            this.Name = "ctpInvoice";
            this.Size = new System.Drawing.Size(345, 840);
            this.Load += new System.EventHandler(this.ctpInvoice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkCredit;
        private CustomTextBox tbxFileNumber;
        private System.Windows.Forms.Label label1;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private CustomTaskPanes.CustomRichTextBox tbxAddress;
        private System.Windows.Forms.Button btnLookup;
        private CustomTaskPanes.CustomComboBox cboRecipient;
        private CustomTaskPanes.CustomComboBox cboTitle;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxForename;
        private CustomTextBox tbxSurname;
        private CustomTextBox tbxSuffix;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private CustomTextBox tbxPayableBy;
        private System.Windows.Forms.Label label9;
        private CustomTaskPanes.CustomRichTextBox tbxExtraAddress;
        private System.Windows.Forms.CheckBox chkAddAddress;
        private CustomTextBox tbxOurRef;
        private CustomTextBox tbxYourRef;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkMonies;
        private CustomTextBox tbxMonies;
        private System.Windows.Forms.Label label12;
        private CustomTextBox tbxMatter;
        private System.Windows.Forms.ListView lvwServices;
        private CustomTextBox tbxDescription;
        private CustomTextBox tbxNet;
        private CustomTaskPanes.CustomComboBox cboType;
        private CustomTaskPanes.CustomComboBox cboVat;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private CustomTextBox tbxCompany;
        private System.Windows.Forms.CheckBox chkIndividual;
        private System.Windows.Forms.ListView lvwDisbursements;
        private System.Windows.Forms.ListView lvwNonVAT;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnUpdate;
    }
}
