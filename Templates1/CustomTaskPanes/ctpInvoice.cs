﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    public partial class ctpInvoice : UserControl
    {
        private WorkSite8Application DMS;
        private XmlDocument XmlDoc;
        private XmlDocument XmlSave;
        private clsInterAction IA;
        private Boolean bNoUpdating;
        private int VATSelected = 0;
        private Word.Table servicesTable;
        private Word.Table disbursementsTable;
        private Word.Table nonVATTable;
        private Word.Table VATTable;
        private ListView lastListView;


        public ctpInvoice()
         
        {
            InitializeComponent();
        }

        private void ctpInvoice_Load(object sender, EventArgs e)
        {
            XmlDoc = new XmlDocument();
            CustomTaskPanes.clsHelper helper = new CustomTaskPanes.clsHelper();
            XmlDoc.Load(helper.Params);

            foreach (XmlNode nodNode in XmlDoc.SelectNodes("//title/param"))
                this.cboTitle.Items.Add(nodNode.InnerText);
            foreach (XmlNode nodNode in XmlDoc.SelectNodes("//invoicetype/param"))
                this.cboType.Items.Add(nodNode.InnerText);
            foreach (XmlNode nodNode in XmlDoc.SelectNodes("//vat/param"))
                this.cboVat.Items.Add(nodNode.InnerText);

            foreach(Control ctl in this.Controls)
                if(ctl.Tag!=null)
                    ctl.TextChanged += new EventHandler(ctl_TextChanged);

            SetCreditInvoice();
            SetDate();
            SetExtraAddressEnabled();
            SetParagraphSpacingOnPBandFAO();
            SetMonies();
            SetLVHeadings();
            servicesTable = SetFirstTable();
            lvwServices.SelectedIndexChanged += new EventHandler(lvw_SelectedIndexChanged);
            lvwDisbursements.SelectedIndexChanged += new EventHandler(lvw_SelectedIndexChanged);
            lvwNonVAT.SelectedIndexChanged += new EventHandler(lvw_SelectedIndexChanged);
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnLookup.Enabled = online;
            chkCredit.Width=140;
        }

        private void BuildSaveTree()
        {
            if (bNoUpdating)
                return;
            XmlSave = new XmlDocument();
            XmlNode topItem = XmlSave.CreateElement(string.Empty, "savings", string.Empty);
            XmlSave.AppendChild(topItem);
            foreach(Control ctl in this.Controls)
            {
                XmlNode node = XmlSave.CreateElement(string.Empty, ctl.Name, string.Empty);
                topItem.AppendChild(node);
                XmlAttribute att = XmlSave.CreateAttribute("type");
                att.InnerText = ctl.GetType().ToString();
                node.Attributes.Append(att);

                if(ctl.GetType().ToString().ToLower().Contains("checkbox"))
                {
                    CheckBox chk = (CheckBox)ctl;
                    if (chk.Checked)
                        node.InnerText = "-1";
                    else
                        node.InnerText = "0";
                }

                if(ctl.GetType().ToString().ToLower().Contains("forms.textbox"))
                { 
                    TextBox tbx = (TextBox)ctl;
                    node.InnerText = tbx.Text;
                }

                if (ctl.GetType().ToString().ToLower().Contains("richtextbox"))
                {
                    RichTextBox tbx = (RichTextBox)ctl;
                    node.InnerText = tbx.Text;
                }

                if (ctl.GetType().ToString().ToLower().Contains("datetime"))
                {
                    DateTimePicker tbx = (DateTimePicker)ctl;
                    node.InnerText = tbx.Text;
                }

                if(ctl.GetType().ToString().ToLower().Contains("listview"))
                {
                    ListView lvw = (ListView)ctl;
                    foreach(ListViewItem lvi in lvw.Items)
                    {
                        XmlNode subnode = XmlSave.CreateElement(string.Empty, "subnode", string.Empty);
                        node.AppendChild(subnode);
                        XmlNode column1 = XmlSave.CreateElement(string.Empty, "column1", string.Empty);
                        column1.InnerText = lvi.SubItems[0].Text;
                        subnode.AppendChild(column1);
                        XmlNode column2 = XmlSave.CreateElement(string.Empty, "column2", string.Empty);
                        column2.InnerText = lvi.SubItems[1].Text;
                        subnode.AppendChild(column2);
                        XmlNode column3 = XmlSave.CreateElement(string.Empty, "column3", string.Empty);
                        column3.InnerText = lvi.SubItems[2].Text;
                        subnode.AppendChild(column3);
                        XmlNode column4 = XmlSave.CreateElement(string.Empty, "column4", string.Empty);
                        column4.InnerText = lvi.SubItems[3].Text;
                        subnode.AppendChild(column4);


                    }
                }

                if (ctl.GetType().ToString().ToLower().Contains("combobox"))
                {
                    ComboBox cbo = (ComboBox)ctl;
                    node.InnerText = cbo.Text;
                }

                try
                { 
                    Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["invoiceDetails"];
                    vbl.Value = XmlSave.OuterXml;
                }
                catch
                {
                    Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("invoiceDetails", XmlSave.OuterXml);
                }
            }
        }

        public void RepopulateInvoiceTaskPane()
        {
            bNoUpdating = true;
            try
            {
                Word.Variable vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["invoiceDetails"];
                XmlSave = new XmlDocument();
                XmlSave.LoadXml(vbl.Value);
            }
            catch
            {
                bNoUpdating = false;
                return;
            }

            foreach(XmlNode node in XmlSave.DocumentElement.ChildNodes)
            {
                String type = node.SelectSingleNode("@type").InnerText;
                switch (type)
                {
                    case "System.Windows.Forms.CheckBox":
                        CheckBox chk = (CheckBox)this.Controls[node.Name];
                        if (node.InnerText == "-1")
                            chk.Checked = true;
                        else
                            chk.Checked = false;
                        break;

                    case "CustomTextBox":
                        TextBox tbx = (TextBox)this.Controls[node.Name];
                        tbx.Text = node.InnerText;
                        break;
                    
                    case "CustomTaskPanes.CustomComboBox":
                        ComboBox cbo = (ComboBox)this.Controls[node.Name];
                        cbo.Text = node.InnerText;
                        break;

                    case "CustomTaskPanes.CustomRichTextBox":
                        RichTextBox rtbx = (RichTextBox)this.Controls[node.Name];
                        rtbx.Text = node.InnerText;
                        break;

                    case "System.Windows.Forms.ListView":
                        ListView lvw = (ListView)this.Controls[node.Name];
                        foreach(XmlNode subnode in node.ChildNodes)
                        {
                            ListViewItem lvi = new ListViewItem(subnode.ChildNodes[0].InnerText);
                            lvw.Items.Add(lvi);
                            foreach(XmlNode column in subnode.SelectNodes("column2|column3|column4"))
                            {
                                lvi.SubItems.Add(column.InnerText);
                            }
                        }
                        break;
                }
            }
            bNoUpdating = false;
            BuildRecipientList();
            bNoUpdating = true;
            cboRecipient.Text = XmlSave.SelectSingleNode("//cboRecipient").InnerText;
            bNoUpdating = false;
        }
        
        
        void lvw_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView lvw = (ListView)sender;
            lastListView = lvw;
            if (lvw.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = lvw.SelectedItems[0];
            tbxDescription.Text = lvi.SubItems[0].Text;
            tbxNet.Text = lvi.SubItems[1].Text;
            switch(lvw.Name)
            {
                case "lvwServices":
                    cboType.SelectedIndex = 0;
                    break;
                case "lvwDisbursements":
                    cboType.SelectedIndex = 1;
                    break;
                case "lvwNonVAT":
                    cboType.SelectedIndex = 2;
                    break;
            }
        }

        private void SetDate()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("date", false);
            if (ctl != null)
                ctl.Range.Text = dateTimePicker.Text;
        }

        void ctl_TextChanged(object sender, EventArgs e)
        {
            if (bNoUpdating)
                return;
            Control tbx = (Control)sender;
            if (tbx.Name.ToLower() == "tbxcompany" || tbx.Name.ToLower() == "cborecipient")
            {
                AddresseeOrCompany();
            }
            else
            {
                Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
                if (ctl != null)
                    ctl.Range.Text = tbx.Text;
            }
                SetParagraphSpacingOnPBandFAO();

                if (tbx.Name.ToLower() == "tbxmonies")
                    ProcessBalance();

                BuildSaveTree();
        }

        public void Fetch()
        {
            if (DMS == null)
                DMS = Globals.ThisAddIn.DMS;
            if(DMS.IsOnline)
            { 
                this.tbxFileNumber.Text = DMS.ClientMatterCode;
                this.tbxMatter.Text = DMS.MatterDescription;
            }
            //this.tbxOurRef.Text = DMS.ClientMatterCode;
            //FetchAuthor(DMS);
            //SetDate();
            //FetchSubject(DMS);

        }

        private void btnLookup_Click(object sender, EventArgs e)
        {
            BuildLists("");
            BuildSaveTree();
        }

        private void SetParagraphSpacingOnPBandFAO()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("payableBy", false);
            if (tbxPayableBy.Text.Length == 0)
            {
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceExactly;
                ctl.Range.ParagraphFormat.LineSpacing = 1;
                ctl = clsWordApp.getContentControl("payableByLabel", false);
                if (ctl != null)
                    ctl.Range.Text = "";
            }
            else
            { 
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                ctl = clsWordApp.getContentControl("payableByLabel", false);
                if (ctl != null)
                    ctl.Range.Text = "PAYABLE BY:";
            }

            ctl = clsWordApp.getContentControl("fao", false);

            if(chkIndividual.Checked)
            { 
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceExactly;
                ctl.Range.ParagraphFormat.LineSpacing = 1;
            }
            else
                ctl.Range.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;

        }
        
        private void BuildLists(String id)
        {
            bNoUpdating = true;
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            InterAction.IAAddress iaddress;
            if (id.Length == 0)
            {
                icontact = IA.GetContact(tbxForename.Text, tbxSurname.Text, tbxCompany.Text);
                iaddress = IA.GetAddress(icontact);
            }
            else
            {
                icontact = IA.GetContactByIALID(id);
                iaddress = null;
            }
            if (icontact == null)
                return;
            if (icontact.NameTitle.Length > 0)
                cboTitle.Text = icontact.NameTitle;

            tbxForename.Text = icontact.FirstName;
            tbxSurname.Text = icontact.LastName;
            tbxSuffix.Text = icontact.NameSuffix;
            tbxCompany.Text = icontact.CompanyName;
            bNoUpdating = false;
            tbxAddress.Text = icontact.SelectedAddress.FormattedAddress;
            BuildRecipientList();
            
        }

        private void chkIndividual_CheckedChanged(object sender, EventArgs e)
        {
            cboTitle.Enabled = !chkIndividual.Checked;
            tbxForename.Enabled = !chkIndividual.Checked;
            tbxSurname.Enabled = !chkIndividual.Checked;
            tbxSuffix.Enabled = !chkIndividual.Checked;
            cboRecipient.Enabled = !chkIndividual.Checked;
            cboRecipient.Text = "";
            AddresseeOrCompany();
            SetParagraphSpacingOnPBandFAO();
            BuildSaveTree();

        }

        private void BuildRecipientList()
        {
            if (bNoUpdating)
                return;
            cboRecipient.Items.Clear();
            String recip = "";
            if(!chkIndividual.Checked)
            { 
                foreach (XmlNode node in XmlDoc.SelectNodes("//naming/param"))
                {
                    try
                    {
                        recip = node.InnerText.Replace("[Title]", cboTitle.Text).Replace("[Initial]", tbxForename.Text.Substring(0, 1)).Replace("[Forename]", tbxForename.Text);
                        recip = recip.Replace("[Surname]", tbxSurname.Text).Replace("[Suffix]", tbxSuffix.Text).Trim();
                        cboRecipient.Items.Add(recip);
                    }
                    catch { }
                }
                try
                {
                    cboRecipient.SelectedIndex = 0;
                }
                catch { }
            }
            
        }

        private void cboTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxForename_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxSurname_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void tbxSuffix_TextChanged(object sender, EventArgs e)
        {
            BuildRecipientList();
        }

        private void cboRecipient_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void AddresseeOrCompany()
        {
            if (chkIndividual.Checked)
            {
                tbxCompany.Tag = "addressee";
                cboRecipient.Tag = "fao";
            }
            else
            {
                tbxCompany.Tag = "fao";
                cboRecipient.Tag = "addressee";
            }
             
            Word.ContentControl ctl = clsWordApp.getContentControl(tbxCompany.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbxCompany.Text;

           ctl = clsWordApp.getContentControl(cboRecipient.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = cboRecipient.Text;
        }

        private void chkCredit_CheckedChanged(object sender, EventArgs e)
        {
            SetCreditInvoice();
            BuildSaveTree();
        }

        private void SetCreditInvoice()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("invCred", false);
            if (chkCredit.Checked)
                ctl.Range.Text = "CREDIT NOTE";
            else
                ctl.Range.Text = "TAX INVOICE";

            ctl = clsWordApp.getContentControl("invoiceDescription", false);
            if (chkCredit.Checked)
                ctl.Range.Text = "CREDIT NOTE NO.";
            else
                ctl.Range.Text = "INVOICE NO.";
        }

        private void chkAddAddress_CheckedChanged(object sender, EventArgs e)
        {
            SetExtraAddressEnabled();
            BuildSaveTree();
        }

        private void SetExtraAddressEnabled()
        {
            tbxExtraAddress.Enabled = chkAddAddress.Checked;
        }

        private void chkMonies_CheckedChanged(object sender, EventArgs e)
        {
            SetMonies();
            PutMonies();
            BuildSaveTree();

        }

        private void SetMonies()
        {
            tbxMonies.Enabled = chkMonies.Checked;
        }

        private void PutMonies()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("balance", false);
            if (ctl == null)
                return;
            Word.Table tbl = ctl.Range.Tables[1];
            Word.Row row;
            if(chkMonies.Checked)
            {
                if (tbl.Rows.Count == 1)
                   row = tbl.Rows.Add(tbl.Rows[1]);
                else
                   row = tbl.Rows[1];
                    
                row.Cells[2].Range.Text = "Less Paid";
                row.Cells[3].Range.Text = tbxMonies.Text;
            }
            else
                if(tbl.Rows.Count > 1)
                tbl.Rows[1].Delete();
        }

        private void tbxMonies_TextChanged(object sender, EventArgs e)
        {
            PutMonies();
        }

        private void SetLVHeadings()
        {
            
            lvwServices.Columns.Add("Desc");
            lvwServices.Columns.Add("Net");
            lvwServices.Columns.Add("VAT");
            lvwServices.Columns.Add("Gross");
            lvwServices.Columns[0].Width = 100;

            lvwDisbursements.Columns.Add("Desc");
            lvwDisbursements.Columns.Add("Net");
            lvwDisbursements.Columns.Add("VAT");
            lvwDisbursements.Columns.Add("Gross");
            lvwDisbursements.HeaderStyle = ColumnHeaderStyle.None;
            lvwDisbursements.Columns[0].Width = 100;

            lvwNonVAT.Columns.Add("Desc");
            lvwNonVAT.Columns.Add("Net");
            lvwNonVAT.Columns.Add("VAT");
            lvwNonVAT.Columns.Add("Gross");
            lvwNonVAT.HeaderStyle = ColumnHeaderStyle.None;
            lvwNonVAT.Columns[0].Width = 100;
        }
        
        private int CountOfInputTables()
        {
            int count = 0;
            foreach (Word.Table tbl in Globals.ThisAddIn.Application.ActiveDocument.Tables)
            {
                Word.Style stl = tbl.get_Style();
                if (stl.NameLocal == "Invoice")
                    count++;
            }
            return count; 
        }

        private Word.Table SetFirstTable()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            foreach(Word.Table tbl in doc.Tables)
            {
                Word.Style stl = tbl.get_Style();
                if (stl.NameLocal == "Invoice")
                {
                    return tbl;
                }
            }
            return null;
        }

        
        private void BuildTables()
        {
            Double vatRate;
            ListView lvw;
            Word.Table tbl = null;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rng = clsWordApp.getContentControl("matter",false).Range;
            rng.MoveEnd(Word.WdUnits.wdCharacter,1);
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rng.InsertParagraph();
            rng.InsertParagraph();
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

            for(int i = 1; i<=4; i++)
            { 
                tbl = null;
                switch(i)
                {
                    case 1:
                        if(lvwServices.Items.Count>0)
                        { 
                        tbl = doc.Tables.Add(rng, 2, 2);
                        servicesTable = tbl;
                            tbl.Cell(1,1).Range.Text = "Supply of Legal Services";
                            tbl.Cell(2, 2).Range.Text = GetServicesTotal().ToString("0.00");
                        }
                        break;

                    case 2:
                        if (lvwDisbursements.Items.Count > 0)
                        {
                            tbl = doc.Tables.Add(rng, 2, 2);
                            disbursementsTable = tbl;
                            tbl.Cell(1,1).Range.Text = "Disbursements";
                            tbl.Cell(2, 2).Range.Text = GetDisbursementsTotal().ToString("0.00");
                        }
                        break;

                    case 3:
                        if (lvwServices.Items.Count > 0 || lvwDisbursements.Items.Count > 0)
                        {
                            tbl = doc.Tables.Add(rng, 3, 2);
                            VATTable = tbl;
                            tbl.Cell(1, 1).Range.Text = "Subtotal of VATable items";
                            if (lvwServices.Items.Count > 0)
                                lvw = lvwServices;
                            else
                                lvw = lvwDisbursements;

                            vatRate = Convert.ToDouble(lvw.Items[0].SubItems[2].Text) / Convert.ToDouble(lvw.Items[0].SubItems[1].Text) * 100;
                            tbl.Cell(2, 1).Range.Text = "                VAT (charged at " + vatRate + "%)";
                            tbl.Cell(1, 2).Range.Text = GetNetTotal().ToString("0.00");
                            tbl.Cell(2, 2).Range.Text = GetVATTotal().ToString("0.00");
                            tbl.Cell(3, 2).Range.Text = GetGrossTotal().ToString("0.00");
                       }
                        break;

                    case 4:
                        if(lvwNonVAT.Items.Count > 0)
                        {
                            tbl = doc.Tables.Add(rng, 2, 2);
                            nonVATTable = tbl;
                            tbl.Cell(1, 1).Range.Text = "Non-VATable items paid on behalf of the client";
                            tbl.Cell(2, 2).Range.Text = GetNonVATTotal().ToString("0.00");
                        }

                        break;

                        
                }
                
                if (tbl != null)
                {
                    tbl.Columns[1].Width = 331.45F;
                    tbl.Columns[2].Width = 110.15F;
                    Word.Style stl = doc.Styles["Invoice"];
                    tbl.set_Style(stl);
                    tbl.ApplyStyleLastColumn = true;
                    tbl.ApplyStyleLastRow = true;
                    tbl.Range.Font.Reset();
                    rng = tbl.Range;
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    rng.InsertParagraph();
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                    tbl = null;
                }
            }
        }

        private void RemoveTables()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            for(int i = doc.Tables.Count; i > 0; i-- )
            { 
                Word.Table tbl = doc.Tables[i];
                Word.Range rng = tbl.Range;
                rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                Word.Style stl = tbl.get_Style();
                if (stl.NameLocal == "Invoice")
                    rng.Delete();
            }
            Word.Table tbllast = doc.Tables[doc.Tables.Count];
            try
            {
               doc.Range(tbllast.Range.Start - 2, tbllast.Range.Start - 1).Delete();
            }
            catch { }
        }

        private void PopulateTables()
        {
            foreach(ListViewItem lvi in lvwServices.Items)
            {
                Word.Row row = servicesTable.Rows.Last;
                Word.Row newrow = servicesTable.Rows.Add(row);
                newrow.Cells[1].Range.Text = lvi.SubItems[0].Text;
                newrow.Cells[2].Range.Text = lvi.SubItems[1].Text;
            }
            foreach (ListViewItem lvi in lvwDisbursements.Items)
            {
                Word.Row row = disbursementsTable.Rows.Last;
                Word.Row newrow = disbursementsTable.Rows.Add(row);
                newrow.Cells[1].Range.Text = lvi.SubItems[0].Text;
                newrow.Cells[2].Range.Text = lvi.SubItems[1].Text;
            }

            foreach (ListViewItem lvi in lvwNonVAT.Items)
            {
                Word.Row row = nonVATTable.Rows.Last;
                Word.Row newrow = nonVATTable.Rows.Add(row);
                newrow.Cells[1].Range.Text = lvi.SubItems[0].Text;
                newrow.Cells[2].Range.Text = lvi.SubItems[1].Text;
            }

            Word.Range rng = SetFirstTable().Cell(1, 2).Range;
            rng.Text = "£";
            rng.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
            rng.Font.Bold = -1;

            ProcessBalance();
            

        }
        
       private void ProcessBalance()
        {
            Word.ContentControl ctl = clsWordApp.getContentControl("balance", false); 
           Double balance = GetGrossTotal() + GetNonVATTotal() - GetMonies();
            if (ctl != null)
                ctl.Range.Text = balance.ToString("0.00");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddInvoiceItem();
            RemoveTables();
            BuildTables();
            PopulateTables();
            BuildSaveTree();
        }

        private ListViewItem AddInvoiceItem()
        {
            if (cboType.Text.Length == 0)
            {
                MessageBox.Show("You are missing a type (service, disbursement etc).");
                return null;
            }
            if (cboVat.Text.Length == 0)
            {
                MessageBox.Show("You are missing a VAT rate.");
                return null;
            }


            ListViewItem lvi = new ListViewItem(tbxDescription.Text);


            double vatfloat = GetVATMultiplier();
            double netfloat = Convert.ToDouble(tbxNet.Text);
            lvi.SubItems.Add(netfloat.ToString("0.00"));
            double vatrate = netfloat * vatfloat;
            double gross = netfloat + vatrate;
            lvi.SubItems.Add(vatrate.ToString("0.00"));
            lvi.SubItems.Add(gross.ToString("0.00"));
            switch (cboType.SelectedIndex)
            {
                case 0:
                    lvwServices.Items.Add(lvi);
                    break;

                case 1:
                    lvwDisbursements.Items.Add(lvi);
                    break;

                case 2:
                    lvwNonVAT.Items.Add(lvi);
                    break;
            }
            return lvi;
        }

        private Double GetVATTotal()
        {
            Double vat = 0;
            foreach (ListViewItem lvi in lvwServices.Items)
                vat = vat + Convert.ToDouble(lvi.SubItems[2].Text);

            foreach (ListViewItem lvi in lvwDisbursements.Items)
                vat = vat + Convert.ToDouble(lvi.SubItems[2].Text);

            return vat;

        }

        private Double GetNetTotal()
        {
            Double net = 0;
            foreach (ListViewItem lvi in lvwServices.Items)
                net = net + Convert.ToDouble(lvi.SubItems[1].Text);

            foreach (ListViewItem lvi in lvwDisbursements.Items)
                net = net + Convert.ToDouble(lvi.SubItems[1].Text);

            return net;
        }

        private Double GetGrossTotal()
        {
            Double gross = 0;
            foreach (ListViewItem lvi in lvwServices.Items)
                gross = gross + Convert.ToDouble(lvi.SubItems[3].Text);

            foreach (ListViewItem lvi in lvwDisbursements.Items)
                gross = gross + Convert.ToDouble(lvi.SubItems[3].Text);

            return gross;
        }

        private Double GetServicesTotal()
        {
            Double gross = 0;
            foreach (ListViewItem lvi in lvwServices.Items)
                gross = gross + Convert.ToDouble(lvi.SubItems[1].Text);
            return gross;
        }

        private Double GetDisbursementsTotal()
        {
            Double gross = 0;
            foreach (ListViewItem lvi in lvwDisbursements.Items)
                gross = gross + Convert.ToDouble(lvi.SubItems[1].Text);
            return gross;
        }

        private Double GetNonVATTotal()
        {
            Double gross = 0;
            foreach (ListViewItem lvi in lvwNonVAT.Items)
                gross = gross + Convert.ToDouble(lvi.SubItems[1].Text);
            return gross;
        }
        
        
        private void RecalculateVAT(ListView lvw)
        {
             Double netfloat;
             Double vatfloat;
            Double vat;
            Double gross;
            foreach (ListViewItem lvi in lvw.Items)
            { 
                netfloat = Convert.ToDouble(lvi.SubItems[1].Text);
                vatfloat = GetVATMultiplier();
                vat = netfloat * vatfloat;
                lvi.SubItems[2].Text = vat.ToString("0.00");
                gross = netfloat + vat;
                lvi.SubItems[3].Text = gross.ToString("0.00");
  
            }
        }

        private Double GetMonies()
        {
            if (chkMonies.Checked && tbxMonies.Text.Length>0)
                return Convert.ToDouble(tbxMonies.Text);
            else
                return 0;
        }
        
        private Double GetVATMultiplier()
        {
            String vat = cboVat.Text.Replace("%", "");
            double vatfloat = Convert.ToDouble(vat);
            return vatfloat / 100;
        }

        private void cboVat_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (bNoUpdating)
                goto endpoint;
            if(lvwServices.Items.Count > 0 || lvwDisbursements.Items.Count > 0)
            {
                if (MessageBox.Show("You have changed the VAT rate.  All items will be recalculated.  Do you wish to continue?",
                    Globals.ThisAddIn.APP_NAME1, MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    RecalculateVAT(lvwServices);
                    RecalculateVAT(lvwDisbursements);
                    RemoveTables();
                    BuildTables();
                    PopulateTables();
                }
                else
                    cboVat.SelectedIndex = VATSelected;
            }
endpoint:
            VATSelected = cboVat.SelectedIndex;
        }

        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bNoUpdating = true;
            cboVat.Enabled = cboType.SelectedIndex != 2;
            if (cboType.SelectedIndex == 2)
                cboVat.SelectedIndex = 0;
            bNoUpdating = false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lastListView == null)
                return;
            if(lastListView.SelectedItems.Count ==0)
            {
                MessageBox.Show("You need to select an item in one of the lists below before you can update it.");
                return;
            }
            Boolean newlistview = false;
            switch(lastListView.Name)
            {
                case "lvwServices":
                    if (cboType.SelectedIndex != 0)
                        newlistview = true;
                    break;
                case "lvwDisbursements":
                    if (cboType.SelectedIndex != 1)
                        newlistview = true;
                    break;
                case "lvwNonVAT":
                    if (cboType.SelectedIndex != 2)
                        newlistview = true;
                    break;
            }
            ListViewItem lvi = lastListView.SelectedItems[0];
            if(newlistview)
            {
                lvi.Remove();
                lvi = AddInvoiceItem();
                lvi.Selected = true;
            }
            else{
            lvi.SubItems[0].Text = tbxDescription.Text;
            double vatfloat = GetVATMultiplier();
            double netfloat = Convert.ToDouble(tbxNet.Text);
            lvi.SubItems[1].Text = (netfloat.ToString("0.00"));
            double vatrate = netfloat * vatfloat;
            double gross = netfloat + vatrate;
            lvi.SubItems[2].Text = (vatrate.ToString("0.00"));
            lvi.SubItems[3].Text = (gross.ToString("0.00"));
                }
            lvi.Selected = true;
            RemoveTables();
            BuildTables();
            PopulateTables();
            BuildSaveTree();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = lastListView.SelectedItems[0];
            lvi.Remove();
            RemoveTables();
            BuildTables();
            PopulateTables();
            BuildSaveTree();
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            BuildSaveTree();
        }

        private void tbxFileNumber_TextChanged(object sender, EventArgs e)
        {

        }

       

       
    }
}
