﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpFax
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbxYourRef = new CustomTextBox();
            this.tbxOurRef = new CustomTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new CustomTaskPanes.CustomDateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.tbxTitle = new CustomTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxAuthor = new CustomTextBox();
            this.tbxFax = new CustomTextBox();
            this.tbxMail = new CustomTextBox();
            this.tbxAuthorTel = new CustomTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxSubject = new CustomTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboSignOff = new CustomTaskPanes.CustomComboBox();
            this.tbxToName = new CustomTextBox();
            this.tbxToOrg = new CustomTextBox();
            this.tbxToFax = new CustomTextBox();
            this.btnToLookup = new System.Windows.Forms.Button();
            this.btnToAdd = new System.Windows.Forms.Button();
            this.btnToRemove = new System.Windows.Forms.Button();
            this.lvwTo = new System.Windows.Forms.ListView();
            this.lvwCC = new System.Windows.Forms.ListView();
            this.btnCCRemove = new System.Windows.Forms.Button();
            this.btnCCAdd = new System.Windows.Forms.Button();
            this.btnCCLookup = new System.Windows.Forms.Button();
            this.tbxCCFax = new CustomTextBox();
            this.tbxCCOrg = new CustomTextBox();
            this.tbxCCName = new CustomTextBox();
            this.numPages = new System.Windows.Forms.NumericUpDown();
            this.btnAuthor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnFetch = new System.Windows.Forms.Button();
            this.btnUpdateTo = new System.Windows.Forms.Button();
            this.btnUpdateCC = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).BeginInit();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Your ref";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(11, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 16);
            this.label14.TabIndex = 44;
            this.label14.Text = "Our ref";
            // 
            // tbxYourRef
            // 
            this.tbxYourRef.Location = new System.Drawing.Point(95, 41);
            this.tbxYourRef.Name = "tbxYourRef";
            this.tbxYourRef.Size = new System.Drawing.Size(233, 20);
            this.tbxYourRef.TabIndex = 2;
            this.tbxYourRef.Tag = "yourRef";
            this.tbxYourRef.TextChanged += new System.EventHandler(this.tbxYourRef_TextChanged);
            // 
            // tbxOurRef
            // 
            this.tbxOurRef.Location = new System.Drawing.Point(95, 12);
            this.tbxOurRef.Name = "tbxOurRef";
            this.tbxOurRef.Size = new System.Drawing.Size(233, 20);
            this.tbxOurRef.TabIndex = 1;
            this.tbxOurRef.Tag = "ourRef";
            this.tbxOurRef.TextChanged += new System.EventHandler(this.tbxOurRef_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 72);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(92, 71);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(237, 20);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.Tag = "date";
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 667);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 55;
            this.label12.Text = "Author role";
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(92, 664);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(237, 20);
            this.tbxTitle.TabIndex = 14;
            this.tbxTitle.Tag = "role";
            this.tbxTitle.TextChanged += new System.EventHandler(this.tbxTitle_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 742);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "Author email";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 716);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 52;
            this.label10.Text = "Author fax";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(10, 690);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 51;
            this.label9.Text = "Author tel";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 641);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Author";
            // 
            // tbxAuthor
            // 
            this.tbxAuthor.Location = new System.Drawing.Point(92, 638);
            this.tbxAuthor.Name = "tbxAuthor";
            this.tbxAuthor.Size = new System.Drawing.Size(200, 20);
            this.tbxAuthor.TabIndex = 13;
            this.tbxAuthor.Tag = "authorName";
            this.tbxAuthor.TextChanged += new System.EventHandler(this.tbxAuthor_TextChanged);
            // 
            // tbxFax
            // 
            this.tbxFax.Location = new System.Drawing.Point(92, 713);
            this.tbxFax.Name = "tbxFax";
            this.tbxFax.Size = new System.Drawing.Size(236, 20);
            this.tbxFax.TabIndex = 16;
            this.tbxFax.Tag = "authorFax";
            this.tbxFax.TextChanged += new System.EventHandler(this.tbxFax_TextChanged);
            // 
            // tbxMail
            // 
            this.tbxMail.Location = new System.Drawing.Point(92, 739);
            this.tbxMail.Name = "tbxMail";
            this.tbxMail.Size = new System.Drawing.Size(234, 20);
            this.tbxMail.TabIndex = 17;
            this.tbxMail.Tag = "authorEmail";
            this.tbxMail.TextChanged += new System.EventHandler(this.tbxMail_TextChanged);
            // 
            // tbxAuthorTel
            // 
            this.tbxAuthorTel.Location = new System.Drawing.Point(92, 687);
            this.tbxAuthorTel.Name = "tbxAuthorTel";
            this.tbxAuthorTel.Size = new System.Drawing.Size(236, 20);
            this.tbxAuthorTel.TabIndex = 15;
            this.tbxAuthorTel.Tag = "authorTelephone";
            this.tbxAuthorTel.TextChanged += new System.EventHandler(this.tbxAuthorTel_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 559);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 57;
            this.label6.Text = "Subject";
            // 
            // tbxSubject
            // 
            this.tbxSubject.Location = new System.Drawing.Point(92, 556);
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.Size = new System.Drawing.Size(237, 20);
            this.tbxSubject.TabIndex = 10;
            this.tbxSubject.Tag = "subject";
            this.tbxSubject.TextChanged += new System.EventHandler(this.tbxSubject_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 614);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Sign-off";
            // 
            // cboSignOff
            // 
            this.cboSignOff.FormattingEnabled = true;
            this.cboSignOff.Location = new System.Drawing.Point(92, 611);
            this.cboSignOff.Name = "cboSignOff";
            this.cboSignOff.Size = new System.Drawing.Size(238, 21);
            this.cboSignOff.TabIndex = 12;
            this.cboSignOff.Tag = "signOff";
            this.cboSignOff.SelectedIndexChanged += new System.EventHandler(this.cboSignOff_SelectedIndexChanged);
            // 
            // tbxToName
            // 
            this.tbxToName.Location = new System.Drawing.Point(92, 107);
            this.tbxToName.Name = "tbxToName";
            this.tbxToName.Size = new System.Drawing.Size(200, 20);
            this.tbxToName.TabIndex = 4;
            // 
            // tbxToOrg
            // 
            this.tbxToOrg.Location = new System.Drawing.Point(92, 135);
            this.tbxToOrg.Name = "tbxToOrg";
            this.tbxToOrg.Size = new System.Drawing.Size(200, 20);
            this.tbxToOrg.TabIndex = 5;
            // 
            // tbxToFax
            // 
            this.tbxToFax.Location = new System.Drawing.Point(92, 163);
            this.tbxToFax.Name = "tbxToFax";
            this.tbxToFax.Size = new System.Drawing.Size(199, 20);
            this.tbxToFax.TabIndex = 6;
            // 
            // btnToLookup
            // 
            this.btnToLookup.Location = new System.Drawing.Point(298, 107);
            this.btnToLookup.Name = "btnToLookup";
            this.btnToLookup.Size = new System.Drawing.Size(22, 19);
            this.btnToLookup.TabIndex = 64;
            this.btnToLookup.Text = "...";
            this.btnToLookup.UseVisualStyleBackColor = true;
            this.btnToLookup.Click += new System.EventHandler(this.btnToLookup_Click);
            // 
            // btnToAdd
            // 
            this.btnToAdd.Location = new System.Drawing.Point(298, 126);
            this.btnToAdd.Name = "btnToAdd";
            this.btnToAdd.Size = new System.Drawing.Size(22, 19);
            this.btnToAdd.TabIndex = 65;
            this.btnToAdd.Text = "+";
            this.btnToAdd.UseVisualStyleBackColor = true;
            this.btnToAdd.Click += new System.EventHandler(this.btnToAdd_Click);
            // 
            // btnToRemove
            // 
            this.btnToRemove.Location = new System.Drawing.Point(298, 145);
            this.btnToRemove.Name = "btnToRemove";
            this.btnToRemove.Size = new System.Drawing.Size(22, 19);
            this.btnToRemove.TabIndex = 66;
            this.btnToRemove.Text = "-";
            this.btnToRemove.UseVisualStyleBackColor = true;
            this.btnToRemove.Click += new System.EventHandler(this.btnToRemove_Click);
            // 
            // lvwTo
            // 
            this.lvwTo.Location = new System.Drawing.Point(92, 200);
            this.lvwTo.MultiSelect = false;
            this.lvwTo.Name = "lvwTo";
            this.lvwTo.Size = new System.Drawing.Size(225, 111);
            this.lvwTo.TabIndex = 67;
            this.lvwTo.UseCompatibleStateImageBehavior = false;
            this.lvwTo.SelectedIndexChanged += new System.EventHandler(this.lvwTo_SelectedIndexChanged);
            // 
            // lvwCC
            // 
            this.lvwCC.Location = new System.Drawing.Point(92, 410);
            this.lvwCC.MultiSelect = false;
            this.lvwCC.Name = "lvwCC";
            this.lvwCC.Size = new System.Drawing.Size(225, 111);
            this.lvwCC.TabIndex = 74;
            this.lvwCC.UseCompatibleStateImageBehavior = false;
            this.lvwCC.SelectedIndexChanged += new System.EventHandler(this.lvwCC_SelectedIndexChanged);
            // 
            // btnCCRemove
            // 
            this.btnCCRemove.Location = new System.Drawing.Point(298, 354);
            this.btnCCRemove.Name = "btnCCRemove";
            this.btnCCRemove.Size = new System.Drawing.Size(22, 19);
            this.btnCCRemove.TabIndex = 73;
            this.btnCCRemove.Text = "-";
            this.btnCCRemove.UseVisualStyleBackColor = true;
            this.btnCCRemove.Click += new System.EventHandler(this.btnCCRemove_Click);
            // 
            // btnCCAdd
            // 
            this.btnCCAdd.Location = new System.Drawing.Point(298, 336);
            this.btnCCAdd.Name = "btnCCAdd";
            this.btnCCAdd.Size = new System.Drawing.Size(22, 19);
            this.btnCCAdd.TabIndex = 72;
            this.btnCCAdd.Text = "+";
            this.btnCCAdd.UseVisualStyleBackColor = true;
            this.btnCCAdd.Click += new System.EventHandler(this.btnCCAdd_Click);
            // 
            // btnCCLookup
            // 
            this.btnCCLookup.Location = new System.Drawing.Point(298, 317);
            this.btnCCLookup.Name = "btnCCLookup";
            this.btnCCLookup.Size = new System.Drawing.Size(22, 19);
            this.btnCCLookup.TabIndex = 71;
            this.btnCCLookup.Text = "...";
            this.btnCCLookup.UseVisualStyleBackColor = true;
            this.btnCCLookup.Click += new System.EventHandler(this.btnCCLookup_Click);
            // 
            // tbxCCFax
            // 
            this.tbxCCFax.Location = new System.Drawing.Point(92, 373);
            this.tbxCCFax.Name = "tbxCCFax";
            this.tbxCCFax.Size = new System.Drawing.Size(199, 20);
            this.tbxCCFax.TabIndex = 9;
            // 
            // tbxCCOrg
            // 
            this.tbxCCOrg.Location = new System.Drawing.Point(92, 345);
            this.tbxCCOrg.Name = "tbxCCOrg";
            this.tbxCCOrg.Size = new System.Drawing.Size(200, 20);
            this.tbxCCOrg.TabIndex = 8;
            // 
            // tbxCCName
            // 
            this.tbxCCName.Location = new System.Drawing.Point(92, 317);
            this.tbxCCName.Name = "tbxCCName";
            this.tbxCCName.Size = new System.Drawing.Size(200, 20);
            this.tbxCCName.TabIndex = 7;
            // 
            // numPages
            // 
            this.numPages.Location = new System.Drawing.Point(92, 582);
            this.numPages.Name = "numPages";
            this.numPages.Size = new System.Drawing.Size(48, 20);
            this.numPages.TabIndex = 11;
            this.numPages.Tag = "pageCount";
            this.numPages.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPages.ValueChanged += new System.EventHandler(this.numPages_ValueChanged);
            // 
            // btnAuthor
            // 
            this.btnAuthor.Location = new System.Drawing.Point(298, 639);
            this.btnAuthor.Name = "btnAuthor";
            this.btnAuthor.Size = new System.Drawing.Size(22, 19);
            this.btnAuthor.TabIndex = 76;
            this.btnAuthor.Text = "...";
            this.btnAuthor.UseVisualStyleBackColor = true;
            this.btnAuthor.Click += new System.EventHandler(this.btnAuthor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 77;
            this.label1.Text = "To";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 78;
            this.label2.Text = "Organisation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 79;
            this.label3.Text = "Fax";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 80;
            this.label4.Text = "CC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 376);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 82;
            this.label7.Text = "Fax";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 348);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 81;
            this.label16.Text = "Organisation";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 584);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 83;
            this.label17.Text = "Page count";
            // 
            // btnFetch
            // 
            this.btnFetch.Location = new System.Drawing.Point(11, 765);
            this.btnFetch.Name = "btnFetch";
            this.btnFetch.Size = new System.Drawing.Size(97, 30);
            this.btnFetch.TabIndex = 84;
            this.btnFetch.Text = "Reset";
            this.btnFetch.UseVisualStyleBackColor = true;
            this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
            // 
            // btnUpdateTo
            // 
            this.btnUpdateTo.Location = new System.Drawing.Point(298, 164);
            this.btnUpdateTo.Name = "btnUpdateTo";
            this.btnUpdateTo.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateTo.TabIndex = 85;
            this.btnUpdateTo.Text = "!";
            this.btnUpdateTo.UseVisualStyleBackColor = true;
            this.btnUpdateTo.Click += new System.EventHandler(this.btnUpdateTo_Click);
            // 
            // btnUpdateCC
            // 
            this.btnUpdateCC.Location = new System.Drawing.Point(298, 373);
            this.btnUpdateCC.Name = "btnUpdateCC";
            this.btnUpdateCC.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateCC.TabIndex = 86;
            this.btnUpdateCC.Text = "!";
            this.btnUpdateCC.UseVisualStyleBackColor = true;
            this.btnUpdateCC.Click += new System.EventHandler(this.btnUpdateCC_Click);
            // 
            // ctpFax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnUpdateCC);
            this.Controls.Add(this.btnUpdateTo);
            this.Controls.Add(this.btnFetch);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAuthor);
            this.Controls.Add(this.numPages);
            this.Controls.Add(this.lvwCC);
            this.Controls.Add(this.btnCCRemove);
            this.Controls.Add(this.btnCCAdd);
            this.Controls.Add(this.btnCCLookup);
            this.Controls.Add(this.tbxCCFax);
            this.Controls.Add(this.tbxCCOrg);
            this.Controls.Add(this.tbxCCName);
            this.Controls.Add(this.lvwTo);
            this.Controls.Add(this.btnToRemove);
            this.Controls.Add(this.btnToAdd);
            this.Controls.Add(this.btnToLookup);
            this.Controls.Add(this.tbxToFax);
            this.Controls.Add(this.tbxToOrg);
            this.Controls.Add(this.tbxToName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboSignOff);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxSubject);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxAuthor);
            this.Controls.Add(this.tbxFax);
            this.Controls.Add(this.tbxMail);
            this.Controls.Add(this.tbxAuthorTel);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbxYourRef);
            this.Controls.Add(this.tbxOurRef);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dateTimePicker1);
            this.Name = "ctpFax";
            this.Size = new System.Drawing.Size(345, 813);
            this.Load += new System.EventHandler(this.ctpFax_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private CustomTextBox tbxYourRef;
        private CustomTextBox tbxOurRef;
        private System.Windows.Forms.Label label13;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label12;
        private CustomTextBox tbxTitle;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private CustomTextBox tbxAuthor;
        private CustomTextBox tbxFax;
        private CustomTextBox tbxMail;
        private CustomTextBox tbxAuthorTel;
        private System.Windows.Forms.Label label6;
        private CustomTextBox tbxSubject;
        private System.Windows.Forms.Label label5;
        private CustomTaskPanes.CustomComboBox cboSignOff;
        private CustomTextBox tbxToName;
        private CustomTextBox tbxToOrg;
        private CustomTextBox tbxToFax;
        private System.Windows.Forms.Button btnToLookup;
        private System.Windows.Forms.Button btnToAdd;
        private System.Windows.Forms.Button btnToRemove;
        private System.Windows.Forms.ListView lvwTo;
        private System.Windows.Forms.ListView lvwCC;
        private System.Windows.Forms.Button btnCCRemove;
        private System.Windows.Forms.Button btnCCAdd;
        private System.Windows.Forms.Button btnCCLookup;
        private CustomTextBox tbxCCFax;
        private CustomTextBox tbxCCOrg;
        private CustomTextBox tbxCCName;
        private System.Windows.Forms.NumericUpDown numPages;
        private System.Windows.Forms.Button btnAuthor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnFetch;
        private System.Windows.Forms.Button btnUpdateTo;
        private System.Windows.Forms.Button btnUpdateCC;
    }
}
