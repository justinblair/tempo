﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpBlueBack
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxDefendant = new CustomTaskPanes.CustomRichTextBox();
            this.cboDefendant = new CustomTaskPanes.CustomComboBox();
            this.tbxPlaintiff = new CustomTaskPanes.CustomRichTextBox();
            this.cboVersus = new CustomTaskPanes.CustomComboBox();
            this.cboPlaintiff = new CustomTaskPanes.CustomComboBox();
            this.tbxIndex = new CustomTextBox();
            this.cboIndex = new CustomTaskPanes.CustomComboBox();
            this.tbxHeading = new CustomTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxOther = new CustomTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxDistrict = new CustomTextBox();
            this.tbxFederal = new CustomTextBox();
            this.tbxCounty = new CustomTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbnState = new System.Windows.Forms.RadioButton();
            this.rbnOther = new System.Windows.Forms.RadioButton();
            this.rbnFederal = new System.Windows.Forms.RadioButton();
            this.chkDraft = new System.Windows.Forms.CheckBox();
            this.btnAff = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxDefendant
            // 
            this.tbxDefendant.Location = new System.Drawing.Point(25, 274);
            this.tbxDefendant.Name = "tbxDefendant";
            this.tbxDefendant.Size = new System.Drawing.Size(165, 45);
            this.tbxDefendant.TabIndex = 101;
            this.tbxDefendant.Tag = "";
            this.tbxDefendant.Text = "";
            this.tbxDefendant.TextChanged += new System.EventHandler(this.tbxDefendant_TextChanged);
            // 
            // cboDefendant
            // 
            this.cboDefendant.FormattingEnabled = true;
            this.cboDefendant.Location = new System.Drawing.Point(203, 298);
            this.cboDefendant.Name = "cboDefendant";
            this.cboDefendant.Size = new System.Drawing.Size(117, 21);
            this.cboDefendant.TabIndex = 100;
            this.cboDefendant.Tag = "defendants";
            // 
            // tbxPlaintiff
            // 
            this.tbxPlaintiff.Location = new System.Drawing.Point(25, 195);
            this.tbxPlaintiff.Name = "tbxPlaintiff";
            this.tbxPlaintiff.Size = new System.Drawing.Size(165, 45);
            this.tbxPlaintiff.TabIndex = 99;
            this.tbxPlaintiff.Tag = "";
            this.tbxPlaintiff.Text = "";
            this.tbxPlaintiff.TextChanged += new System.EventHandler(this.tbxPlaintiff_TextChanged);
            // 
            // cboVersus
            // 
            this.cboVersus.FormattingEnabled = true;
            this.cboVersus.Location = new System.Drawing.Point(25, 247);
            this.cboVersus.Name = "cboVersus";
            this.cboVersus.Size = new System.Drawing.Size(117, 21);
            this.cboVersus.TabIndex = 98;
            this.cboVersus.Tag = "vAgainst";
            // 
            // cboPlaintiff
            // 
            this.cboPlaintiff.FormattingEnabled = true;
            this.cboPlaintiff.Location = new System.Drawing.Point(203, 219);
            this.cboPlaintiff.Name = "cboPlaintiff";
            this.cboPlaintiff.Size = new System.Drawing.Size(117, 21);
            this.cboPlaintiff.TabIndex = 97;
            this.cboPlaintiff.Tag = "plaintiffs";
            // 
            // tbxIndex
            // 
            this.tbxIndex.Location = new System.Drawing.Point(157, 169);
            this.tbxIndex.Name = "tbxIndex";
            this.tbxIndex.Size = new System.Drawing.Size(163, 20);
            this.tbxIndex.TabIndex = 96;
            this.tbxIndex.Tag = "caseNumber";
            // 
            // cboIndex
            // 
            this.cboIndex.FormattingEnabled = true;
            this.cboIndex.Location = new System.Drawing.Point(25, 168);
            this.cboIndex.Name = "cboIndex";
            this.cboIndex.Size = new System.Drawing.Size(117, 21);
            this.cboIndex.TabIndex = 95;
            this.cboIndex.Tag = "caseType";
            // 
            // tbxHeading
            // 
            this.tbxHeading.Location = new System.Drawing.Point(95, 135);
            this.tbxHeading.Name = "tbxHeading";
            this.tbxHeading.Size = new System.Drawing.Size(170, 20);
            this.tbxHeading.TabIndex = 93;
            this.tbxHeading.Tag = "type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 92;
            this.label3.Text = "Heading";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(96, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 91;
            this.label2.Text = "County";
            // 
            // tbxOther
            // 
            this.tbxOther.Location = new System.Drawing.Point(95, 111);
            this.tbxOther.Name = "tbxOther";
            this.tbxOther.Size = new System.Drawing.Size(225, 20);
            this.tbxOther.TabIndex = 90;
            this.tbxOther.Tag = "court";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(96, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 89;
            this.label1.Text = "District";
            // 
            // tbxDistrict
            // 
            this.tbxDistrict.Location = new System.Drawing.Point(141, 84);
            this.tbxDistrict.Name = "tbxDistrict";
            this.tbxDistrict.Size = new System.Drawing.Size(179, 20);
            this.tbxDistrict.TabIndex = 88;
            this.tbxDistrict.Tag = "county";
            // 
            // tbxFederal
            // 
            this.tbxFederal.Location = new System.Drawing.Point(95, 60);
            this.tbxFederal.Name = "tbxFederal";
            this.tbxFederal.Size = new System.Drawing.Size(225, 20);
            this.tbxFederal.TabIndex = 87;
            this.tbxFederal.TextChanged += new System.EventHandler(this.tbxFederal_TextChanged);
            // 
            // tbxCounty
            // 
            this.tbxCounty.Location = new System.Drawing.Point(141, 35);
            this.tbxCounty.Name = "tbxCounty";
            this.tbxCounty.Size = new System.Drawing.Size(179, 20);
            this.tbxCounty.TabIndex = 86;
            this.tbxCounty.Tag = "county";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbnState);
            this.panel1.Controls.Add(this.rbnOther);
            this.panel1.Controls.Add(this.rbnFederal);
            this.panel1.Location = new System.Drawing.Point(14, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(76, 105);
            this.panel1.TabIndex = 85;
            // 
            // rbnState
            // 
            this.rbnState.AutoSize = true;
            this.rbnState.Location = new System.Drawing.Point(11, 3);
            this.rbnState.Name = "rbnState";
            this.rbnState.Size = new System.Drawing.Size(50, 17);
            this.rbnState.TabIndex = 63;
            this.rbnState.TabStop = true;
            this.rbnState.Text = "State";
            this.rbnState.UseVisualStyleBackColor = true;
            this.rbnState.CheckedChanged += new System.EventHandler(this.rbnState_CheckedChanged);
            // 
            // rbnOther
            // 
            this.rbnOther.AutoSize = true;
            this.rbnOther.Location = new System.Drawing.Point(11, 78);
            this.rbnOther.Name = "rbnOther";
            this.rbnOther.Size = new System.Drawing.Size(51, 17);
            this.rbnOther.TabIndex = 65;
            this.rbnOther.TabStop = true;
            this.rbnOther.Text = "Other";
            this.rbnOther.UseVisualStyleBackColor = true;
            this.rbnOther.CheckedChanged += new System.EventHandler(this.rbnOther_CheckedChanged);
            // 
            // rbnFederal
            // 
            this.rbnFederal.AutoSize = true;
            this.rbnFederal.Location = new System.Drawing.Point(11, 27);
            this.rbnFederal.Name = "rbnFederal";
            this.rbnFederal.Size = new System.Drawing.Size(60, 17);
            this.rbnFederal.TabIndex = 64;
            this.rbnFederal.TabStop = true;
            this.rbnFederal.Text = "Federal";
            this.rbnFederal.UseVisualStyleBackColor = true;
            this.rbnFederal.CheckedChanged += new System.EventHandler(this.rbnFederal_CheckedChanged);
            // 
            // chkDraft
            // 
            this.chkDraft.AutoSize = true;
            this.chkDraft.Location = new System.Drawing.Point(25, 12);
            this.chkDraft.Name = "chkDraft";
            this.chkDraft.Size = new System.Drawing.Size(49, 17);
            this.chkDraft.TabIndex = 84;
            this.chkDraft.Text = "Draft";
            this.chkDraft.UseVisualStyleBackColor = true;
            this.chkDraft.CheckedChanged += new System.EventHandler(this.chkDraft_CheckedChanged);
            // 
            // btnAff
            // 
            this.btnAff.Location = new System.Drawing.Point(19, 809);
            this.btnAff.Name = "btnAff";
            this.btnAff.Size = new System.Drawing.Size(117, 26);
            this.btnAff.TabIndex = 102;
            this.btnAff.Text = "Service to Pleading";
            this.btnAff.UseVisualStyleBackColor = true;
            this.btnAff.Click += new System.EventHandler(this.btnAff_Click);
            // 
            // ctpBlueBack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnAff);
            this.Controls.Add(this.tbxDefendant);
            this.Controls.Add(this.cboDefendant);
            this.Controls.Add(this.tbxPlaintiff);
            this.Controls.Add(this.cboVersus);
            this.Controls.Add(this.cboPlaintiff);
            this.Controls.Add(this.tbxIndex);
            this.Controls.Add(this.cboIndex);
            this.Controls.Add(this.tbxHeading);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxOther);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxDistrict);
            this.Controls.Add(this.tbxFederal);
            this.Controls.Add(this.tbxCounty);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkDraft);
            this.Name = "ctpBlueBack";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpBlueBack_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTaskPanes.CustomRichTextBox tbxDefendant;
        private CustomTaskPanes.CustomComboBox cboDefendant;
        private CustomTaskPanes.CustomRichTextBox tbxPlaintiff;
        private CustomTaskPanes.CustomComboBox cboVersus;
        private CustomTaskPanes.CustomComboBox cboPlaintiff;
        private CustomTextBox tbxIndex;
        private CustomTaskPanes.CustomComboBox cboIndex;
        private CustomTextBox tbxHeading;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private CustomTextBox tbxOther;
        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxDistrict;
        private CustomTextBox tbxFederal;
        private CustomTextBox tbxCounty;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbnState;
        private System.Windows.Forms.RadioButton rbnOther;
        private System.Windows.Forms.RadioButton rbnFederal;
        private System.Windows.Forms.CheckBox chkDraft;
        private System.Windows.Forms.Button btnAff;
    }
}
