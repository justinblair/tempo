﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpAgenda
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboType = new CustomTaskPanes.CustomComboBox();
            this.tbxTitle = new CustomTextBox();
            this.dateTimePicker = new CustomTaskPanes.CustomDateTimePicker();
            this.tbxTime = new CustomTextBox();
            this.tbxLocation = new CustomTextBox();
            this.dtpNextMeeting = new CustomTaskPanes.CustomDateTimePicker();
            this.tbxMinutes = new CustomTextBox();
            this.btnUpdateTo = new System.Windows.Forms.Button();
            this.lvwTo = new System.Windows.Forms.ListView();
            this.btnToRemove = new System.Windows.Forms.Button();
            this.btnToAdd = new System.Windows.Forms.Button();
            this.tbxInvited = new CustomTextBox();
            this.btnUpdateApologies = new System.Windows.Forms.Button();
            this.lvwApologies = new System.Windows.Forms.ListView();
            this.btnRemoveApologies = new System.Windows.Forms.Button();
            this.btnAddApologies = new System.Windows.Forms.Button();
            this.tbxApologies = new CustomTextBox();
            this.btnToLookup = new System.Windows.Forms.Button();
            this.btnApologiesLookup = new System.Windows.Forms.Button();
            this.btnFromApologies = new System.Windows.Forms.Button();
            this.btnToApologies = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblInvited = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cboType
            // 
            this.cboType.FormattingEnabled = true;
            this.cboType.Location = new System.Drawing.Point(84, 14);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(180, 21);
            this.cboType.TabIndex = 0;
            this.cboType.Tag = "type";
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(86, 45);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(260, 20);
            this.tbxTitle.TabIndex = 1;
            this.tbxTitle.Tag = "title";
            this.tbxTitle.TextChanged += new System.EventHandler(this.tbxTitle_TextChanged);
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(87, 78);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(176, 20);
            this.dateTimePicker.TabIndex = 2;
            this.dateTimePicker.Tag = "dateOfMeeting";
            this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // tbxTime
            // 
            this.tbxTime.Location = new System.Drawing.Point(86, 107);
            this.tbxTime.Name = "tbxTime";
            this.tbxTime.Size = new System.Drawing.Size(176, 20);
            this.tbxTime.TabIndex = 3;
            this.tbxTime.Tag = "time";
            this.tbxTime.TextChanged += new System.EventHandler(this.tbxTime_TextChanged);
            // 
            // tbxLocation
            // 
            this.tbxLocation.Location = new System.Drawing.Point(86, 142);
            this.tbxLocation.Name = "tbxLocation";
            this.tbxLocation.Size = new System.Drawing.Size(260, 20);
            this.tbxLocation.TabIndex = 4;
            this.tbxLocation.Tag = "location";
            this.tbxLocation.TextChanged += new System.EventHandler(this.tbxLocation_TextChanged);
            // 
            // dtpNextMeeting
            // 
            this.dtpNextMeeting.Location = new System.Drawing.Point(86, 177);
            this.dtpNextMeeting.Name = "dtpNextMeeting";
            this.dtpNextMeeting.Size = new System.Drawing.Size(176, 20);
            this.dtpNextMeeting.TabIndex = 5;
            this.dtpNextMeeting.Tag = "dateOfNextMeeting";
            this.dtpNextMeeting.ValueChanged += new System.EventHandler(this.dtpNextMeeting_ValueChanged);
            // 
            // tbxMinutes
            // 
            this.tbxMinutes.Enabled = false;
            this.tbxMinutes.Location = new System.Drawing.Point(84, 213);
            this.tbxMinutes.Name = "tbxMinutes";
            this.tbxMinutes.Size = new System.Drawing.Size(260, 20);
            this.tbxMinutes.TabIndex = 6;
            this.tbxMinutes.Tag = "minsTakenBy";
            this.tbxMinutes.TextChanged += new System.EventHandler(this.tbxMinutes_TextChanged);
            // 
            // btnUpdateTo
            // 
            this.btnUpdateTo.Location = new System.Drawing.Point(322, 315);
            this.btnUpdateTo.Name = "btnUpdateTo";
            this.btnUpdateTo.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateTo.TabIndex = 91;
            this.btnUpdateTo.Text = "!";
            this.btnUpdateTo.UseVisualStyleBackColor = true;
            this.btnUpdateTo.Click += new System.EventHandler(this.btnUpdateTo_Click);
            // 
            // lvwTo
            // 
            this.lvwTo.Location = new System.Drawing.Point(84, 277);
            this.lvwTo.MultiSelect = false;
            this.lvwTo.Name = "lvwTo";
            this.lvwTo.Size = new System.Drawing.Size(232, 115);
            this.lvwTo.TabIndex = 90;
            this.lvwTo.UseCompatibleStateImageBehavior = false;
            this.lvwTo.SelectedIndexChanged += new System.EventHandler(this.lvwTo_SelectedIndexChanged);
            // 
            // btnToRemove
            // 
            this.btnToRemove.Location = new System.Drawing.Point(322, 296);
            this.btnToRemove.Name = "btnToRemove";
            this.btnToRemove.Size = new System.Drawing.Size(22, 19);
            this.btnToRemove.TabIndex = 89;
            this.btnToRemove.Text = "-";
            this.btnToRemove.UseVisualStyleBackColor = true;
            this.btnToRemove.Click += new System.EventHandler(this.btnToRemove_Click);
            // 
            // btnToAdd
            // 
            this.btnToAdd.Location = new System.Drawing.Point(322, 277);
            this.btnToAdd.Name = "btnToAdd";
            this.btnToAdd.Size = new System.Drawing.Size(22, 19);
            this.btnToAdd.TabIndex = 88;
            this.btnToAdd.Text = "+";
            this.btnToAdd.UseVisualStyleBackColor = true;
            this.btnToAdd.Click += new System.EventHandler(this.btnToAdd_Click);
            // 
            // tbxInvited
            // 
            this.tbxInvited.Location = new System.Drawing.Point(84, 251);
            this.tbxInvited.Name = "tbxInvited";
            this.tbxInvited.Size = new System.Drawing.Size(232, 20);
            this.tbxInvited.TabIndex = 87;
            // 
            // btnUpdateApologies
            // 
            this.btnUpdateApologies.Enabled = false;
            this.btnUpdateApologies.Location = new System.Drawing.Point(322, 495);
            this.btnUpdateApologies.Name = "btnUpdateApologies";
            this.btnUpdateApologies.Size = new System.Drawing.Size(22, 19);
            this.btnUpdateApologies.TabIndex = 96;
            this.btnUpdateApologies.Text = "!";
            this.btnUpdateApologies.UseVisualStyleBackColor = true;
            this.btnUpdateApologies.Click += new System.EventHandler(this.btnUpdateApologies_Click);
            // 
            // lvwApologies
            // 
            this.lvwApologies.Enabled = false;
            this.lvwApologies.Location = new System.Drawing.Point(84, 457);
            this.lvwApologies.MultiSelect = false;
            this.lvwApologies.Name = "lvwApologies";
            this.lvwApologies.Size = new System.Drawing.Size(232, 115);
            this.lvwApologies.TabIndex = 95;
            this.lvwApologies.UseCompatibleStateImageBehavior = false;
            this.lvwApologies.SelectedIndexChanged += new System.EventHandler(this.lvwApologies_SelectedIndexChanged);
            // 
            // btnRemoveApologies
            // 
            this.btnRemoveApologies.Enabled = false;
            this.btnRemoveApologies.Location = new System.Drawing.Point(322, 476);
            this.btnRemoveApologies.Name = "btnRemoveApologies";
            this.btnRemoveApologies.Size = new System.Drawing.Size(22, 19);
            this.btnRemoveApologies.TabIndex = 94;
            this.btnRemoveApologies.Text = "-";
            this.btnRemoveApologies.UseVisualStyleBackColor = true;
            this.btnRemoveApologies.Click += new System.EventHandler(this.btnRemoveApologies_Click);
            // 
            // btnAddApologies
            // 
            this.btnAddApologies.Enabled = false;
            this.btnAddApologies.Location = new System.Drawing.Point(322, 457);
            this.btnAddApologies.Name = "btnAddApologies";
            this.btnAddApologies.Size = new System.Drawing.Size(22, 19);
            this.btnAddApologies.TabIndex = 93;
            this.btnAddApologies.Text = "+";
            this.btnAddApologies.UseVisualStyleBackColor = true;
            this.btnAddApologies.Click += new System.EventHandler(this.btnAddApologies_Click);
            // 
            // tbxApologies
            // 
            this.tbxApologies.Enabled = false;
            this.tbxApologies.Location = new System.Drawing.Point(84, 431);
            this.tbxApologies.Name = "tbxApologies";
            this.tbxApologies.Size = new System.Drawing.Size(232, 20);
            this.tbxApologies.TabIndex = 92;
            // 
            // btnToLookup
            // 
            this.btnToLookup.Location = new System.Drawing.Point(322, 252);
            this.btnToLookup.Name = "btnToLookup";
            this.btnToLookup.Size = new System.Drawing.Size(22, 19);
            this.btnToLookup.TabIndex = 97;
            this.btnToLookup.Text = "...";
            this.btnToLookup.UseVisualStyleBackColor = true;
            this.btnToLookup.Click += new System.EventHandler(this.btnToLookup_Click);
            // 
            // btnApologiesLookup
            // 
            this.btnApologiesLookup.Enabled = false;
            this.btnApologiesLookup.Location = new System.Drawing.Point(322, 431);
            this.btnApologiesLookup.Name = "btnApologiesLookup";
            this.btnApologiesLookup.Size = new System.Drawing.Size(22, 19);
            this.btnApologiesLookup.TabIndex = 98;
            this.btnApologiesLookup.Text = "...";
            this.btnApologiesLookup.UseVisualStyleBackColor = true;
            this.btnApologiesLookup.Click += new System.EventHandler(this.btnApologiesLookup_Click);
            // 
            // btnFromApologies
            // 
            this.btnFromApologies.Enabled = false;
            this.btnFromApologies.Image = global::Templates1.Properties.Resources.down_arrow_thumb2;
            this.btnFromApologies.Location = new System.Drawing.Point(170, 404);
            this.btnFromApologies.Name = "btnFromApologies";
            this.btnFromApologies.Size = new System.Drawing.Size(22, 19);
            this.btnFromApologies.TabIndex = 100;
            this.btnFromApologies.UseVisualStyleBackColor = true;
            this.btnFromApologies.Click += new System.EventHandler(this.btnFromApologies_Click);
            // 
            // btnToApologies
            // 
            this.btnToApologies.Enabled = false;
            this.btnToApologies.Image = global::Templates1.Properties.Resources.up_arrow_thumb2;
            this.btnToApologies.Location = new System.Drawing.Point(198, 404);
            this.btnToApologies.Name = "btnToApologies";
            this.btnToApologies.Size = new System.Drawing.Size(22, 19);
            this.btnToApologies.TabIndex = 99;
            this.btnToApologies.UseVisualStyleBackColor = true;
            this.btnToApologies.Click += new System.EventHandler(this.btnToApologies_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 102;
            this.label2.Text = "Title";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 103;
            this.label3.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 104;
            this.label4.Text = "Time";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 105;
            this.label5.Text = "Location";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 106;
            this.label6.Text = "Next meeting";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 107;
            this.label7.Text = "Minutes taken";
            // 
            // lblInvited
            // 
            this.lblInvited.Location = new System.Drawing.Point(7, 255);
            this.lblInvited.Name = "lblInvited";
            this.lblInvited.Size = new System.Drawing.Size(74, 16);
            this.lblInvited.TabIndex = 108;
            this.lblInvited.Tag = "invitedAttended";
            this.lblInvited.Click += new System.EventHandler(this.lblInvited_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 438);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 109;
            this.label9.Text = "Apologies";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 254);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 110;
            this.label8.Text = "Attendees";
            // 
            // ctpAgenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblInvited);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFromApologies);
            this.Controls.Add(this.btnToApologies);
            this.Controls.Add(this.btnApologiesLookup);
            this.Controls.Add(this.btnToLookup);
            this.Controls.Add(this.btnUpdateApologies);
            this.Controls.Add(this.lvwApologies);
            this.Controls.Add(this.btnRemoveApologies);
            this.Controls.Add(this.btnAddApologies);
            this.Controls.Add(this.tbxApologies);
            this.Controls.Add(this.btnUpdateTo);
            this.Controls.Add(this.lvwTo);
            this.Controls.Add(this.btnToRemove);
            this.Controls.Add(this.btnToAdd);
            this.Controls.Add(this.tbxInvited);
            this.Controls.Add(this.tbxMinutes);
            this.Controls.Add(this.dtpNextMeeting);
            this.Controls.Add(this.tbxLocation);
            this.Controls.Add(this.tbxTime);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.cboType);
            this.Name = "ctpAgenda";
            this.Size = new System.Drawing.Size(363, 863);
            this.Load += new System.EventHandler(this.ctpAgenda_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTaskPanes.CustomComboBox cboType;
        private CustomTextBox tbxTitle;
        private CustomTaskPanes.CustomDateTimePicker dateTimePicker;
        private CustomTextBox tbxTime;
        private CustomTextBox tbxLocation;
        private CustomTaskPanes.CustomDateTimePicker dtpNextMeeting;
        private CustomTextBox tbxMinutes;
        private System.Windows.Forms.Button btnUpdateTo;
        private System.Windows.Forms.ListView lvwTo;
        private System.Windows.Forms.Button btnToRemove;
        private System.Windows.Forms.Button btnToAdd;
        private CustomTextBox tbxInvited;
        private System.Windows.Forms.Button btnUpdateApologies;
        private System.Windows.Forms.ListView lvwApologies;
        private System.Windows.Forms.Button btnRemoveApologies;
        private System.Windows.Forms.Button btnAddApologies;
        private CustomTextBox tbxApologies;
        private System.Windows.Forms.Button btnToLookup;
        private System.Windows.Forms.Button btnApologiesLookup;
        private System.Windows.Forms.Button btnToApologies;
        private System.Windows.Forms.Button btnFromApologies;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblInvited;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
    }
}
