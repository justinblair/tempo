﻿namespace Templates1.CustomTaskPanes.CustomTaskPaneControls
{
    partial class CustomPaneTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxTextBox = new System.Windows.Forms.TextBox();
            this.lblLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbxTextBox
            // 
            this.tbxTextBox.Location = new System.Drawing.Point(71, 3);
            this.tbxTextBox.Name = "tbxTextBox";
            this.tbxTextBox.Size = new System.Drawing.Size(265, 20);
            this.tbxTextBox.TabIndex = 0;
            // 
            // lblLabel
            // 
            this.lblLabel.AutoSize = true;
            this.lblLabel.Location = new System.Drawing.Point(8, 6);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(43, 13);
            this.lblLabel.TabIndex = 1;
            this.lblLabel.Text = "lblLabel";
            // 
            // CustomPaneTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblLabel);
            this.Controls.Add(this.tbxTextBox);
            this.Name = "CustomPaneTextBox";
            this.Size = new System.Drawing.Size(339, 29);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxTextBox;
        private System.Windows.Forms.Label lblLabel;
    }
}
