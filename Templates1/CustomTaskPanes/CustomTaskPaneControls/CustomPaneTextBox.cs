﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes.CustomTaskPaneControls
{
    public partial class CustomPaneTextBox : UserControl
    {
        public Word.ContentControls WordControls { get; set; }

        public CustomPaneTextBox(Word.ContentControls controls)
        {
            InitializeComponent();
             WordControls = controls;
            this.lblLabel.Text = WordControls[1].Title;
            this.tbxTextBox.TextChanged += new EventHandler(tbxTextBox_TextChanged);
        }

        void tbxTextBox_TextChanged(object sender, EventArgs e)
        {
            foreach (Word.ContentControl ctl in WordControls)
                ctl.Range.Text = tbxTextBox.Text;
        }
    }
}
