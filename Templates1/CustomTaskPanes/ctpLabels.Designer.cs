﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpLabels
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxLabel = new CustomTaskPanes.CustomRichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbnAll = new System.Windows.Forms.RadioButton();
            this.rbnOne = new System.Windows.Forms.RadioButton();
            this.btnc0c0 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnInterAction = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxLabel
            // 
            this.tbxLabel.Location = new System.Drawing.Point(85, 30);
            this.tbxLabel.Name = "tbxLabel";
            this.tbxLabel.Size = new System.Drawing.Size(252, 115);
            this.tbxLabel.TabIndex = 0;
            this.tbxLabel.Text = "";
            this.tbxLabel.TextChanged += new System.EventHandler(this.tbxLabel_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbnOne);
            this.panel1.Controls.Add(this.rbnAll);
            this.panel1.Location = new System.Drawing.Point(21, 156);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(64, 205);
            this.panel1.TabIndex = 1;
            // 
            // rbnAll
            // 
            this.rbnAll.AutoSize = true;
            this.rbnAll.Location = new System.Drawing.Point(13, 17);
            this.rbnAll.Name = "rbnAll";
            this.rbnAll.Size = new System.Drawing.Size(36, 17);
            this.rbnAll.TabIndex = 0;
            this.rbnAll.TabStop = true;
            this.rbnAll.Text = "All";
            this.rbnAll.UseVisualStyleBackColor = true;
            this.rbnAll.CheckedChanged += new System.EventHandler(this.rbnAll_CheckedChanged);
            // 
            // rbnOne
            // 
            this.rbnOne.AutoSize = true;
            this.rbnOne.Location = new System.Drawing.Point(15, 47);
            this.rbnOne.Name = "rbnOne";
            this.rbnOne.Size = new System.Drawing.Size(45, 17);
            this.rbnOne.TabIndex = 1;
            this.rbnOne.TabStop = true;
            this.rbnOne.Text = "One";
            this.rbnOne.UseVisualStyleBackColor = true;
            // 
            // btnc0c0
            // 
            this.btnc0c0.Location = new System.Drawing.Point(103, 204);
            this.btnc0c0.Name = "btnc0c0";
            this.btnc0c0.Size = new System.Drawing.Size(56, 24);
            this.btnc0c0.TabIndex = 2;
            this.btnc0c0.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Addressee";
            // 
            // btnInterAction
            // 
            this.btnInterAction.Location = new System.Drawing.Point(50, 57);
            this.btnInterAction.Name = "btnInterAction";
            this.btnInterAction.Size = new System.Drawing.Size(25, 23);
            this.btnInterAction.TabIndex = 4;
            this.btnInterAction.Text = "...";
            this.btnInterAction.UseVisualStyleBackColor = true;
            this.btnInterAction.Click += new System.EventHandler(this.btnInterAction_Click);
            // 
            // ctpLabels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.btnInterAction);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnc0c0);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbxLabel);
            this.Name = "ctpLabels";
            this.Size = new System.Drawing.Size(360, 852);
            this.Load += new System.EventHandler(this.ctpLabels_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomTaskPanes.CustomRichTextBox tbxLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbnOne;
        private System.Windows.Forms.RadioButton rbnAll;
        private System.Windows.Forms.Button btnc0c0;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInterAction;
    }
}
