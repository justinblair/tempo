﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    
    class LetterParameters
    {
        private XmlDocument XmlDoc;
        public LetterParameters()
        {
            XmlDoc = new XmlDocument();
            XmlDoc.Load(new clsHelper().Params);
        }

        private ArrayList Process(String path)
        {
            ArrayList al = new ArrayList();
            foreach (XmlNode nodNode in XmlDoc.SelectNodes(path))
                al.Add(nodNode.InnerText);
            al.Sort();
            return al;
        }

        internal ArrayList Salutation
        {
            get { return Process("//salutation/param"); }
        }

        internal ArrayList Court
        {
            get { return Process("//court/param"); }
        }

        internal ArrayList Division
        {
            get { return Process("//division/param"); }
        }

        internal ArrayList Representing
        {
            get { return Process("//representing/param"); }
        }

        internal ArrayList Claimant
        {
            get { return Process("//claimant/param"); }
        }

        internal ArrayList Defendant
        {
            get { return Process("//defendant/param"); }
        }
        
        internal ArrayList Claim
        {
            get { return Process("//claim/param"); }
        }
        
        internal ArrayList Confidentiality
        {
            get
            {         
            return Process("//conf/param");
             }
        }

        internal ArrayList Instructions
        {
            get
            {
                return Process("//instrucs/param");
            }
        }

        internal ArrayList Delivery
        {
            get
            {
                return Process("//delivery/param");
            }
        }

        internal ArrayList Prejudice
        {
            get
            {
                return Process("//prej/param");
            }
        }

        internal ArrayList SpecialDelivery
        {
            get
            {
                return Process("//spcdel/param");
            }
        }

       internal ArrayList SignOff
        {
            get 
            { 
                return Process("//signoff/param"); 
            }
        }

        internal ArrayList USDelivery
       {
            get
           { 
                return Process("//deliveryUS/param"); 
           }
       }

        internal ArrayList USClosing
       {
            get
           {
               return Process("//closingUS/param"); 
           }
       }

        internal ArrayList USPrivacy
        {
            get
            {
                return Process("//privacyUS/param");
            }
        }

        
    }
    
    
    class clsHelper
    {

        private ControlTagCollection controlTagCollection = null;
        public clsHelper()
        {

        }

        public void SetTabOrder(UserControl utl)
        {
            foreach (Control ctl in utl.Controls)
                if (!ctl.GetType().ToString().ToLower().Contains("label"))
                {
                    ctl.TabIndex = Convert.ToInt16(ctl.Location.Y);
                    //try
                    //{
                    //    CustomControlFunctions con = new CustomControlFunctions((ICustomControl)ctl);
                    //    con.ApplyControlTag(utl);
                    //}
                    //catch { }
                }
        }
        
        public string Params = System.IO.Path.Combine(Templates1.ThisAddIn.PROGRAMDATA, Templates1.ThisAddIn.DATA,"LetterParameters.xml");

        private System.Drawing.Color backgroundColour = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
        private System.Drawing.Color foregroundColour = System.Drawing.Color.WhiteSmoke;
           

        public void SetColour(UserControl user)
        {
            //System.Drawing.Image img = System.Drawing.Image.FromFile(@"C:\Program Files\Mishcon De Reya\Mishcon de Reya Templates\Images\FormBackground.png");
            //user.BackgroundImage = img;
            Globals.ThisAddIn.SuppressEvents = true;
            user.BackColor = backgroundColour;
            foreach (Control ctl in user.Controls)
            { 
                if(ctl.GetType().ToString().ToLower().Contains("panel"))
                {
                    Panel pnl = (Panel)ctl;
                    foreach (RadioButton r in pnl.Controls)
                        r.ForeColor = System.Drawing.Color.Orange;
                }
                
                try
                {
                    Label lbl = (Label)ctl;
                    lbl.ForeColor = System.Drawing.Color.Orange;
                    lbl.BackColor = backgroundColour;
                }
                catch { }
                try
                {
                    ComboBox cmb = (ComboBox)ctl;
                    cmb.BackColor = backgroundColour;
                    cmb.ForeColor = foregroundColour;
                }
                catch { }
                try
                {
                    CheckBox chk = (CheckBox)ctl;
                    chk.ForeColor = foregroundColour;
                    chk.BackColor = backgroundColour;
                }
                catch { }

                try
                {
                    TextBox tbx = (TextBox)ctl;
                    tbx.ForeColor = foregroundColour;
                    tbx.BackColor = backgroundColour;
                }
                catch { }

                try
                {
                    ListView lvw = (ListView)ctl;
                    lvw.BackColor = backgroundColour;
                    lvw.ForeColor = foregroundColour;

                }
                catch { }

                try
                {
                    CheckedListBox lbx = (CheckedListBox)ctl;
                    lbx.BackColor = backgroundColour;
                    lbx.ForeColor = foregroundColour;
                }
                catch { }

                try
                {
                    RichTextBox rtb = (RichTextBox)ctl;
                    rtb.BackColor = backgroundColour;
                    rtb.ForeColor = foregroundColour;
                    
                }
                catch { }

                try
                {
                    DateTimePicker dtp = (DateTimePicker)ctl;
                    dtp.CalendarTitleBackColor = backgroundColour;
                    dtp.CalendarTitleForeColor = foregroundColour;
                    dtp.CalendarForeColor = foregroundColour;
                    dtp.CalendarTrailingForeColor = foregroundColour;
                }
                catch { }

                try
                {
                    NumericUpDown nud = (NumericUpDown)ctl;
                    nud.BackColor = backgroundColour;
                    nud.ForeColor = foregroundColour;
                }
                catch { }

                try
                {
                    RadioButton rbn = (RadioButton)ctl;
                    rbn.ForeColor = System.Drawing.Color.Orange;
                    rbn.BackColor = backgroundColour;
                }
                catch { }

                try
                {
                    Forms.StylesUITreeView tvw = (Forms.StylesUITreeView)ctl;
                    tvw.BackColor = backgroundColour;
                    tvw.ForeColor = foregroundColour;
                    tvw.LineColor = System.Drawing.Color.Orange;
                }
                catch { }
            }
            Globals.ThisAddIn.SuppressEvents = false;

        }

        /// <summary>
        /// Introduced for mobile device resolutions - moves the controls on the task pane to create a "page" feel
        /// </summary>
        /// <param name="control">the taskpane in focus</param>
        /// <param name="page">the page number to determine which controls are visible/to be moved</param>
        public void ModifyForScreenResolution(Microsoft.Office.Tools.CustomTaskPane control, int page)
        {

            if (control == null)
                return;
            int ctlHeight = control.Height;
            int ctlWidth = control.Width;
            Word.Window win = (Word.Window)control.Window;
            int resHeight = win.Height;
            int lowerLimit = (ctlHeight - 50) * (page - 1);
            int upperLimit = (ctlHeight - 50) * page;
  
            int y = ResetControlLocations(control.Control.Name, control);
            int pageCount = Convert.ToInt16((double)y/((double)ctlHeight-100));
            

            
            foreach (Control ctl in control.Control.Controls)
            {
                if (pageCount > 1)
                {
                    if (ctl.Name != "btnPage")
                        CheckControlLocation(ctl, control.Control.Name); //Some controls seem to snap to grid when moved and the location isn't correct
                    if (ctl.Location.Y < lowerLimit || ctl.Location.Y + ctl.Height > upperLimit)
                    {
                        ctl.Visible = false;
                    }
                    else
                    {

                        ctl.Location = new Point(ctl.Location.X, ctl.Location.Y - lowerLimit);
                        ctl.Visible = true;
                    }
                }
                else
                    ctl.Visible = true;
            }
            


            
                CreateButton(control.Control, page, pageCount, ctlHeight, ctlWidth);

        }

        /// <summary>
        /// Checks to see if the control has been moved back to its original position
        /// </summary>
        /// <param name="control">a control on the task pane</param>
        /// <param name="userControlName">the control's parent usercontrol name</param>
        private void CheckControlLocation(Control control, String userControlName)
        {
            IEnumerable<ControlCollection> col = from d in Globals.ThisAddIn.ControlsCollection where d.UserControlName == userControlName select d;
            ControlCollection collection = null;
            foreach (ControlCollection d in col)
                collection = d;
            IEnumerable<StoredControl> sc = from d in collection where d.ControlName == control.Name select d;
            StoredControl c = null;
            foreach (StoredControl s in sc)
                c = s;
            if (control.Location != c.Location)
                control.Location = c.Location;

        }
        
        /// <summary>
        /// Creates a next page button to scroll through the pages on the taskpane
        /// </summary>
        /// <param name="parent">the usercontrol used by the task pane</param>
        /// <param name="currentPage">the current page number</param>
        /// <param name="pageCount">the number of pages the task pane requires</param>
        /// <param name="height">the height of the task pane at a lower resolution</param>
        /// <param name="width">the width of the task pane at a lower resolution</param>
        private void CreateButton(UserControl parent, int currentPage, int pageCount, int height, int width)
        {
            
            Control[] ctl = parent.Controls.Find("btnPage",true);
            if(ctl !=null)
                foreach(Control c in ctl)
                    parent.Controls.Remove(c);
            if (pageCount == 1)
                return;
            Button button = new Button();
            button.Name = "btnPage";
            if (currentPage == pageCount)
                button.Text = "<<";
            else
                button.Text = ">>";
            button.Location = new Point(3*width/4, height - 50);
            button.Size = new Size(30, 18);
            button.Click += new EventHandler(button_Click);
            button.UseVisualStyleBackColor = true;
            button.Tag = currentPage + 1;
            parent.Controls.Add(button);
            parent.Refresh();
        }

        /// <summary>
        /// Event for the newly create page change button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (button.Text == "<<")
                ModifyForScreenResolution(clsWordApp.GetActivePane(), 1);
            else
                ModifyForScreenResolution(clsWordApp.GetActivePane(), Convert.ToInt16(button.Tag));
        }

        /// <summary>
        /// Resets the positions of the controls on the taskpane
        /// </summary>
        /// <param name="userControlName">the name of the user control on the task pane</param>
        /// <param name="control">the task pane</param>
        /// <returns></returns>
        private int ResetControlLocations(String userControlName, Microsoft.Office.Tools.CustomTaskPane control)
        {
            IEnumerable<ControlCollection> col = from d in Globals.ThisAddIn.ControlsCollection  where d.UserControlName == userControlName select d;
            ControlCollection collection = null;
            foreach(ControlCollection d in col)
                collection = d;
            int y = 0;
            foreach (StoredControl ctl in collection)
            {
                Control c = control.Control.Controls.Find(ctl.ControlName, true)[0];
                if(c.Location != ctl.Location)
                { 
                    c.Left = ctl.Location.X;
                    c.Top = ctl.Location.Y;
                }
                c.Refresh();
                
                if (ctl.Location.Y + ctl.Size.Height > y)
                    y = ctl.Location.Y + ctl.Size.Height;

            }

            return y;

        }

                
    }

    /// <summary>
    /// Two classes used to store the original sizes and locations of the controls on a task pane
    /// </summary>
    public class ControlCollection: List<StoredControl>
    {
        
        public String UserControlName{get;set;}
        
        public ControlCollection(String userControlName)
        { 
            UserControlName = userControlName;
        }

    }

    
    
    
    public class StoredControl
    {
        public String ControlName { get; set; }
        public Point Location { get; set; }
        public Size Size { get; set; }
        
        public StoredControl(String controlName, Point location, Size size)
        {
            ControlName = controlName;
            Location = location;
            Size = size;
        }
    }

    

    public class ControlTagCollection : List<ControlTag>
    {
        public ControlTagCollection()
        { }
    }

    public class ListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ObjectCompare;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the CaseInsensitiveComparer object
            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);

            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }

    }
}
