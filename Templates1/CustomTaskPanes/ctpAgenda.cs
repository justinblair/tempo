﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1.CustomTaskPanes
{
    /// <summary>
    /// The agenda taskpane which doubles up for minutes as well
    /// </summary>
    public partial class ctpAgenda : UserControl
    {
        private clsInterAction IA;
        
        public ctpAgenda()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Loads the agenda taskpane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctpAgenda_Load(object sender, EventArgs e)
        {
            cboType.Items.Add("Agenda");
            cboType.Items.Add("Minutes");
            lblInvited.TextChanged += new EventHandler(lblInvited_TextChanged);
            this.lvwTo.Columns.Add("Invited/Attended");
            this.lvwApologies.Columns.Add("Apologies");
            lvwApologies.View = View.Details;
            lvwTo.View = View.Details;
            lvwApologies.Columns[0].Width = lvwApologies.Width;
            lvwTo.Columns[0].Width = lvwTo.Width;
            
            InterActionOnLine iaol = new InterActionOnLine();
            Boolean online = iaol.IsOnline;
            btnToLookup.Enabled = online;
            btnApologiesLookup.Enabled = online;
        }

        /// <summary>
        /// Triggered when the invited listview is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        void lblInvited_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Updates the content control in the document with text from the target control
        /// </summary>
        /// <param name="sender"></param>
        private void TextBoxUpdate(object sender)
        {
            Control tbx = (Control)sender;
            Word.ContentControl ctl = clsWordApp.getContentControl(tbx.Tag.ToString(), false);
            if (ctl != null)
                ctl.Range.Text = tbx.Text;
        }

        /// <summary>
        /// Triggered when the type selection box is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Word.ContentControl control;
            TextBoxUpdate(sender);
            this.tbxMinutes.Enabled = this.cboType.SelectedIndex == 1;
            foreach (Control ctl in this.Controls)
                if (ctl.Name.ToLower().Contains("apologies"))
                    ctl.Enabled = this.cboType.SelectedIndex == 1;
            if (this.cboType.SelectedIndex == 1) //Minutes
            {
                control = clsWordApp.getContentControl("minutesTakenBy", false);
                if (control != null) control.DropdownListEntries[1].Select();
                control = clsWordApp.getContentControl("minsTakenBy", false);
                if (control != null) control.Range.Text = tbxMinutes.Text;
                lblInvited.Text = "Attended";

            }
            else //Agenda
            {
                control = clsWordApp.getContentControl("minutesTakenBy", false);
                if (control != null) control.Range.Text = "";
                control = clsWordApp.getContentControl("minsTakenBy", false);
                if (control != null) control.Range.Text = "";
                lblInvited.Text = "Invited";
            }

        }

        /// <summary>
        /// Triggered when then title box is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxTitle_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the date/time control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when then the time box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxTime_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the location box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxLocation_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the next meeting date/time picker is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void dtpNextMeeting_ValueChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Triggered when the minutes text box is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbxMinutes_TextChanged(object sender, EventArgs e)
        {
            TextBoxUpdate(sender);
        }

        /// <summary>
        /// Repopyulates the agenda/minutes taskpane with data from the document when it is opened
        /// </summary>
        public void RepopulateAgendaTaskPane()
        {
            foreach (System.Windows.Forms.Control control in this.Controls)
            {
                if (control.Tag != null)
                {
                    Word.ContentControl ctl = clsWordApp.getContentControl(control.Tag.ToString(), false);
                    try
                    {
                        if (!ctl.ShowingPlaceholderText)
                            control.Text = ctl.Range.Text;
                    }
                    catch { }
                }
            }

            RepopulateListView(this.lvwTo, true);
            RepopulateListView(this.lvwApologies, false);

        }

        /// <summary>
        /// Repopulated the target list view box with data from the document
        /// </summary>
        /// <param name="lbx"></param>
        /// <param name="ToList"></param>
        
        private void RepopulateListView(ListView lbx, Boolean ToList)
        {
            char[] paragraph = Microsoft.VisualBasic.Constants.vbCrLf.ToCharArray();
            Word.ContentControl ctl;
            String[] to = null;
            
            if (ToList)
            {
                ctl = clsWordApp.getContentControl("invited", false);
                if (!ctl.ShowingPlaceholderText)
                to = ctl.Range.Text.Split(paragraph);
                
            }
            else
            {
                ctl = clsWordApp.getContentControl("apologies", false);
                if (!ctl.ShowingPlaceholderText)
                to = ctl.Range.Text.Split(paragraph);
               
            }
            int i = -1;
            if (to !=null)
            foreach (String item in to)
            {
                i++;
                if (to[i].Length > 0)
                {
                    ListViewItem lvw = new ListViewItem(item);
                    lbx.Items.Add(lvw);
                }
            }
        }

        private void lblInvited_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Triggered when the lookup button is pressed.  Accesses InterAction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxInvited.Text = icontact.FullName;
            this.lvwTo.SelectedItems.Clear();
        }

        /// <summary>
        /// Gets a contact from InterAction by search
        /// </summary>
        /// <returns></returns>
        private InterAction.IAContact GetContact()
        {
            if (IA == null)
                IA = new clsInterAction();
            InterAction.IAContact icontact;
            icontact = IA.GetContact();
            return icontact;
        }

        /// <summary>
        /// Triggered when the apologies lookup button is clicked.  Retrieves a contact from InterAction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnApologiesLookup_Click(object sender, EventArgs e)
        {
            InterAction.IAContact icontact = GetContact();
            if (icontact == null) return;
            this.tbxApologies.Text = icontact.FullName;
            this.lvwApologies.SelectedItems.Clear();
        }

        /// <summary>
        /// Triggered when the add button is clicked.  Adds an item to the invited list view control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToAdd_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(this.tbxInvited.Text);
            this.lvwTo.Items.Add(lvi);
            this.lvwTo.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwTo.Focus();
            UpdateToLists("invited", lvwTo);
        }

        /// <summary>
        /// Updates the content control in the document with data from the target listview control
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="lvw"></param>
        
        private void UpdateToLists(String tag, ListView lvw)
        {
            Word.ContentControl ctl = clsWordApp.getContentControl(tag, false);
            String buffer = "";
            foreach (ListViewItem lvi in lvw.Items)
                buffer = buffer + lvi.Text + Microsoft.VisualBasic.Constants.vbCrLf;
            if (ctl != null)
                ctl.Range.Text = buffer;
  
        }

        /// <summary>
        /// Triggered when the add button for apologies is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddApologies_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem(this.tbxApologies.Text);
            this.lvwTo.Items.Add(lvi);
            this.lvwTo.SelectedItems.Clear();
            lvi.Selected = true;
            this.lvwApologies.Focus();
            UpdateToLists("apologies", lvwApologies);
        }

        /// <summary>
        /// Removes an item from the listview box when the remove button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToRemove_Click(object sender, EventArgs e)
        {
            if (this.lvwTo.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwTo.SelectedItems)
                    this.lvwTo.Items.Remove(lvi);
                UpdateToLists("invited", lvwTo);
            }
        }

        /// <summary>
        /// Triggered when the remove button is clicked for apologies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnRemoveApologies_Click(object sender, EventArgs e)
        {
            if (this.lvwApologies.SelectedItems.Count > 0)
            {
                foreach (ListViewItem lvi in this.lvwApologies.SelectedItems)
                    this.lvwApologies.Items.Remove(lvi);
                UpdateToLists("apologies", lvwApologies);
            }
        }

        /// <summary>
        /// Triggered when the update button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnUpdateTo_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwTo.SelectedItems[0];
            int index = lvi.Index;
            this.lvwTo.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxInvited.Text);
            this.lvwTo.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateToLists("invited", lvwTo);
        }

        /// <summary>
        /// Triggered when the update apologies button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnUpdateApologies_Click(object sender, EventArgs e)
        {
            if (lvwApologies.SelectedItems.Count == 0)
                return;
            ListViewItem lvi = this.lvwApologies.SelectedItems[0];
            int index = lvi.Index;
            this.lvwApologies.Items.Remove(lvi);
            lvi = new ListViewItem(this.tbxApologies.Text);
            this.lvwApologies.Items.Insert(index, lvi);
            lvi.Selected = true;
            UpdateToLists("apologies", lvwApologies);
        }

        /// <summary>
        /// Triggered when the apologies button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnToApologies_Click(object sender, EventArgs e)
        {
            if (lvwApologies.SelectedItems.Count == 0)
                return;
            foreach (ListViewItem lvi in lvwApologies.SelectedItems)
            {           
                lvwApologies.Items.Remove(lvi);
                lvwTo.Items.Add(lvi);
            }
            UpdateToLists("apologies", lvwApologies);
            UpdateToLists("invited", lvwTo);
        }

        /// <summary>
        /// Triggered when the from apologies button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void btnFromApologies_Click(object sender, EventArgs e)
        {
            if (lvwTo.SelectedItems.Count == 0)
                return;
            foreach (ListViewItem lvi in lvwTo.SelectedItems)
            {
                lvwTo.Items.Remove(lvi);
                lvwApologies.Items.Add(lvi);
            }
            UpdateToLists("apologies", lvwApologies);
            UpdateToLists("invited", lvwTo);
        }

        /// <summary>
        /// Triggered when the listview control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void lvwTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lvwTo.SelectedItems.Count>0)
            this.tbxInvited.Text = lvwTo.SelectedItems[0].Text;
            
        }

        /// <summary>
        /// Triggered when the apologies list view control is updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void lvwApologies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvwApologies.SelectedItems.Count > 0)
                this.tbxApologies.Text = lvwApologies.SelectedItems[0].Text;
            Word.ContentControl ctl = clsWordApp.getContentControl("apologiesLabel", false);
            if (lvwApologies.Items.Count == 0)
                ctl.Range.Text = "";
            else
                ctl.DropdownListEntries[1].Select();
            

        }
    }
}
