﻿namespace Templates1.CustomTaskPanes
{
    partial class ctpCourt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxClaimNumber = new CustomTextBox();
            this.cboClaim = new CustomTaskPanes.CustomComboBox();
            this.cboCourt = new CustomTaskPanes.CustomComboBox();
            this.cboDivision = new CustomTaskPanes.CustomComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxParty1 = new CustomTextBox();
            this.cboClaimant = new CustomTaskPanes.CustomComboBox();
            this.cboDefendants = new CustomTaskPanes.CustomComboBox();
            this.tbxDefendants = new CustomTextBox();
            this.tbxTitle = new CustomTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxRef = new CustomTextBox();
            this.cboFax = new CustomTaskPanes.CustomComboBox();
            this.tbxEmail = new CustomTextBox();
            this.cboClaimDefendant = new CustomTaskPanes.CustomComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pbxDivider = new System.Windows.Forms.PictureBox();
            this.chkCover = new System.Windows.Forms.CheckBox();
            this.chkBack = new System.Windows.Forms.CheckBox();
            this.btnResetCover = new System.Windows.Forms.Button();
            this.btnResetBack = new System.Windows.Forms.Button();
            this.pbCover = new Templates1.Forms.NewProgressBar();
            this.pbBack = new Templates1.Forms.NewProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 101;
            this.label1.Text = "Type/Number";
            // 
            // tbxClaimNumber
            // 
            this.tbxClaimNumber.Location = new System.Drawing.Point(214, 52);
            this.tbxClaimNumber.Name = "tbxClaimNumber";
            this.tbxClaimNumber.Size = new System.Drawing.Size(88, 20);
            this.tbxClaimNumber.TabIndex = 100;
            this.tbxClaimNumber.Tag = "claimNumber";
            // 
            // cboClaim
            // 
            this.cboClaim.FormattingEnabled = true;
            this.cboClaim.Location = new System.Drawing.Point(96, 51);
            this.cboClaim.Name = "cboClaim";
            this.cboClaim.Size = new System.Drawing.Size(102, 21);
            this.cboClaim.TabIndex = 103;
            this.cboClaim.Tag = "claim";
            // 
            // cboCourt
            // 
            this.cboCourt.FormattingEnabled = true;
            this.cboCourt.Location = new System.Drawing.Point(96, 78);
            this.cboCourt.Name = "cboCourt";
            this.cboCourt.Size = new System.Drawing.Size(206, 21);
            this.cboCourt.TabIndex = 104;
            this.cboCourt.Tag = "court";
            // 
            // cboDivision
            // 
            this.cboDivision.FormattingEnabled = true;
            this.cboDivision.Location = new System.Drawing.Point(96, 105);
            this.cboDivision.Name = "cboDivision";
            this.cboDivision.Size = new System.Drawing.Size(206, 21);
            this.cboDivision.TabIndex = 105;
            this.cboDivision.Tag = "division";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 106;
            this.label2.Text = "Court";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 107;
            this.label3.Text = "Division";
            // 
            // tbxParty1
            // 
            this.tbxParty1.Location = new System.Drawing.Point(96, 141);
            this.tbxParty1.Name = "tbxParty1";
            this.tbxParty1.Size = new System.Drawing.Size(206, 20);
            this.tbxParty1.TabIndex = 108;
            this.tbxParty1.Tag = "party1";
            // 
            // cboClaimant
            // 
            this.cboClaimant.FormattingEnabled = true;
            this.cboClaimant.Location = new System.Drawing.Point(96, 167);
            this.cboClaimant.Name = "cboClaimant";
            this.cboClaimant.Size = new System.Drawing.Size(206, 21);
            this.cboClaimant.TabIndex = 109;
            this.cboClaimant.Tag = "claimants";
            // 
            // cboDefendants
            // 
            this.cboDefendants.FormattingEnabled = true;
            this.cboDefendants.Location = new System.Drawing.Point(96, 229);
            this.cboDefendants.Name = "cboDefendants";
            this.cboDefendants.Size = new System.Drawing.Size(206, 21);
            this.cboDefendants.TabIndex = 111;
            this.cboDefendants.Tag = "defendants";
            // 
            // tbxDefendants
            // 
            this.tbxDefendants.Location = new System.Drawing.Point(96, 203);
            this.tbxDefendants.Name = "tbxDefendants";
            this.tbxDefendants.Size = new System.Drawing.Size(206, 20);
            this.tbxDefendants.TabIndex = 110;
            this.tbxDefendants.Tag = "party2";
            // 
            // tbxTitle
            // 
            this.tbxTitle.Location = new System.Drawing.Point(96, 267);
            this.tbxTitle.Name = "tbxTitle";
            this.tbxTitle.Size = new System.Drawing.Size(206, 20);
            this.tbxTitle.TabIndex = 112;
            this.tbxTitle.Tag = "title";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 113;
            this.label4.Text = "Claimant";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 114;
            this.label5.Text = "Defendant";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 115;
            this.label7.Text = "Title";
            // 
            // tbxRef
            // 
            this.tbxRef.Location = new System.Drawing.Point(96, 426);
            this.tbxRef.Name = "tbxRef";
            this.tbxRef.Size = new System.Drawing.Size(206, 20);
            this.tbxRef.TabIndex = 116;
            this.tbxRef.Tag = "reference";
            // 
            // cboFax
            // 
            this.cboFax.FormattingEnabled = true;
            this.cboFax.Location = new System.Drawing.Point(96, 399);
            this.cboFax.Name = "cboFax";
            this.cboFax.Size = new System.Drawing.Size(206, 21);
            this.cboFax.TabIndex = 117;
            this.cboFax.Tag = "firmFax";
            // 
            // tbxEmail
            // 
            this.tbxEmail.Location = new System.Drawing.Point(96, 452);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(206, 20);
            this.tbxEmail.TabIndex = 118;
            this.tbxEmail.Tag = "mail";
            // 
            // cboClaimDefendant
            // 
            this.cboClaimDefendant.FormattingEnabled = true;
            this.cboClaimDefendant.Location = new System.Drawing.Point(96, 496);
            this.cboClaimDefendant.Name = "cboClaimDefendant";
            this.cboClaimDefendant.Size = new System.Drawing.Size(206, 21);
            this.cboClaimDefendant.TabIndex = 119;
            this.cboClaimDefendant.Tag = "claimantDefendant";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 402);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 120;
            this.label8.Text = "Fax";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 431);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 121;
            this.label9.Text = "Reference";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 459);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 122;
            this.label10.Text = "Email";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 499);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 123;
            this.label11.Text = "Representing";
            // 
            // pbxDivider
            // 
            this.pbxDivider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbxDivider.Location = new System.Drawing.Point(19, 349);
            this.pbxDivider.Name = "pbxDivider";
            this.pbxDivider.Size = new System.Drawing.Size(293, 1);
            this.pbxDivider.TabIndex = 125;
            this.pbxDivider.TabStop = false;
            // 
            // chkCover
            // 
            this.chkCover.AutoSize = true;
            this.chkCover.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.chkCover.Location = new System.Drawing.Point(21, 18);
            this.chkCover.Name = "chkCover";
            this.chkCover.Size = new System.Drawing.Size(92, 17);
            this.chkCover.TabIndex = 127;
            this.chkCover.Text = "Cover Page";
            this.chkCover.UseVisualStyleBackColor = true;
            this.chkCover.CheckedChanged += new System.EventHandler(this.chkCover_CheckedChanged);
            // 
            // chkBack
            // 
            this.chkBack.AutoSize = true;
            this.chkBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.chkBack.Location = new System.Drawing.Point(21, 365);
            this.chkBack.Name = "chkBack";
            this.chkBack.Size = new System.Drawing.Size(88, 17);
            this.chkBack.TabIndex = 128;
            this.chkBack.Text = "Back Page";
            this.chkBack.UseVisualStyleBackColor = true;
            this.chkBack.CheckedChanged += new System.EventHandler(this.chkBack_CheckedChanged);
            // 
            // btnResetCover
            // 
            this.btnResetCover.Location = new System.Drawing.Point(16, 302);
            this.btnResetCover.Name = "btnResetCover";
            this.btnResetCover.Size = new System.Drawing.Size(97, 30);
            this.btnResetCover.TabIndex = 129;
            this.btnResetCover.Text = "Reset";
            this.btnResetCover.UseVisualStyleBackColor = true;
            this.btnResetCover.Click += new System.EventHandler(this.btnResetCover_Click);
            // 
            // btnResetBack
            // 
            this.btnResetBack.Location = new System.Drawing.Point(16, 536);
            this.btnResetBack.Name = "btnResetBack";
            this.btnResetBack.Size = new System.Drawing.Size(97, 30);
            this.btnResetBack.TabIndex = 130;
            this.btnResetBack.Text = "Reset";
            this.btnResetBack.UseVisualStyleBackColor = true;
            this.btnResetBack.Click += new System.EventHandler(this.btnResetBack_Click);
            // 
            // pbCover
            // 
            this.pbCover.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pbCover.Location = new System.Drawing.Point(128, 17);
            this.pbCover.Name = "pbCover";
            this.pbCover.Size = new System.Drawing.Size(138, 17);
            this.pbCover.TabIndex = 134;
            this.pbCover.Value = 1;
            // 
            // pbBack
            // 
            this.pbBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.pbBack.Location = new System.Drawing.Point(128, 363);
            this.pbBack.Name = "pbBack";
            this.pbBack.Size = new System.Drawing.Size(138, 17);
            this.pbBack.TabIndex = 135;
            this.pbBack.Value = 1;
            // 
            // ctpCourt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pbBack);
            this.Controls.Add(this.pbCover);
            this.Controls.Add(this.btnResetBack);
            this.Controls.Add(this.btnResetCover);
            this.Controls.Add(this.chkBack);
            this.Controls.Add(this.chkCover);
            this.Controls.Add(this.pbxDivider);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboClaimDefendant);
            this.Controls.Add(this.tbxEmail);
            this.Controls.Add(this.cboFax);
            this.Controls.Add(this.tbxRef);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxTitle);
            this.Controls.Add(this.cboDefendants);
            this.Controls.Add(this.tbxDefendants);
            this.Controls.Add(this.cboClaimant);
            this.Controls.Add(this.tbxParty1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboDivision);
            this.Controls.Add(this.cboCourt);
            this.Controls.Add(this.cboClaim);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxClaimNumber);
            this.Name = "ctpCourt";
            this.Size = new System.Drawing.Size(345, 709);
            this.Load += new System.EventHandler(this.ctpCourt_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxDivider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CustomTextBox tbxClaimNumber;
        private CustomTaskPanes.CustomComboBox cboClaim;
        private CustomTaskPanes.CustomComboBox cboCourt;
        private CustomTaskPanes.CustomComboBox cboDivision;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private CustomTextBox tbxParty1;
        private CustomTaskPanes.CustomComboBox cboClaimant;
        private CustomTaskPanes.CustomComboBox cboDefendants;
        private CustomTextBox tbxDefendants;
        private CustomTextBox tbxTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private CustomTextBox tbxRef;
        private CustomTaskPanes.CustomComboBox cboFax;
        private CustomTextBox tbxEmail;
        private CustomTaskPanes.CustomComboBox cboClaimDefendant;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pbxDivider;
        private System.Windows.Forms.CheckBox chkCover;
        private System.Windows.Forms.CheckBox chkBack;
        private System.Windows.Forms.Button btnResetCover;
        private System.Windows.Forms.Button btnResetBack;
        private Forms.NewProgressBar pbCover;
        private Forms.NewProgressBar pbBack;
    }
}
