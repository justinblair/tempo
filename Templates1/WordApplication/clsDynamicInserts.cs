﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.VisualBasic;

namespace Templates1
{
    class Watermark
    {
        private Word.Document document;
        private String text;
        private List<Word.Shape> shapes;

        public Word.Document Document
        {
            get { return document; }
            set { document = value; }
        }
        

        
        public void SetText(String text)
        {
            
            foreach(Word.Section sect in this.document.Sections)
                foreach(Word.HeaderFooter hdr in sect.Headers)
                    foreach(Word.Shape shp in hdr.Shapes)
                    {
                        if (shp.Name.Contains("Watermark"))
                            shp.TextEffect.Text = text;
                    }
            
            
            
            //if(this.shapes!=null)
            //{
            //    foreach (Word.Shape shp in this.shapes)
            //        try
            //        {
            //            shp.TextEffect.Text = text;
            //        }
            //        catch { }
            //}
        }
        
        
        public String Text
        {
            get { return text; }
            set { 
                    text = value;
                    foreach (Word.Shape shp in this.shapes)
                        shp.TextEffect.Text = text;
            }
        }
  
        public List<Word.Shape> Shapes
        {
            get { return shapes; }
            set { shapes = value; }
        }

        public Watermark(Word.Document doc)
        {
            this.document = doc;
            this.shapes = new List<Word.Shape>();

        }

        public void Remove()
        {

            foreach (Word.Section sect in this.document.Sections)
                foreach (Word.HeaderFooter hdr in sect.Headers)
                    for (int i = hdr.Shapes.Count; i > 0; i-- )
                    {
                        Word.Shape shp = hdr.Shapes[i];
                        if (shp.Name.Contains("Watermark"))
                            shp.Delete();
                    }


                        //for (int i = this.shapes.Count - 1; i >= 0; i--)
                        //{
                        //    this.Shapes[i].Delete();
                        //    //this.Shapes.Remove(this.Shapes[i]);
                        //}
            this.shapes = null; 
        }

        public void Add(String text)
        {
            this.text = text;
            int i = 0;
            foreach (Word.Section sect in this.document.Sections)
                foreach (Word.HeaderFooter hdr in sect.Headers)
                {
                    i++;
                    Word.Shape shp = hdr.Shapes.AddTextEffect(Microsoft.Office.Core.MsoPresetTextEffect.msoTextEffect1, text, "Gill Sans MT", 2, 0, 0, 0, 0);
                    shp.Name = "Watermark" + i;
                    shp.Rotation = 306;
                    //shp.Height = 90.70866F;
                    shp.Height = 90;
                    shp.Width = sect.PageSetup.PageHeight;
                    //shp.TextEffect.FontSize = 72;
                    shp.Line.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                    shp.Fill.Solid();
                    shp.Fill.ForeColor.RGB = 12632256;                                                 
                    if (shp.Width > 586.7717F)
                        shp.Width = 586.7717F;
                    shp.RelativeHorizontalPosition = Word.WdRelativeHorizontalPosition.wdRelativeHorizontalPositionPage;
                    shp.RelativeVerticalPosition = Word.WdRelativeVerticalPosition.wdRelativeVerticalPositionPage;
                    shp.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoTrue;
                    //shp.ScaleWidth(20, Microsoft.Office.Core.MsoTriState.msoFalse);
                    //shp.ScaleHeight(20, Microsoft.Office.Core.MsoTriState.msoFalse);
                    shp.TextEffect.Alignment = Microsoft.Office.Core.MsoTextEffectAlignment.msoTextEffectAlignmentCentered;
                    
                    shp.Top = (float)Word.WdShapePosition.wdShapeCenter;
                    shp.Left = (float)Word.WdShapePosition.wdShapeCenter;
                    shapes.Add(shp);
                }
        }
    }
    
    
    class clsDynamicInserts
    {
        private Word.Document sourceDocument;
        private Word.Document endDocument;
        public String BuildingBlocksPath = System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS, "Building Blocks.dotx");
    

        public static void ShowHideLogo(Word.Document doc, Boolean hide)
        {
            if (clsWordApp.GetTemplateName(doc).ToLower().Contains("letter") || clsWordApp.GetTemplateName(doc).ToLower().Contains("invoice"))
            foreach (Word.Section sect in doc.Sections)
            { 
                foreach (Word.HeaderFooter hdr in sect.Headers)
                    foreach (Word.Shape shp in hdr.Shapes)
                        if (shp.Title == "MdRLogo")
                        if (hide)
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                        else
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;

            foreach (Word.HeaderFooter hdr in sect.Footers)
                foreach (Word.Shape shp in hdr.Shapes)
                    if (shp.Title == "MdRLogo")
                    if (hide)
                        shp.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                    else
                        shp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
            }
            doc.Application.ScreenRefresh();
        }
        
        
        public void ImportNonLegalDocumentType()
        {
            sourceDocument = Globals.ThisAddIn.Application.ActiveDocument;
            endDocument = Globals.ThisAddIn.Application.Documents.Add(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.TEMPLATES, "MdRNonLegal.dotx"));
            Word.Range rng;
            rng = StartPoint();
            rng.Paragraphs[1].set_Style(endDocument.Styles["Normal"]);
            foreach (Word.Paragraph para in sourceDocument.Range().Paragraphs)
                if (para.Range.Text != "")
                {
                    
                    rng.InsertAfter(para.Range.Text);

                    rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                    rng.set_Style(endDocument.Styles["Normal"]);
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
                   
                    
                }
            for(int i = endDocument.Paragraphs.Count; i >0; i--)
            {
                if (endDocument.Paragraphs[i].Range.Text.Length<=2)
                    endDocument.Paragraphs[i].Range.Delete();
            }
        }

        private Word.Range StartPoint()
        {
            Word.Range rng = endDocument.Range(0, 0);


            do
            {
                rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);

            }

            while (rng.Information[Word.WdInformation.wdActiveEndPageNumber] < 2);
            return rng;


        }
        
        public static void UpdateDocumentReference(Word.Document doc)
        {
            // For new templates using a content control for the document reference
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            
            clsOfficeData officeData = Globals.ThisAddIn.Officedata;
            try
            {
               List<Word.ContentControl> list = clsWordApp.getContentControls("docRef", doc);
                foreach (Word.ContentControl ctl in list)
                {
                    if(officeData.IsNYOffice)
                         ctl.Range.Text = Globals.ThisAddIn.DMS.DatabaseAndDocumentNumberVersion;
                    else
                        ctl.Range.Text = Globals.ThisAddIn.DMS.DocumentNumberAndVersion;

                    ctl.Range.Font.Size = 8;
                }
            }
            catch { }
            //Novaplex document reference is in a field
            try
            {
                Word.Field field = null;
                foreach (Word.Range stry in doc.StoryRanges)
                    foreach (Word.Field fld in stry.Fields)
                        if (fld.Code.Text == "DMSLink.(Default).Reference")
                        {
                            field = fld;
                            break;
                        }

                String docNum = Globals.ThisAddIn.DMS.DocumentNumberAndVersion;
                if (field != null)
                    field.Result.Text = docNum;
            }
            catch { }
            //US Word Exchange uses a document reference applied to a field
            try
            {
                doc.CustomDocumentProperties["DocRef"].Value = Globals.ThisAddIn.DMS.DatabaseAndDocumentNumberVersion;
                foreach (Word.Range stry in doc.StoryRanges)
                    foreach (Word.Field fld in stry.Fields)
                        fld.Update();
            }
            catch { }

        }

        public static void SetLogo(Word.Document doc, HeaderLogo logo)
        {
            foreach (Word.Section sect in doc.Sections)
                foreach (Word.HeaderFooter hdr in sect.Headers)
                {
                    foreach (Word.Shape shp in hdr.Shapes)
                        if (shp.Title == logo.Name)
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                        else
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                    foreach (Word.ShapeRange shp in hdr.Range.InlineShapes)
                        if (shp.Title == logo.Name)
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoTrue;
                        else
                            shp.Visible = Microsoft.Office.Core.MsoTriState.msoFalse;
                }
        }
        
        public static void PopulateInvoiceNotes(Word.Document doc)
        {
            clsOfficeData OfficeData = Globals.ThisAddIn.Officedata;
            if(Globals.ThisAddIn.Application.Documents.Count>0)
            foreach( Word.Range story in doc.StoryRanges)
                foreach(Word.ContentControl cc in story.ContentControls)
                    switch(cc.Tag)
                    {
                        case "firmName":
                            cc.Range.Text = OfficeData.Offices.ActiveOffice.Fullname;
                            break;
                        case "bankAccount":
                            cc.Range.Text = OfficeData.Offices.ActiveOffice.BankAccount;
                            break;
                        case "customerServices":
                            cc.Range.Text = OfficeData.Offices.ActiveOffice.CustomerServices;
                            break;
                    }
        }

        
        public static void DropAddressBlock()
        {

            clsOfficeData OfficeData = Globals.ThisAddIn.Officedata;
            
            if(Globals.ThisAddIn.Application.Documents.Count>0)
            foreach( Word.Range story in Globals.ThisAddIn.Application.ActiveDocument.StoryRanges)
                foreach(Word.ContentControl cc in story.ContentControls)
            {
                
                    switch(cc.Tag)
            {

            
                case "firmName":
                cc.Range.Text = OfficeData.Offices.ActiveOffice.Fullname;
                break;

                case "firmAddress":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Address;
                    break;

                case "firmTelephone":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Telephone;
                    break;
                case "firmFax":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Fax;
                    break;

                case "firmMail":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Email;
                    break;
                case "rubric":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Rubric;
                    break;
                case "faxTransError":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.FaxTransError;
                    break;
                case "vat":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.VAT;
                    break;
                case "footerCentre":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.FooterCentre;
                    break;
                case "dx":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.DX;
                    break;
                case "web":
                    cc.Range.Text = OfficeData.Offices.ActiveOffice.Web;
                    break;
            
            }
        }
        }

        public static String GetAddress(XmlNode nodNode)
        {
            String strBuffer="";
            foreach(XmlNode node in nodNode.SelectNodes("address"))
                 strBuffer = strBuffer + node.InnerText + Constants.vbCrLf;
            
            return strBuffer;
        }

        public static void CreateCVContact(ADUser user)
        {
            Microsoft.Office.Interop.Word.ContentControl ctl = clsWordApp.getContentControl("name", true);
            if (ctl != null) ctl.Range.Text = user.FullName;
            ctl = clsWordApp.getContentControl("role", true);
            if (ctl != null) ctl.Range.Text = user.Title;
            ctl = clsWordApp.getContentControl("mail", true);
            if (ctl != null) ctl.Range.Text = user.Mail;
            ctl = clsWordApp.getContentControl("telephone", true);
            if (ctl != null) ctl.Range.Text = user.TelephoneNumber;
            
            SetPicture("picture", user);
            //SetPicture("pictureSmall", user);
        }

        public static void SetPicture(String ctlName, ADUser user)
        {
            String photoPath = @"\\Dc1-iaapp01\Employee Pics\" + user.Login + ".jpg";
            if (!System.IO.File.Exists(photoPath))
            { 
                //photoPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),@"Images\photonotavailable.jpg");
                photoPath = System.Environment.GetEnvironmentVariable("PROGRAMFILES") + @"\Mishcon De Reya\Mishcon de Reya Templates\Images\photonotavailable.jpg";
            }
            

            Microsoft.Office.Interop.Word.ContentControl ctl = clsWordApp.getContentControl(ctlName, true);
            Word.Range rng;
            if (ctl != null)
            {
                rng = ctl.Range;
                if (ctl.Range.InlineShapes.Count > 0)
                    ctl.Range.InlineShapes[1].Delete();
                Microsoft.Office.Interop.Word.InlineShape inl = Globals.ThisAddIn.Application.ActiveDocument.InlineShapes.AddPicture(photoPath, false, true, rng);

                switch (ctlName)
                {
                    case "picture":
                        inl.Width = 116;
                        inl.Height = 77;
                        break;

                    case "pictureSmall":
                        inl.Width = 76;
                        inl.Height = 53;
                        Word.Cell cell = inl.Range.Cells[1];
                        cell.Width = 76;
                        cell.TopPadding = 0;
                        cell.LeftPadding = 0;
                        cell.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalTop;
                        break;
                }
            }
            else
            {
                rng = Globals.ThisAddIn.Application.Selection.Tables[1].Cell(1, 1).Range;
                if (rng.InlineShapes.Count > 0)
                    rng.InlineShapes[1].Delete();
                Microsoft.Office.Interop.Word.InlineShape inl = Globals.ThisAddIn.Application.ActiveDocument.InlineShapes.AddPicture(photoPath, false, true, rng);
                switch(ctlName)
                {
                    //case "pictureSmall":
                    //    inl.Width = 76;
                    //    inl.Height = 53;
                    //    Word.Cell cell = inl.Range.Cells[1];
                    //    cell.Width = 76;
                    //    cell.TopPadding = 0;
                    //    cell.LeftPadding = 0;
                    //    cell.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalTop;
                    //    break;

                    case "picture":
                        inl.LockAspectRatio = Microsoft.Office.Core.MsoTriState.msoTrue;
                        inl.Width = 85.05F;
                        Word.Cell cell = inl.Range.Cells[1];
                        
                        cell.TopPadding = 0;
                        cell.LeftPadding = 0;
                        cell.RightPadding = 0;
                        cell.BottomPadding = 0;
                        cell.VerticalAlignment = Word.WdCellVerticalAlignment.wdCellAlignVerticalTop;
                        break;

                }
            }

        }

        public static void InsertCourtBackPage()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rngRange = doc.Range();
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            doc.Bookmarks.Add("CourtBackSheet", rngRange);
            rngRange.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            Word.Template tmp = doc.get_AttachedTemplate();
            Word.BuildingBlock bb = tmp.BuildingBlockEntries.Item("Court back sheet");
            bb.Insert(rngRange, true);
            DropAddressBlock();
            
        }

        public static void RemoveCourtBackPage()
        {
            if (!Globals.ThisAddIn.Application.ActiveDocument.Bookmarks.Exists("CourtBackSheet")) return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rngRange = doc.Bookmarks["CourtBackSheet"].Range;
            rngRange.End = doc.Range().End;
            rngRange.Delete();
        }

        public static void InsertFrontPage(String bbEntry)
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rngRange = doc.Range();
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            rngRange.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            doc.Bookmarks.Add("GotoTOC");
            rngRange = doc.Sections[1].Range;
            doc.Bookmarks.Add("FrontSheet", rngRange);
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            Word.Template tmp = doc.get_AttachedTemplate();
            Word.BuildingBlock bb = tmp.BuildingBlockEntries.Item(bbEntry);
            bb.Insert(rngRange, true);
            doc.Sections[2].PageSetup.DifferentFirstPageHeaderFooter = 0;
            foreach (Word.HeaderFooter htf in doc.Sections[2].Footers)
            { 
                htf.LinkToPrevious = false;
                htf.PageNumbers.StartingNumber = 1;
            }
            foreach (Word.HeaderFooter htf in doc.Sections[1].Footers)
                foreach (Word.PageNumber pgn in htf.PageNumbers)
                    pgn.Delete();

            if(doc.Bookmarks.Exists("ToC"))
            {
                rngRange = doc.Range(doc.Sections[2].Range.Start, doc.Bookmarks["ToC"].Range.End);
                doc.Bookmarks.Add("ToC", rngRange);
            }
        }

        public static void RemoveFrontPage()
        {
            if (!Globals.ThisAddIn.Application.ActiveDocument.Bookmarks.Exists("FrontSheet")) return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rngRange = doc.Bookmarks["FrontSheet"].Range;
            if (rngRange.Sections[1].Index != 1 || rngRange.Sections[1].Range.Information[Word.WdInformation.wdActiveEndPageNumber] != 1)
                if (System.Windows.Forms.MessageBox.Show("There appears to be more than the cover page in Section " + rngRange.Sections[1].Index + "." + 
                    Microsoft.VisualBasic.Constants.vbCrLf +  
                    "Do you wish to delete the entire section containing the cover page?",
                    Globals.ThisAddIn.APP_NAME1, System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        rngRange.Sections[1].Range.Delete();
                else{}

            else
                rngRange.Sections[1].Range.Delete();
        }

        public static void OnBuildingBlockLegalCover(Word.Document doc)
        {
            DropAddressBlock();
            Word.Section sect = doc.Sections[1];
            sect.PageSetup.DifferentFirstPageHeaderFooter = -1;
            sect.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 0;
        }

        public static void OnBuildingBlockCourtCover(Word.Document doc, Word.Range Range)
        {
            Word.Range rngRange = Range.Duplicate;
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngRange.MoveEnd(Word.WdUnits.wdParagraph, 1);
            rngRange.Delete();
            clsDynamicInserts.DropAddressBlock();
            doc.Bookmarks.Add("CourtBackSheet", Range);
        }

        public static void OnBuildingBlockNonLegalCover(Word.Document doc, Word.Range Range)
        {
            Word.Range rngRange = Range.Duplicate;
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngRange.MoveEnd(Word.WdUnits.wdCharacter, 1);
            rngRange.Delete();
            rngRange = Range.Duplicate;
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            doc.Sections.Add(rngRange);
            Word.Section sect = doc.Sections[1];
            sect.PageSetup.LeftMargin = 236.6929F;
            sect.PageSetup.RightMargin = 28.6363F;
            
            if(doc.Sections.Count>1)
            { 
            sect = doc.Sections[2];
            sect.PageSetup.DifferentFirstPageHeaderFooter = 0;
            sect.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.StartingNumber = 1;
            }
        }

        
        //public static void AddWatermark(String text)
        //{
        //    Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            
        //}

        //public static void RemoveWaterMarks()
        //{
        //     Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
        //     foreach (Word.Section sect in doc.Sections)
        //         foreach (Word.HeaderFooter hdr in sect.Headers)
        //             for (int i = hdr.Shapes.Count; i > 0; i--)
        //                 try
        //                 {
        //                     if (hdr.Shapes[i].TextEffect.Text != "")
        //                         hdr.Shapes[i].Delete();
        //                 }
        //                 catch { }
        //}

        //public Boolean HasWatermark
        //{
        //  get
        //    {
        //        try
        //        {
        //            if (Globals.ThisAddIn.Application.Documents.Count > 0)
        //            {
        //                Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
        //                foreach (Word.Section sect in doc.Sections)
        //                    foreach (Word.HeaderFooter hdr in sect.Headers)
        //                        for (int i = hdr.Shapes.Count; i > 0; i--)
        //                            try
        //                            {
        //                                if (hdr.Shapes[i].TextEffect.Text != "")
        //                                    return true;
        //                            }
        //                            catch { }
        //                return false;
        //            }
        //            return false;
        //        }
        //        catch { return false; }
        //    }
            
        //}

        public static void InsertNotesPage(Word.Document Doc)
        {
            Word.Range rng = clsTOC.GetPage(2);
            if (rng.Tables.Count > 0)
            {
                try
                {
                    rng.Tables[1].Split(rng.Rows[1]);
                }
                catch { }
            }

            Word.Template temp = GetBuildingBlocksTemplate();
            Word.BuildingBlock bb = temp.BuildingBlockEntries.Item("NOTES");
            rng = bb.Insert(rng, true);
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rng.InsertBreak(Word.WdBreakType.wdPageBreak);
            
            

        }

        
        private static Word.Template GetBuildingBlocksTemplate()
        {
            Word.Template temp = null;
            Globals.ThisAddIn.Application.Templates.LoadBuildingBlocks();
            foreach (Word.Template tmp in Globals.ThisAddIn.Application.Templates)
                if (tmp.FullName == System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS ,"Building Blocks.dotx"))
                { 
                    temp = tmp;
                    break;
                }
            return temp;
        }
        
        public static void RemoveNotesPage(Word.Document Doc)
        {
            Word.Range rng = Doc.Bookmarks["InvoiceNotes"].Range;
            rng.Delete();
            rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
            rng.Delete();
            
        }

        public static void DeleteAffidavitOfService()
        {
            Word.Document doc = Globals.ThisAddIn.PleadingDocument;
            if (doc.Bookmarks.Exists("bmkAffidavit"))
                doc.Bookmarks["bmkAffidavit"].Range.Delete();
        }
        
        
        public static Word.Document CreateAffidavitOfService()
        { 
            clsDynamicInserts csi = new clsDynamicInserts();
            XmlOffice office = csi.GetOffice();
            Language language = csi.GetLanguage();
            Template temp = csi.GetStandardDocTemplate();
            Document doc = Globals.ThisAddIn.WordApp.CreateDocument(temp, language, office, false);
            Word.Template tmp = doc.WordDocument.get_AttachedTemplate();
            Word.Range rng = doc.WordDocument.Range(0,0);
            
            Word.Range range = tmp.BuildingBlockEntries.Item("Affidavit of Service").Insert(rng);
            doc.WordDocument.Bookmarks.Add("bmkAffidavit", range);
            CustomTaskPanes.ctpBlueBack afds = Globals.ThisAddIn.WordApp.GenerateBlueBackPane("Affidavit of Service");
            afds.SetAffidavit();
            return doc.WordDocument;
        }

        public static Word.Document CreateAffirmationOfService()
        {
            clsDynamicInserts csi = new clsDynamicInserts();
            XmlOffice office = csi.GetOffice();
            Language language = csi.GetLanguage();
            Template temp = csi.GetStandardDocTemplate();
            Document doc = Globals.ThisAddIn.WordApp.CreateDocument(temp, language, office, false);
            Word.Template tmp = doc.WordDocument.get_AttachedTemplate();
            Word.Range rng = doc.WordDocument.Range(0, 0);

            Word.Range range = tmp.BuildingBlockEntries.Item("Affirmation of Service").Insert(rng);
            doc.WordDocument.Bookmarks.Add("bmkAffirmation", range);
            CustomTaskPanes.ctpBlueBack afds = Globals.ThisAddIn.WordApp.GenerateBlueBackPane("Affirmation of Service");
            afds.SetAffirmationOfService();
            return doc.WordDocument;
        }

        public Language GetLanguage()
        {
            return Globals.ThisAddIn.Officedata.Languages[Globals.ThisAddIn.Ribbonopts.Lang];
        }
        
        
        public XmlOffice GetOffice()
        {
            return Globals.ThisAddIn.Officedata.Offices[Globals.ThisAddIn.Ribbonopts.Office];
        }
        
        public Template GetStandardDocTemplate()
        {
            Template output = null;
            foreach(Template tmp in Globals.ThisAddIn.Officedata.Templates)
            {
                if (tmp.Filename.Contains("mdrLegalUS"))
            {
                output = tmp;
                break;
                }
            }
            
            
            return output;
        }

        public static void InsertCertificateOfService()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rng = doc.Paragraphs.Last.Range;
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            int start = rng.Start;
            rng.InsertBreak(Word.WdBreakType.wdPageBreak);
            rng.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            Word.Template temp = doc.get_AttachedTemplate();
            rng = temp.BuildingBlockEntries.Item("Certificate of Service").Insert(rng);
             int end = rng.End;
            doc.Bookmarks.Add("bmkCertOfService", doc.Range(start, end));
        }

        public static void RemoveCertificateOfService()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (doc.Bookmarks.Exists("bmkCertOfService"))
                doc.Bookmarks["bmkCertOfService"].Range.Delete();
        }
    }
}
