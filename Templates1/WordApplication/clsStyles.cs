﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office;

using System.Xml;

namespace Templates1
{
    
    public class StyleUIClone
    {
        public List<StyleGroupingClone> StyleGroupingsClone { get; set; }
        public StyleUIClone()
        {
            StyleGroupingsClone = new List<StyleGroupingClone>();
        }

        public void LoadStyleUI(StyleUI styleui)
        {
            
            foreach(StyleGrouping group in styleui.StyleGroupings)
            {
                StyleGroupingClone g = new StyleGroupingClone();
                g.Name = group.Name;
                g.Custom = group.Custom;
                g.Priority = group.Priority;
                foreach(Style style in group.Styles)
                {
                    StyleClone s = new StyleClone();
                    s.Name = style.Name;
                    s.Priority = style.Priority;
                    s.Visibility = style.Visibility;
                    g.Styles.Add(s);

                }
                StyleGroupingsClone.Add(g);
            }
        }
    }

    public class StyleClone
    {
        public int Priority { get; set; }
        public Boolean Visibility { get; set; }
        public String Name { get; set; }

        public StyleClone() { }
    }

    public class StyleGroupingClone
    {
        public int Priority { get; set; }
        public String Name { get; set; }
        public Boolean Custom { get; set; }
        public List<StyleClone> Styles { get; set; }
        public StyleGroupingClone() 
        {
            Styles = new List<StyleClone>();
        }
    }
    
    
    
    public class StyleUI
    {
        internal List<StyleGrouping> StyleGroupings { get; set; }
        private String[] builtinSearchStrings = { "Heading:1" };
        private String[] customSearchStrings = {"MdR Heading:2","MdR Level:3", "MdR Text:4", "MdR Body:4", "MdR Schedule:5", "MdR Article:6", "MdR Appendix:7" };
        private Document document;
        private StyleUIClone prefStyleUI;

        public StyleUIClone PrefStyleUI
        {
            get { return prefStyleUI; }
            set { prefStyleUI = value; }
        }

       public Document Document
        {
            get { return document; }
            set { document = value; }
        }

        internal StyleUI()
        {
            StyleGroupings = new List<StyleGrouping>();
            

        }

        internal void BuildStyleGroups()
        {
            foreach (Style style in document.Styles)
                style.Grouped = false;

            LoadDefaults(customSearchStrings, prefStyleUI, false);
            LoadDefaults(builtinSearchStrings, prefStyleUI, true);

            Styles rogueStyles = document.Styles.RogieStyles(document);
            if (rogueStyles.Count > 0)
            {
                StyleGrouping rogueGroup = new StyleGrouping(String.Empty);
                rogueGroup.Priority = 50;

                rogueGroup.Custom = true;
                rogueGroup.Name = "Rogue styles";
                rogueStyles = new Styles(rogueStyles.OrderBy(c => c.Name));
                foreach (Style rogue in rogueStyles)
                {
                    rogue.Priority = 50;
                    rogue.WordStyle.Priority = 50;
                    rogueGroup.Styles.Add(rogue);
                }
                StyleGroupings.Add(rogueGroup);

            }

            StyleGroupings = StyleGroupings.OrderBy(c => c.Priority).ToList();
        }

        internal void LoadDefaults(String[] customSearchStrings, StyleUIClone prefStyleUI, Boolean builtIn)
        {
            IEnumerable<StyleGroupingClone> prefGroup = null;
            int priority;
            Boolean visibility;

            foreach (String custom in customSearchStrings)
            {
                String searchString = custom.Split(new char[] { ':' })[0];

                StyleGrouping grouping = new StyleGrouping(searchString);

                grouping.Name = searchString + " styles";
                
                grouping.Custom = !builtIn;
                if (prefStyleUI != null)
                {
                    prefGroup = from d in prefStyleUI.StyleGroupingsClone where d.Name == grouping.Name select d;
                    
                    if (prefGroup.Count() > 0)
                        priority = prefGroup.ToList()[0].Priority;
                    else
                        priority = Convert.ToInt16(custom.Split(new char[] { ':' })[1]);
                }
                else
                    priority = Convert.ToInt16(custom.Split(new char[] { ':' })[1]);

                grouping.Priority = priority;

                IEnumerable<Style> styles = from d in this.document.Styles where d.Builtin == builtIn && d.Name.ToLower().Contains(searchString.ToLower()) select d;
                styles = styles.OrderBy(c => c.Name);
                foreach (Style style in styles)
                {

                    if (prefStyleUI != null)
                    {
                        IEnumerable<StyleClone> singleStyle = from d in prefGroup.ToList()[0].Styles where d.Name == style.Name select d;
                        if (singleStyle.Count() > 0)
                            visibility = singleStyle.ToList()[0].Visibility;
                        else
                            visibility = style.WordStyle.Visibility;
                    }
                    else
                        visibility = style.WordStyle.Visibility;
                    
                    style.Priority = priority;
                    style.WordStyle.Priority = priority;
                    style.WordStyle.Visibility = visibility;
                    style.Grouped = true;
                    grouping.Styles.Add(style);
                }
                StyleGroupings.Add(grouping);
            }

            IEnumerable<Style> customStyles = from d in document.Styles where d.Builtin == builtIn && !d.Grouped select d;
            StyleGrouping customGroup = new StyleGrouping(String.Empty);

            if (builtIn)
                customGroup.Name = "Other built-in styles";
            else
                customGroup.Name = "Other custom styles";
            customGroup.Custom = !builtIn;

            if (prefStyleUI != null)
            {
                prefGroup = from d in prefStyleUI.StyleGroupingsClone where d.Name == customGroup.Name select d;

                if (prefGroup.Count() > 0)
                    priority = prefGroup.ToList()[0].Priority;
                else
                    if (builtIn)
                        priority = 99;
                    else
                        priority = 40;
            }
            else
                if (builtIn)
                    priority = 99;
                else
                    priority = 40;


            customGroup.Priority = priority;
            customStyles = customStyles.OrderBy(c => c.Name);
            foreach (Style style in customStyles)
            {
                if (prefStyleUI != null)
                {
                    IEnumerable<StyleClone> singleStyle = from d in prefGroup.ToList()[0].Styles where d.Name == style.Name select d;
                    if (singleStyle.Count() > 0)
                        visibility = singleStyle.ToList()[0].Visibility;
                    else
                        visibility = style.WordStyle.Visibility;
                }
                else
                    visibility = style.WordStyle.Visibility;
                
                style.Priority = priority;
                style.WordStyle.Visibility = visibility;
                style.WordStyle.Priority = priority;
                style.Grouped = true;
                customGroup.Styles.Add(style);
            }
            StyleGroupings.Add(customGroup);
        }
    }
    
    
    public class StyleGrouping
    {
        internal Styles Styles { get; set; }
        internal int Priority { get; set; }
        internal Boolean Custom { get; set; }
        internal String Name { get; set; }
        private String searchString;

        internal StyleGrouping(String searchString)
        {
            this.searchString = searchString;
            Styles = new Styles();
        }
    }
    
    public class StyleMaps : List<StyleMap>
    {
        public StyleMaps()
        {

        }

        public StyleMaps(IEnumerable<StyleMap> maps) 
            : base (maps)
        {

        }
    }
    
    
    public class StyleMap
    {
        private Style source;

        public Style Source
        {
            get { return source; }
            set { source = value; }
        }
        private Style destination;

        public Style Destination
        {
            get { return destination; }
            set { destination = value; }
        }

        public StyleMap(Style source, Style destination)
        {
            this.source = source;
            this.destination = destination;
        }
    }
    
    public class Styles : List<Style>
    {
        public Styles()
        {

        }

        public Styles(IEnumerable<Style> styles)
           : base(styles)
        {
        }

        public Boolean RoguesExist(Document doc)
        {
            try
            {
                return doc.WordDocument.Styles.Count > doc.Styles.Count;
            }
            catch { return false; }
        }

        public Styles RogieStyles(Document doc)
        {
            Styles sts = new Styles();
            foreach (Word.Style style in doc.WordDocument.Styles)
            {
                IEnumerable<Style> result = from d in doc.Styles where d.Name == style.NameLocal select d;
                if (result.Count() == 0)
                {
                    Style s = new Style(style);
                    sts.Add(s);
                }
            }
            return sts;
        }
    }
    
    public class Style 
    {
        private String name;
        private Word.Style wordStyle;
        private int listLevelNumber;
        private Boolean bold;
        private Word.WdOutlineLevel outlineLevel;
        private Boolean probablyHeading;
        private Boolean probablyLevel;
        private Boolean probablyBody;
        private double leftindent;
        private Boolean builtin;
        private int priority;
        private Boolean visibility;
        private Word.WdStyleType type;
        private Word.WdListNumberStyle listnumberstyle;
        private Boolean appendix;
        private Boolean schedule;
        private String themeID;
        private Boolean grouped;
        private Word.ListTemplate listTemplate;

        public Word.ListTemplate ListTemplate
        {
            get { return listTemplate; }
            set { listTemplate = value; }
        }

        public Boolean Grouped
        {
            get { return grouped; }
            set { grouped = value; }
        }

        public String ThemeID
        {
            get { return themeID; }
            set { themeID = value; }
        }

        public Boolean Schedule
        {
            get { return schedule; }
            set { schedule = value; }
        }
        public Boolean Appendix
        {
            get { return appendix; }
            set { appendix = value; }
        }
        
        private Boolean quickstyle;
        
        public Boolean Quickstyle
        {
            get { return quickstyle; }
            set { quickstyle = value; }
        }
        

        public Word.WdListNumberStyle Listnumberstyle
        {
            get { return listnumberstyle; }
            set { listnumberstyle = value; }
        }

        public Word.WdStyleType Type
        {
            get { return type; }
            set { type = value; }
        }

        public Boolean Visibility
        {
            get { return visibility; }
            set { visibility = value; }
        }



        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }


        public Boolean Builtin
        {
            get { return builtin; }
            set { builtin = value; }
        }

        public double Leftindent
        {
            get { return leftindent; }
            set { leftindent = value; }
        }

        public Boolean ProbablyBody
        {
            get { return probablyBody; }
            set { probablyBody = value; }
        }

        public Boolean ProbablyLevel
        {
            get { return probablyLevel; }
            set { probablyLevel = value; }
        }

        public Boolean ProbablyHeading
        {
            get { return probablyHeading; }
            set { probablyHeading = value; }
        }

        public Word.WdOutlineLevel OutlineLevel
        {
            get { return outlineLevel; }
            set { outlineLevel = value; }
        }

        public Boolean Bold
        {
            get { return bold; }
            set { bold = value; }
        }

        public int ListLevelNumber
        {
            get { return listLevelNumber; }
            set { listLevelNumber = value; }
        }

        public Word.Style WordStyle
        {
            get { return wordStyle; }
            set { wordStyle = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public Style() { }
        
        public Style(Word.Style style)
        {
            try
            {
                this.name = style.NameLocal;
                this.listTemplate = style.ListTemplate;
                this.themeID = style.Font.NameOther;
                this.wordStyle = style;
                this.type = style.Type;
                this.builtin = style.BuiltIn;
                this.priority = style.Priority;
                this.visibility = style.Visibility;
                this.quickstyle = style.QuickStyle;
                try
                {
                    this.listLevelNumber = style.ListLevelNumber;
                }
                catch { }
                if (style.Font.Bold == 0)
                    this.bold = false;
                else
                    this.bold = true;
                try
                {
                    if (this.listTemplate != null)
                        this.probablyHeading = (this.listLevelNumber != 0 && this.bold && this.listTemplate.ListLevels.Count > 1);
                }
                catch { }
                if (!this.probablyHeading)
                    this.probablyHeading = this.themeID == "+Headings";
                
                try
                {
                    if (style.Type == Word.WdStyleType.wdStyleTypeList)
                        if (style.ListTemplate != null)
                            this.listnumberstyle = style.ListTemplate.ListLevels[this.listLevelNumber].NumberStyle;
                        else
                            this.listnumberstyle = Word.WdListNumberStyle.wdListNumberStyleNone;
                }
                catch { }
                try
                {
                    this.outlineLevel = style.ParagraphFormat.OutlineLevel;
                }
                catch
                {
                    this.outlineLevel = Word.WdOutlineLevel.wdOutlineLevelBodyText;
                }

                try
                {
                    if (this.listTemplate != null)
                        this.probablyLevel = (this.listLevelNumber != 0 && this.bold == false && this.listTemplate.ListLevels.Count > 1 &&
                            style.ListTemplate.ListLevels[style.ListLevelNumber].NumberFormat != "" && style.Type != Word.WdStyleType.wdStyleTypeList);
                }
                catch { }

                try
                {
                    if (this.listTemplate == null)
                        this.probablyBody = (style.ParagraphFormat.LeftIndent > 0 && this.listLevelNumber == 0
                            && this.bold == false && style.Type != Word.WdStyleType.wdStyleTypeList && style.BuiltIn == false);
                    else
                        this.probablyBody = style.ListTemplate.ListLevels[style.ListLevelNumber].NumberFormat == "" &&
                            style.Type != Word.WdStyleType.wdStyleTypeList && style.BuiltIn == false;
                    this.leftindent = style.ParagraphFormat.LeftIndent;
                }
                catch
                {
                    this.probablyBody = false;
                }

                //if (this.probablyBody)
                //    System.Windows.Forms.MessageBox.Show("Body style found: " + style.NameLocal);

                try
                {
                    if (style.ListTemplate != null)
                    {
                        if (style.ListTemplate.ListLevels.Count > 0)
                        {
                            if (style.ListTemplate.ListLevels[1].NumberFormat.ToLower().Contains("appendix"))
                            {
                                this.probablyHeading = false;
                                this.probablyLevel = false;
                                this.appendix = true;
                            }
                            if (style.ListTemplate.ListLevels[1].NumberFormat.ToLower().Contains("schedule"))
                            {
                                this.probablyHeading = false;
                                this.probablyLevel = false;
                                this.schedule = true;
                            }
                        }
                    }
                }
                catch { }
            }
            catch {  }
        }
    }
    
    class clsStyles
    {
        
        public static void ConvertFromNovaPlex(Document doc)
        {
            Template temp = doc.Template;
            String fileName = temp.Filename;
            Word.Template tmp = doc.WordDocument.get_AttachedTemplate();
            Boolean isNPdoc = tmp.FullName.ToLower().Contains(temp.Np.ToLower());
            if (!isNPdoc)
                return;
            String path = System.IO.Path.Combine(
                Globals.ThisAddIn.Application.Options.DefaultFilePath[Word.WdDefaultFilePath.wdWorkgroupTemplatesPath],fileName);
           // Word.Template t = Globals.ThisAddIn.Application.Templates[path];
            if(System.IO.File.Exists(path))
                doc.WordDocument.set_AttachedTemplate(path);

            for(int i = 1; i<6; i++)
            {
                if(!StyleExists("MdR Heading " + i, doc.WordDocument))
                { 
                    Word.Style stl = doc.WordDocument.Styles.Add("MdR Heading " + i);
                    Word.Style baseStyle = doc.WordDocument.Styles["MdR Level " + i];
                    stl.set_BaseStyle(baseStyle);
                }
                
            }

            foreach(Word.Paragraph para in doc.WordDocument.Paragraphs)
            {
                Word.Style stl = para.get_Style();
                 for(int i = 1; i<6; i++)
                 {
                     if (stl.NameLocal == "MdR Level " + i)
                         if (para.Range.Font.Bold != 0)
                             para.set_Style(doc.WordDocument.Styles["MdR Heading " + i]);
                     }
            }

            for (int i = 1; i < 6; i++)
            {
                Word.Style style = doc.WordDocument.Styles["MdR Level " + i];
                style.Font.Bold = 0;
                style.Font.AllCaps = 0;
                Word.Style stl = doc.WordDocument.Styles["MdR Heading " + i];
                stl.Font.Bold = -1;
                if (i == 1)
                    stl.Font.AllCaps = -1;
            }

            StyleUI styleui = new StyleUI();
            styleui.Document = doc;
            styleui.BuildStyleGroups();
            //PrioritiseStyles(doc.WordDocument);
            try
            {
                doc.WordDocument.Variables["styleslist"].Delete();
            }
            catch { }
            doc.Styles = ProcessStyles(doc.WordDocument);
            Globals.ThisAddIn.RefreshRogueStyleCheck();

            switch(clsWordApp.GetTemplateName(doc.WordDocument))
            {
                case "mdrLegal.dotx":
                    CustomTaskPanes.ctpLegal userControl = (CustomTaskPanes.ctpLegal) Globals.ThisAddIn.WordApp.LaunchTaskPane(doc.WordDocument, doc.Template);
                    String styles = userControl.GetTOCStyles();

                    if(doc.WordDocument.TablesOfContents.Count>0)
                    { 
                        Word.Range rng = doc.WordDocument.TablesOfContents[1].Range;
                        rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                        int page = rng.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber];
                        doc.WordDocument.TablesOfContents[1].Delete();
                        do
                        {
                            rng.MoveStart(Word.WdUnits.wdParagraph, -1);
                        } while (rng.Paragraphs[1].get_Style().NameLocal != "TOC Heading" && rng.Paragraphs[1].get_Style().NameLocal != "MdR Centred" && rng.Start>1);
                        rng.Delete();
                        clsTOC.InsertTOC(page, "", styles);
                     }
                    break;

                default:
                    Globals.ThisAddIn.WordApp.LaunchTaskPane(doc.WordDocument, doc.Template);
                    break;
           }
            
        }
        
        public static void PrioritiseStyles(Word.Document doc)
        {
            foreach (Word.Style stl in doc.Styles)
                try
                {
                    if (stl.NameLocal.ToLower().Contains("schedule")||stl.NameLocal.ToLower().Contains("centred"))
                    {
                        stl.Priority = 20;
                        stl.Visibility = false;
                        stl.QuickStyle = false;
                    }
                    else if (stl.NameLocal.ToLower().Contains("heading") && !stl.BuiltIn)
                    {
                        stl.Priority = 5;
                        stl.Visibility = false;
                        stl.QuickStyle = true;
                    }
                    else if (stl.NameLocal.ToLower().Contains("level"))
                    {
                        stl.Priority = 6;
                        stl.Visibility = false;
                        stl.QuickStyle = true;
                    }
                    else if (stl.NameLocal.ToLower().Contains("body") && !stl.BuiltIn)
                    {
                        stl.Priority = 7;
                        stl.Visibility = false;
                        stl.QuickStyle = true;
                    }
                    else if (stl.BuiltIn)
                    {
                        stl.Priority = 99;
                        stl.Visibility = true;
                        stl.QuickStyle = false;
                    }
                    else
                    {
                        stl.Priority = 40;
                        stl.Visibility = true;
                        stl.QuickStyle = false;
                    }
                }
                catch { }
        }
        
        public static String CheckNumbering1(Word.Document doc)
        {
            ClearComments(doc);
            Boolean found = false;
                        
            foreach (Word.Paragraph para in doc.ListParagraphs)
            {
                Word.Range rng = para.Range;
                Boolean pass = false;
                if(para.Next() !=null)
                    if(para.Next().Range.ListFormat.ListTemplate!=null)
                    { 
                        rng.MoveEnd(Word.WdUnits.wdParagraph, 1);
                        if (rng.ListFormat.SingleListTemplate == false)
                        {
                            //It's fine to mix MdR Base with MdR List which are list styles
                            Word.Style style1 = rng.Paragraphs[1].Range.ListStyle;
                            Word.Style style2 = rng.Paragraphs[2].Range.ListStyle;
                            if (style1 != null)
                                if (style1.NameLocal.Contains("MdR"))
                                    if (style2 != null)
                                        if (style2.NameLocal.Contains("MdR"))
                                            pass = true;
                            if (pass == false)
                            { 
                                found = true;
                                Word.Comment comment = doc.Comments.Add(rng);
                                comment.Range.Text = "Mixed list templates";
                                comment.Initial = Globals.ThisAddIn.APP_NAME1;
                                comment.ShowTip = true;
                            }
                        }
                    }
            }
            if (found)
                return "Mixed list templates were found.  Comments have been inserted in the relevant places."
                    + Environment.NewLine + "Re-apply styles at the affected areas and re-check.";
            else
                return "No mixed list templates were found.";
        }
        
        
        public static String CheckNumbering2(Word.Document doc)
        {
            Boolean found = false;
            foreach(Word.Paragraph para in doc.Paragraphs)
            {
                String[] s = para.Range.Text.Split(Microsoft.VisualBasic.Constants.vbTab.ToCharArray());
                if(s.Length>1)
                {
                    String t = s[0].Trim();
                    if(t.Length>0)
                    {
                        found = true;
                        try
                        {
                            Word.Range rng = para.Range;
                            rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                            Word.Comment comment = doc.Comments.Add(rng);
                            comment.Range.Text = "Manual numbering";
                            comment.Initial = Globals.ThisAddIn.APP_NAME1;
                            comment.ShowTip = true;
                        }
                        catch { }
                    }
                }
            }
            if (found)
                return "Manual numbering has been identified in your document.  Comments have been inserted.";
            else
                return "No manual numbering detected.";
        }
        
        public static void ClearComments(Word.Document doc)
        {
            
            for (int i = doc.Comments.Count; i > 0; i--)
                if (doc.Comments[i].Initial == Globals.ThisAddIn.APP_NAME1)
                    doc.Comments[i].Delete();
        }
        
        public static void ApplyAutoNumberbyLevel(string pstrdefault)
        {
            Word.Range rngRange = Globals.ThisAddIn.Application.Selection.Range;
            Word.Range rngBuffer = rngRange.Duplicate;
            Word.Document doc = (Word.Document)rngRange.Parent;
            do
            {
                rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
                rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                if (Convert.ToInt16(rngBuffer.Paragraphs[1].OutlineLevel) == Convert.ToInt16(pstrdefault))
                {
                    Word.Style style = rngBuffer.Paragraphs[1].Range.get_Style();
                    if (style.NameLocal.ToLower().Contains("heading"))
                    {
                        String stName = "MdR Level ";
                        int i = (Convert.ToInt16(style.NameLocal.Substring(style.NameLocal.Length - 2, style.NameLocal.Length - 1)) + 1);
                        style = doc.Styles[stName + i];
                    }
                    foreach (Word.Paragraph para in rngRange.Paragraphs)
                        para.Range.set_Style(style);
                    return;
                }

            } while (rngBuffer.Paragraphs[1].Range.Start > 1);

            try
            {
                Word.Style style = Globals.ThisAddIn.Application.ActiveDocument.Styles["Mdr Level " + (pstrdefault)];
                foreach(Word.Paragraph para in rngRange.Paragraphs)
                    para.Range.set_Style(style);
            }
            catch { }
        }

       public static void ApplyBodyLevel(string level)
        {
            try
            {
                Word.Range rngRange = Globals.ThisAddIn.Application.Selection.Range;
                Word.Style style = null;
                if(clsStyles.StyleExists("MdR Body " + level,rngRange.Parent))
                    style = Globals.ThisAddIn.Application.ActiveDocument.Styles["Mdr Body " + level];
                else if (clsStyles.StyleExists("MdR Text " + level, rngRange.Parent))
                    style = Globals.ThisAddIn.Application.ActiveDocument.Styles["Mdr Text " + level];
                if(style!=null)
                    foreach (Word.Paragraph para in rngRange.Paragraphs)
                        para.Range.set_Style(style);
            }
            catch { }
        }
        
        
        public static void ApplyAutoHeading()
       {
           Word.Range rngRange = Globals.ThisAddIn.Application.Selection.Range;
           Word.Range rngBuffer = rngRange.Duplicate;
           String styleStart;

           if (IsInSection(rngRange))
               styleStart = "MdR Schedule Heading";
           else
               styleStart = "MdR Heading";
           do
           {
               rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
               rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
               if (rngBuffer.Paragraphs[1].Range.get_Style().NameLocal.Contains(styleStart))
               {
                   Word.Style style = rngBuffer.Paragraphs[1].Range.get_Style();
                   foreach (Word.Paragraph para in rngRange.Paragraphs)
                       para.Range.set_Style(style);
                   return;
               }

           } while (rngBuffer.Paragraphs[1].Range.Start > 1);

           rngRange.set_Style(Globals.ThisAddIn.Application.ActiveDocument.Styles[styleStart + " " + 1]);
       }
        
        
        public static void ApplyAutoNumbering()
        {
            Word.Range rngRange = Globals.ThisAddIn.Application.Selection.Range;
            Word.Range rngBuffer = rngRange.Duplicate;
            Word.Document doc = (Word.Document)rngRange.Parent;
            String styleStart;
            String styleStart2;

            if(IsInSection(rngRange))
            {
                styleStart = "MdR Schedule Level";
                styleStart2 = "MdR Schedule Heading";
            }
            else
            {
                styleStart = "MdR Level";
                styleStart2 = "MdR Heading";
            }
            rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
            do
            {
                rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
                rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                Boolean relevant;
                //if (Globals.ThisAddIn.Officedata.IsNYOffice)
                    relevant = rngBuffer.Paragraphs[1].Range.get_Style().NameLocal.Contains(styleStart) || 
                        rngBuffer.Paragraphs[1].Range.get_Style().NameLocal.Contains(styleStart2);
                //else
                    //relevant = Convert.ToInt16(rngBuffer.Paragraphs[1].OutlineLevel) < 10;

                if (relevant)
                {
                    Word.Style style = rngBuffer.Paragraphs[1].Range.get_Style();
                    if (style.NameLocal.ToLower().Contains("heading"))
                    {
                        String stName = styleStart + " ";
                        int i = Convert.ToInt16(style.NameLocal.Substring(style.NameLocal.Length - 1, 1)) + 1;
                        style = doc.Styles[stName + i];
                    }
                    foreach (Word.Paragraph para in rngRange.Paragraphs)
                        para.Range.set_Style(style);
                    return;
                }

            } while (rngBuffer.Paragraphs[1].Range.Start > 1);

            rngRange.set_Style(Globals.ThisAddIn.Application.ActiveDocument.Styles[styleStart + " " + 1]);
        }

        
        public static Boolean IsInSection(Word.Range rng)
        {
            Word.Range rngBuffer = rng.Duplicate;

            do
            {
                rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                if (rng.Paragraphs[1].get_Style().NameLocal.ToLower().Contains("schedule"))
                    return true;
                rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
            }
                while (rngBuffer.Start>1);
            return false;
        }
        
        public static void SetIndent(int direction)
        {
          try
            {
                Word.Selection sel = Globals.ThisAddIn.Application.Selection;
                Word.Range rngRange = sel.Range;
                Word.Style style = sel.Paragraphs[1].Range.get_Style();
                Word.Style stlBullet = (Word.Style)sel.Range.ListStyle;
                String newstyle = style.NameLocal.Substring(0, style.NameLocal.Length - 1) + (Convert.ToInt16(style.NameLocal.Substring(style.NameLocal.Length - 1)) + direction);
                style = Globals.ThisAddIn.Application.ActiveDocument.Styles[newstyle];
                foreach (Word.Paragraph para in rngRange.Paragraphs)
                {
                    para.Range.set_Style(style);
                    if(stlBullet.NameLocal != "No List")
                        para.Range.set_Style(Globals.ThisAddIn.Application.ActiveDocument.Styles[stlBullet]);
                }
                
            }
            catch { }
        }

        public static void ApplyUSBullet()
        {
            Word.Selection sel = Globals.ThisAddIn.Application.Selection;
            //ApplyAutoBody(sel.Range, false, null, false);
            ApplyAutoNumbering();
            
            if (StyleExists("MdR Bullet", Globals.ThisAddIn.Application.ActiveDocument))
                sel.Range.set_Style(Globals.ThisAddIn.Application.ActiveDocument.Styles["MdR Bullet"]);
        }
        
        public static void ApplyAutoBody(Word.Range rng, Boolean skip, Styles deststyles, Boolean usedeststyles)
        {
            try
            {
                Word.Style style;
                Word.Range rngRange = rng;
                Word.Range rngBuffer = rngRange.Duplicate;
                if(!skip)
                rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
                do
                {
                    rngBuffer.MoveStart(Word.WdUnits.wdParagraph, -1);
                    rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                    
                    int i = Convert.ToInt16(rngBuffer.Paragraphs[1].OutlineLevel);
                    if (i < 10)
                    {
                        Word.Document doc = (Word.Document)rng.Parent;
                        Boolean stlExists;
                        if (usedeststyles)
                            stlExists = StyleExists("Mdr Body " + i, deststyles);
                        else
                            stlExists = StyleExists("Mdr Body " + i, doc);

                        if (stlExists)
                            style = doc.Styles["Mdr Body " + i];
                        else
                            style = doc.Styles["Mdr Text " + i];
                        foreach (Word.Paragraph para in rngRange.Paragraphs)
                            para.Range.set_Style(style);
                        return;
                    }

                } while (rngBuffer.Paragraphs[1].Range.Start > 1);
            }
            catch { }
        }

        public static Styles ProcessStyles(Word.Document doc)
        {
            if (doc.Name.Contains("dotx") || doc.Name.Contains("dotm"))
            {
                try
                {
                    doc.Variables["styleslist"].Delete();
                }
                catch { }
                return new Styles();
            }
            Styles styles = new Styles();
            String stylesList = String.Empty;
            try
            {
                Word.Variable vbl = doc.Variables["stylesList"];
                String[] stylesvbl = vbl.Value.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String style in stylesvbl)
                {
                    styles.Add(new Style(doc.Styles[style]));
                }
            }
            catch
            {
                foreach (Word.Style style in doc.Styles)
                {
                    Templates1.Style s = new Templates1.Style(style);
                    styles.Add(s);
                    stylesList += style.NameLocal + "|";
                }
                Word.Variable vbl;
                try
                {
                    vbl = doc.Variables["stylesList"];
                    vbl.Value = stylesList;
                }
                catch
                {
                    vbl = doc.Variables.Add("stylesList", stylesList);
                }
            }
            return styles;
        }
        
        public static Boolean StyleExists(String style, Word.Document doc)
        {
            try
            {
                Word.Style stl = doc.Styles[style];
                return stl != null;
            }
            catch { return false; }
        }

        public static Boolean StyleExists(String style, Styles styles)
        {
            IEnumerable<Style> found = from d in styles where d.Name == style select d;
            return found.Count() > 0;
        }

        public static void SafePaste()
        {
            if(Globals.ThisAddIn.Application.Documents.Count ==0)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            doc.Protect(Word.WdProtectionType.wdNoProtection, false, "", false, true);
            Word.Selection sel = Globals.ThisAddIn.Application.Selection;
            sel.set_Style(Word.WdBuiltinStyle.wdStyleNormal); 
            sel.PasteAndFormat(Word.WdRecoveryType.wdUseDestinationStylesRecovery);
            Word.Template tmp = doc.get_AttachedTemplate();
            if (tmp.ListTemplates.Count != doc.ListTemplates.Count)
                for (int i = doc.ListTemplates.Count; i > tmp.ListTemplates.Count; i--)
                    tmp.ListTemplates.Add();
                tmp.Saved = true;
                    doc.Unprotect("");
        }

     }
}
