﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class clsBuildingBlocks
    {
        
        public clsBuildingBlocks()
        {

        }
        
        public Word.Template BuildingBlocksTemplate
        {
            get 
            {
                Word.Template temp = Globals.ThisAddIn.Application.Templates[System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ThisAddIn.BUILDINGBLOCKS, "Building Blocks.dotx")];
                return temp;
            }
        }

        public void NormaliseTombstones(Word.Range rngRange)
        {
            Word.Table tbl = rngRange.Tables[1];
            foreach (Word.Row row in tbl.Rows)
                for (int i = 1; i <= row.Cells.Count; i++)
                    row.Cells[i].Width = tbl.Rows.Last.Cells[i].Width;
        }
    }
}
