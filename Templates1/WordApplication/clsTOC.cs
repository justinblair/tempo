﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    /// <summary>
    /// Class to handle tables of contents
    /// </summary>
    class clsTOC
    {
        /// <summary>
        /// Deletes the table of contents by referencing particular bookmarks positioned by other routines
        /// </summary>
        public static void DeleteTableOfContents()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (doc.TablesOfContents.Count == 0) return;
            if (!doc.Bookmarks.Exists("ToC")) return;
            Word.Range rngRange = doc.Bookmarks["ToC"].Range;
            rngRange.Delete();
            if (!doc.Bookmarks.Exists("ToCSection")) return;
            rngRange = doc.Bookmarks["ToCSection"].Range;
            rngRange.Delete();
            
        }
        /// <summary>
        /// Moves the TOC to a new position which is a page number
        /// </summary>
        /// <param name="position">The page to move the TOC to</param>
        /// <param name="bbEntry">Not used</param>
        /// <param name="styles">A string of styles identifying which TOC levels to use for each</param>
        public static void MoveTableOfContents(int position, string bbEntry, string styles)
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Range rngRange = GetPage(position);
            DeleteTableOfContents();
            InsertTOC(position, bbEntry, styles);

        }
        /// <summary>
        /// Gets the top part of the requested page
        /// </summary>
        /// <param name="count">The page number in question</param>
        /// <returns></returns>
        public static Word.Range GetPage(int count)
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (count == 0)
                return Globals.ThisAddIn.Application.Selection.Range;
            Word.Range rngRange = doc.Range(0, 0);
            if (count == 1)
                return rngRange;
            if (count > doc.Range().Information[Word.WdInformation.wdActiveEndPageNumber] || count < 0) 
                count = doc.Range().Information[Word.WdInformation.wdActiveEndPageNumber];
            
            do {
                rngRange.MoveEnd(Word.WdUnits.wdParagraph, 1);
            } while (rngRange.Information[Word.WdInformation.wdActiveEndPageNumber]<count);

            rngRange.MoveEnd(Word.WdUnits.wdParagraph, -1);
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            return rngRange;
        }
        /// <summary>
        /// Updates the TOC and other fields in the document.  An override function called from the ribbon's Update Table button
        /// </summary>
        public static void UpdateTable()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            foreach (Word.TableOfContents toc in doc.TablesOfContents)
            {
                Word.WdTabLeader tab = toc.TabLeader;
                toc.Update();
                toc.TabLeader = tab;
                if (Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Template.Name.ToLower().Contains("pleading"))
                    clsTOC.AdjustTOCForPleading(toc);
            }
            foreach (Word.Field fld in doc.Fields)
                fld.Update();
        }

        /// <summary>
        /// Selects the first TOC in the document
        /// </summary>
        public static void SelectTable()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if (doc.TablesOfContents.Count > 0)
                doc.TablesOfContents[1].Range.Select();
        }

        /// <summary>
        /// Checks to see if the document contains one or more tables of contents
        /// </summary>
        /// <returns></returns>
        public static Boolean HasTOC()
        {
            return Globals.ThisAddIn.Application.ActiveDocument.TablesOfContents.Count > 0;
        }

        /// <summary>
        /// Inserts a TOC at the the required page referencing the styles supplied with their TOC levels.
        /// </summary>
        /// <param name="page">The page to place the TOC</param>
        /// <param name="bbEntry">Not used</param>
        /// <param name="Styles">A string of styles matching up with TOC levels</param>
        public static void InsertTOC(int page, string bbEntry, string Styles)
        {
            Boolean IsPleading = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Template.Name.ToLower().Contains("pleading");
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Template tmp = doc.get_AttachedTemplate();
            //Word.BuildingBlock bb = tmp.BuildingBlockEntries.Item(bbEntry);
            Word.Range rngRange = clsTOC.GetPage(page);
            
            Boolean bRequiresSectionBreak = false;
            if (clsWordApp.ContentControlExistsByID("859163546"))
            {
                switch(page)
                { 
                    case 0:
                        bRequiresSectionBreak = true;
                        break;
                    case 1:
                        bRequiresSectionBreak = false;
                        break;
                    case 2:
                        bRequiresSectionBreak = false;
                        break;
                    case 3:
                        bRequiresSectionBreak = true;
                        break;
               }
            }
            else
            {
                switch (page)
                {
                    case 0:
                        bRequiresSectionBreak = true;
                        break;
                    case 1:
                        bRequiresSectionBreak = false;
                        break;
                    case 2:
                        if (IsPleading)
                            bRequiresSectionBreak = false;
                        else
                            bRequiresSectionBreak = true;
                        break;
                    case 3:
                        bRequiresSectionBreak = true;
                        break;
                }
            }
            int start = rngRange.Start;
            if (bRequiresSectionBreak)
                rngRange.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngRange.Text = "TABLE OF CONTENTS";
            if(clsStyles.StyleExists("MdR Centered Bold",doc))
                rngRange.set_Style(doc.Styles["MdR Centered Bold"]);
            else
            {
                rngRange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;
                rngRange.Font.Bold = -1;
            }
            rngRange.InsertParagraphAfter();
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngRange.ParagraphFormat.Alignment = Word.WdParagraphAlignment.wdAlignParagraphLeft;
            if (!IsPleading && !Globals.ThisAddIn.Officedata.IsNYOffice)
            {
                rngRange.Text = "No." + Microsoft.VisualBasic.Constants.vbTab + "Heading" + Microsoft.VisualBasic.Constants.vbTab + "Page";
                if (clsStyles.StyleExists("MdR Plain", doc))
                    rngRange.set_Style(doc.Styles["MdR Plain"]);

                rngRange.ParagraphFormat.TabStops.Add(0F, Word.WdAlignmentTabAlignment.wdLeft);
                rngRange.ParagraphFormat.TabStops.Add(39.68F, Word.WdAlignmentTabAlignment.wdLeft);
                rngRange.ParagraphFormat.TabStops.Add(453.54F, Word.WdAlignmentTabAlignment.wdRight);
                rngRange.InsertParagraphAfter();
                rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            }

            Word.TableOfContents toc = doc.TablesOfContents.Add(rngRange, RightAlignPageNumbers: true, IncludePageNumbers: true,
                UseHeadingStyles: false, UseHyperlinks: true);
            
            toc.LowerHeadingLevel = 9;
            toc.UpperHeadingLevel = 9;
            
            String[] buffer = Styles.Split(";".ToCharArray());
            foreach(string item in buffer)
                try
                {
                    String style = item.Split(",".ToCharArray())[0];
                    short level = Convert.ToInt16(item.Split(",".ToCharArray())[1]);
                    switch(style)
                    {
                        case "Heading 1":
                            toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading1], level);
                                break;
                        case "Heading 2":
                            toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading2], level);
                            break;
                        case "Heading 3":
                            toc.HeadingStyles.Add(doc.Styles[Word.WdBuiltinStyle.wdStyleHeading3], level);
                            break;
                        default:
                            toc.HeadingStyles.Add(doc.Styles[style], level);
                            break;
                    }
                    
                }
                catch { }
            toc.TabLeader = Word.WdTabLeader.wdTabLeaderDots;
            toc.Update();
            toc.TabLeader = Word.WdTabLeader.wdTabLeaderDots;
            
            rngRange = toc.Range;
            rngRange.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngRange.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
            rngRange = doc.Range(start, rngRange.End);
            doc.Bookmarks.Add("ToC", rngRange);
            doc.Sections[toc.Range.Sections[1].Index + 1].PageSetup.DifferentFirstPageHeaderFooter = 0;
            foreach (Word.HeaderFooter htf in doc.Sections[toc.Range.Sections[1].Index + 1].Footers)
            {
                htf.LinkToPrevious = false;
                htf.PageNumbers.StartingNumber = 1;
                htf.PageNumbers.NumberStyle = Word.WdPageNumberStyle.wdPageNumberStyleArabic;
            }
             String xml = doc.Sections[toc.Range.Sections[1].Index + 1].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.WordOpenXML;
            Word.Section sect = toc.Range.Sections[1];
            foreach (Word.HeaderFooter htf in sect.Footers)
            { 
                htf.LinkToPrevious = false;
                htf.Range.InsertXML(xml);
                htf.PageNumbers.NumberStyle = Word.WdPageNumberStyle.wdPageNumberStyleLowercaseRoman;
                htf.PageNumbers.StartingNumber = 1;
            }
            if (IsPleading)
                AdjustTOCForPleading(toc);
           
            
        }
        /// <summary>
        /// Strips out the words "Aricle" and "Section" which are part of the numbering schema in a pleading (US) document
        /// </summary>
        /// <param name="toc">The table of contents to update</param>
        public static void AdjustTOCForPleading(Word.TableOfContents toc)
        {
            Word.Range rng = toc.Range.Fields[1].Result;
            Word.Document doc = (Word.Document)rng.Parent;
            foreach(Word.Paragraph para in doc.ListParagraphs)
            {
                Word.Style style = para.get_Style();
                if (style.NameLocal == doc.Styles[Word.WdBuiltinStyle.wdStyleHeading1].NameLocal || 
                    style.NameLocal == doc.Styles[Word.WdBuiltinStyle.wdStyleHeading2].NameLocal ||
                    style.NameLocal == doc.Styles[Word.WdBuiltinStyle.wdStyleHeading3].NameLocal)
                {
                    Word.Find fnd = toc.Range.Fields[1].Result.Find;
                    fnd.Text = para.Range.ListFormat.ListString + Microsoft.VisualBasic.Constants.vbTab;
                    fnd.Replacement.Text = " ";
                    fnd.Replacement.Font.Hidden = -1;
                    
                    fnd.Wrap = Word.WdFindWrap.wdFindStop;
                    fnd.Forward = true;
                    fnd.MatchCase = false;
                    fnd.Format = true;
                    fnd.Execute(FindText: para.Range.ListFormat.ListString + Microsoft.VisualBasic.Constants.vbTab, ReplaceWith: " ", Replace: true);
                    if (fnd.Found)
                        System.Windows.Forms.MessageBox.Show("Something");
                }
            }
        }
    }
}
