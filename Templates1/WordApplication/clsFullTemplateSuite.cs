﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Templates1
{
    public class FullTemplateSuite
    {
        private String APP_PATH = System.Environment.GetEnvironmentVariable("PROGRAMFILES") + @"\Mishcon De Reya\Mishcon de Reya Templates";
        private String HolderFile { get; set; }
        public FullTemplateSuite()
        {
            this.HolderFile = APP_PATH + @"\FullTemplateSuite.config";
        }

        public Boolean LoadFullTemplateSuite
        {
            get
            {
                return false;//System.IO.File.Exists(this.HolderFile);
            }
        }
    }
}
