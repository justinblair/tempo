﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    
    class PageSetup
    {
        public float LeftMargin {get; set;}
        public float RightMargin { get; set; }
        public float TopMargin { get; set; }
        public float BottomMargin { get; set; }
        public Word.Document Document { get; set; }
        public PageSetup(Word.Document doc)
        {
            this.Document = doc;
            Word.Section sect = doc.Sections[1];
            this.LeftMargin = sect.PageSetup.LeftMargin;
            this.RightMargin = sect.PageSetup.RightMargin;
            this.TopMargin = sect.PageSetup.TopMargin;
            this.BottomMargin = sect.PageSetup.BottomMargin;

        }

        public void Reapply()
        {
            foreach(Word.Section sect in this.Document.Sections)
            {
                sect.PageSetup.LeftMargin = this.LeftMargin;
                sect.PageSetup.RightMargin = this.RightMargin;
                sect.PageSetup.TopMargin = this.TopMargin;
                sect.PageSetup.BottomMargin = this.BottomMargin;
                if(sect.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.ContentControls.Count == 0)
                { 
                    Word.ContentControl ctl = this.Document.ContentControls.Add(Word.WdContentControlType.wdContentControlRichText,
                        sect.Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].Range);
                    ctl.Title = "Document reference";
                    ctl.Tag = "docRef";
                }
            }
            clsDynamicInserts.UpdateDocumentReference(this.Document);
        }
    }
    
    
    class clsConversion
    {
        private Word.Document sourcedoc;
        private Word.Document destinationdocument;
        private Styles sourcestyles;
        private Styles destinationstyles;
        private Styles destinationnewstyles;
        private PageSetup ps;
        

        public clsConversion(Word.Document doc)
        {
            
            this.sourcedoc = doc;
            sourcestyles = new Styles();
            foreach(Word.Style stl in sourcedoc.Styles)
            { 
                Style styl = new Style(stl);
                sourcestyles.Add(styl);
            }
        }

        public Styles CreateDestinationDocument()
        {
            Template temp =  Globals.ThisAddIn.Officedata.Templates.DefaultTemplates[Globals.ThisAddIn.Ribbonopts.ConversionTemplate];
            
            destinationdocument = Globals.ThisAddIn.WordApp.CreateDocument(
                temp, Globals.ThisAddIn.Officedata.Languages.ActiveLanguage,
                Globals.ThisAddIn.Officedata.Offices.ActiveOffice, true).WordDocument;
            ps = new PageSetup(destinationdocument);
                //host.Documents.Add(host.Options.DefaultFilePath[Word.WdDefaultFilePath.wdWorkgroupTemplatesPath]
               //   1`` + @"\" + filename);
            //Globals.ThisAddIn.WordApp.Documents.Add(new Document(destinationdocument, temp, Globals.ThisAddIn.Officedata.Languages.ActiveLanguage,
                //Globals.ThisAddIn.Officedata.Offices.ActiveOffice));
            destinationstyles = new Styles();
            foreach (Word.Style stl in destinationdocument.Styles)
            {
                Style styl = new Style(stl);
                destinationstyles.Add(styl);
            }
            return destinationstyles;
        }

        
        public StyleMaps BuildMappings()
        {
            StyleMaps stylemaps = new StyleMaps();
            IEnumerable<Style> headings = from d in sourcestyles  where d.ProbablyHeading == true && d.ListLevelNumber > 0 && d.Builtin == false select d;
            if(headings.Count() == 0)
                 headings = from d in sourcestyles  where d.ProbablyHeading == true && (int)d.OutlineLevel < 10 && d.Builtin == false select d;
            Styles sourceheadingstyles = new Styles(headings);

            headings = from d in destinationstyles where d.ProbablyHeading == true && d.ListLevelNumber > 0 && d.Builtin == false select d;
            if (headings.Count() == 0)
                headings = from d in destinationstyles where d.ProbablyHeading == true && (int)d.OutlineLevel < 10 && d.Builtin == false select d;
             Styles destinationheadingstyles = new Styles(headings);

            foreach(Style stl in sourceheadingstyles)
            {
                headings = from d in destinationheadingstyles where (d.ListLevelNumber == stl.ListLevelNumber || d.OutlineLevel == stl.OutlineLevel) && 
                                d.ListLevelNumber>0 && d.ListTemplate.ListLevels.Count > 1 && d.Builtin == false select d;
                if(headings.Count()>0)
                { 
                    StyleMap map = new StyleMap(stl, headings.ToList()[0]);
                    stylemaps.Add(map);
                }
                if(stl.Type == Word.WdStyleType.wdStyleTypeList)
                {
                    headings = from d in destinationheadingstyles where d.Listnumberstyle == stl.Listnumberstyle select d;
                    if(headings.Count()>0)
                    {
                        StyleMap map = new StyleMap(stl, headings.ToList()[0]);
                        stylemaps.Add(map);
                    }
                }
            }

            StyleMaps bodymaps = new StyleMaps();
            //Loop through the styles and find those which could be body styles with a number in them.  Most firms include a number in the style name.
            for (int i = 1; i < 10; i++)
            {
                IEnumerable<Style> sourcebodies = from d in sourcestyles where d.ProbablyBody && d.Name.Contains(i.ToString()) select d;
                IEnumerable<Style> destbodies = from d in destinationstyles where d.ProbablyBody && d.Name.Contains(i.ToString()) select d;
                if (sourcebodies.Count() > 0 && destbodies.Count() > 0)
                    bodymaps.Add(new StyleMap(sourcebodies.ToList()[0], destbodies.ToList()[0]));
            }

            //Add the bodymaps to the stylemaps list
            IEnumerable<StyleMap> maps = (IEnumerable<StyleMap>)stylemaps.Concat((IEnumerable<StyleMap>)bodymaps);
            stylemaps = new StyleMaps(maps);

            // Pull out the levels
            IEnumerable<Style> levels = from d in sourcestyles where d.ProbablyLevel == true && d.ListLevelNumber > 0 && d.Builtin == false select d;

            Styles sourcelevelstyles = new Styles(levels.OrderBy(c => c.ListLevelNumber));

            levels = from d in destinationstyles where d.ProbablyLevel == true && d.ListLevelNumber > 0 && d.Builtin == false select d;
            Styles destinationlevelstyles = new Styles(levels);

            foreach (Style stl in sourcelevelstyles)
            {
                levels = from d in destinationlevelstyles where d.ListLevelNumber == stl.ListLevelNumber && d.Builtin == false select d;
                if(levels.Count()>0)
                { 
                StyleMap map = new StyleMap(stl, levels.ToList()[0]);
                stylemaps.Add(map);
                }
            }

            return stylemaps;
        }
        
        public Word.Document RunConversion(StyleMaps stylemaps, Styles deststyles)
        {
            Word.Style style;
            sourcedoc.Range().Copy();
            destinationdocument.Range(0, 0).Paste();
            System.Windows.Forms.Clipboard.Clear();
            foreach (Word.Paragraph para in destinationdocument.Paragraphs)
            {
                Word.Style stl = para.get_Style();
                if(!stl.NameLocal.Contains("TOC"))
                { 
                    IEnumerable<StyleMap> stylemap = (from d in stylemaps where d.Source.Name == stl.NameLocal select d);
                    if (stylemap.Count() > 0)
                    {
                        String deststyle = stylemap.ToList()[0].Destination.Name;
                        style = destinationdocument.Styles[deststyle];
                    }
                    else
                        if (stl.ListTemplate == null)
                            style = destinationdocument.Styles[Word.WdBuiltinStyle.wdStyleNormal];
                        else
                        {
                            if(stl.ParagraphFormat.OutlineLevel==Word.WdOutlineLevel.wdOutlineLevelBodyText)
                                stl.ParagraphFormat.OutlineLevel = (Word.WdOutlineLevel)stl.ListLevelNumber;
                            style = destinationdocument.Styles["MdR Base"];
                        }
               
                    try
                    {
                        para.set_Style(style);
                    }
                    catch { }
                    //List styles operate differently to paragraph and linked styles.  They can be applied programmatcially but the paragraph will only show the
                    // underlying style and not the list style in the styles pane.  We therefore need to highlight the list style and match it with one in the
                    // destination document.
                   // Word.Style liststyle = para.Range.ListStyle;
                   // if (liststyle.NameLocal != "No List")
                   // {
                   //     for (int i = 1; i < 10; i++)
                   //     {
                   //         stylemap = from d in stylemaps where d.Source.Listnumberstyle == liststyle.ListTemplate.ListLevels[i].NumberStyle select d;
                   //         if (stylemap.Count() > 0)
                   //         {
                   //             String deststyle = stylemap.ToList()[0].Destination.Name;
                   //             style = destinationdocument.Styles[deststyle];
                   //             para.set_Style(style);
                   //         }
                   //     }

                   //}

                   // if (style.ListLevelNumber == 0)
                   //     clsStyles.ApplyAutoBody(para.Range, true, deststyles, true);
                   // if(style.ListTemplate!=null)
                   //     if(style.ListTemplate.ListLevels[style.ListLevelNumber].NumberFormat=="")
                   //         clsStyles.ApplyAutoBody(para.Range, true, deststyles, true);
                   // stl = para.Range.ListStyle;
                }

            }

            //Launch the task pane and build a table of contents
            try
            {
                CustomTaskPanes.ctpLegal userControl = (CustomTaskPanes.ctpLegal)Globals.ThisAddIn.WordApp.LaunchTaskPane(destinationdocument, Globals.ThisAddIn.WordApp.Documents.ActiveDocument(true).Template);
                String styles = userControl.GetTOCStyles();

                if (destinationdocument.TablesOfContents.Count > 0)
                {
                    Word.Range rng = destinationdocument.TablesOfContents[1].Range;
                    rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                    int page = rng.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber];
                    destinationdocument.TablesOfContents[1].Delete();
                    clsTOC.InsertTOC(page, "", styles);
                }
            }
            catch { }

            return destinationdocument;
        }

        public void TidyUpStyles(Word.Document doc)
        {
            for (int i = sourcestyles.Count; i > 0; i--)
                try
                {
                    IEnumerable<Style> dest = from d in destinationstyles where d.Name == sourcestyles[i].Name select d;
                    if (dest.Count()==0)
                        destinationdocument.Styles[sourcestyles[i].Name].Delete();
                }
                catch { }

            foreach (Style s in destinationstyles)
            {
                try
                {
                    doc.Styles[s.Name].Priority = s.Priority;
                    doc.Styles[s.Name].Visibility = s.Visibility;
                    doc.Styles[s.Name].QuickStyle = s.Quickstyle;
                }
                catch { }
            }

            ps.Reapply();
        }
    }
}
