﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class clsMarkers
    {
        private Word.Document doc;
        public clsMarkers(Word.Document doc)
        {
            this.doc = doc;
        }

        public void FindNext(Boolean forward)
        {
            Word.Find fnd = Globals.ThisAddIn.Application.Selection.Find;
            fnd.Text = @"\[*\]";
            fnd.Forward = forward;
            fnd.MatchWildcards = true;
            fnd.Wrap = Word.WdFindWrap.wdFindContinue;
            fnd.Execute();
         }

        public void InsertMarker()
        {
            Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
            rng.Text = "[" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "]";
        }
    }
}
