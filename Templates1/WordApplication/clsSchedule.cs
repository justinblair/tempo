﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class Schedules : List<Schedule>
    {
        public Schedules()
        { }
    }
    
    class Schedule
    {
        private Word.Document doc;
        public Schedule(Word.Document doc)
        {
            this.doc = doc;
        }

        public void Create()
        {
            Word.WdCollapseDirection dir;
            Word.Range rngBuffer = AppendixLocation(doc);
            if (rngBuffer == null)
            {
                rngBuffer = doc.Paragraphs.Last.Range;
                dir = Word.WdCollapseDirection.wdCollapseEnd;
                rngBuffer.Collapse(dir);
                rngBuffer.InsertParagraph();
                rngBuffer.set_Style(doc.Styles["MdR Schedule"]);
                rngBuffer.Select();

            }
            else
            { 
                dir = Word.WdCollapseDirection.wdCollapseStart;
            rngBuffer.Collapse(dir);
            rngBuffer.Select();
            Word.Selection sel = Globals.ThisAddIn.Application.Selection;
            sel.Paragraphs[1].SelectNumber();
            sel.MoveLeft(Word.WdUnits.wdCharacter, 1);
            sel.TypeParagraph();
            sel.set_Style(doc.Styles["MdR Schedule"]);
            //rngBuffer.MoveStart(Word.WdUnits.wdCharacter, -1);
            //rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            //rngBuffer.InsertParagraph();
            
           
            //rngBuffer.set_Style(doc.Styles["MdR Schedule"]);
            //rngBuffer.MoveStart(Word.WdUnits.wdCharacter, -1);
            //rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
            }

                        
            int sectionIndex = rngBuffer.Sections[1].Index;
            if(doc.Sections[sectionIndex].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.Count>0)
            { 
                rngBuffer.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
                rngBuffer = doc.Sections[sectionIndex + 1].Range;
                rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                foreach (Word.HeaderFooter htf in doc.Sections[sectionIndex + 1].Headers)
                    htf.LinkToPrevious = false;
                foreach (Word.HeaderFooter htf in doc.Sections[sectionIndex + 1].Footers)
                    htf.LinkToPrevious = false;
                for (int i = doc.Sections[sectionIndex + 1].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.Count; i > 0; i--)
                    doc.Sections[sectionIndex + 1].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers[i].Delete();
            }

            
            

        }

        private Word.Range AppendixLocation(Word.Document doc)
        {
            Word.Find fnd = doc.Range().Find;
            fnd.ClearFormatting();
            fnd.set_Style(doc.Styles["MdR Appendix"]);
            fnd.Forward = true;
            fnd.Wrap = Word.WdFindWrap.wdFindStop;
            fnd.Execute();
            if(fnd.Found)
            {
                Word.Range rng = fnd.Parent;
                rng.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                return rng;
            }
            return null;


        }
    }

    class Appendix
    {
        private Word.Document doc;
        public Appendix(Word.Document doc)
        {
            this.doc = doc;
        }

        public void Create()
        {
            Word.Range rngBuffer = doc.Range().Paragraphs.Last.Range;
            rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseEnd);
            rngBuffer.InsertParagraph();
            int sectionIndex = rngBuffer.Sections[1].Index;
            if(doc.Sections[sectionIndex].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.Count>0)
            { 
                rngBuffer.InsertBreak(Word.WdBreakType.wdSectionBreakNextPage);
                rngBuffer = doc.Sections[sectionIndex + 1].Range;
                rngBuffer.Collapse(Word.WdCollapseDirection.wdCollapseStart);
                foreach (Word.HeaderFooter htf in doc.Sections[sectionIndex + 1].Headers)
                    htf.LinkToPrevious = false;
                foreach (Word.HeaderFooter htf in doc.Sections[sectionIndex + 1].Footers)
                    htf.LinkToPrevious = false;
                for (int i = doc.Sections[sectionIndex + 1].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers.Count; i > 0; i--)
                    doc.Sections[sectionIndex + 1].Footers[Word.WdHeaderFooterIndex.wdHeaderFooterPrimary].PageNumbers[i].Delete();
            }
            rngBuffer.set_Style(doc.Styles["MdR Appendix"]);
            rngBuffer.Select();
            
        }
    }
}
