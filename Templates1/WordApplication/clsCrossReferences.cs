﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class clsCrossReferences
    {
        public clsCrossReferences()
        {

        }

        public String Check(Word.Document doc)
        {
            //clsStyles.ClearComments(doc);
            Boolean found = false;
            foreach (Word.Range story in doc.StoryRanges)
                foreach(Word.Field fld in story.Fields)
                {
                    fld.Update(); 
                    if(fld.Type == Word.WdFieldType.wdFieldRef)
                    { 
                        String result = fld.Result.Text.Replace("  ", " ");
                        if (result.ToLower().Contains("!error"))
                        {
                            found = true;
                            Word.Comment comment = doc.Comments.Add(fld.Result, "Broken cross-reference");
                            comment.Initial = Globals.ThisAddIn.APP_NAME1;
                            comment.ShowTip = true;
                        }
                    }
                }
            if (found)
                return "At least one broken cross-reference has been found.  Comments have been added at the relevant places";
            else
                return "No broken cross-references have been found in the document.";
        }
    }
}
