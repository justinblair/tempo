﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    class ShortcutManager
    {
        private GlobalEventProvider eventProvider;
        private Boolean IsCtrl;
        private Boolean IsShift;
        private Boolean IsAlt;

        internal ShortcutManager()
        {
            eventProvider = new GlobalEventProvider();
            eventProvider.KeyPress += new System.Windows.Forms.KeyPressEventHandler(eventProvider_KeyPress);
            eventProvider.KeyDown += new System.Windows.Forms.KeyEventHandler(eventProvider_KeyDown);
            eventProvider.KeyUp += new KeyEventHandler(eventProvider_KeyUp);
        }

        void eventProvider_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 162)
                IsCtrl = false;
            if (e.KeyValue == 160)
                IsShift = false;
            if (e.KeyValue == 164)
                IsAlt = false;
        }

        void eventProvider_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return;

                int level = 0;
                String style = String.Empty;

                switch (e.KeyCode)
                {
                    case Keys.D1:
                        level = 1;
                        break;
                    case Keys.D2:
                        level = 2;
                        break;
                    case Keys.D3:
                        level = 3;
                        break;
                    case Keys.D4:
                        level = 4;
                        break;
                    case Keys.D5:
                        level = 5;
                        break;
                    case Keys.Control:
                        IsCtrl = true;
                        break;
                    case Keys.Shift:
                        IsShift = true;
                        break;
                    case Keys.Alt:
                        IsAlt = true;
                        break;
                }

                if (e.KeyValue == 162)
                    IsCtrl = true;
                if (e.KeyValue == 160)
                    IsShift = true;
                if (e.KeyValue == 164)
                    IsAlt = true;

                if (!IsCtrl && !IsShift && IsAlt && e.KeyCode == Keys.H)
                    clsStyles.ApplyAutoHeading();
                if (!IsCtrl && !IsShift && IsAlt && e.KeyCode == Keys.L)
                    clsStyles.ApplyAutoNumbering();
                if (!IsCtrl && !IsShift && IsAlt && e.KeyCode == Keys.B)
                {
                    Word.Range rng = Globals.ThisAddIn.Application.Selection.Range;
                    foreach (Word.Paragraph para in rng.Paragraphs)
                        clsStyles.ApplyAutoBody(para.Range, false, null, false);
                }

                if (IsCtrl && !IsShift && !IsAlt)
                    style = "MdR Level " + level;
                if (IsCtrl && IsShift && !IsAlt)
                    style = "MdR Heading " + level;
                if (IsAlt && !IsCtrl && !IsShift)
                    if (clsStyles.StyleExists("MdR Text " + level, Globals.ThisAddIn.Application.ActiveDocument))
                        style = "MdR Text " + level;
                    else
                        style = "MdR Body " + level;
                if (level != 0 && style != String.Empty)
                    ApplyStyle(style);
            }
            catch { }
        }

        void eventProvider_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            
        }

        private void ApplyStyle(String style)
        {
            if(Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if(clsStyles.StyleExists(style, doc))
            {
                Word.Style styleWord = doc.Styles[style];
                foreach(Word.Paragraph para in  Globals.ThisAddIn.Application.Selection.Range.Paragraphs) 
                    para.set_Style(styleWord);
            }
        }
    }
}
