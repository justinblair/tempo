﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterAction;
using Word = Microsoft.Office.Interop.Word;

namespace Templates1
{
    /// <summary>
    /// A class to determine if InterAction is available on the PC
    /// </summary>
    class InterActionOnLine
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public InterActionOnLine()
        {

        }
        /// <summary>
        /// Returns true if InterAction is online by pinging the IA server or checking to see if InterAction is installed.
        /// </summary>
        public Boolean IsOnline
        {
            get
            {
                if (!System.IO.File.Exists(System.IO.Path.Combine(System.Environment.GetEnvironmentVariable("PROGRAMFILES"), @"LexisNexis\InterAction\Desktop Integration\IAAddIn2010.dll")))
                    return false;
                InterAction.Connection con = new InterAction.Connection();

                try
                {
                    con = new InterAction.Connection();
                    return con.Globals.IAPingServer();
                }
            catch { return false; }
            }
        }
    }
    
    /// <summary>
    /// A wrapper class for accessing InterAction
    /// </summary>
    class clsInterAction
    {
        private InterAction.Connection con;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public clsInterAction()
        {
            con = new InterAction.Connection();
            if(con.Globals.IAPingServer())
                con.Login();

        }

        /// <summary>
        /// Gets a contact from its unique identifier
        /// </summary>
        /// <param name="id">the ID number</param>
        /// <returns>An InterAction contact</returns>
        public InterAction.IAContact GetContactByIALID(String id)
        {
            return con.GetContactById(IAContactClass.IAContactClassIAL, id);

        }

        /// <summary>
        /// Launches the find contact dialog with the address to be selected on the right hand side
        /// </summary>
        /// <returns>The details of the contact</returns>
        public string GetAddressee()
        {
            InterAction.IAContact icontact;
            InterAction.IAAddress iaddress;

            InterAction.IAFindDlgOptions options = new InterAction.IAFindDlgOptions();
            options.ShowAPEType = IAAPEType.IAAPETypeAddress;
            options.InitialSearchList = IAFindDlgSearchList.IAContactSearchListFirm;
            
            Word.Variable vbl;
            
            icontact = con.FindContactDlg(options);
            if (icontact == null)
                return "";
            iaddress = icontact.SelectedAddress;
            
            try
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["iFindAddressee"];
            }
            catch {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("iFindAddressee");
            }
            vbl.Value = icontact.IALID;
            

            if (iaddress == null)
                return (icontact.NameTitle + " " + icontact.FirstName + " " + icontact.LastName + " " + icontact.NameSuffix).Trim() + Microsoft.VisualBasic.Constants.vbCrLf +
                icontact.JobTitle + Microsoft.VisualBasic.Constants.vbCrLf + icontact.CompanyName;
            else
                return (icontact.NameTitle + " " + icontact.FirstName + " " + icontact.LastName + " " + icontact.NameSuffix).Trim() + Microsoft.VisualBasic.Constants.vbCrLf +
               icontact.JobTitle + Microsoft.VisualBasic.Constants.vbCrLf + icontact.CompanyName + Microsoft.VisualBasic.Constants.vbCrLf + iaddress.FormattedAddress;

        }

        /// <summary>
        /// Gets a contact from interaction by launching the search dialog with some fields already entered
        /// </summary>
        /// <param name="firstname">The first name of the contact</param>
        /// <param name="surname">The surname of the contact</param>
        /// <param name="company">The contact's company</param>
        /// <returns>An Interaction contact object</returns>
        public IAContact GetContact(String firstname, String surname, String company)
        {
            InterAction.IAContact icontact;
            InterAction.IAFindDlgOptions options = new InterAction.IAFindDlgOptions();
            options.ShowAPEType = IAAPEType.IAAPETypeAddress;
            options.InitialSearchList = IAFindDlgSearchList.IAContactSearchListFirm;
            options.FirstName = firstname;
            options.LastName = surname;
            options.CompanyName = company;

            icontact = con.FindContactDlg(options);
            Word.Variable vbl;
            try
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["iFindAddressee"];
            }
            catch
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("iFindAddressee");
            }
            if (icontact != null)
                vbl.Value = icontact.IALID;
            return icontact;
        }
        
        /// <summary>
        /// Launches the search dialog to search for a contact
        /// </summary>
        /// <returns>In IA Contact object</returns>
        public IAContact GetContact()
        {
            InterAction.IAContact icontact;
            InterAction.IAFindDlgOptions options = new InterAction.IAFindDlgOptions();
            options.ShowAPEType = IAAPEType.IAAPETypeAddress;
            options.InitialSearchList = IAFindDlgSearchList.IAContactSearchListFirm;

            if (!con.Globals.IAPingServer())
                return null;

            icontact = con.FindContactDlg(options);
            Word.Variable vbl;
            try
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["iFindAddressee"];
            }
            catch
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("iFindAddressee");
            }
            if(icontact!=null)
            vbl.Value = icontact.IALID;
            return icontact;
        }

        /// <summary>
        /// Looks up the address of a contact
        /// </summary>
        /// <param name="icontact">the contact with the address</param>
        /// <returns>An IA Address object</returns>
        public IAAddress GetAddress(IAContact icontact)
        {
            InterAction.IAAddress iaddress;
            if (icontact == null) return null;
            iaddress = icontact.SelectedAddress;
            Word.Variable vbl;
            try
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables["iFindAddress"];
            }
            catch
            {
                vbl = Globals.ThisAddIn.Application.ActiveDocument.Variables.Add("iFindAddress");
            }
            vbl.Value = iaddress.AddressID;
            return iaddress;
        }

    }
}
