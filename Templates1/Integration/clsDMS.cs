﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;

//using Com.Interwoven.WorkSite.iManage;
//using IMANEXTLib;
//using IMANEXTLib2;
using Com.iManage.WorkSiteOfficeAddins;
using Com.iManage.WorkSiteAddinInterfaces;
using IMANEXT2Lib;
using IMANEXTLib;
using IMANEXT3Lib;
using IManSyncCheckin;
//using iManO2K;
using IManage;
using IManExtUnmanagedUtil;

using Word = Microsoft.Office.Interop.Word;


namespace Templates1
{
    /// <summary>
    /// Wrapper class around all DMS functionality and events.  Probably the most complex class in the templates thanks to the complexity of the FileSite API.  
    /// Much of the code has been copied from other sources and should be thoroughly tested before use.
    /// </summary>
    public class WorkSite8Application
    {
        private const String CLASS_NAME = "WorkSite8Application:";
        private const String APP_NAME = "Microsoft Word";

        private iManageExtensibility mExtensibility;
        private IManFileSaveCmd mFileSaveCmd;
        private PortableNewProfileDlg mPortableNewProfileDlg;
        private NewVersionCmd mNewVersionCmd;
        private EditProfileCmd mEditProfileCmd;

        private Boolean mIsInstalled;
        private Boolean mIsDocUndergoingSaveAsProfiledInWorksite;
        private Boolean mCancelledPortableNewProfileDlg;

        private Word.Application HostApp;
        private Boolean disablePrintHandling = false;
        private DMSProfileDefaults profileDefaults;
        private String tempPath;
        public Boolean CancelPressed = false;
        private IManSession londonSession;
        internal ManDMS mandms {get;set;}

        private String folderPath = "";

        internal enum DMSOfficeLocation
        {
            London,
            NewYork,
            None,
            Other
        }

        internal DMSOfficeLocation OfficeLocation
        {
            get
            {
                if (londonSession != null)
                    if (londonSession.Connected)
                        return TestSession(londonSession);
                    else
                        return DMSOfficeLocation.None;

                System.Windows.Forms.MessageBox.Show("No dedicated session available");
                
                return DMSOfficeLocation.None;
            }
        }

        private DMSOfficeLocation TestSession(IManSession session)
        {
             //if (Environment.UserName.ToLower() == "justin.blair" && ThisAddIn.FIXJBTONY)
                        //return DMSOfficeLocation.NewYork;
            
            switch (session.ServerName.ToUpper())
            {
                case "DMS":
                    return DMSOfficeLocation.London;
                default:
                    return DMSOfficeLocation.NewYork;
                    
            }
        }

        internal Boolean IsPreferredDatabase(String database)
        {
            if (Environment.UserName.ToLower() == "justin.blair" && database.ToLower() == "legal1us" && ThisAddIn.FIXJBTONY)
                return true;

            if(londonSession!=null)
                if(londonSession.Connected)
                    foreach (IManDatabase db in londonSession.Databases)
                        if (db.Name.ToLower() == database.ToLower())
                            return true;
           
            return false;
        }
        
        internal Boolean IsConnectedToLondon
        {
            get {
                
                if (londonSession != null)
                    if (londonSession.Connected)
                        if (londonSession.ServerName == "DMS" || londonSession.ServerName == "NYDMS")
                            return true;

                 return false;
            }
        }
       

        /// <summary>
        /// When the NBI create event is fired, document number, checkout path, comment, database and version. Not used
        /// will be passed as parameters
        /// </summary>
        /// <param name="docNum"></param>
        /// <param name="checkoutpath"></param>
        /// <param name="comment"></param>
        /// <param name="db"></param>
        /// <param name="version"></param>
        public delegate void CreateNBIEvent(string docNum, string checkoutpath, string comment, string db, string version);
        /// <summary>
        /// NBI Event, will be raised when a NBI document with a xml comment is opened for the first time from the DMS
        /// </summary>
        public event CreateNBIEvent CreateNBI;


        public WorkSite8Application()
        {
            mandms = new ManDMS();
            
        }

        /// <summary>
        /// Constructor, Initialse the application with a reference 
        /// to the Host Application Word.Application
        /// </summary>
        /// <param name="HostApplication">The Word.Application object</param>
        public WorkSite8Application(Word.Application HostApplication)
        {
            mandms = new ManDMS();
            CreateLondonSession();
            HostApp = HostApplication;

            // perform test to see if WorkSite is actually installed on this workstation
            AssertIManageIsInstalled();
         
           
            if (mIsInstalled)
            {
                // initialise the WorkSite Extensibility object, so we can listen for its events

                mExtensibility = AssertIManO2kExtensibility(HostApplication) as iManageExtensibility;
                mExtensibility.Connect();
                bool deletedResiliency = false;

                if (mExtensibility == null)
                {
                    Registry.RemoveResiliencyKey();
                    mExtensibility = AssertIManO2kExtensibility(HostApplication) as iManageExtensibility;
                    deletedResiliency = true;
                }

                if (mExtensibility == null)
                {
                    if (deletedResiliency)
                    {
                        MessageBox.Show("Microsoft Word needs to be restarted in order to re-enable Worksite.", Globals.ThisAddIn.APP_NAME1,
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        //HostApplication.GetType().InvokeMember("Quit", BindingFlags.InvokeMethod, null, HostApplication, null);
                    }
                    //else
                    //{
                    //    throw new Exception(CLASS_NAME + " a connection to WorkSite could not be established, " +
                    //        "please check to see if the add-in has become disabled.");
                    //}
                }
                else
                {
                    //this.CreateEvents();
                }
            }
        }
        /// <summary>
        /// Returns the window ID of the identified process
        /// </summary>
        /// <param name="Name">The process whose window id we are looking for</param>
        /// <returns></returns>
        private int WindowId(String Name)
        {
            Process[] processes = Process.GetProcessesByName(Name);
            foreach (Process p in processes)
            {
                return p.MainWindowHandle.ToInt32();
                
            }
            return 0;
        }

        /// <summary>
        /// Gets the DMS as a MANDMS object
        /// </summary>
        /// 

        public void ConnectToWord(Word.Document doc)
        {
            WSCommandManager wSCommandManager = new WSCommandManager();
            ConnectWord connectWord = new ConnectWord(wSCommandManager);

          
        }


        internal ManDMS GetDMS
        {
 

        get
            {
                
                return mandms;
            }
        }
        /// <summary>
        /// Gets the active session
        /// </summary>



        internal IManSession GetSession
        {
            get
            {
                try
                {
                    if (londonSession != null)
                        if (londonSession.Connected)
                            return londonSession;

                    foreach (IManSession session in this.GetDMS.Sessions)
                        if (session.Connected)
                            return session;
                }
                catch (Exception) { }

                return null;
            }
        }

        internal void CreateLondonSession()
        {

            if (londonSession != null)
                return;

            IManSession session = null;

            ManDMS dms;
            if (mandms != null)
                dms = mandms;
            else
                dms = GetDMS;

            try
            {
                //if(Environment.UserName.ToLower() == "justin.blair" && ThisAddIn.FIXJBTONY)
                //{
                //    DialogResult res = System.Windows.Forms.MessageBox.Show("Click 'Yes' for London, 'No' for New York", "Connect", MessageBoxButtons.YesNo);
                //    if (res == DialogResult.Yes)
                //        session = dms.Sessions.Add("DMS");
                //    else
                //        session = dms.Sessions.Add("NYDMS");
                //    session.TrustedLogin();
                //    londonSession = session;
                //    return;
                //}
                
                session = dms.Sessions.Add("DMS");
                session.TrustedLogin();
                londonSession = session;
            }
            catch
            {
                session = dms.Sessions.Add("NYDMS");
                try
                {
                    session.TrustedLogin();
                    londonSession = session;
                }
                catch { }
                
            }
          if(ThisAddIn.SHOWMESSAGES)
              System.Windows.Forms.MessageBox.Show("Session: " + londonSession.ServerName + "Connected: " + londonSession.Connected);
        }
        /// <summary>
        /// Gets the preferred database which is the database connected to the active session
        /// </summary>
        internal IManDatabase GetPreferredDatabase
        {
            get
            {
                try
                {
                    IManSession sess = this.GetSession;
                    return sess.PreferredDatabase;
                }
                catch (Exception) { }

                return null;
            }
        }
        /// <summary>
        /// Gets a database
        /// </summary>
        /// <param name="DatabaseName">The name of the database to return</param>
        /// <returns>An IManDatabase object representing the database to be returned</returns>
        internal IManDatabase GetDatabase(String DatabaseName, Boolean useLondonSession)
        {
            IManDatabase foundDB = null;

            try
            {
                if (useLondonSession)
                    if (londonSession != null)
                        if (londonSession.Connected)
                            foreach (IManDatabase db in londonSession.Databases)
                                if (DatabaseName.ToLower() == (db.Name.ToLower()))
                                    return db;
            }
            catch { }

             try
            {
                Boolean foundDatabaseName = false;

                foreach (IManSession session in this.GetDMS.Sessions)
                {
                    if (session.Connected)
                    {
                        foreach (IManDatabase db in session.Databases)
                        {
                            if ((DatabaseName.Equals(db.Name)))
                            {
                                foundDB = db;
                                foundDatabaseName = true;
                                break;
                            }
                        }

                        if (foundDatabaseName)
                            break;
                    }
                }

                if (foundDB == null && DatabaseName == "PD_KNOWHOW")
                {
                    IManSession session = GetDMS.Sessions.Add("LONDON-DM");
                    session.TrustedLogin();
                    foundDB = session.Databases.ItemByName("PD_KNOWHOW");
                }

                return foundDB;
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets a document from the database
        /// </summary>
        /// <param name="DatabaseName">The name of the database to be searched</param>
        /// <param name="DocNumber">The document number</param>
        /// <param name="VersionNumber">The version number</param>
        /// <returns>An IManDocument representing the document to be retrieved</returns>
        internal IManDocument GetDocument(String DatabaseName, long DocNumber, long VersionNumber, Boolean useLondonSession)
        {
            try
            {
                IManDocument returnVal;
                IManDatabase db = this.GetDatabase(DatabaseName, useLondonSession);
                if (db == null)
                    return null;

                if (VersionNumber == 0)
                {
                    // no version number was passed in: fetch the latest version
                    returnVal = db.GetDocument(Convert.ToInt32(DocNumber), 1);
                    returnVal = returnVal.LatestVersion;
                }
                else
                {
                    returnVal = db.GetDocument(Convert.ToInt32(DocNumber), Convert.ToInt32(VersionNumber));
                }
                return returnVal;
            }
            catch (Exception) { }

            return null;
        }
        /// <summary>
        /// Gets a document based on its checked-out location on the local drive
        /// </summary>
        /// <param name="filePath">The path of the document</param>
        /// <returns>An IManDocument: a document in FileSite</returns>
        internal IManDocument GetDocument(String filePath)
        {
            try
            {
                //londonSession.Databases.ItemByName("Legal1").
                return mExtensibility.GetDocumentFromPath(filePath) as IManDocument;
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Opens a FileSite document
        /// </summary>
        /// <param name="Doc">The document in FileSite</param>
        /// <returns>The location of where the document has been opened to on the local drive</returns>
        internal String OpenIManDoc(NRTDocument Doc)
        {
            String returnVal = null;

            try
            {

                NRTDocument[] docs = new NRTDocument[1];
                docs[0] = Doc;

                ContextItems contexts = new ContextItems();

                contexts.Add("SelectedNRTDocuments", docs);
                contexts.Add("IManExt.OpenCmd.NoCmdUI", true);
                contexts.Add("IManExt.OpenCmd.Integration", true);

                OpenCmd cmd = new OpenCmd();

                cmd.Initialize(contexts);
                cmd.Update();

                if (cmd.Status == 0) //(cmd.Status && IMANEXTLib.CommandStatus.nrActiveCommand
                {
                    cmd.Execute();

                    try { returnVal = contexts.Item("IManExt.OpenCmd.ING.FileLocation").ToString(); }
                    catch { }

                    contexts.Remove("IManExt.OpenCmd.NoCmdUI");
                    contexts.Remove("IManExt.OpenCmd.Integration");
                }

                return returnVal;
            }
            catch
            {
                returnVal = null;
            }

            return returnVal;
        }

        

        /// <summary>
        /// Add a custom history event to the history table for the associated document
        /// </summary>
        /// <param name="filePath">The path of the checked out document on the local drive</param>
        /// <param name="ActivityDescription">A description of the activity</param>
        /// <param name="ExtraText">Any extra text that should be included with the activity</param>
        public void AddCustomHistoryEvent(String filePath, String ActivityDescription, String ExtraText)
        {
            try
            {
                // fetch the IManDocument object inferred by the method parameters
                IManDocument imanDoc = this.GetDocument(filePath);

                // TODO: should really make this more generic
                imanDoc.HistoryList.Add(imHistEvent.imHistoryEditTime, 0, 0, "WINWORD", ActivityDescription + ExtraText, "", "", "");
                imanDoc.Refresh();
            }
            catch
            {

            }
        }
        /// <summary>
        /// Check-out a document and return the check-out path as a string
        /// </summary>
        /// <param name="DatabaseName"></param>
        /// <param name="DocumentNumber"></param>
        /// <param name="Version"></param>
        /// <returns></returns>
        public String CheckoutDocument(String DatabaseName, Int32 DocumentNumber, Object Version)
        {
            const String PROC_NAME = CLASS_NAME + "IDMSApplication_CheckoutDocument";

            try
            {
                IManDocument imanDoc = this.GetDocument(DatabaseName, DocumentNumber, 0, false);

                if (imanDoc == null)
                    throw new Exception(PROC_NAME + " Document was not found in the specified database.");

                String returnVal = this.OpenIManDoc(imanDoc as NRTDocument);
                return returnVal;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Copy a profiled document to a location on the windows file system
        /// </summary>
        /// <param name="FilePath">The path to where the document is to be copied</param>
        /// <param name="DatabaseName">The name of the database</param>
        /// <param name="DocumentNumber">The number of the document</param>
        /// <param name="Version">The version of the document</param>
        /// <returns>True if the copy was successful</returns>
        public Boolean CopyDocumentToPath(String FilePath, String DatabaseName, Int32 DocumentNumber, Object Version)
        {
            const String PROC_NAME = CLASS_NAME + "IDMSApplication_CopyDocumentToPath";

            if (Version != Type.Missing)
            {
                int result;
                if (!int.TryParse(Version.ToString(), out result))
                {
                    throw new Exception(PROC_NAME + " Version parameter must be numeric.");
                }
            }
            try
            {
                if (Version == Type.Missing)
                {
                    // assume that we should fetch the latest version if this parameter is not supplied;
                    // (GetDocument method treats version 0 as the latest version)
                    Version = 0;
                }

                IManDocument imanDoc = this.GetDocument(DatabaseName, DocumentNumber, Convert.ToInt16(Version),false);

                if (imanDoc == null)
                    throw new Exception(PROC_NAME + " Document was not found in the specified database.");

                imanDoc.GetCopy(FilePath, imGetCopyOptions.imNativeFormat);

                Boolean returnVal = System.IO.File.Exists(FilePath);

                return returnVal;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Intended to check a document in but not completed.  Could be reused later.  Not used currently.
        /// </summary>
        /// <param name="filename">The checkout path of the document</param>
        public void CheckDocumentsIn(String filename)
        {
            NRTDocument doc = mExtensibility.GetDocumentFromPath(filename) as NRTDocument;
            IManSyncCheckin.SynchCheckinCmd objCommand = new IManSyncCheckin.SynchCheckinCmd();
            IMANEXTLib.ContextItems objContext = new IMANEXTLib.ContextItems();
            if (WindowId("WINWORD") == 0)
                return;
            objContext.Add("NRTDMS", GetDMS);
            objContext.Add("Parent Window", WindowId("WINWORD"));
            objContext.Add("DontCheckForKey", "Y");

            objCommand.Initialize (objContext);
                objCommand.Update();
                
                if(objCommand.Status == (int) CommandStatus.nrActiveCommand)
                {
                    objCommand.Execute();
                }
             
        }
        /// <summary>
        /// Calls the FileSaveAsBinding macro in iMan02k.dotx template which launches the F12 dialog box and saves the document
        /// or a new version.  Not used.
        /// </summary>
        public void FileSaveAsBinding()
        {
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            if(IsProfiled(doc))
                Globals.ThisAddIn.Application.Run("FileSaveAsBinding");
            else
            {
                doc.Saved = false;
                doc.Save();
            }
        }
        
        /// <summary>
        /// Launches the F12 dialog.  Incomplete.  Not used.
        /// </summary>
        /// <param name="filename">The checkout path of the document to be saved.</param>
        
        public void SaveDocument(String filename)
        {
            Boolean enableNewVersion = true;
            Boolean enableNewDocument = true;
            if(mExtensibility.CurrentMode == ConnectionMode.nrConnectedMode || mExtensibility.CurrentMode == ConnectionMode.nrPortableMode)
            {
                IManDocument doc = (IManDocument)mExtensibility.GetDocumentFromPath(filename);
                if (!doc.Database.Session.Connected)
                    return;
                if(doc.Size == 0)
                {
                    enableNewVersion = false;
                    enableNewDocument = false;
                }
                if (mExtensibility.CurrentMode == ConnectionMode.nrPortableMode)
                    enableNewVersion = false;
                IMANEXTLib.SaveAsOptionCmd cmd = new IMANEXTLib.SaveAsOptionCmd();
                IMANEXTLib.ContextItems contexts = new IMANEXTLib.ContextItems();
                contexts.Add("NRTDocument", doc);
                contexts.Add("Parent Window", WindowId("WINWORD"));
                contexts.Add("IManExt.SaveAsOptionCmd.ShowLocalSaveAs", true);
                contexts.Add("IManExt.SaveAsOptionCmd.ShowSaveAsTypeOption", false);
                contexts.Add("IManExt.CheckIn.EnableReplaceOriginal", true);
                contexts.Add("IManExt.SaveAsOptionCmd.SaveAsTypesArray", null);
                contexts.Add("IManExt.CheckIn.EnableNewVersion", enableNewVersion);
                contexts.Add("IManExt.CheckIn.EnableNewDocument", enableNewDocument);
                cmd.Initialize(contexts);
                cmd.Update();
                if (cmd.Status == (int)CommandStatus.nrActiveCommand)
                    cmd.Execute();
                //object oErr = null;
                //doc.CheckIn(filename, imCheckinDisposition.imCheckinReplaceOriginal, imCheckinOptions.imKeepCheckedOut, oErr);
                //doc.Sync(filename, imCheckinDisposition.imCheckinReplaceOriginal, System.DateTime.Now, oErr);
            }
        }
        
        /// <summary>
        /// Checks to see if the DMS is online
        /// </summary>
        public Boolean IsOnline
        {
            get {
                if (mExtensibility == null)
                    return false;
                
                if (mExtensibility.CurrentMode == ConnectionMode.nrConnectedMode
                    || mExtensibility.CurrentMode == ConnectionMode.nrPortableMode)
                {
                    try
                    {
                        IManSessions sessions = GetDMS.Sessions;
                        foreach (IManSession session in sessions)
                            if (session.Connected)
                                return true;
                        return false;
                    }
                    catch { return false; }
                }
                else
                    return false;
            }
    }


        /// <summary>
        /// Create a new profile from a Word.Document, launching the SaveAs dialog as part of the process.  Works in connected and portable modes.
        /// </summary>
        /// <param name="OfficeDocument"></param>
        /// <returns>The path to where the document is checked out</returns>
        public String CreateNewProfileFromDoc(Object OfficeDocument)
        {
            try
            {
                String returnVal = "";
               

                switch (mExtensibility.CurrentMode)
                {
                    case ConnectionMode.nrConnectedMode | ConnectionMode.nrindeterminatemode:
                        returnVal = CreateNewProfileFromDoc_ConnectedMode(OfficeDocument);
                        break;

                    case ConnectionMode.nrPortableMode:
                        returnVal = CreateNewProfileFromDoc_PortableMode(OfficeDocument);
                        break;

                    default:
                        // Local & Mobile do not currently support this function: simply return the
                        // path to the OfficeDocument object
                        returnVal = OfficeDocument.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, OfficeDocument, null).ToString();
                        break;
                }

                return returnVal;
            }
            catch (Exception)
            {

            }

            return "";
        }
        /// <summary>
        /// Returns true if the DMS is installed
        /// </summary>
        public Boolean IsInstalled
        {
            get { return mIsInstalled; }
        }
        /// <summary>
        /// Checks to see if a document is in FileSite
        /// </summary>
        /// <param name="OfficeDocument">The Word document</param>
        /// <returns>Returns true if a document is profiled in the DMS</returns>
        public Boolean IsProfiled(Object OfficeDocument)
        {
            try
            {
                Boolean returnVal;
                String docFileName = OfficeDocument.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, OfficeDocument, null).ToString();

                // call the Extensibility object's GetDocumentFromPath method to return an IManDocument object
                Object imanDoc = mExtensibility.GetDocumentFromPath(docFileName);

                // if the IManDocument is null, the document is not profiled
                returnVal = (imanDoc != null);

                return returnVal;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// DMS Profile Defaults
        /// </summary>
        public DMSProfileDefaults ProfileDefaults
        {
            get { return profileDefaults; }
            set { profileDefaults = value; }
        }

        /// <summary>
        /// Creates FileSite events
        /// </summary>
        private void CreateEvents()
        {
            //mExtensibility.DocumentBeforePrint += new __iManageExtensibility_DocumentBeforePrintEventHandler(mExtensibility_DocumentBeforePrint);

            mExtensibility.DocumentBeforeSaveAsOptions += MExtensibility_DocumentBeforeSaveAsOptions;
            mExtensibility.OnCreateNewVersion += MExtensibility_OnCreateNewVersion;
            mExtensibility.OnEditProfile += MExtensibility_OnEditProfile;
            mExtensibility.OnIManFileSave += MExtensibility_OnIManFileSave;
            mExtensibility.DocumentBeforeOpen += MExtensibility_DocumentBeforeOpen;
            //mExtensibility.DocumentBeforeSaveAsOptions += new __iManageExtensibility_DocumentBeforeSaveAsOptionsEventHandler(mExtensibility_DocumentBeforeSaveAsOptions);
            //Extensibility.OnCreateNewVersion += new __iManageExtensibility_OnCreateNewVersionEventHandler(mExtensibility_OnCreateNewVersion);
            //mExtensibility.OnEditProfile += new __iManageExtensibility_OnEditProfileEventHandler(mExtensibility_OnEditProfile);
            //mExtensibility.OnIManFileSave += new __iManageExtensibility_OnIManFileSaveEventHandler(mExtensibility_OnIManFileSave);
           // mExtensibility.DocumentBeforeOpen += new __iManageExtensibility_DocumentBeforeOpenEventHandler(mExtensibility_DocumentBeforeOpen);
            
        }

        private void MExtensibility_DocumentBeforeOpen(string FileName, ref bool Cancel)
        {
            IManDocument prf = mExtensibility.GetDocumentFromPath(FileName) as IManDocument;
            String comment = prf.Comment;

            if (AssertComment(comment))
            {
                Cancel = true;
                this.CreateNBI(prf.Number.ToString(), prf.CheckoutPath, prf.Comment, prf.Database.Name, prf.Version.ToString());
            }
        }

        private void MExtensibility_OnIManFileSave(object objIManFileSaveCmd)
        {
            mFileSaveCmd = objIManFileSaveCmd as IManFileSaveCmd;

            //mFileSaveCmd.OnInitDialog += MFileSaveCmd_OnInitDialog;
            //mFileSaveCmd.PreOnOK += MFileSaveCmd_PreOnOK;
            //mFileSaveCmd.PostOnOK += MFileSaveCmd_PostOnOK;
            try
            {
                mNewVersionCmd.OnInitDialog += new IMANEXTLib._ICommandEvents_OnInitDialogEventHandler(mNewVersionCmd_OnInitDialog);
                mNewVersionCmd.PreOnOK += new IMANEXTLib._ICommandEvents_PreOnOKEventHandler(mNewVersionCmd_PreOnOK);
                mNewVersionCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mNewVersionCmd_PostOnOK);
            }
            catch { }
        }

        private void MFileSaveCmd_PostOnOK(object pMyInterface)
        {
            if (Globals.ThisAddIn.Application.Documents.Count > 0)
                clsDynamicInserts.UpdateDocumentReference(Globals.ThisAddIn.Application.ActiveDocument);
        }

        private void MFileSaveCmd_PreOnOK(object pMyInterface)
        {
            Boolean cancelDialog = false;
            IManFileSaveDlg dlg = pMyInterface as IManFileSaveDlg;

            dlg.CloseOnOK = !cancelDialog;
        }

        private void MFileSaveCmd_OnInitDialog(object pMyInterface)
        {
            try
            {
                // set mandatory fields on the EAI FileSave dialog
                imProfileAttributeID[] requiredAttribs = new imProfileAttributeID[5];

                requiredAttribs[0] = imProfileAttributeID.imProfileType;
                requiredAttribs[1] = imProfileAttributeID.imProfileAuthor;
                requiredAttribs[2] = imProfileAttributeID.imProfileOperator;
                requiredAttribs[3] = imProfileAttributeID.imProfileDescription;
                requiredAttribs[4] = imProfileAttributeID.imProfileClass;

                IManFileSaveDlg dlg = pMyInterface as IManFileSaveDlg;
                dlg.RequiredList = requiredAttribs;

                // new documents that are based on existing profiled documents should:
                //   1. maintain the document class
                //   2. reset the default Author to the current user
                // This class-level variable is set by the Extensibility object's DocumentBeforeSaveAsOptions event
                // It is intended that this If clause should only execute for a SaveAs operation where the New Document
                // option is selected

                if (mIsDocUndergoingSaveAsProfiledInWorksite)
                {
                    Object activeOfficeDoc = GetActiveOfficeDocument();
                    String activeOfficeDocFullPath = activeOfficeDoc.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, activeOfficeDoc, null).ToString();

                    if (mExtensibility.GetDocumentFromPath(activeOfficeDocFullPath) is IManDocument originalIManDoc)
                    {
                        this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileClass, originalIManDoc.Class.Name));
                        this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileAuthor, GetSession.UserID));  //.Add imProfileAuthor, GetSession().UserID
                    }

                    //we can now reset the local flag
                    mIsDocUndergoingSaveAsProfiledInWorksite = false;
                }

                // iterate over NewProfileDefaults collection and set field values accordingly

                foreach (DMSProfileDefault profDefault in this.profileDefaults)
                {
                    // Location attribute is handled by setting DocOpenLocation property, as this is not
                    // accessible from the dialog's SetAttributeValueByID method
                    if ((imProfileAttributeID)profDefault.Id == imProfileAttributeID.imProfileLocation)
                        dlg.DocOpenLocation = profDefault.Value.ToString();
                    else
                        WriteProfileValueToDialog(dlg, (imProfileAttributeID)profDefault.Id, profDefault.Value.ToString());
                }
            }
            catch
            {

            }
        }

        private void MExtensibility_OnEditProfile(object objEditProfileCmd)
        {
            mEditProfileCmd = objEditProfileCmd as EditProfileCmd;
            mEditProfileCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mEditProfileCmd_PostOnOK);
        }

        private void MExtensibility_OnCreateNewVersion(object objNewVersionCmd)
        {
            mNewVersionCmd = objNewVersionCmd as NewVersionCmd;
            mNewVersionCmd.OnInitDialog += new IMANEXTLib._ICommandEvents_OnInitDialogEventHandler(mNewVersionCmd_OnInitDialog);
            mNewVersionCmd.PreOnOK += new IMANEXTLib._ICommandEvents_PreOnOKEventHandler(mNewVersionCmd_PreOnOK);
            mNewVersionCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mNewVersionCmd_PostOnOK);
        }

        private void MExtensibility_DocumentBeforeSaveAsOptions(object Doc, ref bool IgnoreIManageSave)
        {
            mIsDocUndergoingSaveAsProfiledInWorksite = this.IsProfiled(Doc);
        }


        // IMANEXT2Lib.IManFileSaveCmd Implementation
        /// <summary>
        /// Sets the required fields in the SaveAs dialog
        /// </summary>
        /// <param name="pMyInterface"></param>
        void mFileSaveCmd_OnInitDialog(object pMyInterface)
        {
            try
            {
                // set mandatory fields on the EAI FileSave dialog
                imProfileAttributeID[] requiredAttribs = new imProfileAttributeID[5];

                requiredAttribs[0] = imProfileAttributeID.imProfileType;
                requiredAttribs[1] = imProfileAttributeID.imProfileAuthor;
                requiredAttribs[2] = imProfileAttributeID.imProfileOperator;
                requiredAttribs[3] = imProfileAttributeID.imProfileDescription;
                requiredAttribs[4] = imProfileAttributeID.imProfileClass;

                IManFileSaveDlg dlg = pMyInterface as IManFileSaveDlg;
                dlg.RequiredList = requiredAttribs;

                // new documents that are based on existing profiled documents should:
                //   1. maintain the document class
                //   2. reset the default Author to the current user
                // This class-level variable is set by the Extensibility object's DocumentBeforeSaveAsOptions event
                // It is intended that this If clause should only execute for a SaveAs operation where the New Document
                // option is selected

                if (mIsDocUndergoingSaveAsProfiledInWorksite)
                {
                    Object activeOfficeDoc = GetActiveOfficeDocument();
                    String activeOfficeDocFullPath = activeOfficeDoc.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, activeOfficeDoc, null).ToString();
                    IManDocument originalIManDoc = mExtensibility.GetDocumentFromPath(activeOfficeDocFullPath) as IManDocument;

                    if (originalIManDoc != null)
                    {
                        this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileClass, originalIManDoc.Class.Name));
                        this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileAuthor, GetSession.UserID));  //.Add imProfileAuthor, GetSession().UserID
                    }

                    //we can now reset the local flag
                    mIsDocUndergoingSaveAsProfiledInWorksite = false;
                }

                // iterate over NewProfileDefaults collection and set field values accordingly

                foreach (DMSProfileDefault profDefault in this.profileDefaults)
                {
                    // Location attribute is handled by setting DocOpenLocation property, as this is not
                    // accessible from the dialog's SetAttributeValueByID method
                    if ((imProfileAttributeID)profDefault.Id == imProfileAttributeID.imProfileLocation)
                        dlg.DocOpenLocation = profDefault.Value.ToString();
                    else
                        WriteProfileValueToDialog(dlg, (imProfileAttributeID)profDefault.Id, profDefault.Value.ToString());
                }
            }
            catch
            {

            }
        }
        /// <summary>
        /// Triggered when the user clicks OK in the SaveAs dialog
        /// </summary>
        /// <param name="pMyInterface"></param>
        void mFileSaveCmd_PreOnOK(object pMyInterface)
        {
            Boolean cancelDialog = false;
            IManFileSaveDlg dlg = pMyInterface as IManFileSaveDlg;

            dlg.CloseOnOK = !cancelDialog;
        }

        // IMANEXTLib.NewVersionCmd Implementation
        void mNewVersionCmd_OnInitDialog(object pMyInterface)
        {
            mIsDocUndergoingSaveAsProfiledInWorksite = false;
        }
        void mNewVersionCmd_PreOnOK(object pMyInterface)
        {
            NewVersionDlg dlg = pMyInterface as NewVersionDlg;
            Boolean cancelDialog = false;
            dlg.CloseOnOK = !cancelDialog;
        }

        // IMANEXTLib.PortableNewProfileDlg Implementation
        void mPortableNewProfileDlg_OnInitDialog(object pMyInterface)
        {
            mCancelledPortableNewProfileDlg = false;

            // set mandatory fields on the EAI FileSave dialog
            imProfileAttributeID[] requiredAttribs = new imProfileAttributeID[5];

            requiredAttribs[0] = imProfileAttributeID.imProfileType;
            requiredAttribs[1] = imProfileAttributeID.imProfileAuthor;
            requiredAttribs[2] = imProfileAttributeID.imProfileOperator;
            requiredAttribs[3] = imProfileAttributeID.imProfileDescription;
            requiredAttribs[4] = imProfileAttributeID.imProfileClass;
           

            NewProfileDlg dlg = pMyInterface as NewProfileDlg;

            dlg.RequiredList = requiredAttribs;

           
            this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileType, "WORDX"));
            this.profileDefaults.Add(new DMSProfileDefault(imProfileAttributeID.imProfileOperator, Environment.UserName.ToUpper()));

            // iterate over NewProfileDefaults collection and set field values accordingly

            foreach (DMSProfileDefault kvp in this.profileDefaults)
            {
                WriteProfileValueToDialog(dlg, (imProfileAttributeID)kvp.Id, kvp.Value.ToString());
            }
        }
        void mPortableNewProfileDlg_OnCancel(object pMyInterface)
        {
            mCancelledPortableNewProfileDlg = true;
        }

        /// <summary>
        /// Adds the print job to the history of the document
        /// </summary>
        /// <param name="printjob">A print job object representing the printing of the document</param>
        internal void templates_DocumentAfterPrint(PrintJob printjob)
        {
            String fullName = printjob.Document.FullName;
            IManDocument imanDoc = mExtensibility.GetDocumentFromPath(fullName) as IManDocument;
            IManHistoryList imanHistory = imanDoc.HistoryList;
            String text = printjob.Duplex == 1 ? "Printed pages " + printjob.Pages + " of " + printjob.Jobtype + " simplex": 
                "Printed pages " + printjob.Pages + " of " + printjob.Jobtype + " duplex";
            //imanHistory.Add(imHistEvent.imHistoryPrint, 0, printjob.PageCountVariable, "WINWORD",text , printjob.Printer.Name, "", "");
            try
            {
                //imanHistory.Refresh();
            }
            catch { }
        }

        // iManO2k.iManageExtensibility Implementation
        void mExtensibility_DocumentBeforePrint(object Doc, ref bool IgnoreIManagePrint, ref bool Cancel)
        {
            if (Cancel == true)
                return;

            try
            {
                if (disablePrintHandling)
                {
                    IgnoreIManagePrint = true;

                    // Temporary code to add a print action to the history table.
                    // TODO: refactor and enhance to include JB's code which calculates number of pages printed from the PrintSettings object

                    String fullName = Doc.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, Doc, null).ToString();

                    IManDocument imanDoc = mExtensibility.GetDocumentFromPath(fullName) as IManDocument;
                    
                    IManHistoryList imanHistory = imanDoc.HistoryList;

                    //On Local Error Resume Next
                    Object docProp = Doc.GetType().InvokeMember("CustomDocumentProperties", BindingFlags.InvokeMethod, null, Doc, new Object[] { "ashPageCount" });
                    String lngPageCount = docProp.GetType().InvokeMember("Value", BindingFlags.GetProperty, null, Doc, null).ToString();

                    try
                    {
                        if (Convert.ToInt16(lngPageCount) > 0)
                            imanHistory.Add(imHistEvent.imHistoryPrint, 0, Convert.ToInt16(lngPageCount), "WINWORD", "", "", "", "");
                        else
                            imanHistory.Add(imHistEvent.imHistoryPrint, 0, 0, "WINWORD", "Number of pages was not successfully determined", "", "", "");
                    }
                    catch { }
                    imanHistory.Refresh();
                }
            }
            catch
            {

            }
        }
        void mExtensibility_DocumentBeforeSaveAsOptions(object Doc, ref bool IgnoreIManageSave)
        {
            mIsDocUndergoingSaveAsProfiledInWorksite = this.IsProfiled(Doc);
        }
        void mExtensibility_OnCreateNewVersion(object objNewVersionCmd)
        {
            mNewVersionCmd = objNewVersionCmd as NewVersionCmd;
            mNewVersionCmd.OnInitDialog += new IMANEXTLib._ICommandEvents_OnInitDialogEventHandler(mNewVersionCmd_OnInitDialog);
            mNewVersionCmd.PreOnOK += new IMANEXTLib._ICommandEvents_PreOnOKEventHandler(mNewVersionCmd_PreOnOK);
            mNewVersionCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mNewVersionCmd_PostOnOK);
        }

        void mNewVersionCmd_PostOnOK(object pMyInterface)
        {
            clsDynamicInserts.UpdateDocumentReference(Globals.ThisAddIn.Application.ActiveDocument); ;
        }
        void mExtensibility_OnEditProfile(object objEditProfileCmd)
        {
            mEditProfileCmd = objEditProfileCmd as EditProfileCmd;
            mEditProfileCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mEditProfileCmd_PostOnOK);
        }

        void mEditProfileCmd_PostOnOK(object pMyInterface)
        {
            UserControl ctl = clsWordApp.GetActiveControl();
            this.FetchAuthor(ctl);
        }
        void mExtensibility_OnIManFileSave(object objIManFileSaveCmd)
        {
            mFileSaveCmd = objIManFileSaveCmd as IManFileSaveCmd;

            mNewVersionCmd.OnInitDialog += new IMANEXTLib._ICommandEvents_OnInitDialogEventHandler(mNewVersionCmd_OnInitDialog);
            mNewVersionCmd.PreOnOK += new IMANEXTLib._ICommandEvents_PreOnOKEventHandler(mNewVersionCmd_PreOnOK);
            mNewVersionCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mNewVersionCmd_PostOnOK);
        }
        void mExtensibility_DocumentBeforeOpen(string FileName, ref bool Cancel)
        {
            IManDocument prf = mExtensibility.GetDocumentFromPath(FileName) as IManDocument;
            String comment = prf.Comment;

            if (AssertComment(comment))
            {
                Cancel = true;
                this.CreateNBI(prf.Number.ToString(), prf.CheckoutPath, prf.Comment, prf.Database.Name, prf.Version.ToString());
            }
        }

        // Public properties
        /// <summary>
        /// Set the save location for creating a new document (for use when creating a document in outlook
        /// using the new - ashurst word document on a filesite folder)
        /// </summary>
        public String FolderPath
        {
            set { folderPath = value; }
        }
        /// <summary>
        /// Assert if the comment contains the nbirequestdata string
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public Boolean AssertComment(String comment)
        {
            return comment.ToLower().Contains("nbirequestdata");
        }
        /// <summary>
        /// Checks to see if the user is connected to a particular DM server
        /// and connects using trusted login
        /// </summary>
        /// <param name="serverName"></param>
        /// <returns></returns>
        public Boolean CheckAndConnect(String serverName)
        {
            foreach (IManSession session in this.GetDMS.Sessions)
                if (session.ServerName == serverName)
                    return true;

            IManSession Session = this.GetDMS.Sessions.Add(serverName);

            try
            {
                Session.TrustedLogin();
            }
            catch (Exception) { }

            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        public String DocRef
        {
            get
            {
                String returnVal = "";
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                if (prf != null)
                {
                    String Client = prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom1).ToString();
                    String Matter = prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom2).ToString();
                    String AuthorID = prf.GetAttributeValueByID(imProfileAttributeID.imProfileAuthor).ToString();

                    returnVal = AuthorID;

                    if (returnVal.Length > 0)
                        returnVal += @"\";

                    if (Client.Length > 0)
                        returnVal = returnVal + Client + "." + Matter;
                }

                return returnVal;
            }
        }
        /// <summary>
        /// Returns the document reference (db name, db author, number and version)
        /// </summary>
        
        public String MatterDescription
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);
                foreach (IManFolder fld in prf.Folders)
                    return fld.Workspace.GetAttributeValueByID(imProfileAttributeID.imProfileCustom2Description).ToString();
                return "";
            }
        }

        public String ClientDescription
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);
                foreach (IManFolder fld in prf.Folders)
                    return fld.Workspace.GetAttributeValueByID(imProfileAttributeID.imProfileCustom1Description).ToString();
                return "";
            }
        }
        
        
        public String ClientMatterCode
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                String docRef = "";

                if (prf != null)
                {
                    docRef = prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom1) + ".";
                    docRef += prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom2);
                   
                }

                return docRef;
            }
        }


     
        
        public String DocumentReference
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                String docRef = "";

                if (prf != null)
                {
                    docRef = prf.Database.Name + "\\";
                    docRef += prf.Author.Name + "\\";
                    docRef += prf.Number + "." + prf.Version.ToString("00");
                }

                return docRef;
            }
        }

        public String DatabaseAndDocumentNumberVersion
        {
            get
            {
                if (HostApp.Documents.Count == 0)
                    return "";
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                if (prf != null)
                    return prf.Database.Name + "." + prf.Number + "." + prf.Version.ToString();

                return "";
            }
        }
        /// <summary>
        /// Returns the document number and version only
        /// </summary>
        public String DocumentNumberAndVersion
        {
            get
            {
                if (HostApp.Documents.Count == 0)
                    return "";
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                if (prf != null)
                    return prf.Number + "." + prf.Version.ToString();

                return "";
            }
        }
        /// <summary>
        /// IManage document database name.
        /// </summary>
        /// <param name="fullName">The full path to the active word document.</param>
        /// <returns>Returns the database that the document resides in</returns>
        public String DatabaseName(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.Database.Name;

            return "";
        }
        /// <summary>
        /// Returns the document number only.
        /// </summary>
        public String DocumentNumber
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                if (prf != null)
                    return prf.Number.ToString();

                return "";
            }
        }
        /// <summary>
        /// Returns the unique SID of the document, this is very similar to an NRL
        /// link and can be used to retriev a document from the dms
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public String ObjectID(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.ObjectID;

            return "";
        }
        /// <summary>
        /// Returns the comment field from the document profile.
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public String Comment(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.Comment;

            return "";
        }
        /// <summary>
        /// Returns the DMS document description
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public String Description(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.Description;

            return "";
        }
        /// <summary>
        /// Returns the author FullName property from the profile.
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        public String Author(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.Author.FullName;

            return "";
        }
        /// <summary>
        /// The Author ID for the current document.
        /// </summary>
        /// <param name="fullName">The full path to the active document</param>
        /// <returns>Return the author name property (ID)</returns>
        public String AuthorID(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return prf.Author.Name;

            return "";
        }
        /// <summary>
        /// Sets the author id on the active document
        /// </summary>
        /// <param name="name"></param>

        public void SetAuthor(string name)
        {
            IManDocument prf = this.GetDocument(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            NRTDocument doc = (NRTDocument)prf;

            if (prf != null)
            {
                IManUser user = GetSession.PreferredDatabase.GetUser(name);
                NRTUser nrtuser = doc.Database.GetUser(name);

                try
                {
                    doc.SetAttributeValueByID(AttributeID.nrAuthor, nrtuser);
                    object e = null;
                    doc.UpdateProfile(e);
                }
                catch { }
            }
                
        }

        /// <summary>
        /// Returns the document type
        /// </summary>
        /// <param name="fullName"></param>
        /// <returns></returns>
        /// 

        public String ClassType(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
                return System.Globalization.CultureInfo.CurrentCulture.TextInfo.
                                ToTitleCase(prf.Class.Description.ToLower());

            return "";
        }
        /// <summary>
        /// Check-out a document from the DMS using the SID reference
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public String OpenDocumentFromId(String objectId)
        {
            IManDocument imandoc = this.GetDMS.GetObjectByID(objectId) as IManDocument;
            return this.OpenIManDoc(imandoc as NRTDocument);
        }
        /// <summary>
        /// Returns the file number (client number and matter number)
        /// </summary>
        public String FileNumber
        {
            get
            {
                IManDocument prf = this.GetDocument(HostApp.ActiveDocument.FullName);

                if (prf != null)
                {
                    String Client = prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom1).ToString();
                    String Matter = prf.GetAttributeValueByID(imProfileAttributeID.imProfileCustom2).ToString();

                    if (Client.Length + Matter.Length > 0)
                        return Client + "." + Matter;
                }

                return "";
            }
        }
        /// <summary>
        /// Checks out the document from worksite.
        /// </summary>
        /// <param name="DatabaseName">The database that the document resides on.</param>
        /// <param name="DocumentNumber">The document number.</param>
        /// <returns>Returns the path to the document in the checkout folder.</returns>
        public String CheckOut(String DatabaseName, int DocumentNumber)
        {
            try
            {
                IManDocument imanDoc = this.GetDocument(DatabaseName, DocumentNumber, 0, false);

                if (!imanDoc.CheckedOut)
                    imanDoc.CheckOut(imanDoc.CheckoutPath, imCheckOutOptions.imDontReplaceExistingFile, DateTime.Now, "");

                imanDoc.SetAttributeByID(imProfileAttributeID.imProfileType, "WORDX");
                imanDoc.Update();

                return imanDoc.CheckoutPath;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Updates the reference stored as a custom document property
        /// </summary>
        /// <param name="TemplateId"></param>
        /// <param name="DocRef"></param>
        /// <param name="OurRef"></param>
        
        /// <summary>
        /// Clears the comment field in the profile
        /// </summary>
        /// <param name="fullName"></param>
        public void ClearComment(String fullName)
        {
            IManDocument prf = this.GetDocument(fullName);

            if (prf != null)
            {
                prf.Comment = "--";
                prf.Update();
            }
        }

        /// <summary>
        /// Update the Caption in Microsoft Word with the 
        /// correct document details (document reference and description)
        /// </summary>
        /// <param name="OfficeDocument"></param>
        /// <param name="documentFullName"></param>
        public void UpdateCaption(Object OfficeDocument, String documentFullName)
        {
            NRTDocument IManageDoc = this.GetDocument(documentFullName) as NRTDocument;
            AssertIntegratedWindowCaptionFromIManDoc(OfficeDocument, IManageDoc);
        }



        /// <summary>
        /// Saves the document to the DMS by checking it in and checking out again
        /// Creates a copy of the document in the temp directory and deletes it on the next save
        /// </summary>
        /// <param name="fileName">The NRTPortbl file location of the current document</param>
        /// <param name="doc">The Word document being saved</param>
        public void QuickSave(String fileName, Word.Document doc)
        {
            if(this.IsOnline)
                if(this.IsProfiled(doc))
                {
                    try
                    {
                        System.IO.File.Delete(tempPath);
                    }
                    catch { }
                    String checkoutPath = System.IO.Path.Combine(System.IO.Path.GetTempPath(), System.IO.Path.GetTempFileName());
                    tempPath = checkoutPath;
                    NRTDocument prf = this.GetDocument(fileName) as NRTDocument;
                    doc.Saved = false;
                    doc.Save();

                    try
                    {
                        if(System.IO.File.Exists(checkoutPath))
                            System.IO.File.Delete(checkoutPath);
                    }
                    catch { }

                    System.IO.File.Copy(doc.FullName, checkoutPath);
                    object oErr = null;
                    //The only way to get the document to check in is to copy to a temp location, check in and return the checkout path to
                    // the currently open file.  Checking in using the current file path causes a Worksite access error.
                    prf.CheckIn(checkoutPath, CheckinDisposition.nrReplaceOriginal, CheckinOptions.nrKeepCheckedOut, oErr);
                    prf.CheckoutPath = doc.FullName;

                    try
                    {
                        if (System.IO.File.Exists(checkoutPath))
                            System.IO.File.Delete(checkoutPath);
                    }
                    catch { }
                }
                else
                {
                    doc.Saved = false;
                    doc.Save();
                }
            else
            {
                doc.Saved = false;
                doc.Save();
            }
        }

        /// <summary>
        /// Convoluted way of checking in a document.  Checks the document in then checks it back out.  
        /// If the document isn't checked in the file is saved by the user through the Windwows SaveFileDialog
        /// </summary>
        /// <param name="fileName">The path of the open document</param>
        public void SaveAndReplace(String fileName)
        {
            String checkoutPath = "";

            NRTDocument prf = this.GetDocument(fileName)
                as NRTDocument;

            NRTDocument[] docs = new NRTDocument[1];
            docs[0] = prf;

            ContextItems contextItems = new ContextItems();
            CheckinCmd checkinCmd = new CheckinCmd();

            contextItems.Add("SelectedNRTDocuments", docs);
            contextItems.Add("IManExt.CheckInCmd.NewVersion", false);
            contextItems.Add("IManExt.CalledFromIntegration", true);
            contextItems.Add("IManExt.CheckIn.EnableNewDocument", false);
            contextItems.Add("IManExt.CheckIn.EnableReplaceOriginal", true);
            contextItems.Add("IManExt.CheckinCmd.CheckInOption", true);
            contextItems.Add("IManExt.CheckinCmd.NoCmdUI", true);
            contextItems.Add("IManExt.CheckinCmd.Integration", true);

            checkinCmd.Initialize(contextItems);
            checkinCmd.Update();

            if (checkinCmd.Status == 0) // cmd.Status && IMANEXTLib.CommandStatus.nrActiveCommand
            {
                checkinCmd.Execute();

                bool bRefresh = Convert.ToBoolean(contextItems.Item("IManExt.Refresh"));

                if (bRefresh)
                {
                    try
                    {
                        checkoutPath = prf.LatestVersion.CheckoutPath;

                        if (checkoutPath.Length == 0)
                        {
                            checkoutPath = this.CheckoutDocument(prf.LatestVersion.Database.Name,
                                prf.LatestVersion.Number, prf.LatestVersion.Version);
                        }


                    }
                    catch (Exception) { }
                }
                else
                {
                    System.Windows.Forms.SaveFileDialog dlg = new SaveFileDialog();
                    dlg.Filter = "Word Document (*.docx)|*.docx";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        checkoutPath = dlg.FileName;

                        Object ErrorResults = null;

                        String oldPath = prf.CheckoutPath;

                        prf.CheckIn(prf.CheckoutPath, CheckinDisposition.nrReplaceOriginal,
                            CheckinOptions.nrDontKeepCheckedOut, ErrorResults);

                        System.IO.File.Delete(oldPath);

                    }
                } 
                contextItems.Remove("IManExt.Refresh");
            } 
            
        }
        
        
        /// <summary>
        /// For use when converting doc to docx.  Saves a new version of the document
        /// </summary>
        /// <param name="fileName">The path of the document to save</param>
        /// <returns>The checkout path of the document</returns>
        public String SaveNewVersion(String fileName)
        {
            String checkoutPath = "";

            NRTDocument prf = this.GetDocument(fileName)
                as NRTDocument;

            NRTDocument[] docs = new NRTDocument[1];
            docs[0] = prf;
            
            ContextItems contextItems = new ContextItems();
            CheckinCmd checkinCmd = new CheckinCmd();
            
            contextItems.Add("SelectedNRTDocuments", docs);
            contextItems.Add("IManExt.CheckInCmd.NewVersion", true);
            contextItems.Add("IManExt.CalledFromIntegration", true);
            contextItems.Add("IManExt.CheckIn.EnableNewDocument", false);
            contextItems.Add("IManExt.CheckIn.EnableReplaceOriginal", false);
            contextItems.Add("IManExt.CheckinCmd.CheckInOption", true);
            //contextItems.Add("IManExt.CheckinCmd.NoCmdUI", true);
            contextItems.Add("IManExt.CheckinCmd.Integration", true);

            checkinCmd.Initialize(contextItems);
            checkinCmd.Update();

            if (checkinCmd.Status == 0) // cmd.Status && IMANEXTLib.CommandStatus.nrActiveCommand
            {
                checkinCmd.Execute();

                bool bRefresh = Convert.ToBoolean(contextItems.Item("IManExt.Refresh"));

                if (bRefresh)
                {
                    try
                    {
                        (prf.LatestVersion as IManDocument).SetAttributeByID(imProfileAttributeID.imProfileType, "WORDX");
                        object err = null;
                        prf.LatestVersion.UpdateProfile(ref err);

                        checkoutPath = prf.LatestVersion.CheckoutPath;

                        if (checkoutPath.Length == 0)
                        {
                            checkoutPath = this.CheckoutDocument(prf.LatestVersion.Database.Name,
                                prf.LatestVersion.Number, prf.LatestVersion.Version);
                        }


                    }
                    catch (Exception) { }
                }
                else
                {
                    System.Windows.Forms.SaveFileDialog dlg = new SaveFileDialog();
                    dlg.Filter = "Word Document (*.docx)|*.docx";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        checkoutPath = dlg.FileName;

                        Object ErrorResults = null;

                        String oldPath = prf.CheckoutPath;

                        prf.CheckIn(prf.CheckoutPath, CheckinDisposition.nrReplaceOriginal,
                            CheckinOptions.nrDontKeepCheckedOut, ErrorResults);

                        System.IO.File.Delete(oldPath);

                    }
                }

                contextItems.Remove("IManExt.Refresh");
            }

            return checkoutPath;
        }

        // Private methods & properties

        /// <summary>
        /// Attempts to connect to the COMAddin in Word to see if it exists
        /// </summary>
        /// <param name="App">Word.Application</param>
        /// <returns>An iManageExtensibility object</returns>
        private object AssertIManO2kExtensibility(Object App)
        {
            try
            {
                iManageExtensibility returnVal = new iManageExtensibility();
                returnVal.InitializeLifetimeService();
                returnVal.DMS = mandms;
                returnVal.Connect();
                //returnVal.DMS = GetDMS;
               
                

                //String buffer = "WorkSiteOffice2007Addins.Connect";

                //Object addins = App.GetType().InvokeMember("COMAddIns", BindingFlags.GetProperty, null, App, null);
                //Object addin = addins.GetType().InvokeMember("Item", BindingFlags.InvokeMethod, null, addins, new Object[] { buffer });
                //// return the Object property which should represent the iManageExtensibility object
                //object obj = addin.GetType().InvokeMember("Object", BindingFlags.GetProperty, null, addin, null) ;
                //// returnVal = obj.GetType().InvokeMember("Context", BindingFlags.GetProperty, null, addin, null) as iManageExtensibility;

                //returnVal = (iManageExtensibility) obj;
                //string e = obj.GetType().ToString();

               //obj.Connect();
                return returnVal;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Shows the EditProfile dialog box but does nothing afterwards
        /// </summary>
        public void ShowEditProfieCmd()
        {
            ContextItems contexts = new ContextItems();
            EditProfileCmd cmd = new EditProfileCmd();

            contexts.Add("IManDMS", this.GetDMS);
            contexts.Add("SelectedIManSession", this.GetSession);

            IManDatabase db = this.GetPreferredDatabase;

            // derive the application type from the document specified in the method parameter
            String docType = GetAppTypeFromDoc(db, Globals.ThisAddIn.Application.ActiveDocument);

            if (docType.Length == 0)
            {
                // if nothing was returned, attempt to derive the application type from the host application's name
                docType = GetAppTypeFromHostAppName(db, APP_NAME);
            }
            else
            {

            }

            if (docType.Length > 0)
            {
                // a valid application type (DocType) was evaluated, so add this to the list of context items
                contexts.Add("IManExt.Import.DocType", docType);
            }

            mEditProfileCmd = cmd;
            cmd.Initialize(contexts);
            
            cmd.Execute();

        }

        /// <summary>
        /// Creates a new profile for a new document
        /// </summary>
        /// <param name="OfficeDocument">A Word.Document</param>
        /// <returns>The checkout path of the document</returns>

        private String CreateNewProfileFromDoc_ConnectedMode(Object OfficeDocument)
        {
            String returnVal = "";

            // initialise the context items with the current DMS and active session
            ContextItems contexts = new ContextItems();

            contexts.Add("IManDMS", this.GetDMS);
            contexts.Add("SelectedIManSession", this.GetSession);

            IManDatabase db = this.GetPreferredDatabase;

            // derive the application type from the document specified in the method parameter
            String docType = GetAppTypeFromDoc(db, OfficeDocument);

            if (docType.Length == 0)
            {
                // if nothing was returned, attempt to derive the application type from the host application's name
                docType = GetAppTypeFromHostAppName(db, APP_NAME);
            }
            else
            {

            }

            if (docType.Length > 0)
            {
                // a valid application type (DocType) was evaluated, so add this to the list of context items
                contexts.Add("IManExt.Import.DocType", docType);
            }

            if (this.folderPath.Length > 0)
            {
                contexts.Add("IManExt2.DefaultLocation", this.folderPath);
                this.folderPath = "";
            }

            // instantiate and initialise the IManFileSaveCmd with the context items
            IManFileSaveCmd cmd = new IManFileSaveCmd();

            // set class-level IManFileSaveCmd object to be this instance of the command
            // (it is declared WithEvents, so will raise the same events as through any other method)
            mFileSaveCmd = cmd;
            try
            {
                mNewVersionCmd.OnInitDialog += new IMANEXTLib._ICommandEvents_OnInitDialogEventHandler(mNewVersionCmd_OnInitDialog);
                mNewVersionCmd.PreOnOK += new IMANEXTLib._ICommandEvents_PreOnOKEventHandler(mNewVersionCmd_PreOnOK);
                mNewVersionCmd.PostOnOK += new IMANEXTLib._ICommandEvents_PostOnOKEventHandler(mNewVersionCmd_PostOnOK);

            }
            catch { }
                mFileSaveCmd.OnCancel += new IMANEXT2Lib._ICommandEvents_OnCancelEventHandler(mFileSaveCmd_OnCancel);

            cmd.Initialize(contexts);
            cmd.Update();

            if (cmd.Status == 0) // cmd.Status && IMANEXTLib.CommandStatus.nrActiveCommand
            {
                // the command is ready to be executed
                cmd.Execute();

                Boolean clickedOK;

                // NB: unfortunately must use error handling here as there is no elegant method of
                // determining if the command was a success
                try
                {
                    clickedOK = Convert.ToBoolean(contexts.Item("IManExt.Refresh"));
                }
                catch { clickedOK = false; }

                if (clickedOK)
                {
                    // the command was successfully executed by the user: fetch the actual newly-imported document
                    IManDocument newIManDoc = contexts.Item("ImportedDocument") as IManDocument;

                    // fetch the path it should be checked out to
                    String newIManDocCheckoutPath = newIManDoc.CheckoutPath;

                    // utilise the CreateLocalDocument method to generate a PRF file for this document
                    PortableUpdate ptblUpdate = new PortableUpdate();
                    ptblUpdate.CreateLocalDocument(newIManDoc as NRTDocument, false);

                    // save the live Office document to the checkout path
                    OfficeDocument.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, OfficeDocument, new Object[] { newIManDocCheckoutPath });

                    // finally ensure that the window caption is correctly set
                    AssertIntegratedWindowCaptionFromIManDoc(OfficeDocument, newIManDoc as NRTDocument);

                    returnVal = newIManDocCheckoutPath;
                }
            }

            // reset class-level IManFileSaveCmd object
            mFileSaveCmd = null;
            return returnVal;

        }

        /// <summary>
        /// Closes and discards the current document if the Cancel button is pressed in the FileSaveCmd
        /// </summary>
        /// <param name="pMyInterface">The FileSaveCmd interface which is returned by the API</param>
        void mFileSaveCmd_OnCancel(object pMyInterface)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                    Globals.ThisAddIn.Application.ActiveDocument.Close(Word.WdSaveOptions.wdDoNotSaveChanges);
            }
            catch { }

            CancelPressed = true;
        }
        /// <summary>
        /// Updates the document reference with the new number and version when the OK button is pressed
        /// </summary>
        /// <param name="pMyInterface">The mFIleSaveCmd interface returned by the API</param>
        void mFileSaveCmd_PostOnOK(object pMyInterface)
        {
            if(Globals.ThisAddIn.Application.Documents.Count>0)
            clsDynamicInserts.UpdateDocumentReference(Globals.ThisAddIn.Application.ActiveDocument);
        }
        /// <summary>
        /// Creates a new profile if the DMS is in portable mode (Ie offline)
        /// </summary>
        /// <param name="OfficeDocument">The Word.Document to save</param>
        /// <returns>The checkout path of the document</returns>
        private String CreateNewProfileFromDoc_PortableMode(Object OfficeDocument)
        {
            NRTDocument objDocument = null;
            Object oErr = null;
            String strPath;
            long[] reqs = new long[3];
            String strDatabase;

            mExtensibility = AssertIManO2kExtensibility(this.HostApp) as iManageExtensibility;

            INRTSession oSess = mExtensibility.Context.Item("PortableNRTSession") as INRTSession;
            NRTDatabases objDatabases = oSess.Databases;

            mPortableNewProfileDlg = new PortableNewProfileDlg();

            mPortableNewProfileDlg.OnInitDialog += new IMANEXTLib.
             _INewProfileDlgEvents_OnInitDialogEventHandler(mPortableNewProfileDlg_OnInitDialog);

            mPortableNewProfileDlg.OnCancel += new IMANEXTLib.
            _INewProfileDlgEvents_OnCancelEventHandler(mPortableNewProfileDlg_OnCancel);

            mPortableNewProfileDlg.NRTDocument = objDocument;
            mPortableNewProfileDlg.Databases = objDatabases;

            strDatabase = Registry.ReadRegistry(Microsoft.Win32.Registry.CurrentUser, @"Software\Interwoven\WorkSite\8.0\Portable\5308\RecentDatabases", "0");

            mPortableNewProfileDlg.CloseOnOK = true;
            mPortableNewProfileDlg.Show(WindowId("winword"));

            if (mCancelledPortableNewProfileDlg)
                return "";

            //On Local Error Resume Next
            strDatabase = mPortableNewProfileDlg.DestinationObject.GetType().InvokeMember("Name", BindingFlags.GetProperty, null, mPortableNewProfileDlg.DestinationObject, null).ToString();

            Registry.WriteRegistryAsString(@"HKEY_CURRENT_USER\Software\Interwoven\WorkSite\8.0\Portable\Default Database", strDatabase);

            objDocument = objDatabases.Item(strDatabase).CreateDocument();

            strPath = Path.GetTempFileName() + ".docx";

            for (long lngAtt = 3; lngAtt <= 30; lngAtt++)
            {
                try
                {
                    objDocument.SetAttributeValueByID((AttributeID)lngAtt, mPortableNewProfileDlg.GetAttributeByID((AttributeID)lngAtt));
                }
                catch { }
            }

            Object activeDoc = GetActiveOfficeDocument();
            activeDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, activeDoc, new Object[] { strPath });
            activeDoc.GetType().InvokeMember("Saved", BindingFlags.SetProperty, null, activeDoc, new Object[] { true });

            objDocument = objDocument.CheckIn(strPath, CheckinDisposition.nrNewDocument, CheckinOptions.nrKeepCheckedOut, oErr);
            activeDoc.GetType().InvokeMember("SaveAs", BindingFlags.InvokeMethod, null, activeDoc, new Object[] { objDocument.CheckoutPath });

            AssertIntegratedWindowCaptionFromIManDoc(activeDoc, objDocument);

            if (File.Exists(strPath))
                File.Delete(strPath);

            return objDocument.CheckoutPath;

        }
        /// <summary>
        /// Retrieves the document type from a Word.Document (WORD or WORDX)
        /// </summary>
        /// <param name="Database">The session's database</param>
        /// <param name="OfficeDocument">A Word.Document</param>
        /// <returns>The application type as a string</returns>
        private String GetAppTypeFromDoc(IManDatabase Database, Object OfficeDocument)
        {
            try
            {
                String returnVal;
                String fullPath = OfficeDocument.GetType().InvokeMember("FullName", BindingFlags.GetProperty, null, OfficeDocument, null).ToString();

                IManDocumentType docType = Database.GetDocumentTypeFromPath(fullPath);
                returnVal = docType.Name;

                return returnVal;
            }
            catch
            {
                return "";
            }
        }
        
        /// <summary>
        /// Gets the application type (WORDX,WORD)
        /// </summary>
        /// <param name="Database">The session's database</param>
        /// <param name="ApplicationName">Not required</param>
        /// <returns></returns>
        
        private String GetAppTypeFromHostAppName(IManDatabase Database, String ApplicationName)
        {
            String returnVal = "";

            try
            {
                //ApplicationName

                IManDocumentTypes docTypes = Database.SearchDocumentTypes("Word 2007", imSearchAttributeType.imSearchFullName, true);

                if (docTypes.Count > 0)
                {
                    IManDocumentType docType = docTypes.Item(1);
                    returnVal = docType.Name;
                }
                return returnVal;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Sets the window's caption
        /// </summary>
        /// <param name="OfficeDocument">The document in the window</param>
        /// <param name="IManageDoc">The document as a FileSIte document</param>
        /// <returns></returns>
        
        private String AssertIntegratedWindowCaptionFromIManDoc(Object OfficeDocument, NRTDocument IManageDoc)
        {
            // initialise return value to be Name property of document
            String returnVal = OfficeDocument.GetType().InvokeMember("Name", BindingFlags.GetProperty, null, OfficeDocument, null).ToString();
            String windowCaption = "";

            try
            {
                windowCaption = GetIntegratedWindowCaptionFromIManDoc(IManageDoc);
                Object windows = OfficeDocument.GetType().InvokeMember("Windows", BindingFlags.GetProperty, null, OfficeDocument, null);

                // pass in the appropriate flags (evaluated above)
                Object window = windows.GetType().InvokeMember("Item", BindingFlags.InvokeMethod, null, windows, new Object[] { 1 });

                // set the Caption property of the document's Window
                window.GetType().InvokeMember("Caption", BindingFlags.SetProperty, null, window, new Object[] { windowCaption });

                return returnVal;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Builds up the caption based on document number, version etc
        /// </summary>
        /// <param name="IManageDoc">A FileSIte document</param>
        /// <returns>The caption to go on the window</returns>
        private String GetIntegratedWindowCaptionFromIManDoc(NRTDocument IManageDoc)
        {
            try
            {
                String returnVal = "#";

                returnVal += IManageDoc.Number.ToString();
                returnVal += "v";
                returnVal += IManageDoc.Version.ToString();
                returnVal += "<";
                returnVal += IManageDoc.Database.Name;
                returnVal += "> - ";
                returnVal += IManageDoc.Description;

                return returnVal;
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Gets the path to an application from the registry
        /// </summary>
        /// <param name="ProgID">The ProgID of the application</param>
        /// <returns>The path of the application</returns>
        private String GetAppPathFromProgId(String ProgID)
        {
            String returnVal = Registry.ReadRegistry(Microsoft.Win32.Registry.ClassesRoot, ProgID + @"\CLSID", "");
            returnVal = Registry.ReadRegistry(Microsoft.Win32.Registry.ClassesRoot, @"CLSID\" + returnVal + @"\InprocServer32", "");

            return returnVal;
        }

        /// <summary>
        /// Gets the activedocument
        /// </summary>
        /// <returns>A Word.Document</returns>
        private Object GetActiveOfficeDocument()
        {
            try
            {
                return this.HostApp.GetType().InvokeMember("ActiveDocument", BindingFlags.GetProperty, null, this.HostApp, null);
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// Checks to see if any documents are open
        /// </summary>
        /// <returns>True if there is at least one document open</returns>
        private Boolean IsDocumentActive()
        {
            try
            {
                Object documentCollection = this.HostApp.GetType().InvokeMember("Documents", BindingFlags.GetProperty, null, this.HostApp, null);
                return (Convert.ToInt16(documentCollection.GetType().InvokeMember("Count", BindingFlags.GetProperty, null, documentCollection, null)) > 0);
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// Validates whether or not FileSite is installed
        /// </summary>
        private void AssertIManageIsInstalled()
        {
            // first attempt to get path to IManage DLL by fetching from the registry details
            // of the root IManage COM object
            String pathToIManage = FileSiteCheck.GetAppPathFromProgId("IManage.ManDMS");

            if (pathToIManage.Length > 0)
            {
                // we have a path: as a safety measure assert that the IManage.dll object actually exists
                mIsInstalled = System.IO.File.Exists(pathToIManage);
            }
        }
        /// <summary>
        /// Writes a profile property to a dialog box
        /// </summary>
        /// <param name="Dialog">The dialog box</param>
        /// <param name="Field">The property</param>
        /// <param name="Value">The value of the property</param>
        private void WriteProfileValueToDialog(Object Dialog, imProfileAttributeID Field, String Value)
        {
            try
            {
                Dialog.GetType().InvokeMember("SetAttributeValueByID", BindingFlags.InvokeMethod, null, Dialog,
                    new Object[] { Convert.ToInt64(Field), Value, true });
            }
            catch { }
        }

        /// <summary>
        /// Populates the user control with author details based on the author of the current document
        /// </summary>
        /// <param name="control">The user control (task pane) to fill in</param>
        public void FetchAuthor(UserControl control)
        {
            String author = this.AuthorID(Globals.ThisAddIn.Application.ActiveDocument.FullName);
            ADSearcher ads = new ADSearcher(author);
            ADUser adu = ads.GetUser();
            try
            {
                control.Controls["tbxAuthorTel"].Text = adu.TelephoneNumber;
                control.Controls["tbxMail"].Text = adu.Mail;
                control.Controls["tbxFax"].Text = adu.Fax;
                control.Controls["tbxAuthor"].Text = adu.FullName;
                control.Controls["tbxTitle"].Text = adu.Title;
            }
            catch { }
        }

        internal Boolean DownloadDocument(String database, long documentNumber, long version, String path, DateTime lastModified)
        {
            try
            {
                 IManDocument doc = GetDocument(database, documentNumber, version, true);
                 if (doc == null)
                 {
                     if (ThisAddIn.SHOWMESSAGES)
                     System.Windows.Forms.MessageBox.Show("Unable to find document number " + documentNumber + " from " + database + " to " + path);
                     return false;
                 }
                 NRTDocument nrtDoc = (NRTDocument)doc;
                if (lastModified < nrtDoc.EditDate)
                {
                    try
                    {
                    if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                    }
                    catch { path += "^2"; }

                    doc.GetCopy(path, imGetCopyOptions.imNativeFormat);
                    return true;
                }
                return false;
            }
            catch
            {
                throw new InvalidOperationException();
                return false; }
        }

        internal void DisconnectTempSession()
        {
            londonSession.Logout();
            londonSession = null;
        }
    }
    /// <summary>
    /// A List class which can only contain DMSProfileDefault objects
    /// </summary>
    public class DMSProfileDefaults : List<DMSProfileDefault> { }
    /// <summary>
    /// Object which represents a DMS profile value
    /// </summary>
    public class DMSProfileDefault
    {
        private Object id;
        private String value;

        /// <summary>
        /// Constructor to Initialise the name and the value 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public DMSProfileDefault(Object id, String value)
        {
            this.id = id;
            this.value = value;
        }
        /// <summary>
        /// Get/Set the id
        /// </summary>
        public Object Id
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// Get/Set the value
        /// </summary>
        public String Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }

   public class FileSiteCheck
   {
       public static Boolean IsInstalled()
       {
           String pathToIManage = FileSiteCheck.GetAppPathFromProgId("IManage.ManDMS");

           if (pathToIManage.Length > 0)
           {
               // we have a path: as a safety measure assert that the IManage.dll object actually exists
               return System.IO.File.Exists(pathToIManage);
           }
           return false;
       }

       public static String GetAppPathFromProgId(String ProgID)
       {
           String returnVal = Registry.ReadRegistry(Microsoft.Win32.Registry.ClassesRoot, ProgID + @"\CLSID", "");
           returnVal = Registry.ReadRegistry(Microsoft.Win32.Registry.ClassesRoot, @"CLSID\" + returnVal + @"\InprocServer32", "");

           return returnVal;
       }
   }
}


