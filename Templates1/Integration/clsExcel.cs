﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace Templates1
{
    public class clsExcel
    {
        private Boolean NeedToDisposeOfExcel;
        private Excel.Application xlApp;
        
        public clsExcel()
        {

        }

        public Excel.Application LoadExcel()
        {
            xlApp = null;
            System.Type type = System.Type.GetTypeFromProgID("Excel.Application");
            try
            {
                xlApp = (Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                NeedToDisposeOfExcel = false;
            }
            catch { }
            if (xlApp == null)
            {
                xlApp = (Excel.Application)System.Activator.CreateInstance(type);
                NeedToDisposeOfExcel = true;
            }
            xlApp.Visible = true;
            return xlApp;
        }

        public Excel.Worksheet CreateWorkBookWithSheet()
        {
            Excel.Workbook wbk = xlApp.Workbooks.Add();
            return wbk.Sheets[1];
        }
    }
}
