﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Net.Mail;


namespace Templates1
{
 /// <summary>
 /// A wrapper for Outlook
 /// </summary>
    public class clsOutlook
    {
        private Outlook.Application olApp;
        private const String ERRORLOG = "TempoErrorLog.log";
        /// <summary>
        /// Constructor either gets the open version of Outlook or creates a new instance
        /// </summary>
        public clsOutlook()
        {
            try
            {
                olApp = (Outlook.Application)Microsoft.VisualBasic.Interaction.GetObject("", "Outlook.Application");
            }
            catch 
            { 
                olApp = new Outlook.Application();
            }
            
        }
        /// <summary>
        /// Retrieves contact details from the Outlook address book
        /// </summary>
        /// <param name="lvwContacts">A list view object into which to put the details</param>
        public void GetContacts(ListView lvwContacts)
        {
            lvwContacts.Items.Clear();
            Outlook.NameSpace ns = olApp.GetNamespace("MAPI");
     
            Outlook.MAPIFolder cf  =  ns.GetDefaultFolder (Outlook.OlDefaultFolders.olFolderContacts);
            Outlook.Items ctcItems =  cf.Items;
            Outlook.ContactItem ctc;
            for (int j = 1; j < (ctcItems.Count + 1); j++)
            {
                ctc = (Outlook.ContactItem)ctcItems[j];
                ListViewItem lvi = new ListViewItem((ctc.FirstName + " " + ctc.LastName).Trim());
                lvi.SubItems.Add(ctc.CompanyName);
                lvi.SubItems.Add(ctc.JobTitle);
                lvi.SubItems.Add(ctc.BusinessAddress);
                lvi.SubItems.Add(ctc.Title);
                lvi.SubItems.Add(ctc.BusinessFaxNumber);
                lvwContacts.Items.Add(lvi);
            }
        }

        /// <summary>
        /// Filters a list view object down to those contacts that match the criteria
        /// </summary>
        /// <param name="lvwContacts">The list view control</param>
        /// <param name="filter">The filter to apply</param>
        public void GetContactsFiltered(ListView lvwContacts, String filter)
        {
            lvwContacts.Items.Clear();
            Outlook.NameSpace ns = olApp.GetNamespace("MAPI");

            Outlook.MAPIFolder cf = ns.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts);
            Outlook.Items ctcItems = cf.Items;
            Outlook.ContactItem ctc;
            for (int j = 1; j < (ctcItems.Count + 1); j++)
            {
                ctc = (Outlook.ContactItem)ctcItems[j];
                if(ctc.FullName.ToLower().Contains(filter.ToLower()))
                {
                ListViewItem lvi = new ListViewItem((ctc.FirstName + " " + ctc.LastName).Trim()); 
                lvi.SubItems.Add(ctc.CompanyName);
                lvi.SubItems.Add(ctc.JobTitle);
                lvi.SubItems.Add(ctc.BusinessAddress);
                lvi.SubItems.Add(ctc.Title);
                lvwContacts.Items.Add(lvi);
                }
            }
        }

        /// <summary>
        /// Used to send an email when an error occurs
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public void CreateAndSendMailItem(String subject, String body)
        {
            try
            {
                using (SmtpClient smtp = new SmtpClient("exchsmtp"))
                {
                    smtp.UseDefaultCredentials = true;
                    MailMessage mail = new MailMessage(Environment.UserName + "@" + "mishcon.com", "justin.blair@mishcon.com");
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.Priority = MailPriority.Low;
                    Attachment att = null;

                    if (System.IO.File.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ERRORLOG)))
                    {
                        att = new Attachment(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ERRORLOG));
                        mail.Attachments.Add(att);
                    }
                   
                    smtp.Send(mail);
                    if (att != null)
                        att.Dispose();

                    try
                    {
                        if (System.IO.File.Exists(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ERRORLOG)))
                            System.IO.File.Delete(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ERRORLOG));
                    }
                    catch { }
                }
            }
            catch 
            {
                using (System.IO.StreamWriter stream = new System.IO.StreamWriter(System.IO.Path.Combine(ThisAddIn.PROGRAMDATA, ERRORLOG), true))
                {
                    stream.Write(DateTime.Now + Environment.NewLine + body);
                    stream.Close();
                    stream.Dispose();
                }
            }
            
            
            //if (olApp.Session.ExchangeConnectionMode == Outlook.OlExchangeConnectionMode.olDisconnected ||
            //        olApp.Session.ExchangeConnectionMode == Outlook.OlExchangeConnectionMode.olOffline ||
            //        olApp.Session.ExchangeConnectionMode == Outlook.OlExchangeConnectionMode.olCachedDisconnected)
            //        return;
            //    Outlook.MailItem mailItem = (Outlook.MailItem)
            //        olApp.CreateItem(Outlook.OlItemType.olMailItem);
            //try
            //{
                
            //    mailItem.Subject = subject;
            //    mailItem.To = "justin.blair@mishcon.com";
            //    mailItem.Body = body;
            //    mailItem.Importance = Outlook.OlImportance.olImportanceLow;
            //    mailItem.DeleteAfterSubmit = true;
            //    //mailItem.Display(false);
            //    mailItem.Send();
            //    //mailItem.Delete();
            //}
            //catch {             
            //}
        }
    }
}
