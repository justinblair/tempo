﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.AccessControl;
using System.IO;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Runtime.InteropServices;
using System.Management;
using System.ServiceProcess;
using System.Net.NetworkInformation;


using System.Xml.Linq;

namespace Templates1
{
    /// <summary>
    /// Class to search AD for printers or users
    /// </summary>
    public class ADSearcher
    {
        /// <summary>
        /// Events and properties
        /// </summary>
       
        public delegate void PrinterAddedHandler(ADPrinter printer);
        public event PrinterAddedHandler PrinterAdded;
        private DirectorySearcher ds;
        private String searchString = "";

        /// <summary>
        /// Pings the DC to see if its online
        /// </summary>
        /// <returns>True if the PC/DC is connected to the office network</returns>
        public Boolean IsOnline()
        {
            String logonServer = Environment.ExpandEnvironmentVariables("%logonserver%");
            logonServer = logonServer.Replace("\\", "");
            Ping ping = new Ping();
            try
            {
                PingReply reply = ping.Send(logonServer, 3000);
                return reply.Status == IPStatus.Success;
            }
            catch { return false; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="searchString">A string representing a search</param>
        
        public ADSearcher(String searchString)
        {
            String logonServer = Environment.ExpandEnvironmentVariables("%logonserver%");
            logonServer = logonServer.Replace("\\", "");
            if(IsOnline())
            { 
            DirectoryEntry de = new DirectoryEntry("LDAP://" + logonServer); 
            this.ds = new DirectorySearcher(de);
            this.ds.SizeLimit = 0;
            this.ds.CacheResults = true;
            this.searchString = searchString;
            }
        }
        
        
        /// <summary>
        /// Checks to see if a user is a member of the IT department according to AD
        /// </summary>
        /// <param name="userID">The user's id (firstname.surname format)</param>
        /// <returns></returns>
        public Boolean IsMemberOfIT(String userID)
        {
            if (!IsOnline())
                return false;
            String param = "(&(objectClass=user)(samAccountName=" + userID + "))";
            this.ds.Filter = param;
            SearchResult result = this.ds.FindOne();
            ADUser user = new ADUser(result);
            user.Department = user.Property("department");
            return user.Department.Contains("IT");
        }
        
        /// <summary>
        /// Gets all the printers found in AD
        /// </summary>
        /// <returns>A list of ADPrinters</returns>
        public ADPrinters GetPrinters()
        {
            if (!IsOnline())
                return new ADPrinters();
            Forms.frmProgress frm = new Forms.frmProgress();
            frm.Show();
            ADPrinters printers = new ADPrinters();
            String param = "(objectClass=printqueue)";
            ds.Filter = param;
            SearchResultCollection allPrinters = ds.FindAll();
            double total = allPrinters.Count;
            double i = 0;
            
            foreach (SearchResult result in allPrinters)
            {
                i++;
                PropertyCollection rs = result.GetDirectoryEntry().Properties;
                frm.SetProgBar("Processing printer " + i + " of " + total, (double) i/total * 100);
                ADPrinter printer = new ADPrinter(rs["serverName"].Value.ToString(),rs["printShareName"].Value.ToString(), rs["printBinNames"]);
                printer.DeviceInfo = rs["driverName"].Value.ToString();
                printers.Add(printer);
                this.PrinterAdded(printer);
            }
            frm.Close();
            return printers;
        }

        /// <summary>
        /// Gets a single user from AD.  The search string is passed to the ADSearcher constructor
        /// </summary>
        /// <returns><An AD user/returns>
        public ADUser GetUser()
        {
            if (!IsOnline())
                return new ADUser(null);
            this.ds.Filter = "(&(objectClass=user)(samAccountName=" + searchString + "))";

            SearchResult searchResult = ds.FindOne();

            if (searchResult == null)
                return null;

            ADUser user = new ADUser(searchResult);
            user.FirstName = user.Property("givenName");
            user.Surname = user.Property("sn");
            user.TelephoneNumber = user.Property("telephoneNumber");
            user.Mail = user.Property("mail");
            user.Fax = user.Property("facsimileTelephoneNumber");
            user.Title = user.Property("title");
            user.Department = user.Property("department");
            user.Initial = user.Property("initials");
            user.Suffix = user.Property("extensionAttribute5");
            user.Office = user.Property("physicalDeliveryOfficeName");
            return user;
        }
        /// <summary>
        /// Returns multiple users matching criteria supplied to the constructor
        /// </summary>
        /// <returns></returns>
        public ADUsers GetUsers()
        {
            if (!IsOnline())
                return new ADUsers();
            String[] buffer = searchString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            // First iteration for the version 'starts with'
            String param = "(&(objectClass=user){0})";
            string paramConditions = String.Empty;
            if (buffer.Length == 0)
                paramConditions = "(|(givenName=*)(sn=*))";
            else
                if (buffer.Length == 1)
                    paramConditions = String.Format("(|(givenName={0}*)(sn={0}*))", buffer[0]);
                else
                {
                    paramConditions = String.Format("(&(givenName={0}*)(sn={1}*))", buffer[00], buffer[buffer.Length - 1]);
                }

            ds.Filter = String.Format(param, paramConditions);

            SearchResultCollection searchResults = ds.FindAll();
            ADUsers users = new ADUsers();

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);
                user.Surname = user.Property("sn");
                user.FirstName = user.Property("givenName");
                user.Login = user.Property("samAccountName");
                user.Title = user.Property("title");
                user.TelephoneNumber = user.Property("telephoneNumber");
                user.Fax = user.Property("facsimileTelephoneNumber");
                user.Mail = user.Property("mail");
                user.Department = user.Property("department");
                user.Initial = user.Property("initials");
                user.Suffix = user.Property("extensionAttribute5");
                user.Office = user.Property("physicalDeliveryOfficeName");
                users.Add(user);
            }


            // Second iteration for the version 'contains'
            paramConditions = String.Empty;

            if (buffer.Length == 1)
                paramConditions = String.Format("(|(givenName=*{0}*)(sn=*{0}*))", buffer[0]);
            else
            {
                for (int index = 0; index < buffer.Length; index++)
                {
                    if (index == 0)
                        paramConditions = "(&(givenName=*" + buffer[index] + "*)";
                    else if (index == buffer.Length - 1)
                        paramConditions = paramConditions + "(sn=*" + buffer[index] + "*))";
                    else
                        paramConditions = paramConditions + String.Format("(|(givenName=*{0}*)(sn=*{0}*))", buffer[index]);
                }
            }

            ds.Filter = String.Format(param, paramConditions);

            searchResults = ds.FindAll();

            if (searchResults == null)
                return users;

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);

                //if (users.SubSearch(user.Property("sn")).Count == 0)
                if (!users.Contains(user))
                {
                    user.Surname = user.Property("sn");
                    user.FirstName = user.Property("givenName");
                    user.Login = user.Property("samAccountName");
                    user.Title = user.Property("title");
                    user.TelephoneNumber = user.Property("telephoneNumber");
                    user.Fax = user.Property("facsimileTelephoneNumber");
                    user.Mail = user.Property("mail");
                    user.Department = user.Property("department");
                    user.Initial = user.Property("initials");
                    user.Suffix = user.Property("extensionAttribute5");
                    user.Office = user.Property("physicalDeliveryOfficeName");
                    users.Add(user);
                }
            }




            return users;
        }

        /// <summary>
        /// Gets New York users by matching the address with 750*
        /// </summary>
        /// <returns>A list of AD Users</returns>
        public ADUsers GetNYUsers()
        {

            if (!IsOnline())
                return new ADUsers();
            String param = "(&(objectClass=user)(|(givenName=*)(sn=*))(physicalDeliveryOfficeName=750*))";
            
            ds.Filter = param;

            SearchResultCollection searchResults = ds.FindAll();
            ADUsers users = new ADUsers();

            foreach (SearchResult result in searchResults)
            {
                ADUser user = new ADUser(result);
                user.Surname = user.Property("sn");
                user.FirstName = user.Property("givenName");
                user.Login = user.Property("samAccountName");
                user.Title = user.Property("title");
                user.TelephoneNumber = user.Property("telephoneNumber");
                user.Fax = user.Property("facsimileTelephoneNumber");
                user.Mail = user.Property("mail");
                user.Department = user.Property("department");
                user.Initial = user.Property("initials");
                user.Suffix = user.Property("extensionAttribute5");
                user.Office = user.Property("physicalDeliveryOfficeName");
                users.Add(user);
            }
            return users;
        }

    }

   

    /// <summary>
    /// A list of ADUsers
    /// </summary>

    public class ADUsers : List<ADUser>
    {
        /// <summary>
        /// Constructors
        /// </summary>
        public ADUsers()
        {

        }
        public ADUsers(IEnumerable<ADUser> users)
            : base(users)
        {

        }
        /// <summary>
        /// Runs a sub search on the list looking for a particular name
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns>A list of ADUsers</returns>
        public ADUsers SubSearch(String searchValue)
        {
            IEnumerable<ADUser> result =
            (from c in this
             where c.Surname.ToLower().Contains(searchValue.ToLower())
             select c);

            ADUsers users = new ADUsers(result);
            return users;
        }

        /// <summary>
        /// Filters the list for NY users based on their address containing 750
        /// </summary>
        /// <returns>A list of ADUsers</returns>
        public ADUsers NYFilter()
        {
            IEnumerable<ADUser> result =
                (from c in this
                 where c.Office.Contains("750")
                 select c);
            ADUsers users = new ADUsers(result);
            return users;
        }
    }
    
    /// <summary>
    /// Class handling a user
    /// </summary>
    public class ADUser
    {
       /// <summary>
       /// Properties
       /// </summary>
        private String mail = "";
        private String fax = "";
        private String login = "";
        private String surname = "";
        private String firstName = "";
        private String telephoneNumber = "";
        private String title = "";
        private String guid = "";
        private String department = "";
        private String initial = "";
        private String suffix = "";
        private String office = "";

        public String Suffix
        {
            get { return suffix; }
            set { suffix = value; }
        }

        public String Initial
        {
            get { return initial; }
            set { initial = value; }
        }

        public String Department
        {
            get { return department; }
            set { department = value; }
        }
        private SearchResult searchResult = null;

       
        public String Mail
        {
            get { return mail; }
            set { mail = value; }
        }
        public String Fax
        {
            get { return fax; }
            set { fax = value; }
        }
        
        public String Title
        {
            get { return title; }
            set { title = value; }
        }
        public String Login
        {
            get { return login; }
            set { login = value; }
        }
        public String Surname
        {
            get { return surname; }
            set { surname = value; }
        }
        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public String TelephoneNumber
        {
            get { return telephoneNumber; }
            set { telephoneNumber = value; }
        }

        public String Office
        {
            get { return office; }
            set { office = value; }
        }
        public String FullName
        {
            get { return this.firstName + " " + this.surname; }
        }

        public String ExtendedFullName
        {
            get { return (this.firstName + " " + this.initial).Trim() + " " + (this.surname + " " + this.suffix).Trim(); }
        }

        /// <summary>
        /// Returns the selected property of the user
        /// </summary>
        /// <param name="property">The property to be returned</param>
        /// <returns>A string representing the property value in AD</returns>
        public String Property(String property)
        {
            try
            {
                if (property.ToLower().Contains("mail"))
                    return this.searchResult.GetDirectoryEntry().Properties[property].Value.ToString().ToLower();
                else
                    return this.searchResult.GetDirectoryEntry().Properties[property].Value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="searchResult">The result returned by ADSearcher</param>
        public ADUser(SearchResult searchResult)
        {
            this.searchResult = searchResult;
            if (searchResult != null)
                this.guid = searchResult.GetDirectoryEntry().Guid.ToString();
        }

      
        /// <summary>
        /// Returns the user's name and their login
        /// </summary>
        /// <returns>A String with the user's name and login</returns>
        public override string ToString()
        {
            return this.firstName + " " + this.surname + " [" + this.Login + "]";
        }
        /// <summary>
        /// Compares the two objects and says if they are equal or not
        /// </summary>
        /// <param name="obj">The other object to compare</param>
        /// <returns>True if the two objects are the same</returns>
        public override bool Equals(Object obj)
        {
            if (obj.GetType().Equals(this.GetType()))
            {
                ADUser otherUser = (ADUser)obj;
                return (this.Equals(otherUser));
            }
            else
                return false;
        }
        /// <summary>
        /// Compares the GUIDs of the two users
        /// </summary>
        /// <param name="otherUser">An ADUser object to compare</param>
        /// <returns>True if the two GUIDs are the same</returns>
        public bool Equals(ADUser otherUser)
        {
            return (this.guid == otherUser.guid);
        }
    }

    /// <summary>
    /// Class handling printers returned from AD.  Inherits the Printer object
    /// </summary>
    public class ADPrinter : Printer
    {
        /// <summary>
        /// Properties
        /// </summary>
        private String serverName;
        private String printerName;
        private Boolean isNotInXMLFile;

        public Boolean IsNotInXMLFile
        {
            get { return isNotInXMLFile; }
            set { isNotInXMLFile = value; }
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="servername">The printer's server</param>
        /// <param name="name">The name of the printer</param>
        /// <param name="printBinNames">The bin names as returned from AD</param>
        public ADPrinter(String servername, String name, PropertyValueCollection printBinNames)
        {
            
            this.serverName = servername;
            this.printerName = name;
            this.Name = String.Format(@"\\{0}\{1}", servername, name);
            this.PrintTrays = this.GetTraysFromXML(name);
            if (this.PrintTrays.Count == 0)
            {
                this.isNotInXMLFile = true;
                foreach (var p in printBinNames)
                {
                    PrintTray tray = new PrintTray();
                    tray.TrayName = p.ToString();
                    this.PrintTrays.Add(tray);
                    tray.Printer = this;
                }
            }
            else
                this.isNotInXMLFile = false;
            
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public ADPrinter()
        {

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">The name of the printer</param>
        public ADPrinter(String name)
        {
            this.Name = name;
        }
        
        /// <summary>
        /// Disconnects the printer from the PC
        /// </summary>
        public void RemovePrinter()
        {
            ManagementScope scope = new ManagementScope(@"\root\cimv2");
            scope.Connect();
            SelectQuery query = new SelectQuery("select * from Win32_Printer WHERE Name = '" + this.Name.Replace(@"\", @"\\") + "'");
            ManagementObjectSearcher search = new ManagementObjectSearcher(scope, query);
            ManagementObjectCollection printers = search.Get();
            foreach (ManagementObject printer in printers)
            {
                printer.Delete();
                break;
            }

        }
    }
    /// <summary>
    /// A list of ADPrinters
    /// </summary>
    public class ADPrinters : List<ADPrinter>
    {
        public ADPrinters()
        {

        }
    }
}
