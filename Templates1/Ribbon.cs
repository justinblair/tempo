﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Office = Microsoft.Office.Core;
using System.Xml;
using Word = Microsoft.Office.Interop.Word;
using System.Drawing;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon1();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace Templates1
{
    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        public Office.IRibbonUI ribbon;
        private clsRibbonOptions ribbonopts;
        private clsOfficeData officeData;
        private PersonalSettings personalSettings;
        public delegate void ErrorLoggedEventHandler(Exception e);
        public event ErrorLoggedEventHandler ErrorLogged;
        
        

        public Ribbon(clsRibbonOptions ribbonopts, clsOfficeData officedata)
        {
            this.ribbonopts = ribbonopts;
            this.officeData = officedata;
        }
       
        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            //String xml = GetResourceText("Templates1.Ribbon.xml");
            //xml = Globals.ThisAddIn.PersonalSettings.PrepareXML(xml);
            //return xml;
            FullTemplateSuite fs = new FullTemplateSuite();
            if( fs.LoadFullTemplateSuite)
                return GetResourceText("Templates1.Ribbon.xml");
            else
                return GetResourceText("Templates1.RibbonNLT.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public int mdrConvertItemCount(Office.IRibbonControl control)
        {
            return officeData.Templates.DefaultTemplates.Count;
        }

        public String mdrConvertItemLabel(Office.IRibbonControl control, int index)
        {
            return officeData.Templates.DefaultTemplates[index].Name;
        }

        public void mdrConvertOnAction(Office.IRibbonControl control, String id, int index)
        {
            ribbonopts.ConversionTemplate = index;
        }
        
        public int mdrConvertSelItem(Office.IRibbonControl control)
        {
            return ribbonopts.ConversionTemplate;
        }
        public void RunConversionRoutine(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return;
                Forms.frmProgress frm = new Forms.frmProgress();
                frm.Show();
                frm.SetProgBar("Starting conversion process", 0);
                clsConversion conv = new clsConversion(Globals.ThisAddIn.Application.ActiveDocument);
                frm.SetProgBar("Creating destination document", 10);
                Styles deststyles = conv.CreateDestinationDocument();

                frm.SetProgBar("Building style maps", 30);
                StyleMaps stylemaps = conv.BuildMappings();
                frm.SetProgBar("Running conversion process", 50);
                Word.Document doc = conv.RunConversion(stylemaps, deststyles);
                frm.SetProgBar("Tidying styles", 90);
                conv.TidyUpStyles(doc);
                frm.SetProgBar("Process complete", 100);
                frm.Close();
            }
            catch (Exception e) { this.ErrorLogged(e); }

        }

        public int LabelCount(Office.IRibbonControl control)
        {
            return Globals.ThisAddIn.LblBuilder.Labels.Count;
        }

        public String LabelItem(Office.IRibbonControl control, int index)
        {
            XMLLabel lbl = Globals.ThisAddIn.LblBuilder.Labels[index];
            return String.Format("{0} - {1}", lbl.Id, lbl.Description);
        }
        
        public void BuildLabels(Office.IRibbonControl control, String id, int index)
        {
            try
            {
                Globals.ThisAddIn.LblBuilder.BuildLabels(index);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public Boolean ConvertEnabled(Office.IRibbonControl control)
        {
            return Globals.ThisAddIn.Application.Documents.Count != 0;
        }
        
        
        public void FileSaveOnAction(Office.IRibbonControl control, bool Cancel)
        {
            Globals.ThisAddIn.DMS.FileSaveAsBinding();
        }
        
        
        public void UpdateTOC(Office.IRibbonControl control, bool CancelDefault)
        {
            try
            {
                clsTOC.UpdateTable();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void SyncCheckin(Office.IRibbonControl control)
        {
            try
            {
                //this.ErrorLogged(new Exception());
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                    Globals.ThisAddIn.DMS.QuickSave(Globals.ThisAddIn.Application.ActiveDocument.FullName, Globals.ThisAddIn.Application.ActiveDocument);
                //Globals.ThisAddIn.DMS.FileSaveAsBinding();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void SyncCheckinA(Office.IRibbonControl control, bool CancelDefault)
        {
            try
            {
               
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                    Globals.ThisAddIn.DMS.QuickSave(Globals.ThisAddIn.Application.ActiveDocument.FullName, Globals.ThisAddIn.Application.ActiveDocument);
                //Globals.ThisAddIn.DMS.FileSaveAsBinding();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void CustomPrintPreviewAndPrint()
        {
            ribbon.ActivateTabMso("TabPrint");
            ribbon.InvalidateControl("RetainWatermark");
        }
        
        public void BackstageOnShow(object contextObject)
        {
            try
            {
                if (personalSettings == null)
                    personalSettings = Globals.ThisAddIn.PersonalSettings;
                ribbon.InvalidateControl("RogueStylesList");
                ribbon.InvalidateControl("Col1Grp3");
                ribbonopts.FilePrint = true;
                ribbonopts.LetterHead = false;
                ribbonopts.Engrossment = false;
                ribbonopts.Accounts = false;
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public int LoadDefaultOfficeFromXML(Office.IRibbonControl control)
        {
            return personalSettings.Office;
        }

        public int LoadDefaultLangFromXML(Office.IRibbonControl control)
        {
            return personalSettings.Language;
        }

        public int ColourRGSelectedItem(Office.IRibbonControl control)
        {
            return this.personalSettings.ColourPreference;
        }

        public void ColourRGOnAction(Office.IRibbonControl control, String itemID, int index)
        {
            this.personalSettings.ColourPreference = index;
        }
        
        public void SaveSettings(Office.IRibbonControl control)
        {
            try
            {
                if (personalSettings != null)
                    personalSettings.Save();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public Boolean FileCopiesDuplexPressed(Office.IRibbonControl control)
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            int i = ds.GetPrinterDuplex(Globals.ThisAddIn.Application.ActivePrinter, out oErr);
            switch(i)
            {
                case 1:
                    return false;
                case 2:
                    return true;
            }
            return false;
        }

        public void FileCopiesDuplexOnAction(Office.IRibbonControl control, Boolean pressed)
        {
               this.ribbonopts.FileCopyDuplex = pressed ? 2: 1;
        }
        
        public void Final35OnAction(Office.IRibbonControl control, String itemID, int index)
        {
            ribbonopts.Final35Narrative = index;
        }

        public void TopEngrossOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            if(control.Id=="TopCopies")
                ribbonopts.LetterHead = pressed;
            else
                ribbonopts.Engrossment = pressed;
        }

        public void TopEngrossDuplexOnAction(Office.IRibbonControl control, Boolean pressed)
        {
                ribbonopts.EngrossmentDuplex = pressed ? 2: 1;
        }

        public void CustomEngrossPrintRangeOnChange(Office.IRibbonControl control, String text)
        {
            ribbonopts.CustomRangeEngross = text;
        }
        
        public String CustomLetterPrintRangeGetText(Office.IRibbonControl control)
        {
            try 
            { 
                String text = "1-" + Globals.ThisAddIn.Application.ActiveDocument.Range().Information[Word.WdInformation.wdActiveEndPageNumber];
                ribbonopts.CustomRangeAccounts = text;
                ribbonopts.CustomRangeEngross = text;
                ribbonopts.CustomRange = text;
                return text;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return String.Empty;
        }
        
        public Boolean Final35Visible(Office.IRibbonControl control)
        {
            return officeData.IsNYOffice ? false: true;
        }
        
        public void CustomLetterPrintRangeOnChange(Office.IRibbonControl control, String text)
        {
            ribbonopts.CustomRange = text;
        }

        public int dd3GetItemCount(Office.IRibbonControl control)
        {
            return officeData.Offices.Count;
        }

        public int ddGetItemCount(Office.IRibbonControl control)
        {
            return officeData.Languages.Count;
        }

        public int glGetItemCount(Office.IRibbonControl control)
        {
            return officeData.Offices.ActiveOffice.Templates.Count;
        }

        public string dd3GetItemLabel(Office.IRibbonControl control, int index)
        {
            return officeData.Offices[index].Shortname;
        }

        public string ddGetItemLabel(Office.IRibbonControl control, int index)
        {
            return officeData.Languages[index].Name;
                
        }

        public string glGetItemLabel(Office.IRibbonControl control, int index)
        {
            return officeData.Offices.ActiveOffice.Templates[index].Name;
        }


        public Boolean FileCopiesDLabelsPressed(Office.IRibbonControl control)
        {
            if(Globals.ThisAddIn.Application.Documents.Count>0)
            { 
                Boolean b = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Template.Name.ToLower().Contains("label");
                ribbonopts.IsLabel = b;
                return b;
            }
            else 
            {
                ribbonopts.IsLabel = false;
                return false;
            }
        }
        
        public void ShowPrinterSettings(Office.IRibbonControl control)
        {
            try
            {
                Forms.frmPrinters frm = new Forms.frmPrinters();
                frm.Show();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public Boolean ShowPrintingGroup(Office.IRibbonControl control)
        {
            try
            {
                ADSearcher ads = new ADSearcher(String.Empty);
                return ads.IsMemberOfIT(Environment.UserName);
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return false;
        }
        
        public int GetSelectedItemIndex(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                {
                    if (personalSettings == null)
                        personalSettings = new PersonalSettings();
                    ribbonopts.Office = personalSettings.Office;
                    return personalSettings.Office;
                }
                else
                    return ribbonopts.Office;
            }
            catch (Exception e) { this.ErrorLogged(e);  }
            return 0;
        }

        public int GetSelectedItemIndexLang(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                {
                    if (personalSettings == null)
                        personalSettings = new PersonalSettings();
                    ribbonopts.Lang = personalSettings.Language;
                    return personalSettings.Language;
                }
                else
                    return ribbonopts.Lang;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return 0;
            
        }

        public void SetOffice(Office.IRibbonControl control, string id, int index)
        {
            try
            {
                if (personalSettings == null)
                    personalSettings = new PersonalSettings();
                personalSettings.Office = index;
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void SetLanguage(Office.IRibbonControl control, string id, int index)
        {
            try
            {
                if (personalSettings == null)
                    personalSettings = new PersonalSettings();
                personalSettings.Language = index;
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void ddOnAction(Office.IRibbonControl control, string id, int index)
        {
            try
            {
                switch (id)
                {
                    case "mdrOfficeChoice":
                        ribbonopts.Office = index;

                        if (Globals.ThisAddIn.Application.Documents.Count > 0)
                        {

                            Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Office = officeData.Offices[index];
                            clsDynamicInserts.DropAddressBlock();
                            clsDynamicInserts.UpdateDocumentReference(Globals.ThisAddIn.Application.ActiveDocument);
                        }
                        ribbon.InvalidateControl("mdrCreateNew");
                        ribbon.InvalidateControl("mdrBullet");
                        ribbon.InvalidateControl("mdrSpacing");
                        ribbon.InvalidateControl("mdrSpacing2");
                        Globals.ThisAddIn.Application.Options.AutoFormatReplaceQuotes = officeData.UseCurlyQuotes;
                        Globals.ThisAddIn.Application.Options.AutoFormatAsYouTypeReplaceQuotes = officeData.UseCurlyQuotes;
                        Globals.ThisAddIn.Application.Options.MeasurementUnit = officeData.MeasurementUnits;
                        break;
                    case "mdrLangChoice":
                        ribbonopts.Lang = index;
                        if (Globals.ThisAddIn.Application.Documents.Count > 0)
                            Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Language = officeData.Languages[index];
                        break;

                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void glOnAction(Office.IRibbonControl control, string id, int index)
        {
            try
            {
                Template template = officeData.Offices.ActiveOffice.Templates[index];
                Language language = officeData.Languages.ActiveLanguage;
                XmlOffice office = officeData.Offices.ActiveOffice;
                Document docu = Globals.ThisAddIn.WordApp.CreateDocument(template, language, office, true);
                
                ribbon.Invalidate();
                ribbon.ActivateTab("Templates1");
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public Boolean getVisibleNonLegalHelp(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                    return clsWordApp.GetTemplateName(Globals.ThisAddIn.Application.ActiveDocument).ToLower().Contains("nonlegal") ||
                        clsWordApp.GetTemplateName(Globals.ThisAddIn.Application.ActiveDocument).ToLower().Contains("start");
                else
                    return true;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return true;
        }

        public Boolean getVisibleTempoHelp(Office.IRibbonControl control)
        {
            return true;
        }

        public void TempoHelpFile(Office.IRibbonControl control)
        {
            try
            {
                Forms.frmPDF frm = new Forms.frmPDF();
                frm.File = System.Environment.GetEnvironmentVariable("PROGRAMFILES") + @"\Mishcon De Reya\Tempo\Tempo Documentation.pdf";
                frm.LoadPDF();
                frm.Show();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void NonLegalHelpFile(Office.IRibbonControl control)
        {
            try
            {
                Forms.frmPDF frm = new Forms.frmPDF();
                frm.File =Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Mishcon De Reya\Tempo\Non-legal template user guide (FINAL).pdf";
                frm.LoadPDF();
                frm.Show();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        
        public Boolean getVisibleQP(Office.IRibbonControl control)
        {
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return true;
            Word.Template tmp = Globals.ThisAddIn.Application.ActiveDocument.get_AttachedTemplate();
            return tmp.BuildingBlockEntries.Count > 0;
        }

        public Boolean getVisibleTOC(Office.IRibbonControl control)
        {
            return true;
        }
        
        
        public Boolean getVisibleNumbering(Office.IRibbonControl control)
        {
            if (Globals.ThisAddIn.Application.Documents.Count > 0)
                return clsStyles.StyleExists("MdR Level 1", Globals.ThisAddIn.Application.ActiveDocument);
            else
                return false;
        }

        public Boolean mdrAutoHeadingGetVisible(Office.IRibbonControl control)
        {
            if (Globals.ThisAddIn.Application.Documents.Count > 0)
                return clsStyles.StyleExists("MdR Heading 1", Globals.ThisAddIn.Application.ActiveDocument);
            else
                return false;
        }

        public Boolean mdrBulletgetVisible(Office.IRibbonControl control)
        {
            return officeData.IsNYOffice;
        }
        
        public void OnActionShowTaskPane(Office.IRibbonControl control)
        {
            if (!Globals.ThisAddIn.WordApp.ShowHiddenPane())
            Globals.ThisAddIn.WordApp.ShowHiddenPane();
        }

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
            
        }

        public void SetLevel(Office.IRibbonControl control)
        {
            try
            {
                Globals.ThisAddIn.Application.Selection.set_Style(control.Tag);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        
        public void ndrSpacingOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            try
            {
                Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
                if (clsStyles.StyleExists("Body", doc))
                {
                    Word.Style style = doc.Styles["Body"];
                    if (pressed)
                    {
                        if(clsWordApp.GetTemplateName(doc).Contains("California"))
                        {
                            style.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceMultiple;
                            style.ParagraphFormat.LineSpacing = 19.8F; //=LinesToPoints(1.65)
                        }
                        else
                        {
                            style.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceDouble;
                            style.ParagraphFormat.SpaceAfter = 0;
                        }
                    }
                    else
                    {
                        style.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                        style.ParagraphFormat.SpaceAfter = 12;
                    }

                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public Boolean mdrSpacingGetPressed(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return false;
                Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
                if (clsStyles.StyleExists("Body", doc))
                {
                    Word.Style style = doc.Styles["Body"];
                    if (clsWordApp.GetTemplateName(doc).Contains("California"))
                        return style.ParagraphFormat.LineSpacingRule == Word.WdLineSpacing.wdLineSpaceMultiple;
                    else
                        return style.ParagraphFormat.LineSpacingRule == Word.WdLineSpacing.wdLineSpaceDouble;
                }
                return false;
            }
            catch (Exception e) 
            { 
                this.ErrorLogged(e);
                return false;
            }
        }

        
        public void AutoNumber(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ApplyAutoNumbering();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void AutoHeading(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ApplyAutoHeading();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void ApplyStyleLevel(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ApplyAutoNumberbyLevel(control.Tag);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }


        public void ApplyBodyLevelText(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ApplyBodyLevel(control.Tag);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void AutoBody(Office.IRibbonControl ontrol)
        {
            try
            {
                clsStyles.ApplyAutoBody(Globals.ThisAddIn.Application.Selection.Range, false, null, false);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void CheckNumberingonAction(Office.IRibbonControl control)
        {
            try
            {
                String buffer;
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return;
                Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
                buffer = clsStyles.CheckNumbering1(doc) + Environment.NewLine + Environment.NewLine;
                buffer += clsStyles.CheckNumbering2(doc) + Environment.NewLine + Environment.NewLine;
                clsCrossReferences cr = new clsCrossReferences();
                buffer += cr.Check(doc);
                System.Windows.Forms.MessageBox.Show(buffer, Globals.ThisAddIn.APP_NAME1);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void CLearCommentsFromCheck(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ClearComments(Globals.ThisAddIn.Application.ActiveDocument);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public void RunRestyleRoutine(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ConvertFromNovaPlex(Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false));
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        
       public void RestyleOnAction(Office.IRibbonControl control)
        {
            Forms.frmHouseStyle frm = new Forms.frmHouseStyle();
            frm.Show();
        }
        
        public Boolean UKSpacingVisible(Office.IRibbonControl control)
        {
            return officeData.IsNYOffice ? false : true;
        }
        
        public void UKSpacingAction(Office.IRibbonControl control, String id, int index)
        {
            if (Globals.ThisAddIn.Application.Documents.Count == 0)
                return;
            Word.Document doc = Globals.ThisAddIn.Application.ActiveDocument;
            Word.Style normal = doc.Styles[Word.WdBuiltinStyle.wdStyleNormal];
            switch(index)
            {
                case 0:
                    normal.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceSingle;
                    break;
                case 1:
                    normal.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpace1pt5;
                    break;
                case 2:
                    normal.ParagraphFormat.LineSpacingRule = Word.WdLineSpacing.wdLineSpaceDouble;
                    break;
            }
        }
        
        
        public Boolean getVisible(Office.IRibbonControl control)
        {
            return true;
        }
        
        public void IndentOutdent(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.SetIndent(Convert.ToInt16(control.Tag));
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void mdrBulletOnAction(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.ApplyUSBullet();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

       
        public string getListsDMContent(Office.IRibbonControl control)
        {
            try
            {
                string buffer = "<menu xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\" xmlns:ex=\"http://www.mishcon.com\">";
                int j = 0;
                foreach (Word.Style style in Globals.ThisAddIn.Application.ActiveDocument.Styles)
                {
                    if (!style.BuiltIn)
                        if (style.ListTemplate != null)
                            if (style.ListTemplate.ListLevels.Count == 1)
                            {
                                j++;
                                buffer = buffer + "<button id=\"" + style.NameLocal.Replace(" ", "") + j + "\" label=\"" + style.NameLocal + "\" tag=\"" + style.NameLocal + "\" ";
                                buffer = buffer + "onAction=\"ApplyList\"/>";
                            }
                }

                buffer = buffer + "</menu>";
                return buffer;
            }
            catch (Exception e) 
            { 
                this.ErrorLogged(e);
                return String.Empty;
            }
        }

        public void ApplyList(Office.IRibbonControl control)
        {
            try
            {
                Globals.ThisAddIn.Application.Selection.set_Style(control.Tag);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        public string getContentDMNumbering(Office.IRibbonControl control)
        {
            try
            {
                string buffer = "<menu xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\" xmlns:ex=\"http://www.mishcon.com\">";
                for (int i = 1; i <= 9; i++)
                {

                    buffer += "<splitButton id=\"Level" + i + "Split\"><button id=\"button" + i +
                        "\" tag=\"" + i + "\" onAction=\"ApplyStyleLevel\" label=\"Level " + i + "\" /><menu id=\"menuitem" + i + "\">";
                    buffer += "holder" + i;
                    buffer += "<button id=\"level" + i + "Restart\" onAction=\"ApplyBodyLevelText\" label=\"Body/Text Level " + i + "\" tag=\"" + i + "\"/>";
                    buffer += "</menu></splitButton>";
                }

                int j = 0;
                if (Globals.ThisAddIn.Application.Documents.Count > 0)
                    foreach (Word.Style style in Globals.ThisAddIn.Application.ActiveDocument.Styles)
                    {
                        j++;
                        if (style.BuiltIn == false)
                        {
                            try
                            {
                                if (style.ParagraphFormat.OutlineLevel != Word.WdOutlineLevel.wdOutlineLevelBodyText)
                                {
                                    string level = Convert.ToInt16(style.ParagraphFormat.OutlineLevel).ToString();
                                    buffer = buffer.Replace("holder" + level, "<button id=\"Style" + j + "\" label=\"" +
                                        style.NameLocal + "\" onAction=\"SetLevel\" tag=\"" + style.NameLocal + "\" />" + "holder" + level);
                                }
                            }
                            catch { }
                        }
                    }

                buffer += "</menu>";
                for (int i = 1; i <= 9; i++)
                    buffer = buffer.Replace("holder" + i, "");

                return buffer;
            }
            catch (Exception e) 
            { 
                this.ErrorLogged(e);
                return String.Empty;
            }
        }

        public void RefreshRibbon()
        {
            //ribbon.InvalidateControl("NumberingDM");
            //ribbon.InvalidateControl("mdrNumbering");
            //ribbon.InvalidateControlMso("QuickPartsInsertGallery");
            //ribbon.InvalidateControlMso("TableOfContentsGallery");
            //ribbon.InvalidateControlMso("TableOfContentsUpdate");
            //ribbon.InvalidateControl("mdrOfficeChoice");
            //ribbon.InvalidateControl("mdrBullet");
            //ribbon.InvalidateControl("mdrAutoHeading");
            ribbon.Invalidate();
        }

        public void ClearRecentOnAction(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.PersonalSettings.RecentDocs.ClearItems();
            ribbon.InvalidateControl("Col2Grp3");
            ribbon.ActivateTab("Templates1");
            ribbon.ActivateTab("Templates1Settings");
        }

        public void DummyFunction(Office.IRibbonControl control)
        {

        }
        
        public void ActivateTab()
        {
            ribbon.ActivateTab("Templates1");
        }

        public String MdRPageRangeItemID(Office.IRibbonControl control, int index)
        {
            return "MdRPR" + index;
        }

        public String MdRPageRangeItemLabel(Office.IRibbonControl control, int index)
        {
            switch(index)
            {
                case 0:
                    return "All pages";
                    
                case 1:
                    return "Selected pages";
                    
                case 2:
                    return "Selection";
                    
            }
            return "";
        }

        public void MdRPageRangeOnAction(Office.IRibbonControl control, string id, int index)
        {
            ribbonopts.PrintRange = index;
            ThisRibbonCollection col = Globals.Ribbons;
           
        }

        public int MdRPageRangeSelectedItem(Office.IRibbonControl control)
        {
            return 0;
        }

        public int MdRPageRangeItemCount(Office.IRibbonControl control)
        {
            return 3;
        }

        public System.Drawing.Image getBlankImage(Office.IRibbonControl control)
        {
            return System.Drawing.Image.FromFile(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)+ @"\Mishcon De Reya\Tempo\Images\blank.jpg");
        }

        public Boolean FileCopiesPressed(Office.IRibbonControl control)
        {
            return true;
        }

        public  void InvalidateCustomLetterPrintRange(Office.IRibbonControl control)
        {
            ribbon.InvalidateControl("CustomLetterPrintRange");
           
        }

        public void InvalidateCustomEngrossPrintRange(Office.IRibbonControl control)
        {
            ribbon.InvalidateControl("CustomEngrossPrintRange");
        }

        public void InvalidateCustomAccountsPrintRange(Office.IRibbonControl control)
        {
            ribbon.InvalidateControl("CustomAccountsPrintRange");
        }
        
        public void FileCopiesOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            ribbonopts.FilePrint = pressed;
        }

        public void WaterMarkOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            try
            {
                ribbonopts.WaterMark = pressed;
                ribbon.InvalidateControl("EB1");
                if (!pressed)
                {
                    Document doc = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false);
                    doc.Watermark.Remove();
                    doc.Watermark = null;
                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public Boolean WatermarkGetPressed(Office.IRibbonControl control)
        {
            Boolean pressed =  Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).HasWatermark();
            ribbonopts.WaterMark = pressed;
            return pressed;
        }

        public Boolean EB1Enabled(Office.IRibbonControl control)
        {
            return ribbonopts.WaterMark;
        }

        public void EB1OnChange(Office.IRibbonControl control, String text)
        {
            try 
            { 
                Watermark watermark;
                Document doc = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false);
                if (doc.HasWatermark())
                {
                    watermark = doc.Watermark;
                    watermark.Text = text;
                }
                else
                {
                    watermark = new Watermark(doc.WordDocument);
                    watermark.Add(text);
                    doc.Watermark = watermark;
                }
            }
        
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void RetainWaterMarkOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            ribbonopts.RetainWatermark = pressed;
        }

        public Boolean RetainWatermarkPressed(Office.IRibbonControl control)
        {
            return ribbonopts.RetainWatermark;
        }

        public void CustomPrintRangeOnAction(Office.IRibbonControl control)
        {
            ribbonopts.CustomRange = control.ToString();
        }

        public void ImportButtonOnAction(Office.IRibbonControl control)
        {
            clsDynamicInserts cdi = new clsDynamicInserts();
            cdi.ImportNonLegalDocumentType();
        }

        public void AccountsOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            ribbonopts.Accounts = pressed;
        }

        public void CustomAccountsPrintRangeOnChange(Office.IRibbonControl control, String text)
        {
            ribbonopts.CustomRangeAccounts = text;
        }

        public String RogueStyleCheckLabel(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return "0 rogue styles";
                int count = Globals.ThisAddIn.Application.ActiveDocument.Styles.Count - Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Styles.Count;
                return count + " rogue styles";
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return "0 rogue styles";
        }
        
        public Boolean RogueStyleCheckPressed(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return false;
                Document doc = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false);
                if (doc != null)
                    return doc.Styles.RoguesExist(doc);
                else
                    return false;
                //Word.Template tmp = Globals.ThisAddIn.Application.ActiveDocument.get_AttachedTemplate();
                //return tmp.ListTemplates.Count != Globals.ThisAddIn.Application.ActiveDocument.ListTemplates.Count;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return false;
        }

        public String GetRogueStyles(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return "No documents open.";
                Document doc = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false);
                Styles styles = doc.Styles.RogieStyles(doc);
                if (styles.Count == 0)
                    return "No rogue styles detected.";
                String buffer = string.Empty;
                foreach (Style style in styles)
                {
                    buffer += style.Name + ", ";
                }

                return buffer.Substring(0, buffer.Length - 2);
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return String.Empty;
        }

        public int RogueStylesExist(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return 1;
                Document doc = Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false);
                if (doc != null)
                    if (doc.Styles.RoguesExist(doc)) return 2;
                    else
                        return 1;
                return 1;
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return 1;
        }

        public void RefreshRogueStyleCheck()
        {
            ribbon.InvalidateControl("RogueStyleCheck");
        }

        public void SafePasteOnAction(Office.IRibbonControl control)
        {
            try
            {
                clsStyles.SafePaste();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public Boolean EnabledNumbering(Office.IRibbonControl control)
        {
            return Globals.ThisAddIn.Application.Documents.Count > 0;
            
        }

        public void PopulateContactBox(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return;
                if (Globals.ThisAddIn.WordApp.InContactTable)
                    Globals.ThisAddIn.WordApp.LaunchSearchDialog();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }
        
        
        public Boolean HasContactBoxFunctionality(Office.IRibbonControl control)
        {
            try
            {
                String sName;
                if (Globals.ThisAddIn.Application.Documents.Count == 0)
                    return false;
                else
                {
                    sName = Globals.ThisAddIn.Application.ActiveDocument.get_AttachedTemplate().Name;
                    return Globals.ThisAddIn.WordApp.Documents.ActiveDocument(false).Template.HasContactBoxFunctionality;
                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
            return false;
        }
        
        public void NextMarker(Office.IRibbonControl control)
        {
            try
            {
                clsMarkers markers = new clsMarkers(Globals.ThisAddIn.Application.ActiveDocument);
                if (control.Tag == "1")
                    markers.FindNext(true);
                else
                    markers.FindNext(false);
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void InsertMarker(Office.IRibbonControl control)
        {
            try
            {
                clsMarkers markers = new clsMarkers(Globals.ThisAddIn.Application.ActiveDocument);
                markers.InsertMarker();
            }
            catch (Exception e) { this.ErrorLogged(e); }
        }

        public void OpenRecentDocumentOnAction(Office.IRibbonControl control)
        {
            try 
            { 
                int i = Convert.ToInt16(control.Tag);
                if (i < Globals.ThisAddIn.PersonalSettings.RecentDocs.Count)
                {
                    RecentDocument rd = Globals.ThisAddIn.PersonalSettings.RecentDocs[i];
                    if(rd.Description == rd.FilePath)
                        Globals.ThisAddIn.Application.Documents.Open(rd.Description);
                    else
                    { 
                    IManage.NRTDocument nrt = (IManage.NRTDocument)Globals.ThisAddIn.DMS.GetDocument(
                    Globals.ThisAddIn.DMS.GetPreferredDatabase.Name, (long)Convert.ToInt32(rd.Number), (long)Convert.ToInt32(rd.Version), false);
                    String path = Globals.ThisAddIn.DMS.OpenIManDoc(nrt);
                    Globals.ThisAddIn.Application.Documents.Open(path);
                    }
                }
            }
            catch (Exception e) { this.ErrorLogged(e); }
            
            
            //try { 
            //String[] tag = control.Tag.Split(new char[] { '.' });
            //Com.Interwoven.WorkSite.iManage.NRTDocument nrt = (Com.Interwoven.WorkSite.iManage.NRTDocument)Globals.ThisAddIn.DMS.GetDocument(
            //    Globals.ThisAddIn.DMS.GetPreferredDatabase.Name, (long)Convert.ToInt32(tag[0]), (long)Convert.ToInt32(tag[1]));
            //    String path = Globals.ThisAddIn.DMS.OpenIManDoc(nrt);
            //    Globals.ThisAddIn.Application.Documents.Open(path);
            //    }
            //catch (Exception e) { this.ErrorLogged(e); }
        }

        public String RecentDocsGetLabel(Office.IRibbonControl control)
        {
            int i = Convert.ToInt16(control.Tag);
            if (i < Globals.ThisAddIn.PersonalSettings.RecentDocs.Count)
                return Globals.ThisAddIn.PersonalSettings.RecentDocs[i].Description;
            else
                return String.Empty;
        }

        public String RecentDocsGetLabelLabel(Office.IRibbonControl control)
        {
            int i = Convert.ToInt16(control.Tag);
            if (i < Globals.ThisAddIn.PersonalSettings.RecentDocs.Count)
            {
                RecentDocument rd = Globals.ThisAddIn.PersonalSettings.RecentDocs[i];
                Com.Interwoven.WorkSite.iManage.NRTDocument nrt = (Com.Interwoven.WorkSite.iManage.NRTDocument)Globals.ThisAddIn.DMS.GetDocument(
                Globals.ThisAddIn.DMS.GetPreferredDatabase.Name, (long)Convert.ToInt32(rd.Number), (long)Convert.ToInt32(rd.Version),false);
                if (nrt != null)
                    return nrt.Number + "." + nrt.Version + ": Last accessed by " + nrt.LastUser.FullName + " at " + nrt.AccessTime;
                else
                    return String.Empty;
            }
            else
                return String.Empty;
        }
        
        public void RefreshRecentDocs()
        {
            ribbon.InvalidateControl("Col2Grp3");
        }

        public Boolean IsFullTemplateSuite(Office.IRibbonControl control)
        {
            FullTemplateSuite fs = new FullTemplateSuite();
            return fs.LoadFullTemplateSuite;
        }

        public Boolean IsNotFullTemplateSuite(Office.IRibbonControl control)
        {
            FullTemplateSuite fs = new FullTemplateSuite();
            return fs.LoadFullTemplateSuite == false;
        }

        public String FullTemplateSuiteCreate(Office.IRibbonControl control)
        {
            FullTemplateSuite fs = new FullTemplateSuite();
            return fs.LoadFullTemplateSuite ? "File" : "Create";
        }

        public String FullTemplateSuiteName(Office.IRibbonControl control)
        {
            FullTemplateSuite fs = new FullTemplateSuite();
            return fs.LoadFullTemplateSuite ? Globals.ThisAddIn.APP_NAME1 : "Templates";
        }

        public Boolean EngrossmentVisible(Office.IRibbonControl control)
        {
            return officeData.IsNotNYOffice;
        }

        public void SetNumberingOnAction(Office.IRibbonControl control)
        {
            Forms.frmNumbering frm = new Forms.frmNumbering();
            frm.Show();
        }

        public String DuplexCheckLabel(Office.IRibbonControl control)
        {
            return Globals.ThisAddIn.Application.ActivePrinter;
        }

        public void DuplexCheckOnAction(Office.IRibbonControl control, Boolean pressed)
        {
            this.ribbonopts.Duplex = pressed ? 2 : 1;
        }

        public void SetDuplexOnAction(Office.IRibbonControl control)
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            ds.SetPrinterDuplex(Globals.ThisAddIn.Application.ActivePrinter, this.ribbonopts.Duplex, out oErr);
        }

        public Boolean DuplexCheckPressed(Office.IRibbonControl control)
        {
            DuplexSettings ds = new DuplexSettings();
            String oErr;
            switch(ds.GetPrinterDuplex(Globals.ThisAddIn.Application.ActivePrinter, out oErr))
            {
                case 0:
                    System.Windows.Forms.MessageBox.Show("Could not set duplex for the current printer.  Please check to see if it is duplex capable.");
                    return false;
                case 1:
                    return false;
                    
                case 2:
                    return true;
                    
                case 3:
                    return true;
                    
            }
            return false;
        }

        public void StyleUI_OnAction(Office.IRibbonControl control)
        {
            Microsoft.Office.Tools.CustomTaskPane ctp = Globals.ThisAddIn.GenerateTaskpane(new CustomTaskPanes.ctpStyleUI(), "Style Manager");
            if (ctp.Visible == false)
                ctp.Visible = true;
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
